/*
 * Copyright 2023 Elizabeth Figura
 *
 * This file is part of the Rosé compiler.
 *
 * Rosé is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rosé is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Rosé; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

/*
 * This file incorporates code taken from the vkd3d-shader HLSL compiler and
 * covered under the following license:
 *
 * Copyright 2012 Matteo Bruni for CodeWeavers
 * Copyright 2019-2020 Zebediah Figura for CodeWeavers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "c.h"
#include <stdio.h>

void rose_note(struct rose_ctx *ctx, const struct rose_location *loc,
        enum rose_log_level level, const char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    printf("%s:%u:%u: note: ", loc->source_name, loc->line, loc->column);
    vprintf(fmt, args);
    printf("\n");
    va_end(args);
}

void rose_error(struct rose_ctx *ctx, const struct rose_location *loc, const char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    printf("%s:%u:%u: error: ", loc->source_name, loc->line, loc->column);
    vprintf(fmt, args);
    printf("\n");
    va_end(args);

    if (!ctx->result)
        ctx->result = ROSE_ERROR_INVALID_SHADER;
}

void rose_warning(struct rose_ctx *ctx, const struct rose_location *loc, const char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    printf("%s:%u:%u: warning: ", loc->source_name, loc->line, loc->column);
    vprintf(fmt, args);
    printf("\n");
    va_end(args);
}

void rose_fixme(struct rose_ctx *ctx, const struct rose_location *loc, const char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    printf("Aborting due to not yet implemented feature: ");
    vprintf(fmt, args);
    printf("\n");
    va_end(args);

    if (!ctx->result)
        ctx->result = ROSE_ERROR_NOT_IMPLEMENTED;
}

struct rose_ir_var *rose_get_var(struct rose_scope *scope, const char *name)
{
    struct rose_ir_var *var;

    LIST_FOR_EACH_ENTRY(var, &scope->vars, struct rose_ir_var, scope_entry)
    {
        if (var->name && !strcmp(name, var->name))
            return var;
    }
    if (!scope->upper)
        return NULL;
    return rose_get_var(scope->upper, name);
}

void rose_free_var(struct rose_ir_var *decl)
{
    rose_free((void *)decl->name);
    rose_free(decl);
}

static struct rose_type *rose_new_type(struct rose_ctx *ctx, enum rose_type_class type_class)
{
    struct rose_type *type;

    type = rose_alloc(ctx, sizeof(*type));
    type->class = type_class;

    list_add_tail(&ctx->types, &type->entry);

    return type;
}

struct rose_type *rose_new_pointer_type(struct rose_ctx *ctx, struct rose_type *pointee)
{
    struct rose_type *type;

    type = rose_new_type(ctx, ROSE_CLASS_POINTER);
    type->e.ptr.type = pointee;
    return type;
}

struct rose_type *rose_new_function_type(struct rose_ctx *ctx, struct rose_type *return_type,
        const struct rose_func_parameters *parameters)
{
    struct rose_type *type;

    type = rose_new_type(ctx, ROSE_CLASS_FUNCTION);
    type->e.func.return_type = return_type;
    type->e.func.param_types = rose_calloc(ctx, parameters->count, sizeof(struct rose_type *));
    for (size_t i = 0; i < parameters->count; ++i)
        type->e.func.param_types[i] = parameters->vars[i]->data_type;
    type->e.func.param_count = parameters->count;
    type->e.func.vararg = parameters->vararg;
    return type;
}

static void init_deref(struct rose_ctx *ctx, struct rose_deref *deref,
        struct rose_ir_var *var, struct rose_ir_node *ptr, unsigned int path_len)
{
    memset(deref, 0, sizeof(*deref));
    deref->var = var;
    rose_src_from_node(&deref->ptr, ptr);
    deref->path_len = path_len;

    if (path_len)
        deref->path = rose_calloc(ctx, deref->path_len, sizeof(*deref->path));
}

bool rose_init_deref_from_index_chain(struct rose_ctx *ctx, struct rose_deref *deref, struct rose_ir_node *chain)
{
    struct rose_ir_index *index;
    struct rose_ir_load *load;
    unsigned int chain_len, i;
    struct rose_ir_node *ptr;

    deref->path = NULL;
    deref->path_len = 0;

    assert(chain);

    /* Find the length of the index chain */
    chain_len = 0;
    ptr = chain;
    while (ptr->type == ROSE_IR_INDEX)
    {
        index = rose_ir_index(ptr);

        chain_len++;
        ptr = index->val.node;
    }

    if (ptr->type != ROSE_IR_LOAD)
        return false;
    load = rose_ir_load(ptr);

    init_deref(ctx, deref, load->src.var, load->src.ptr.node, load->src.path_len + chain_len);

    for (i = 0; i < load->src.path_len; ++i)
        rose_src_from_node(&deref->path[i], load->src.path[i].node);

    chain_len = 0;
    ptr = chain;
    while (ptr->type == ROSE_IR_INDEX)
    {
        unsigned int p = deref->path_len - 1 - chain_len;

        index = rose_ir_index(ptr);
        rose_src_from_node(&deref->path[p], index->idx.node);

        chain_len++;
        ptr = index->val.node;
    }
    assert(deref->path_len == load->src.path_len + chain_len);

    return true;
}

struct rose_type *rose_get_element_type_from_path_index(struct rose_ctx *ctx,
        const struct rose_type *type, struct rose_ir_node *idx)
{
    assert(idx);

    switch (type->class)
    {
        case ROSE_CLASS_ARRAY:
            return type->e.array.type;

        case ROSE_CLASS_STRUCT:
        {
            struct rose_ir_constant *c = rose_ir_constant(idx);

            assert(c->value.u.u < type->e.record.field_count);
            return type->e.record.fields[c->value.u.u].type;
        }

        case ROSE_CLASS_NUMERIC:
        case ROSE_CLASS_ERROR:
        case ROSE_CLASS_VOID:
        case ROSE_CLASS_POINTER:
        case ROSE_CLASS_FUNCTION:
            break;
    }

    rose_unreachable();
}

struct rose_type *rose_deref_get_type(struct rose_ctx *ctx, const struct rose_deref *deref)
{
    struct rose_type *type;
    unsigned int i;

    assert(deref);

    if (deref->var)
    {
        type = deref->var->data_type;
    }
    else
    {
        type = deref->ptr.node->data_type;
        assert(type->class == ROSE_CLASS_POINTER);
        type = type->e.ptr.type;
    }

    for (i = 0; i < deref->path_len; ++i)
        type = rose_get_element_type_from_path_index(ctx, type, deref->path[i].node);
    return type;
}

struct rose_type *rose_new_array_type(struct rose_ctx *ctx, struct rose_type *basic_type, unsigned int array_size)
{
    struct rose_type *type;

    type = rose_alloc(ctx, sizeof(*type));
    type->class = ROSE_CLASS_ARRAY;
    type->modifiers = basic_type->modifiers;
    type->e.array.elements_count = array_size;
    type->e.array.type = basic_type;

    list_add_tail(&ctx->types, &type->entry);

    return type;
}

struct rose_type *rose_new_struct_type(struct rose_ctx *ctx, const char *name,
        struct rose_struct_field *fields, size_t field_count)
{
    struct rose_type *type;
    int ret;

    type = rose_alloc(ctx, sizeof(*type));
    type->class = ROSE_CLASS_STRUCT;
    type->name = name;
    type->prefix = ROSE_TYPE_PREFIX_STRUCT;
    type->e.record.fields = fields;
    type->e.record.field_count = field_count;

    list_add_tail(&ctx->types, &type->entry);
    if (name)
    {
        ret = rb_put(&ctx->cur_scope->types[ROSE_TYPE_PREFIX_STRUCT], name, &type->scope_entry);
        assert(!ret);
    }

    return type;
}

struct rose_type *rose_get_type(struct rose_scope *scope,
        enum rose_type_prefix prefix, const char *name, bool recursive)
{
    struct rb_entry *entry = rb_get(&scope->types[prefix], name);

    if (entry)
        return RB_ENTRY_VALUE(entry, struct rose_type, scope_entry);

    if (scope->upper)
    {
        if (recursive)
            return rose_get_type(scope->upper, prefix, name, recursive);
    }

    return NULL;
}

struct rose_function *rose_get_function(struct rose_ctx *ctx, const char *name)
{
    struct rb_entry *entry;

    if ((entry = rb_get(&ctx->functions, name)))
        return RB_ENTRY_VALUE(entry, struct rose_function, entry);
    return NULL;
}

bool rose_types_are_equal(const struct rose_type *t1, const struct rose_type *t2, bool check_type_qualifiers)
{
    /* We always check calling conventions; no need to bother with that here.
     * Don't include NEAR in this mask since a lack of modifiers implicitly
     * means near. */
    static const unsigned int type_qualifiers = ROSE_MODIFIER_CONST | ROSE_MODIFIER_VOLATILE | ROSE_MODIFIER_FAR;

    if (t1 == t2)
        return true;

    if (t1->class != t2->class)
        return false;

    if (check_type_qualifiers && (t1->modifiers & type_qualifiers) != (t2->modifiers & type_qualifiers))
        return false;

    switch (t1->class)
    {
        case ROSE_CLASS_ERROR:
        case ROSE_CLASS_VOID:
            return true;

        case ROSE_CLASS_NUMERIC:
            return t1->base_type == t2->base_type;

        case ROSE_CLASS_ARRAY:
            return t1->e.array.elements_count == t2->e.array.elements_count
                    && rose_types_are_equal(t1->e.array.type, t2->e.array.type, check_type_qualifiers);

        case ROSE_CLASS_STRUCT:
            if (t1->e.record.field_count != t2->e.record.field_count)
                return false;

            for (size_t i = 0; i < t1->e.record.field_count; ++i)
            {
                const struct rose_struct_field *field1 = &t1->e.record.fields[i];
                const struct rose_struct_field *field2 = &t2->e.record.fields[i];

                if (!rose_types_are_equal(field1->type, field2->type, check_type_qualifiers))
                    return false;

                if (strcmp(field1->name, field2->name))
                    return false;
            }
            return true;

        case ROSE_CLASS_POINTER:
            return rose_types_are_equal(t1->e.ptr.type, t2->e.ptr.type, true);

        case ROSE_CLASS_FUNCTION:
            if ((t1->modifiers & (ROSE_MODIFIER_CDECL | ROSE_MODIFIER_PASCAL))
                    != (t2->modifiers & (ROSE_MODIFIER_CDECL | ROSE_MODIFIER_PASCAL)))
                return false;

            if (!rose_types_are_equal(t1->e.func.return_type, t2->e.func.return_type, true))
                return false;

            if (t1->e.func.param_count != t2->e.func.param_count)
                return false;

            if (t1->e.func.vararg != t2->e.func.vararg)
                return false;

            for (size_t i = 0; i < t1->e.func.param_count; ++i)
            {
                if (!rose_types_are_equal(t1->e.func.param_types[i], t2->e.func.param_types[i], true))
                    return false;
            }
            return true;
    }

    rose_unreachable();
}

struct rose_type *rose_type_clone(struct rose_ctx *ctx, struct rose_type *old, char *name, unsigned int modifiers)
{
    struct rose_type *type;
    int ret;

    type = rose_alloc(ctx, sizeof(*type));
    type->name = name;
    type->prefix = ROSE_TYPE_PREFIX_NONE;
    type->class = old->class;
    type->base_type = old->base_type;
    type->modifiers = old->modifiers | modifiers;

    switch (old->class)
    {
        case ROSE_CLASS_ARRAY:
            type->e.array.type = rose_type_clone(ctx, old->e.array.type, NULL, modifiers);
            type->e.array.elements_count = old->e.array.elements_count;
            break;

        case ROSE_CLASS_STRUCT:
        {
            size_t field_count = old->e.record.field_count, i;

            type->e.record.field_count = field_count;
            type->e.record.fields = rose_calloc(ctx, field_count, sizeof(*type->e.record.fields));

            for (i = 0; i < field_count; ++i)
            {
                const struct rose_struct_field *src_field = &old->e.record.fields[i];
                struct rose_struct_field *dst_field = &type->e.record.fields[i];

                dst_field->loc = src_field->loc;
                dst_field->type = rose_type_clone(ctx, src_field->type, NULL, modifiers);
                dst_field->name = rose_strdup(ctx, src_field->name);
            }
            break;
        }

        case ROSE_CLASS_POINTER:
            type->e.ptr.type = old->e.ptr.type;
            break;

        case ROSE_CLASS_FUNCTION:
            type->e.func.param_types = rose_alloc(ctx, old->e.func.param_count * sizeof(struct rose_type *));
            memcpy(type->e.func.param_types, old->e.func.param_types,
                    old->e.func.param_count * sizeof(struct rose_type *));

            type->e.func.param_count = old->e.func.param_count;
            type->e.func.return_type = old->e.func.return_type;
            type->e.func.vararg = old->e.func.vararg;
            break;

        case ROSE_CLASS_NUMERIC:
        case ROSE_CLASS_ERROR:
        case ROSE_CLASS_VOID:
            break;
    }

    list_add_tail(&ctx->types, &type->entry);
    if (name)
    {
        ret = rb_put(&ctx->cur_scope->types[ROSE_TYPE_PREFIX_NONE], name, &type->scope_entry);
        assert(!ret);
    }
    return type;
}

unsigned int rose_type_get_field_offset(const struct rose_type *type, unsigned int field_idx)
{
    unsigned int ret = 0;

    assert(type->class == ROSE_CLASS_STRUCT);
    assert(field_idx <= type->e.record.field_count);
    for (size_t i = 0; i < field_idx; ++i)
    {
        /* C doesn't actually specify any alignment requirements.
         * We're just going to assert that everything is 1-byte-aligned. */
        ret += rose_type_get_size(type->e.record.fields[i].type);
    }
    return ret;
}

unsigned int rose_type_get_size(const struct rose_type *type)
{
    switch (type->class)
    {
        case ROSE_CLASS_VOID:
        case ROSE_CLASS_ERROR:
        case ROSE_CLASS_FUNCTION:
            return 1;

        case ROSE_CLASS_NUMERIC:
            switch (type->base_type)
            {
                case ROSE_TYPE_BOOL:
                case ROSE_TYPE_SCHAR:
                case ROSE_TYPE_UCHAR:
                    return 1;

                case ROSE_TYPE_SINT:
                case ROSE_TYPE_SSHORT:
                case ROSE_TYPE_UINT:
                case ROSE_TYPE_USHORT:
                    return 2;

                case ROSE_TYPE_SLONG:
                case ROSE_TYPE_ULONG:
                    return 4;
            }
            break;

        case ROSE_CLASS_POINTER:
            return rose_type_is_far_pointer(type) ? 4 : 2;

        case ROSE_CLASS_ARRAY:
            return type->e.array.elements_count * rose_type_get_size(type->e.array.type);

        case ROSE_CLASS_STRUCT:
            return rose_type_get_field_offset(type, type->e.record.field_count);
    }

    rose_unreachable();
}

struct rose_ir_node *rose_new_cast(struct rose_ctx *ctx, struct rose_ir_node *node, struct rose_type *type,
        const struct rose_location *loc)
{
    return rose_new_unary_expr(ctx, ROSE_OP1_CAST, node, type, loc);
}

struct rose_ir_node *rose_new_copy(struct rose_ctx *ctx, struct rose_ir_node *node)
{
    /* Use a cast to the same type as a makeshift identity expression. */
    return rose_new_cast(ctx, node, node->data_type, &node->loc);
}

struct rose_ir_var *rose_new_var(struct rose_ctx *ctx, const char *name, struct rose_type *type,
        const struct rose_function *func, unsigned int modifiers, const struct rose_location *loc)
{
    struct rose_ir_var *var;

    var = rose_alloc(ctx, sizeof(*var));
    var->name = name;
    var->data_type = type;
    var->func = func;
    var->loc = *loc;
    list_add_tail(&ctx->cur_scope->vars, &var->scope_entry);
    TRACE("Created new variable \"%s\".\n", name);
    return var;
}

struct rose_ir_var *rose_new_synthetic_var(struct rose_ctx *ctx, const char *template,
        struct rose_type *type, const struct rose_function *func, const struct rose_location *loc)
{
    struct rose_string_buffer *string;
    static unsigned int counter;
    struct rose_ir_var *var;
    const char *name_copy;

    string = rose_get_string_buffer(ctx);
    rose_string_buffer_printf(string, "<%s-%u>", template, counter++);
    name_copy = rose_strdup(ctx, string->buffer);
    if (func)
        var = rose_new_var(ctx, name_copy, type, func, 0, loc);
    else
        var = rose_new_var(ctx, name_copy, type, NULL, ROSE_STORAGE_STATIC, loc);
    rose_release_string_buffer(ctx, string);
    return var;
}

struct rose_ir_node *rose_convert_string_constant(struct rose_ctx *ctx, struct rose_ir_constant *c)
{
    struct rose_ir_load *load;
    struct rose_ir_var *var;
    struct rose_deref deref;

    var = rose_new_synthetic_var(ctx, "string", c->node.data_type, NULL, &c->node.loc);

    x86_add_symbol(&ctx->data_block, var->name);
    for (size_t i = 0; i < c->value.u.str.len; ++i)
        x86_emit_i(&ctx->data_block, X86_OP_DATA, c->value.u.str.str[i], 8);

    rose_init_simple_deref_from_var(&deref, var);
    load = rose_new_load_index(ctx, &deref, NULL, &c->node.loc);
    return &load->node;
}

void rose_copy_deref(struct rose_ctx *ctx, struct rose_deref *deref, const struct rose_deref *other)
{
    memset(deref, 0, sizeof(*deref));

    if (other)
    {
        init_deref(ctx, deref, other->var, other->ptr.node, other->path_len);

        for (unsigned int i = 0; i < deref->path_len; ++i)
            rose_src_from_node(&deref->path[i], other->path[i].node);
    }
}

void rose_deref_append_index(struct rose_ctx *ctx, struct rose_deref *deref, struct rose_ir_node *index)
{
    struct rose_src *new_path = rose_calloc(ctx, deref->path_len + 1, sizeof(*new_path));

    for (unsigned int i = 0; i < deref->path_len; ++i)
    {
        rose_src_from_node(&new_path[i], deref->path[i].node);
        rose_src_remove(&deref->path[i]);
    }
    rose_src_from_node(&new_path[deref->path_len++], index);
    rose_free(deref->path);
    deref->path = new_path;
}

void rose_deref_remove_path(struct rose_deref *deref)
{
    for (unsigned int i = 0; i < deref->path_len; ++i)
        rose_src_remove(&deref->path[i]);
    rose_free(deref->path);

    deref->path = NULL;
    deref->path_len = 0;
}

void rose_cleanup_deref(struct rose_deref *deref)
{
    rose_deref_remove_path(deref);
    rose_src_remove(&deref->ptr);
    rose_src_remove(&deref->offset);
}

/* Initializes a simple variable dereference, so that it can be passed to load/store functions. */
void rose_init_simple_deref_from_var(struct rose_deref *deref, struct rose_ir_var *var)
{
    memset(deref, 0, sizeof(*deref));
    deref->var = var;
}

static void init_node(struct rose_ir_node *node, enum rose_ir_node_type type,
        struct rose_type *data_type, const struct rose_location *loc)
{
    memset(node, 0, sizeof(*node));
    node->type = type;
    node->data_type = data_type;
    node->loc = *loc;
    list_init(&node->uses);
}

struct rose_ir_node *rose_new_addr(struct rose_ctx *ctx,
        const struct rose_deref *deref, const struct rose_location *loc)
{
    struct rose_type *type = rose_new_pointer_type(ctx, rose_deref_get_type(ctx, deref));
    struct rose_ir_addr *addr;

    addr = rose_alloc(ctx, sizeof(*addr));
    init_node(&addr->node, ROSE_IR_ADDR, type, loc);
    rose_copy_deref(ctx, &addr->src, deref);

    return &addr->node;
}

struct rose_ir_node *rose_new_simple_store(struct rose_ctx *ctx, struct rose_ir_var *lhs, struct rose_ir_node *rhs)
{
    struct rose_deref lhs_deref;

    rose_init_simple_deref_from_var(&lhs_deref, lhs);
    return rose_new_store_index(ctx, &lhs_deref, NULL, rhs, &rhs->loc);
}

struct rose_ir_node *rose_new_store_index(struct rose_ctx *ctx, const struct rose_deref *lhs,
        struct rose_ir_node *idx, struct rose_ir_node *rhs, const struct rose_location *loc)
{
    struct rose_ir_store *store;
    unsigned int i;

    assert(lhs);

    store = rose_alloc(ctx, sizeof(*store));
    init_node(&store->node, ROSE_IR_STORE, NULL, loc);

    init_deref(ctx, &store->lhs, lhs->var, lhs->ptr.node, lhs->path_len + !!idx);
    for (i = 0; i < lhs->path_len; ++i)
        rose_src_from_node(&store->lhs.path[i], lhs->path[i].node);
    if (idx)
        rose_src_from_node(&store->lhs.path[lhs->path_len], idx);

    rose_src_from_node(&store->rhs, rhs);

    return &store->node;
}

struct rose_ir_node *rose_new_call(struct rose_ctx *ctx, const struct rose_deref *func,
        size_t arg_count, struct rose_ir_node *const *args, const struct rose_location *loc)
{
    const struct rose_type *func_type = rose_deref_get_type(ctx, func);
    struct rose_ir_call *call;

    call = rose_alloc(ctx, offsetof(struct rose_ir_call, args[arg_count]));
    call->func_type = func_type;
    if (func_type->class == ROSE_CLASS_POINTER)
        func_type = func_type->e.ptr.type;

    assert(func_type->class == ROSE_CLASS_FUNCTION);
    init_node(&call->node, ROSE_IR_CALL, func_type->e.func.return_type, loc);
    rose_copy_deref(ctx, &call->func, func);
    for (size_t i = 0; i < arg_count; ++i)
        rose_src_from_node(&call->args[i], args[i]);
    call->arg_count = arg_count;
    return &call->node;
}

struct rose_ir_node *rose_new_constant(struct rose_ctx *ctx, struct rose_type *type,
        const struct rose_constant_value *value, const struct rose_location *loc)
{
    struct rose_ir_constant *c;

    c = rose_alloc(ctx, sizeof(*c));
    init_node(&c->node, ROSE_IR_CONSTANT, type, loc);
    c->value = *value;
    if (type->class == ROSE_CLASS_ARRAY)
    {
        c->value.u.str.str = rose_alloc(ctx, value->u.str.len);
        memcpy((char *)c->value.u.str.str, value->u.str.str, value->u.str.len);
    }

    return &c->node;
}

struct rose_ir_node *rose_new_bool_constant(struct rose_ctx *ctx, bool b, const struct rose_location *loc)
{
    struct rose_constant_value value;

    value.u.u = b ? ~0u : 0;
    return rose_new_constant(ctx, rose_get_scalar_type(ctx, ROSE_TYPE_BOOL), &value, loc);
}

struct rose_ir_node *rose_new_int_constant(struct rose_ctx *ctx,
        enum rose_base_type type, int32_t n, const struct rose_location *loc)
{
    struct rose_constant_value value;

    value.u.i = n;
    return rose_new_constant(ctx, rose_get_scalar_type(ctx, type), &value, loc);
}

struct rose_ir_node *rose_new_uint_constant(struct rose_ctx *ctx,
        enum rose_base_type type, uint32_t n, const struct rose_location *loc)
{
    struct rose_constant_value value;

    value.u.u = n;
    return rose_new_constant(ctx, rose_get_scalar_type(ctx, type), &value, loc);
}

struct rose_ir_node *rose_new_string_constant(struct rose_ctx *ctx,
        const char *str, size_t len, const struct rose_location *loc)
{
    struct rose_constant_value value;
    struct rose_type *type;

    type = rose_new_array_type(ctx, rose_get_scalar_type(ctx, ROSE_TYPE_SCHAR), len);
    value.u.str.str = str;
    value.u.str.len = len;
    return rose_new_constant(ctx, type, &value, loc);
}

struct rose_ir_node *rose_new_expr(struct rose_ctx *ctx, enum rose_ir_expr_op op,
        struct rose_ir_node *operands[ROSE_MAX_OPERANDS],
        struct rose_type *data_type, const struct rose_location *loc)
{
    struct rose_ir_expr *expr;
    unsigned int i;

    expr = rose_alloc(ctx, sizeof(*expr));
    init_node(&expr->node, ROSE_IR_EXPR, data_type, loc);
    expr->op = op;
    for (i = 0; i < ROSE_MAX_OPERANDS; ++i)
        rose_src_from_node(&expr->operands[i], operands[i]);
    return &expr->node;
}

struct rose_ir_node *rose_new_unary_expr(struct rose_ctx *ctx, enum rose_ir_expr_op op,
        struct rose_ir_node *arg, struct rose_type *data_type, const struct rose_location *loc)
{
    struct rose_ir_node *operands[ROSE_MAX_OPERANDS] = {arg};

    return rose_new_expr(ctx, op, operands, data_type, loc);
}

struct rose_ir_node *rose_new_binary_expr(struct rose_ctx *ctx, enum rose_ir_expr_op op,
        struct rose_ir_node *arg1, struct rose_ir_node *arg2)
{
    struct rose_ir_node *operands[ROSE_MAX_OPERANDS] = {arg1, arg2};

    return rose_new_expr(ctx, op, operands, arg1->data_type, &arg1->loc);
}

static struct rose_ir_node *rose_new_error_expr(struct rose_ctx *ctx)
{
    static const struct rose_location loc = {.source_name = "<error>"};
    struct rose_ir_node *operands[ROSE_MAX_OPERANDS] = {0};

    /* Use a dummy location; we should never report any messages related to
     * this expression. */
    return rose_new_expr(ctx, ROSE_OP0_ERROR, operands, ctx->builtin_types.error, &loc);
}

struct rose_ir_node *rose_new_if(struct rose_ctx *ctx, struct rose_ir_node *condition,
        struct rose_block *then_block, struct rose_block *else_block, const struct rose_location *loc)
{
    struct rose_ir_if *iff;

    iff = rose_alloc(ctx, sizeof(*iff));
    init_node(&iff->node, ROSE_IR_IF, NULL, loc);
    rose_src_from_node(&iff->condition, condition);
    rose_block_init(&iff->then_block);
    rose_block_add_block(&iff->then_block, then_block);
    rose_block_init(&iff->else_block);
    if (else_block)
        rose_block_add_block(&iff->else_block, else_block);
    return &iff->node;
}

struct rose_ir_load *rose_new_load_index(struct rose_ctx *ctx, const struct rose_deref *deref,
        struct rose_ir_node *idx, const struct rose_location *loc)
{
    struct rose_ir_load *load;
    struct rose_type *type;
    unsigned int i;

    type = rose_deref_get_type(ctx, deref);
    if (idx)
        type = rose_get_element_type_from_path_index(ctx, type, idx);

    load = rose_alloc(ctx, sizeof(*load));
    init_node(&load->node, ROSE_IR_LOAD, type, loc);

    init_deref(ctx, &load->src, deref->var, deref->ptr.node, deref->path_len + !!idx);
    for (i = 0; i < deref->path_len; ++i)
        rose_src_from_node(&load->src.path[i], deref->path[i].node);
    if (idx)
        rose_src_from_node(&load->src.path[deref->path_len], idx);

    return load;
}

struct rose_ir_load *rose_new_var_load(struct rose_ctx *ctx, struct rose_ir_var *var,
        const struct rose_location *loc)
{
    struct rose_deref var_deref;

    rose_init_simple_deref_from_var(&var_deref, var);
    return rose_new_load_index(ctx, &var_deref, NULL, loc);
}

struct rose_ir_node *rose_new_pointer_load(struct rose_ctx *ctx,
        struct rose_ir_node *instr, const struct rose_location *loc)
{
    struct rose_ir_load *load;
    struct rose_type *type;

    if (instr->data_type->class == ROSE_CLASS_POINTER)
    {
        type = instr->data_type->e.ptr.type;
    }
    else
    {
        assert(instr->data_type->class == ROSE_CLASS_ARRAY);
        type = instr->data_type->e.array.type;
    }

    load = rose_alloc(ctx, sizeof(*load));
    init_node(&load->node, ROSE_IR_LOAD, type, loc);
    init_deref(ctx, &load->src, NULL, instr, 0);
    return &load->node;
}

struct rose_ir_node *rose_new_index(struct rose_ctx *ctx, struct rose_ir_node *val,
        struct rose_ir_node *idx, const struct rose_location *loc)
{
    struct rose_type *type = val->data_type;
    struct rose_ir_index *index;

    index = rose_alloc(ctx, sizeof(*index));

    type = rose_get_element_type_from_path_index(ctx, type, idx);

    init_node(&index->node, ROSE_IR_INDEX, type, loc);
    rose_src_from_node(&index->val, val);
    rose_src_from_node(&index->idx, idx);
    return &index->node;
}

struct rose_ir_node *rose_new_jump(struct rose_ctx *ctx, enum rose_ir_jump_type type,
        struct rose_ir_node *return_value, const struct rose_location *loc)
{
    struct rose_ir_jump *jump;

    jump = rose_alloc(ctx, sizeof(*jump));
    init_node(&jump->node, ROSE_IR_JUMP, NULL, loc);
    jump->type = type;
    rose_src_from_node(&jump->return_value, return_value);
    return &jump->node;
}

struct rose_ir_node *rose_new_loop(struct rose_ctx *ctx, struct rose_block *block,
        struct rose_block *iter, const struct rose_location *loc)
{
    struct rose_ir_loop *loop;

    loop = rose_alloc(ctx, sizeof(*loop));
    init_node(&loop->node, ROSE_IR_LOOP, NULL, loc);
    rose_block_init(&loop->body);
    rose_block_add_block(&loop->body, block);
    rose_block_init(&loop->iter);
    if (iter)
        rose_block_add_block(&loop->iter, iter);
    return &loop->node;
}

struct rose_ir_node *rose_add_conditional(struct rose_ctx *ctx, struct rose_block *instrs,
        struct rose_ir_node *condition, struct rose_ir_node *if_true, struct rose_ir_node *if_false)
{
    struct rose_block then_block, else_block;
    struct rose_ir_node *iff, *store;
    struct rose_ir_load *load;
    struct rose_ir_var *var;

    assert(rose_types_are_equal(if_true->data_type, if_false->data_type, false));

    var = rose_new_synthetic_var(ctx, "conditional", if_true->data_type, ctx->cur_function, &condition->loc);

    rose_block_init(&then_block);
    rose_block_init(&else_block);

    store = rose_new_simple_store(ctx, var, if_true);
    rose_block_add_instr(&then_block, store);

    store = rose_new_simple_store(ctx, var, if_false);
    rose_block_add_instr(&else_block, store);

    iff = rose_new_if(ctx, condition, &then_block, &else_block, &condition->loc);
    rose_block_add_instr(instrs, iff);

    load = rose_new_var_load(ctx, var, &condition->loc);
    rose_block_add_instr(instrs, &load->node);

    return &load->node;
}

struct clone_instr_map
{
    struct
    {
        const struct rose_ir_node *src;
        struct rose_ir_node *dst;
    } *instrs;
    size_t count, capacity;
};

static struct rose_ir_node *clone_instr(struct rose_ctx *ctx,
        struct clone_instr_map *map, const struct rose_ir_node *instr);

static void clone_block(struct rose_ctx *ctx, struct rose_block *dst_block,
        const struct rose_block *src_block, struct clone_instr_map *map)
{
    const struct rose_ir_node *src;
    struct rose_ir_node *dst;

    rose_block_init(dst_block);

    LIST_FOR_EACH_ENTRY(src, &src_block->instrs, struct rose_ir_node, entry)
    {
        dst = clone_instr(ctx, map, src);
        rose_block_add_instr(dst_block, dst);

        rose_array_reserve(ctx, (void **)&map->instrs, &map->capacity, map->count + 1, sizeof(*map->instrs));
        map->instrs[map->count].dst = dst;
        map->instrs[map->count].src = src;
        ++map->count;
    }
}

static struct rose_ir_node *map_instr(const struct clone_instr_map *map, struct rose_ir_node *src)
{
    size_t i;

    if (!src)
        return NULL;

    for (i = 0; i < map->count; ++i)
    {
        if (map->instrs[i].src == src)
            return map->instrs[i].dst;
    }

    /* The block passed to rose_clone_block() should have been free of external
     * references. */
    rose_unreachable();
}

static void clone_deref(struct rose_ctx *ctx, struct clone_instr_map *map,
        struct rose_deref *dst, const struct rose_deref *src)
{
    unsigned int i;

    init_deref(ctx, dst, src->var, src->ptr.node, src->path_len);

    for (i = 0; i < src->path_len; ++i)
        rose_src_from_node(&dst->path[i], map_instr(map, src->path[i].node));
}

static void clone_src(struct clone_instr_map *map, struct rose_src *dst, const struct rose_src *src)
{
    rose_src_from_node(dst, map_instr(map, src->node));
}

static struct rose_ir_node *clone_addr(struct rose_ctx *ctx, struct clone_instr_map *map, struct rose_ir_addr *src)
{
    struct rose_ir_addr *dst;

    dst = rose_alloc(ctx, sizeof(*dst));
    init_node(&dst->node, ROSE_IR_ADDR, src->node.data_type, &src->node.loc);
    clone_deref(ctx, map, &dst->src, &src->src);
    return &dst->node;
}

static struct rose_ir_node *clone_call(struct rose_ctx *ctx, struct clone_instr_map *map, struct rose_ir_call *src)
{
    struct rose_ir_call *dst;

    dst = rose_alloc(ctx, offsetof(struct rose_ir_call, args[src->arg_count]));
    init_node(&dst->node, ROSE_IR_CALL, src->node.data_type, &src->node.loc);
    clone_deref(ctx, map, &dst->func, &src->func);

    dst->arg_count = src->arg_count;
    for (size_t i = 0; i < src->arg_count; ++i)
        clone_src(map, &dst->args[i], &src->args[i]);
    return &dst->node;
}

static struct rose_ir_node *clone_constant(struct rose_ctx *ctx, struct rose_ir_constant *src)
{
    return rose_new_constant(ctx, src->node.data_type, &src->value, &src->node.loc);
}

static struct rose_ir_node *clone_expr(struct rose_ctx *ctx, struct clone_instr_map *map, struct rose_ir_expr *src)
{
    struct rose_ir_node *operands[ROSE_MAX_OPERANDS];
    unsigned int i;

    for (i = 0; i < ARRAY_SIZE(operands); ++i)
        operands[i] = map_instr(map, src->operands[i].node);

    return rose_new_expr(ctx, src->op, operands, src->node.data_type, &src->node.loc);
}

static struct rose_ir_node *clone_if(struct rose_ctx *ctx, struct clone_instr_map *map, struct rose_ir_if *src)
{
    struct rose_block then_block, else_block;

    clone_block(ctx, &then_block, &src->then_block, map);
    clone_block(ctx, &else_block, &src->else_block, map);
    return rose_new_if(ctx, map_instr(map, src->condition.node), &then_block, &else_block, &src->node.loc);
}

static struct rose_ir_node *clone_jump(struct rose_ctx *ctx, struct clone_instr_map *map, struct rose_ir_jump *src)
{
    return rose_new_jump(ctx, src->type, map_instr(map, src->return_value.node), &src->node.loc);
}

static struct rose_ir_node *clone_load(struct rose_ctx *ctx, struct clone_instr_map *map, struct rose_ir_load *src)
{
    struct rose_ir_load *dst;

    dst = rose_alloc(ctx, sizeof(*dst));
    init_node(&dst->node, ROSE_IR_LOAD, src->node.data_type, &src->node.loc);

    clone_deref(ctx, map, &dst->src, &src->src);
    return &dst->node;
}

static struct rose_ir_node *clone_loop(struct rose_ctx *ctx, struct clone_instr_map *map, struct rose_ir_loop *src)
{
    struct rose_block body, iter;

    clone_block(ctx, &body, &src->body, map);
    clone_block(ctx, &iter, &src->iter, map);
    return rose_new_loop(ctx, &body, &iter, &src->node.loc);
}

static struct rose_ir_node *clone_store(struct rose_ctx *ctx, struct clone_instr_map *map, struct rose_ir_store *src)
{
    struct rose_ir_store *dst;

    dst = rose_alloc(ctx, sizeof(*dst));
    init_node(&dst->node, ROSE_IR_STORE, NULL, &src->node.loc);
    clone_deref(ctx, map, &dst->lhs, &src->lhs);
    clone_src(map, &dst->rhs, &src->rhs);
    return &dst->node;
}

static struct rose_ir_node *clone_index(struct rose_ctx *ctx, struct clone_instr_map *map,
        struct rose_ir_index *src)
{
    return rose_new_index(ctx, map_instr(map, src->val.node), map_instr(map, src->idx.node), &src->node.loc);
}

static struct rose_ir_node *clone_instr(struct rose_ctx *ctx,
        struct clone_instr_map *map, const struct rose_ir_node *instr)
{
    switch (instr->type)
    {
        case ROSE_IR_ADDR:
            return clone_addr(ctx, map, rose_ir_addr(instr));

        case ROSE_IR_CALL:
            return clone_call(ctx, map, rose_ir_call(instr));

        case ROSE_IR_CONSTANT:
            return clone_constant(ctx, rose_ir_constant(instr));

        case ROSE_IR_EXPR:
            return clone_expr(ctx, map, rose_ir_expr(instr));

        case ROSE_IR_IF:
            return clone_if(ctx, map, rose_ir_if(instr));

        case ROSE_IR_INDEX:
            return clone_index(ctx, map, rose_ir_index(instr));

        case ROSE_IR_JUMP:
            return clone_jump(ctx, map, rose_ir_jump(instr));

        case ROSE_IR_LOAD:
            return clone_load(ctx, map, rose_ir_load(instr));

        case ROSE_IR_LOOP:
            return clone_loop(ctx, map, rose_ir_loop(instr));

        case ROSE_IR_STORE:
            return clone_store(ctx, map, rose_ir_store(instr));
    }

    rose_unreachable();
}

void rose_clone_block(struct rose_ctx *ctx, struct rose_block *dst_block,
        const struct rose_block *src_block, struct rose_ir_node **instr)
{
    struct clone_instr_map map = {0};

    clone_block(ctx, dst_block, src_block, &map);
    *instr = map_instr(&map, *instr);
    rose_free(map.instrs);
}

struct rose_function *rose_new_function(struct rose_ctx *ctx, const char *name, struct rose_type *data_type,
        const struct rose_func_parameters *parameters, unsigned int modifiers, const struct rose_location *loc)
{
    struct rose_function *func;
    int ret;

    assert(data_type->class == ROSE_CLASS_FUNCTION);

    func = rose_alloc(ctx, sizeof(*func));
    rose_block_init(&func->body);
    func->name = name;
    func->data_type = data_type;
    func->parameters = *parameters;
    func->storage_modifiers = modifiers;
    func->loc = *loc;

    for (size_t i = 0; i < parameters->count; ++i)
        func->parameters.vars[i]->func = func;

    ret = rb_put(&ctx->functions, func->name, &func->entry);
    assert(!ret);
    return func;
}

static int compare_rose_types_rb(const void *key, const struct rb_entry *entry)
{
    const struct rose_type *type = RB_ENTRY_VALUE(entry, const struct rose_type, scope_entry);
    const char *name = key;

    if (name == type->name)
        return 0;

    if (!name || !type->name)
    {
        ERR("rose_type without a name in a scope?\n");
        return -1;
    }
    return strcmp(name, type->name);
}

static struct rose_scope *rose_new_scope(struct rose_ctx *ctx, struct rose_scope *upper)
{
    struct rose_scope *scope;

    scope = rose_alloc(ctx, sizeof(*scope));
    list_init(&scope->vars);
    for (size_t i = 0; i < ROSE_TYPE_PREFIX_COUNT; ++i)
        rb_init(&scope->types[i], compare_rose_types_rb);
    scope->upper = upper;
    list_add_tail(&ctx->scopes, &scope->entry);
    return scope;
}

void rose_push_scope(struct rose_ctx *ctx)
{
    struct rose_scope *new_scope = rose_new_scope(ctx, ctx->cur_scope);

    TRACE("Pushing a new scope.\n");
    ctx->cur_scope = new_scope;
}

void rose_pop_scope(struct rose_ctx *ctx)
{
    struct rose_scope *prev_scope = ctx->cur_scope->upper;

    assert(prev_scope);
    TRACE("Popping current scope.\n");
    ctx->cur_scope = prev_scope;
}

static const char *const base_type_names[] =
{
    [ROSE_TYPE_UCHAR] = "unsigned char",
    [ROSE_TYPE_USHORT] = "unsigned short",
    [ROSE_TYPE_UINT] = "unsigned int",
    [ROSE_TYPE_ULONG] = "unsigned long",
    [ROSE_TYPE_SCHAR] = "signed char",
    [ROSE_TYPE_SSHORT] = "short",
    [ROSE_TYPE_SINT] = "int",
    [ROSE_TYPE_SLONG] = "long",
    [ROSE_TYPE_BOOL] = "bool",
};

struct rose_string_buffer *rose_type_to_string(struct rose_ctx *ctx, const struct rose_type *type)
{
    struct rose_string_buffer *string, *inner_string;

    string = rose_get_string_buffer(ctx);

    if (type->name)
    {
        if (type->prefix == ROSE_TYPE_PREFIX_STRUCT)
            rose_string_buffer_printf(string, "struct ");
        rose_string_buffer_printf(string, "%s", type->name);
    }
    else
    {
        switch (type->class)
        {
            case ROSE_CLASS_NUMERIC:
                assert(type->base_type < ARRAY_SIZE(base_type_names));
                rose_string_buffer_printf(string, "%s", base_type_names[type->base_type]);
                break;

            case ROSE_CLASS_ARRAY:
            {
                const struct rose_type *t;

                for (t = type; t->class == ROSE_CLASS_ARRAY; t = t->e.array.type)
                    ;

                if ((inner_string = rose_type_to_string(ctx, t)))
                {
                    rose_string_buffer_printf(string, "%s", inner_string->buffer);
                    rose_release_string_buffer(ctx, inner_string);
                }

                for (t = type; t->class == ROSE_CLASS_ARRAY; t = t->e.array.type)
                {
                    if (t->e.array.elements_count == ROSE_ARRAY_ELEMENTS_COUNT_IMPLICIT)
                        rose_string_buffer_printf(string, "[]");
                    else
                        rose_string_buffer_printf(string, "[%u]", t->e.array.elements_count);
                }
                break;
            }

            case ROSE_CLASS_POINTER:
                if ((inner_string = rose_type_to_string(ctx, type->e.ptr.type)))
                {
                    rose_string_buffer_printf(string, "%s *", inner_string->buffer);
                    rose_release_string_buffer(ctx, inner_string);
                }
                break;

            case ROSE_CLASS_FUNCTION:
                rose_string_buffer_printf(string, "<anonymous function>");
                break;

            case ROSE_CLASS_STRUCT:
                rose_string_buffer_printf(string, "<anonymous struct>");
                break;

            case ROSE_CLASS_ERROR:
                rose_string_buffer_printf(string, "<error type>");
                break;

            case ROSE_CLASS_VOID:
                rose_string_buffer_printf(string, "void");
                break;
        }
    }

    if (type->modifiers && (inner_string = rose_modifiers_to_string(ctx, type->modifiers)))
    {
        rose_string_buffer_printf(string, " %s", inner_string->buffer);
        rose_release_string_buffer(ctx, inner_string);
    }
    return string;
}

struct rose_string_buffer *rose_modifiers_to_string(struct rose_ctx *ctx, unsigned int modifiers)
{
    struct rose_string_buffer *string = rose_get_string_buffer(ctx);

    if (modifiers & ROSE_STORAGE_EXTERN)
        rose_string_buffer_printf(string, "extern ");
    if (modifiers & ROSE_STORAGE_STATIC)
        rose_string_buffer_printf(string, "static ");
    if (modifiers & ROSE_MODIFIER_VOLATILE)
        rose_string_buffer_printf(string, "volatile ");
    if (modifiers & ROSE_MODIFIER_CONST)
        rose_string_buffer_printf(string, "const ");
    if (modifiers & ROSE_MODIFIER_NEAR)
        rose_string_buffer_printf(string, "near ");
    if (modifiers & ROSE_MODIFIER_FAR)
        rose_string_buffer_printf(string, "far ");
    if (modifiers & ROSE_MODIFIER_CDECL)
        rose_string_buffer_printf(string, "cdecl ");
    if (modifiers & ROSE_MODIFIER_PASCAL)
        rose_string_buffer_printf(string, "pascal ");

    if (string->content_size)
        string->buffer[--string->content_size] = 0;

    return string;
}

static void dump_instr(struct rose_ctx *ctx, struct rose_string_buffer *buffer,
        const struct rose_ir_node *instr, unsigned int depth);

static void dump_block(struct rose_ctx *ctx, struct rose_string_buffer *buffer,
        const struct rose_block *block, unsigned int depth)
{
    struct rose_ir_node *instr;

    LIST_FOR_EACH_ENTRY(instr, &block->instrs, struct rose_ir_node, entry)
    {
        dump_instr(ctx, buffer, instr, depth);
        rose_string_buffer_printf(buffer, "\n");
    }
}

static void dump_src(struct rose_string_buffer *buffer, const struct rose_src *src)
{
    if (src->node->index)
        rose_string_buffer_printf(buffer, "@%u", src->node->index);
    else
        rose_string_buffer_printf(buffer, "%p", src->node);
}

static void dump_ir_var(struct rose_ctx *ctx, struct rose_string_buffer *buffer, const struct rose_ir_var *var)
{
    struct rose_string_buffer *string;

    if (var->storage_modifiers)
    {
        if ((string = rose_modifiers_to_string(ctx, var->storage_modifiers)))
            rose_string_buffer_printf(buffer, "%s ", string->buffer);
        rose_release_string_buffer(ctx, string);
    }

    string = rose_type_to_string(ctx, var->data_type);
    rose_string_buffer_printf(buffer, "%s", string->buffer);
    if (var->name)
        rose_string_buffer_printf(buffer, " %s", var->name);
    rose_release_string_buffer(ctx, string);
}

static void dump_deref(struct rose_string_buffer *buffer, const struct rose_deref *deref)
{
    unsigned int i;

    if (deref->var)
    {
        rose_string_buffer_printf(buffer, "%s", deref->var->name);
    }
    else
    {
        rose_string_buffer_printf(buffer, "*");
        dump_src(buffer, &deref->ptr);
    }
    if (deref->path_len)
    {
        rose_string_buffer_printf(buffer, "[");
        for (i = 0; i < deref->path_len; ++i)
        {
            rose_string_buffer_printf(buffer, "[");
            dump_src(buffer, &deref->path[i]);
            rose_string_buffer_printf(buffer, "]");
        }
        rose_string_buffer_printf(buffer, "]");
    }
    else if (deref->offset.node)
    {
        rose_string_buffer_printf(buffer, "[");
        dump_src(buffer, &deref->offset);
        rose_string_buffer_printf(buffer, "]");
    }
}

static void dump_ir_addr(struct rose_string_buffer *buffer, const struct rose_ir_addr *addr)
{
    rose_string_buffer_printf(buffer, "&");
    dump_deref(buffer, &addr->src);
}

static void dump_ir_call(struct rose_ctx *ctx, struct rose_string_buffer *buffer, const struct rose_ir_call *call)
{
    dump_deref(buffer, &call->func);
    rose_string_buffer_printf(buffer, "(");

    for (size_t i = 0; i < call->arg_count; ++i)
    {
        if (i)
            rose_string_buffer_printf(buffer, ", ");
        dump_src(buffer, &call->args[i]);
    }
    rose_string_buffer_printf(buffer, ")");
}

static void dump_ir_constant(struct rose_string_buffer *buffer, const struct rose_ir_constant *constant)
{
    const union rose_constant_value_component *value = &constant->value.u;
    struct rose_type *type = constant->node.data_type;

    if (type->class == ROSE_CLASS_ARRAY)
    {
        rose_string_buffer_printf(buffer, "\"%.*s\"", (int)value->str.len, value->str.str);
        return;
    }

    if (type->class == ROSE_CLASS_POINTER)
    {
        rose_string_buffer_printf(buffer, "%#x", value->u);
        return;
    }

    assert(type->class == ROSE_CLASS_NUMERIC);

    switch (type->base_type)
    {
        case ROSE_TYPE_BOOL:
            rose_string_buffer_printf(buffer, "%s", value->u ? "true" : "false");
            break;

        case ROSE_TYPE_SCHAR:
        case ROSE_TYPE_SSHORT:
        case ROSE_TYPE_SINT:
        case ROSE_TYPE_SLONG:
            rose_string_buffer_printf(buffer, "%d", value->i);
            break;

        case ROSE_TYPE_UCHAR:
        case ROSE_TYPE_USHORT:
        case ROSE_TYPE_UINT:
        case ROSE_TYPE_ULONG:
            rose_string_buffer_printf(buffer, "%u", value->u);
            break;
    }
}

static const char *debug_rose_expr_op(enum rose_ir_expr_op op)
{
    static const char *const op_names[] =
    {
        [ROSE_OP0_ERROR]        = "error",
        [ROSE_OP0_VOID]         = "void",

        [ROSE_OP1_BIT_NOT]      = "~",
        [ROSE_OP1_CAST]         = "cast",
        [ROSE_OP1_LOGIC_NOT]    = "!",
        [ROSE_OP1_NEG]          = "-",
        [ROSE_OP1_OFFSETOF]     = "offsetof",
        [ROSE_OP1_SELECTOROF]   = "selectorof",

        [ROSE_OP2_ADD]         = "+",
        [ROSE_OP2_BIT_AND]     = "&",
        [ROSE_OP2_BIT_OR]      = "|",
        [ROSE_OP2_BIT_XOR]     = "^",
        [ROSE_OP2_DIV]         = "/",
        [ROSE_OP2_EQUAL]       = "==",
        [ROSE_OP2_GEQUAL]      = ">=",
        [ROSE_OP2_GREATER]     = ">",
        [ROSE_OP2_LESS]        = "<",
        [ROSE_OP2_LEQUAL]      = "<=",
        [ROSE_OP2_LSHIFT]      = "<<",
        [ROSE_OP2_MOD]         = "%",
        [ROSE_OP2_MUL]         = "*",
        [ROSE_OP2_NEQUAL]      = "!=",
        [ROSE_OP2_RSHIFT]      = ">>",
        [ROSE_OP2_SUB]         = "-",
    };

    return op_names[op];
}

static void dump_ir_expr(struct rose_string_buffer *buffer, const struct rose_ir_expr *expr)
{
    unsigned int i;

    rose_string_buffer_printf(buffer, "%s (", debug_rose_expr_op(expr->op));
    for (i = 0; i < ROSE_MAX_OPERANDS && expr->operands[i].node; ++i)
    {
        if (i)
            rose_string_buffer_printf(buffer, ", ");
        dump_src(buffer, &expr->operands[i]);
    }
    rose_string_buffer_printf(buffer, ")");
}

static void dump_ir_if(struct rose_ctx *ctx, struct rose_string_buffer *buffer,
        const struct rose_ir_if *if_node, unsigned int depth)
{
    rose_string_buffer_printf(buffer, "if (");
    dump_src(buffer, &if_node->condition);
    rose_string_buffer_printf(buffer, ") {\n");
    dump_block(ctx, buffer, &if_node->then_block, depth + 1);
    if (!list_empty(&if_node->else_block.instrs))
    {
        rose_string_buffer_printf(buffer, "      %24s   ", "");
        for (unsigned int i = 0; i < depth; ++i)
            rose_string_buffer_printf(buffer, "  ");
        rose_string_buffer_printf(buffer, "} else {\n");
        dump_block(ctx, buffer, &if_node->else_block, depth + 1);
    }
    rose_string_buffer_printf(buffer, "      %24s   ", "");
    for (unsigned int i = 0; i < depth; ++i)
        rose_string_buffer_printf(buffer, "  ");
    rose_string_buffer_printf(buffer, "}");
}

static void dump_ir_jump(struct rose_string_buffer *buffer, const struct rose_ir_jump *jump)
{
    switch (jump->type)
    {
        case ROSE_IR_JUMP_BREAK:
            rose_string_buffer_printf(buffer, "break");
            break;

        case ROSE_IR_JUMP_CONTINUE:
            rose_string_buffer_printf(buffer, "continue");
            break;

        case ROSE_IR_JUMP_RETURN:
            rose_string_buffer_printf(buffer, "return");
            if (jump->return_value.node)
            {
                rose_string_buffer_printf(buffer, " ");
                dump_src(buffer, &jump->return_value);
            }
            break;
    }
}

static void dump_ir_loop(struct rose_ctx *ctx, struct rose_string_buffer *buffer,
        const struct rose_ir_loop *loop, unsigned int depth)
{
    rose_string_buffer_printf(buffer, "for (;;) {\n");
    dump_block(ctx, buffer, &loop->body, depth + 1);
    rose_string_buffer_printf(buffer, "      %24s   ", "");
    for (unsigned int i = 0; i < depth; ++i)
        rose_string_buffer_printf(buffer, "  ");
    rose_string_buffer_printf(buffer, "}");
}

static void dump_ir_store(struct rose_string_buffer *buffer, const struct rose_ir_store *store)
{
    rose_string_buffer_printf(buffer, "= (");
    dump_deref(buffer, &store->lhs);
    rose_string_buffer_printf(buffer, " ");
    dump_src(buffer, &store->rhs);
    rose_string_buffer_printf(buffer, ")");
}

static void dump_ir_index(struct rose_string_buffer *buffer, const struct rose_ir_index *index)
{
    dump_src(buffer, &index->val);
    rose_string_buffer_printf(buffer, "[idx:");
    dump_src(buffer, &index->idx);
    rose_string_buffer_printf(buffer, "]");
}

static void dump_instr(struct rose_ctx *ctx, struct rose_string_buffer *buffer,
        const struct rose_ir_node *instr, unsigned int depth)
{
    if (instr->index)
        rose_string_buffer_printf(buffer, "%4u: ", instr->index);
    else
        rose_string_buffer_printf(buffer, "%p: ", instr);

    if (instr->data_type)
    {
        struct rose_string_buffer *string;

        string = rose_type_to_string(ctx, instr->data_type);
        rose_string_buffer_printf(buffer, "%24s | ", string->buffer);
        rose_release_string_buffer(ctx, string);
    }
    else
    {
        rose_string_buffer_printf(buffer, "%24s | ", "");
    }

    for (unsigned int i = 0; i < depth; ++i)
        rose_string_buffer_printf(buffer, "  ");

    switch (instr->type)
    {
        case ROSE_IR_ADDR:
            dump_ir_addr(buffer, rose_ir_addr(instr));
            break;

        case ROSE_IR_CALL:
            dump_ir_call(ctx, buffer, rose_ir_call(instr));
            break;

        case ROSE_IR_CONSTANT:
            dump_ir_constant(buffer, rose_ir_constant(instr));
            break;

        case ROSE_IR_EXPR:
            dump_ir_expr(buffer, rose_ir_expr(instr));
            break;

        case ROSE_IR_IF:
            dump_ir_if(ctx, buffer, rose_ir_if(instr), depth);
            break;

        case ROSE_IR_INDEX:
            dump_ir_index(buffer, rose_ir_index(instr));
            break;

        case ROSE_IR_JUMP:
            dump_ir_jump(buffer, rose_ir_jump(instr));
            break;

        case ROSE_IR_LOAD:
            dump_deref(buffer, &rose_ir_load(instr)->src);
            break;

        case ROSE_IR_LOOP:
            dump_ir_loop(ctx, buffer, rose_ir_loop(instr), depth);
            break;

        case ROSE_IR_STORE:
            dump_ir_store(buffer, rose_ir_store(instr));
            break;
    }
}

void rose_dump_block(struct rose_ctx *ctx, const struct rose_block *block)
{
    struct rose_string_buffer buffer;

    rose_string_buffer_init(&buffer);
    dump_block(ctx, &buffer, block, 0);
    rose_string_buffer_trace(&buffer);
    rose_string_buffer_cleanup(&buffer);
}

void rose_dump_function(struct rose_ctx *ctx, const struct rose_function *func)
{
    struct rose_string_buffer buffer;
    size_t i;

    rose_string_buffer_init(&buffer);
    rose_string_buffer_printf(&buffer, "Dumping function %s.\n", func->name);
    rose_string_buffer_printf(&buffer, "Function parameters:\n");
    for (i = 0; i < func->parameters.count; ++i)
    {
        dump_ir_var(ctx, &buffer, func->parameters.vars[i]);
        rose_string_buffer_printf(&buffer, "\n");
    }
    if (func->has_body)
        dump_block(ctx, &buffer, &func->body, 0);

    rose_string_buffer_trace(&buffer);
    rose_string_buffer_cleanup(&buffer);
}

void rose_replace_node(struct rose_ir_node *old, struct rose_ir_node *new)
{
    struct rose_src *src, *next;

    LIST_FOR_EACH_ENTRY_SAFE(src, next, &old->uses, struct rose_src, entry)
    {
        rose_src_remove(src);
        rose_src_from_node(src, new);
    }
    list_remove(&old->entry);
    rose_free_instr(old);
}


void rose_free_type(struct rose_type *type)
{
    struct rose_struct_field *field;
    size_t i;

    rose_free((void *)type->name);
    if (type->class == ROSE_CLASS_STRUCT)
    {
        for (i = 0; i < type->e.record.field_count; ++i)
        {
            field = &type->e.record.fields[i];

            rose_free((void *)field->name);
        }
        rose_free((void *)type->e.record.fields);
    }
    rose_free(type);
}

void rose_free_instr_list(struct list *list)
{
    struct rose_ir_node *node, *next_node;

    if (!list)
        return;
    /* Iterate in reverse, to avoid use-after-free when unlinking sources from
     * the "uses" list. */
    LIST_FOR_EACH_ENTRY_SAFE_REV(node, next_node, list, struct rose_ir_node, entry)
        rose_free_instr(node);
}

void rose_block_cleanup(struct rose_block *block)
{
    rose_free_instr_list(&block->instrs);
}

static void free_ir_addr(struct rose_ir_addr *addr)
{
    rose_cleanup_deref(&addr->src);
    rose_free(addr);
}

static void free_ir_call(struct rose_ir_call *call)
{
    for (size_t i = 0; i < call->arg_count; ++i)
        rose_src_remove(&call->args[i]);
    rose_cleanup_deref(&call->func);
    rose_free(call);
}

static void free_ir_constant(struct rose_ir_constant *constant)
{
    if (constant->node.data_type->class == ROSE_CLASS_ARRAY)
        rose_free((void *)constant->value.u.str.str);
    rose_free(constant);
}

static void free_ir_expr(struct rose_ir_expr *expr)
{
    unsigned int i;

    for (i = 0; i < ARRAY_SIZE(expr->operands); ++i)
        rose_src_remove(&expr->operands[i]);
    rose_free(expr);
}

static void free_ir_if(struct rose_ir_if *if_node)
{
    rose_block_cleanup(&if_node->then_block);
    rose_block_cleanup(&if_node->else_block);
    rose_src_remove(&if_node->condition);
    rose_free(if_node);
}

static void free_ir_jump(struct rose_ir_jump *jump)
{
    rose_src_remove(&jump->return_value);
    rose_free(jump);
}

static void free_ir_load(struct rose_ir_load *load)
{
    rose_cleanup_deref(&load->src);
    rose_free(load);
}

static void free_ir_loop(struct rose_ir_loop *loop)
{
    rose_block_cleanup(&loop->body);
    rose_block_cleanup(&loop->iter);
    rose_free(loop);
}

static void free_ir_store(struct rose_ir_store *store)
{
    rose_src_remove(&store->rhs);
    rose_cleanup_deref(&store->lhs);
    rose_free(store);
}

static void free_ir_index(struct rose_ir_index *index)
{
    rose_src_remove(&index->val);
    rose_src_remove(&index->idx);
    rose_free(index);
}

void rose_free_instr(struct rose_ir_node *node)
{
    assert(list_empty(&node->uses));

    switch (node->type)
    {
        case ROSE_IR_ADDR:
            free_ir_addr(rose_ir_addr(node));
            break;

        case ROSE_IR_CALL:
            free_ir_call(rose_ir_call(node));
            break;

        case ROSE_IR_CONSTANT:
            free_ir_constant(rose_ir_constant(node));
            break;

        case ROSE_IR_EXPR:
            free_ir_expr(rose_ir_expr(node));
            break;

        case ROSE_IR_IF:
            free_ir_if(rose_ir_if(node));
            break;

        case ROSE_IR_INDEX:
            free_ir_index(rose_ir_index(node));
            break;

        case ROSE_IR_JUMP:
            free_ir_jump(rose_ir_jump(node));
            break;

        case ROSE_IR_LOAD:
            free_ir_load(rose_ir_load(node));
            break;

        case ROSE_IR_LOOP:
            free_ir_loop(rose_ir_loop(node));
            break;

        case ROSE_IR_STORE:
            free_ir_store(rose_ir_store(node));
            break;
    }
}

static void free_function(struct rose_function *func)
{
    rose_free(func->parameters.vars);
    rose_block_cleanup(&func->body);
    rose_free((void *)func->name);
    rose_free(func);
}

static void free_function_rb(struct rb_entry *entry, void *context)
{
    free_function(RB_ENTRY_VALUE(entry, struct rose_function, entry));
}

static int compare_function_rb(const void *key, const struct rb_entry *entry)
{
    const char *name = key;
    const struct rose_function *func = RB_ENTRY_VALUE(entry, const struct rose_function, entry);

    return strcmp(name, func->name);
}

static void declare_predefined_types(struct rose_ctx *ctx)
{
    struct rose_type *type;
    unsigned int bt;

    for (bt = 0; bt <= ROSE_TYPE_LAST_SCALAR; ++bt)
    {
        type = rose_new_type(ctx, ROSE_CLASS_NUMERIC);
        type->base_type = bt;
        ctx->builtin_types.scalar[bt] = type;
    }

    ctx->builtin_types.Void = rose_new_type(ctx, ROSE_CLASS_VOID);
    ctx->builtin_types.error = rose_new_type(ctx, ROSE_CLASS_ERROR);
}

static void rose_ctx_init(struct rose_ctx *ctx, const char *source_name)
{
    memset(ctx, 0, sizeof(*ctx));

    ctx->source_files = rose_alloc(ctx, sizeof(*ctx->source_files));
    ctx->source_files[0] = rose_strdup(ctx, source_name);
    ctx->source_file_count = 1;
    ctx->location.source_name = ctx->source_files[0];
    ctx->location.line = ctx->location.column = 1;
    rose_string_buffer_cache_init(&ctx->string_buffers);

    list_init(&ctx->scopes);

    rose_push_scope(ctx);
    ctx->globals = ctx->cur_scope;

    list_init(&ctx->types);
    declare_predefined_types(ctx);

    rb_init(&ctx->functions, compare_function_rb);

    rose_block_init(&ctx->static_initializers);
    list_init(&ctx->extern_vars);

    ctx->error_instr = rose_new_error_expr(ctx);
    rose_block_add_instr(&ctx->static_initializers, ctx->error_instr);

    ctx->data_block.data = true;
}

static void rose_ctx_cleanup(struct rose_ctx *ctx)
{
    struct rose_scope *scope, *next_scope;
    struct rose_ir_var *var, *next_var;
    struct rose_type *type, *next_type;
    unsigned int i;

    for (i = 0; i < ctx->source_file_count; ++i)
        rose_free((void *)ctx->source_files[i]);
    rose_free(ctx->source_files);
    rose_string_buffer_cache_cleanup(&ctx->string_buffers);

    rb_destroy(&ctx->functions, free_function_rb, NULL);

    rose_block_cleanup(&ctx->static_initializers);

    LIST_FOR_EACH_ENTRY_SAFE(scope, next_scope, &ctx->scopes, struct rose_scope, entry)
    {
        LIST_FOR_EACH_ENTRY_SAFE(var, next_var, &scope->vars, struct rose_ir_var, scope_entry)
            rose_free_var(var);
        for (size_t i = 0; i < ROSE_TYPE_PREFIX_COUNT; ++i)
            rb_destroy(&scope->types[i], NULL, NULL);
        rose_free(scope);
    }

    LIST_FOR_EACH_ENTRY_SAFE(type, next_type, &ctx->types, struct rose_type, entry)
        rose_free_type(type);
}

static void resolve_local_reloc(struct rose_ctx *ctx, struct x86_reloc *reloc)
{
    for (size_t i = 0; i < ctx->asm_block_count; ++i)
    {
        const struct x86_block *target_block = &ctx->asm_blocks[i];

        for (size_t j = 0; j < target_block->symbol_count; ++j)
        {
            const struct x86_symbol *symbol = &target_block->symbols[j];

            if (strcmp(symbol->name, reloc->u.name))
                continue;

            if (reloc->type == X86_RELOC_UNRESOLVED_LOCAL)
                reloc->type = X86_RELOC_LOCAL;
            else if (reloc->type == X86_RELOC_UNRESOLVED_OFFSET)
                reloc->type = X86_RELOC_INTERNAL_OFFSET;
            else if (reloc->type == X86_RELOC_UNRESOLVED_SEGMENT)
                reloc->type = X86_RELOC_INTERNAL_SEGMENT;
            else if (reloc->type == X86_RELOC_UNRESOLVED_PAIR)
                reloc->type = X86_RELOC_INTERNAL_PAIR;
            else
                rose_unreachable();

            reloc->u.symbol = symbol;

            TRACE("Found local symbol %s, type %#x.\n", symbol->name, reloc->type);
            return;
        }
    }
}

static void resolve_locals(struct rose_ctx *ctx)
{
    /* Put the data block in the asm blocks list so we don't need to treat it
     * specially (anymore). */

    if (ctx->data_block.count)
    {
        rose_array_reserve(ctx, (void **)&ctx->asm_blocks, &ctx->asm_blocks_capacity,
                ctx->asm_block_count + 1, sizeof(*ctx->asm_blocks));
        ctx->asm_blocks[ctx->asm_block_count++] = ctx->data_block;
    }

    for (size_t i = 0; i < ctx->asm_block_count; ++i)
    {
        const struct x86_block *block = &ctx->asm_blocks[i];

        for (size_t j = 0; j < block->reloc_count; ++j)
            resolve_local_reloc(ctx, &block->relocs[j]);
    }
}

enum rose_result rose_compile_shader(const struct rose_code *source,
        const char *source_name, struct rose_program *out)
{
    struct rose_ctx ctx;
    int ret;

    memset(out, 0, sizeof(*out));

    rose_ctx_init(&ctx, source_name);

    if ((ret = rose_lexer_compile(&ctx, source)) == 2)
        abort();

    if (ctx.result)
    {
        rose_ctx_cleanup(&ctx);
        return ctx.result;
    }

    /* If parsing failed without an error condition being recorded, we
     * plausibly hit some unimplemented feature. */
    if (ret)
    {
        rose_ctx_cleanup(&ctx);
        return ROSE_ERROR_NOT_IMPLEMENTED;
    }

    rose_emit_bytecode(&ctx);
    ret = ctx.result;

    if (!ret)
    {
        resolve_locals(&ctx);

        array_reserve((void **)&out->blocks, &out->blocks_capacity,
                ctx.asm_block_count + 1, sizeof(*out->blocks));
        for (size_t i = 0; i < ctx.asm_block_count; ++i)
            out->blocks[i] = ctx.asm_blocks[i];
        out->block_count = ctx.asm_block_count;
    }

    rose_ctx_cleanup(&ctx);
    return ret;
}
