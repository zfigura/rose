/*
 * Copyright 2023 Elizabeth Figura
 *
 * This file is part of the Rosé compiler.
 *
 * Rosé is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rosé is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Rosé; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "c.h"

static bool is_signed(const struct rose_type *type)
{
    if (type->class != ROSE_CLASS_NUMERIC)
        return true;

    switch (type->base_type)
    {
        case ROSE_TYPE_SCHAR:
        case ROSE_TYPE_SSHORT:
        case ROSE_TYPE_SINT:
        case ROSE_TYPE_SLONG:
            return true;

        case ROSE_TYPE_BOOL:
        case ROSE_TYPE_UCHAR:
        case ROSE_TYPE_USHORT:
        case ROSE_TYPE_UINT:
        case ROSE_TYPE_ULONG:
            return false;
    }

    rose_unreachable();
}

static char *get_local_symbol_name(struct rose_ctx *ctx, const char *template)
{
    struct rose_string_buffer *string = rose_get_string_buffer(ctx);
    char *ret;

    /* GNU as .L has special meaning, in that it won't be exposed in debug
     * information. We don't have debug information. But tradition, and perhaps
     * clarity, demands it. */
    rose_string_buffer_printf(string, ".L%s%u", template, ctx->symbol_counter++);
    ret = rose_strdup(ctx, string->buffer);
    rose_release_string_buffer(ctx, string);
    return ret;
}

static uint16_t get_type_param_size(const struct rose_type *type)
{
    switch (type->class)
    {
        case ROSE_CLASS_NUMERIC:
            switch (type->base_type)
            {
                case ROSE_TYPE_BOOL:
                case ROSE_TYPE_SCHAR:
                case ROSE_TYPE_SINT:
                case ROSE_TYPE_SSHORT:
                case ROSE_TYPE_UCHAR:
                case ROSE_TYPE_UINT:
                case ROSE_TYPE_USHORT:
                    return 2;

                case ROSE_TYPE_SLONG:
                case ROSE_TYPE_ULONG:
                    return 4;
            }
            break;

        case ROSE_CLASS_ARRAY:
        case ROSE_CLASS_STRUCT:
            FIXME("Array or struct passed on stack. How does this work?\n");
            return 4;

        case ROSE_CLASS_POINTER:
            return rose_type_is_far_pointer(type) ? 4 : 2;

        case ROSE_CLASS_ERROR:
        case ROSE_CLASS_FUNCTION:
        case ROSE_CLASS_VOID:
            rose_unreachable();
    }

    rose_unreachable();
}

static bool transform_ir(struct rose_ctx *ctx, bool (*func)(struct rose_ctx *ctx, struct rose_ir_node *, void *),
        struct rose_block *block, void *context)
{
    struct rose_ir_node *instr, *next;
    bool progress = false;

    LIST_FOR_EACH_ENTRY_SAFE(instr, next, &block->instrs, struct rose_ir_node, entry)
    {
        if (instr->type == ROSE_IR_IF)
        {
            struct rose_ir_if *iff = rose_ir_if(instr);

            progress |= transform_ir(ctx, func, &iff->then_block, context);
            progress |= transform_ir(ctx, func, &iff->else_block, context);
        }
        else if (instr->type == ROSE_IR_LOOP)
        {
            struct rose_ir_loop *loop = rose_ir_loop(instr);

            progress |= transform_ir(ctx, func, &loop->body, context);
            progress |= transform_ir(ctx, func, &loop->iter, context);
        }

        progress |= func(ctx, instr, context);
    }

    return progress;
}

static bool lower_string_constants(struct rose_ctx *ctx, struct rose_ir_node *instr, void *context)
{
    struct rose_ir_node *load;

    if (instr->type != ROSE_IR_CONSTANT)
        return false;
    if (instr->data_type->class != ROSE_CLASS_ARRAY)
        return false;

    load = rose_convert_string_constant(ctx, rose_ir_constant(instr));
    list_add_before(&instr->entry, &load->entry);
    rose_replace_node(instr, load);
    return true;
}

static bool lower_index_loads(struct rose_ctx *ctx, struct rose_ir_node *instr, void *context)
{
    struct rose_ir_load *load;
    struct rose_deref deref;

    if (instr->type != ROSE_IR_INDEX)
        return false;

    /* HLSL always makes this a synthetic var, on the grounds that a load really
     * should be an SSA node, and reflect the var's state before it was changed
     * by whatever.
     *
     * The problem is this results in a bunch of redundant copies and makes the
     * IR basically unreadable. HLSL avoids that by using copy-prop. But I don't
     * want to implement copy-prop in Rosé if I can avoid it, both because it's
     * big and complicated, and because getting rid of temps will increase
     * register pressure, which is not an easy problem to solve.
     *
     * C has a spec surrounding things like sequence points. HLSL has no such
     * spec, rather we need to match native behaviour. So in theory, if we avoid
     * any src reaching through a sequence point, we should be fine.
     * Is this the case? Sequence points are:
     *
     * - semicolon and comma operators. These are evaluated entirely separately
     *   and then concatenated.
     *
     * - shortcut logic operations. These are also evaluated separately;
     *   instructions on the right side can't hit instructions on the left.
     *
     * - after all of the args to a function call. Same deal.
     *
     * So if anything's storing to this variable, delimited by the same
     * sequence points, it's not guaranteed to be ordered anyway.
     */

    if (rose_init_deref_from_index_chain(ctx, &deref, instr))
    {
        load = rose_new_load_index(ctx, &deref, NULL, &instr->loc);
        rose_cleanup_deref(&deref);
    }
    else
    {
        struct rose_ir_index *index = rose_ir_index(instr);
        struct rose_ir_node *val = index->val.node;
        struct rose_ir_node *store;
        struct rose_ir_var *var;

        var = rose_new_synthetic_var(ctx, "index-val", val->data_type, context, &instr->loc);
        rose_init_simple_deref_from_var(&deref, var);

        store = rose_new_simple_store(ctx, var, val);
        list_add_before(&instr->entry, &store->entry);

        load = rose_new_load_index(ctx, &deref, index->idx.node, &instr->loc);
    }
    list_add_before(&instr->entry, &load->node.entry);
    rose_replace_node(instr, &load->node);
    return true;
}

static struct rose_ir_node *new_offset_from_path_index(struct rose_ctx *ctx, struct rose_block *block,
        struct rose_type *type, struct rose_ir_node *offset, struct rose_ir_node *idx, const struct rose_location *loc)
{
    struct rose_ir_node *idx_offset = NULL;
    struct rose_ir_node *c;

    rose_block_init(block);

    switch (type->class)
    {
        case ROSE_CLASS_ARRAY:
        {
            unsigned int size = rose_type_get_size(type->e.array.type);

            c = rose_new_uint_constant(ctx, ROSE_TYPE_UINT, size, loc);
            rose_block_add_instr(block, c);

            idx_offset = rose_new_binary_expr(ctx, ROSE_OP2_MUL, c, idx);
            rose_block_add_instr(block, idx_offset);
            break;
        }

        case ROSE_CLASS_STRUCT:
        {
            unsigned int field_idx = rose_ir_constant(idx)->value.u.u;

            c = rose_new_uint_constant(ctx, ROSE_TYPE_UINT, rose_type_get_field_offset(type, field_idx), loc);
            rose_block_add_instr(block, c);
            idx_offset = c;
            break;
        }

        case ROSE_CLASS_ERROR:
        case ROSE_CLASS_FUNCTION:
        case ROSE_CLASS_NUMERIC:
        case ROSE_CLASS_POINTER:
        case ROSE_CLASS_VOID:
            rose_unreachable();
    }

    if (offset)
    {
        idx_offset = rose_new_binary_expr(ctx, ROSE_OP2_ADD, offset, idx_offset);
        rose_block_add_instr(block, idx_offset);
    }

    return idx_offset;
}

static struct rose_ir_node *new_offset_instr_from_deref(struct rose_ctx *ctx, struct rose_block *block,
        const struct rose_deref *deref, const struct rose_location *loc)
{
    struct rose_ir_node *offset = NULL;
    struct rose_type *type;
    unsigned int i;

    rose_block_init(block);

    if (deref->var)
    {
        type = deref->var->data_type;
    }
    else
    {
        type = deref->ptr.node->data_type;
        assert(type->class == ROSE_CLASS_POINTER);
        type = type->e.ptr.type;
    }

    for (i = 0; i < deref->path_len; ++i)
    {
        struct rose_block idx_block;

        offset = new_offset_from_path_index(ctx, &idx_block, type, offset, deref->path[i].node, loc);
        rose_block_add_block(block, &idx_block);

        type = rose_get_element_type_from_path_index(ctx, type, deref->path[i].node);
    }

    return offset;
}

static void replace_deref_path_with_offset(struct rose_ctx *ctx, struct rose_deref *deref, struct rose_ir_node *instr)
{
    struct rose_ir_node *offset;
    struct rose_block block;

    /* register offsets shouldn't be used before this point is reached. */
    assert(!deref->offset.node);

    if ((offset = new_offset_instr_from_deref(ctx, &block, deref, &instr->loc)))
    {
        list_move_before(&instr->entry, &block.instrs);
        rose_src_from_node(&deref->offset, offset);
    }

    rose_deref_remove_path(deref);
}

static bool transform_deref_paths_into_offsets(struct rose_ctx *ctx, struct rose_ir_node *instr, void *context)
{
    switch (instr->type)
    {
        case ROSE_IR_ADDR:
            replace_deref_path_with_offset(ctx, &rose_ir_addr(instr)->src, instr);
            return true;

        case ROSE_IR_LOAD:
            replace_deref_path_with_offset(ctx, &rose_ir_load(instr)->src, instr);
            return true;

        case ROSE_IR_STORE:
            replace_deref_path_with_offset(ctx, &rose_ir_store(instr)->lhs, instr);
            return true;

        case ROSE_IR_CALL:
            replace_deref_path_with_offset(ctx, &rose_ir_call(instr)->func, instr);
            return true;

        case ROSE_IR_CONSTANT:
        case ROSE_IR_EXPR:
        case ROSE_IR_IF:
        case ROSE_IR_INDEX:
        case ROSE_IR_JUMP:
        case ROSE_IR_LOOP:
            return false;
    }

    rose_unreachable();
}

static bool lower_far_pointer_comparisons(struct rose_ctx *ctx, struct rose_ir_node *instr, void *context)
{
    struct rose_ir_node *src1, *src2, *selector1, *selector2, *offset1, *offset2, *xor_high, *xor_low, *or, *res;
    struct rose_type *uint_type = rose_get_scalar_type(ctx, ROSE_TYPE_UINT);
    struct rose_type *bool_type = rose_get_scalar_type(ctx, ROSE_TYPE_BOOL);
    struct rose_ir_expr *expr;

    if (instr->type != ROSE_IR_EXPR)
        return false;
    expr = rose_ir_expr(instr);
    if (expr->op != ROSE_OP2_EQUAL && expr->op != ROSE_OP2_NEQUAL)
        return false;
    src1 = expr->operands[0].node;
    src2 = expr->operands[1].node;

    if (!rose_type_is_far_pointer(src1->data_type))
        return false;
    assert(rose_types_are_equal(src1->data_type, src2->data_type, false));

    selector1 = rose_new_unary_expr(ctx, ROSE_OP1_SELECTOROF, src1, uint_type, &instr->loc);
    list_add_before(&instr->entry, &selector1->entry);

    selector2 = rose_new_unary_expr(ctx, ROSE_OP1_SELECTOROF, src2, uint_type, &instr->loc);
    list_add_before(&instr->entry, &selector2->entry);

    xor_high = rose_new_binary_expr(ctx, ROSE_OP2_BIT_XOR, selector1, selector2);
    list_add_before(&instr->entry, &xor_high->entry);

    offset1 = rose_new_unary_expr(ctx, ROSE_OP1_OFFSETOF, src1, uint_type, &instr->loc);
    list_add_before(&instr->entry, &offset1->entry);

    offset2 = rose_new_unary_expr(ctx, ROSE_OP1_OFFSETOF, src2, uint_type, &instr->loc);
    list_add_before(&instr->entry, &offset2->entry);

    xor_low = rose_new_binary_expr(ctx, ROSE_OP2_BIT_XOR, offset1, offset2);
    list_add_before(&instr->entry, &xor_low->entry);

    or = rose_new_binary_expr(ctx, ROSE_OP2_BIT_OR, xor_high, xor_low);
    list_add_before(&instr->entry, &or->entry);

    res = rose_new_cast(ctx, or, bool_type, &instr->loc);
    list_add_before(&instr->entry, &res->entry);

    if (expr->op == ROSE_OP2_EQUAL)
    {
        res = rose_new_unary_expr(ctx, ROSE_OP1_LOGIC_NOT, res, bool_type, &instr->loc);
        list_add_before(&instr->entry, &res->entry);
    }

    rose_replace_node(instr, res);
    return true;
}

static bool lower_far_pointer_to_bool_casts(struct rose_ctx *ctx, struct rose_ir_node *instr, void *context)
{
    struct rose_type *uint_type = rose_get_scalar_type(ctx, ROSE_TYPE_UINT);
    struct rose_ir_node *src, *selector, *offset, *or, *cast;
    struct rose_ir_expr *expr;

    if (instr->type != ROSE_IR_EXPR)
        return false;
    expr = rose_ir_expr(instr);
    if (expr->op != ROSE_OP1_CAST && expr->op != ROSE_OP1_LOGIC_NOT)
        return false;
    src = expr->operands[0].node;

    if (expr->node.data_type->class != ROSE_CLASS_NUMERIC || expr->node.data_type->base_type != ROSE_TYPE_BOOL)
        return false;
    if (!rose_type_is_far_pointer(src->data_type))
        return false;

    selector = rose_new_unary_expr(ctx, ROSE_OP1_SELECTOROF, src, uint_type, &instr->loc);
    list_add_before(&instr->entry, &selector->entry);

    offset = rose_new_unary_expr(ctx, ROSE_OP1_OFFSETOF, src, uint_type, &instr->loc);
    list_add_before(&instr->entry, &offset->entry);

    or = rose_new_binary_expr(ctx, ROSE_OP2_BIT_OR, selector, offset);
    list_add_before(&instr->entry, &or->entry);

    if (expr->op == ROSE_OP1_CAST)
    {
        cast = rose_new_cast(ctx, or, rose_get_scalar_type(ctx, ROSE_TYPE_BOOL), &instr->loc);
        list_add_before(&instr->entry, &cast->entry);
    }
    else
    {
        cast = rose_new_unary_expr(ctx, ROSE_OP1_LOGIC_NOT, or, rose_get_scalar_type(ctx, ROSE_TYPE_BOOL), &instr->loc);
        list_add_before(&instr->entry, &cast->entry);
    }

    rose_replace_node(instr, cast);
    return true;
}

static bool lower_far_pointer_tests(struct rose_ctx *ctx, struct rose_ir_node *instr, void *context)
{
    struct rose_type *uint_type = rose_get_scalar_type(ctx, ROSE_TYPE_UINT);
    struct rose_ir_node *condition, *selector, *offset, *or;
    struct rose_ir_if *iff;

    if (instr->type != ROSE_IR_IF)
        return false;
    iff = rose_ir_if(instr);
    condition = iff->condition.node;

    if (!rose_type_is_far_pointer(condition->data_type))
        return false;

    selector = rose_new_unary_expr(ctx, ROSE_OP1_SELECTOROF, condition, uint_type, &iff->node.loc);
    list_add_before(&instr->entry, &selector->entry);
    offset = rose_new_unary_expr(ctx, ROSE_OP1_OFFSETOF, condition, uint_type, &iff->node.loc);
    list_add_before(&instr->entry, &offset->entry);
    or = rose_new_binary_expr(ctx, ROSE_OP2_BIT_OR, selector, offset);
    list_add_before(&instr->entry, &or->entry);

    rose_src_remove(&iff->condition);
    rose_src_from_node(&iff->condition, or);

    return true;
}

/* ADDR instructions of near variables are near, but usually they immediately
 * get cast to far. Alternatively, sometimes they're cast from one pointer type
 * to another.
 *
 * This isn't normally a problem, but it makes the global initializer code a
 * bit annoying, and it's a bit easier to handle this in a separate pass,
 * and deal with the mismatch in emit_addr(). */
static bool fold_addr_cast(struct rose_ctx *ctx, struct rose_ir_node *instr, void *context)
{
    struct rose_ir_node *src, *new_addr;
    struct rose_ir_expr *expr;

    if (instr->type != ROSE_IR_EXPR)
        return false;
    expr = rose_ir_expr(instr);
    if (expr->op != ROSE_OP1_CAST)
        return false;
    src = expr->operands[0].node;

    if (!rose_type_is_far_pointer(instr->data_type))
        return false;
    if (src->data_type->class != ROSE_CLASS_POINTER)
        return false;

    if (src->type != ROSE_IR_ADDR)
        return false;

    new_addr = rose_new_addr(ctx, &rose_ir_addr(src)->src, &src->loc);
    new_addr->data_type = expr->node.data_type;
    list_add_before(&src->entry, &new_addr->entry);

    rose_replace_node(instr, new_addr);
    return true;
}

/* Constants are implicitly int (or uint, or larger), meaning that an comparison
 * with an 8-bit expression like "x == 1" will result in an unnecessary cast.
 * Detect these and get rid of them. */
static bool fold_8bit_comparisons(struct rose_ctx *ctx, struct rose_ir_node *instr, void *context)
{
    struct rose_ir_node *arg1, *arg2, *new_c, *new_cmp, *arg1_orig;
    struct rose_ir_expr *expr, *arg1_expr;
    struct rose_ir_constant *arg2_c;

    if (instr->type != ROSE_IR_EXPR)
        return false;
    expr = rose_ir_expr(instr);

    switch (expr->op)
    {
        case ROSE_OP2_EQUAL:
        case ROSE_OP2_GEQUAL:
        case ROSE_OP2_GREATER:
        case ROSE_OP2_LEQUAL:
        case ROSE_OP2_LESS:
        case ROSE_OP2_NEQUAL:
            break;

        default:
            return false;
    }

    arg1 = expr->operands[0].node;
    arg2 = expr->operands[1].node;

    if (arg1->type != ROSE_IR_EXPR)
        return false;
    arg1_expr = rose_ir_expr(arg1);
    if (arg1_expr->op != ROSE_OP1_CAST)
        return false;
    arg1_orig = arg1_expr->operands[0].node;
    if (rose_type_get_size(arg1_orig->data_type) != 1)
        return false;

    if (arg2->type != ROSE_IR_CONSTANT)
        return false;
    arg2_c = rose_ir_constant(arg2);
    if (is_signed(arg2->data_type))
    {
        if (arg2_c->value.u.i < INT8_MIN || arg2_c->value.u.i > INT8_MAX)
            return false;
    }
    else
    {
        if (arg2_c->value.u.u > UINT8_MAX)
            return false;
    }

    new_c = rose_new_int_constant(ctx, arg1_orig->data_type->base_type, arg2_c->value.u.i, &arg2->loc);
    list_add_before(&instr->entry, &new_c->entry);

    new_cmp = rose_new_binary_expr(ctx, expr->op, arg1_orig, new_c);
    list_add_before(&instr->entry, &new_cmp->entry);
    rose_replace_node(instr, new_cmp);

    return true;
}

static bool fold_bit_not(struct rose_ctx *ctx, struct rose_constant_value *dst,
        const struct rose_type *dst_type, const struct rose_ir_constant *src)
{
    enum rose_base_type type = dst_type->base_type;

    assert(type == src->node.data_type->base_type);
    assert(dst_type->class == ROSE_CLASS_NUMERIC);

    switch (dst_type->base_type)
    {
        case ROSE_TYPE_UCHAR:
            dst->u.u = (uint8_t)~src->value.u.u;
            return true;

        case ROSE_TYPE_UINT:
        case ROSE_TYPE_USHORT:
            dst->u.u = (uint16_t)~src->value.u.u;
            return true;

        case ROSE_TYPE_SCHAR:
        case ROSE_TYPE_SINT:
        case ROSE_TYPE_SLONG:
        case ROSE_TYPE_SSHORT:
        case ROSE_TYPE_ULONG:
            dst->u.i = ~src->value.u.i;
            return true;

        case ROSE_TYPE_BOOL:
            dst->u.u = !src->value.u.u;
            return true;
    }
    rose_unreachable();
}

static bool fold_cast(struct rose_ctx *ctx, struct rose_constant_value *dst,
        const struct rose_type *dst_type, const struct rose_ir_constant *src)
{
    const struct rose_type *src_type = src->node.data_type;
    uint32_t int_value = src->value.u.u;

    if (src_type->class != ROSE_CLASS_NUMERIC && src_type->class != ROSE_CLASS_POINTER)
        return false;

    if (dst_type->class == ROSE_CLASS_POINTER)
    {
        dst->u.u = int_value;
        return true;
    }

    assert(dst_type->class == ROSE_CLASS_NUMERIC);

    switch (dst_type->base_type)
    {
        case ROSE_TYPE_UCHAR:
            dst->u.u = (uint8_t)int_value;
            return true;

        case ROSE_TYPE_UINT:
        case ROSE_TYPE_USHORT:
            dst->u.u = (uint16_t)int_value;
            return true;

        case ROSE_TYPE_SCHAR:
        case ROSE_TYPE_SINT:
        case ROSE_TYPE_SLONG:
        case ROSE_TYPE_SSHORT:
        case ROSE_TYPE_ULONG:
            dst->u.u = int_value;
            return true;

        case ROSE_TYPE_BOOL:
            dst->u.u = !!int_value;
            return true;
    }

    rose_unreachable();
}

static bool fold_neg(struct rose_ctx *ctx, struct rose_constant_value *dst,
        const struct rose_type *dst_type, const struct rose_ir_constant *src)
{
    enum rose_base_type type = dst_type->base_type;

    assert(type == src->node.data_type->base_type);
    assert(dst_type->class == ROSE_CLASS_NUMERIC);

    switch (dst_type->base_type)
    {
        case ROSE_TYPE_UCHAR:
            dst->u.u = (uint8_t)-src->value.u.u;
            return true;

        case ROSE_TYPE_UINT:
        case ROSE_TYPE_USHORT:
            dst->u.u = (uint16_t)-src->value.u.u;
            return true;

        case ROSE_TYPE_SCHAR:
        case ROSE_TYPE_SINT:
        case ROSE_TYPE_SLONG:
        case ROSE_TYPE_SSHORT:
        case ROSE_TYPE_ULONG:
            dst->u.i = -src->value.u.i;
            return true;

        case ROSE_TYPE_BOOL:
            dst->u.u = src->value.u.u;
            return true;
    }
    rose_unreachable();
}

static bool fold_add(struct rose_ctx *ctx, struct rose_constant_value *dst, const struct rose_type *dst_type,
        const struct rose_ir_constant *src1, const struct rose_ir_constant *src2)
{
    enum rose_base_type type = dst_type->base_type;

    assert(type == src1->node.data_type->base_type);
    assert(type == src2->node.data_type->base_type);

    switch (type)
    {
        /* Handling ROSE_TYPE_INT through the unsigned field to avoid
         * undefined behavior with signed integers in C. */
        case ROSE_TYPE_SINT:
            dst->u.u = (int16_t)(src1->value.u.u + src2->value.u.u);
            break;

        case ROSE_TYPE_UINT:
            dst->u.u = (uint16_t)(src1->value.u.u + src2->value.u.u);
            break;

        default:
            FIXME("Fold addition for type %#x.\n", type);
            return false;
    }
    return true;
}

static bool fold_sub(struct rose_ctx *ctx, struct rose_constant_value *dst, const struct rose_type *dst_type,
        const struct rose_ir_constant *src1, const struct rose_ir_constant *src2)
{
    enum rose_base_type type = dst_type->base_type;

    assert(type == src1->node.data_type->base_type);
    assert(type == src2->node.data_type->base_type);

    switch (type)
    {
        /* Handling ROSE_TYPE_INT through the unsigned field to avoid
         * undefined behavior with signed integers in C. */
        case ROSE_TYPE_SINT:
            dst->u.u = (int16_t)(src1->value.u.u - src2->value.u.u);
            break;

        case ROSE_TYPE_UINT:
            dst->u.u = (uint16_t)(src1->value.u.u - src2->value.u.u);
            break;

        default:
            FIXME("Fold addition for type %#x.\n", type);
            return false;
    }
    return true;
}

static bool fold_div(struct rose_ctx *ctx, struct rose_constant_value *dst, const struct rose_type *dst_type,
        const struct rose_ir_constant *src1, const struct rose_ir_constant *src2)
{
    enum rose_base_type type = dst_type->base_type;

    assert(type == src1->node.data_type->base_type);
    assert(type == src2->node.data_type->base_type);

    if (is_signed(dst_type))
        dst->u.i = src1->value.u.i / src2->value.u.i;
    else
        dst->u.u = src1->value.u.u / src2->value.u.u;
    return true;
}

static bool fold_mul(struct rose_ctx *ctx, struct rose_constant_value *dst, const struct rose_type *dst_type,
        const struct rose_ir_constant *src1, const struct rose_ir_constant *src2)
{
    enum rose_base_type type = dst_type->base_type;

    assert(type == src1->node.data_type->base_type);
    assert(type == src2->node.data_type->base_type);

    switch (type)
    {
        case ROSE_TYPE_SINT:
            dst->u.u = (int16_t)(src1->value.u.i * src2->value.u.i);
            break;

        case ROSE_TYPE_UINT:
            dst->u.u = (uint16_t)(src1->value.u.u * src2->value.u.u);
            break;

        default:
            FIXME("Fold addition for type %#x.\n", type);
            return false;
    }
    return true;
}

static bool fold_constant_exprs(struct rose_ctx *ctx, struct rose_ir_node *instr, void *context)
{
    struct rose_ir_constant *arg1, *arg2 = NULL;
    struct rose_constant_value res = {0};
    struct rose_ir_node *res_node;
    struct rose_ir_expr *expr;
    bool success;

    if (instr->type != ROSE_IR_EXPR)
        return false;
    expr = rose_ir_expr(instr);
    if (!expr->operands[0].node)
        return false;

    for (unsigned int i = 0; i < ARRAY_SIZE(expr->operands); ++i)
    {
        if (expr->operands[i].node)
        {
            if (expr->operands[i].node->type != ROSE_IR_CONSTANT)
                return false;
        }
    }

    arg1 = rose_ir_constant(expr->operands[0].node);
    if (expr->operands[1].node)
        arg2 = rose_ir_constant(expr->operands[1].node);

    switch (expr->op)
    {
        case ROSE_OP1_BIT_NOT:
            success = fold_bit_not(ctx, &res, instr->data_type, arg1);
            break;

        case ROSE_OP1_CAST:
            success = fold_cast(ctx, &res, instr->data_type, arg1);
            break;

        case ROSE_OP1_NEG:
            success = fold_neg(ctx, &res, instr->data_type, arg1);
            break;

        case ROSE_OP2_ADD:
            success = fold_add(ctx, &res, instr->data_type, arg1, arg2);
            break;

        case ROSE_OP2_BIT_AND:
            res.u.u = arg1->value.u.u & arg2->value.u.u;
            success = true;
            break;

        case ROSE_OP2_BIT_OR:
            res.u.u = arg1->value.u.u | arg2->value.u.u;
            success = true;
            break;

        case ROSE_OP2_DIV:
            success = fold_div(ctx, &res, instr->data_type, arg1, arg2);
            break;

        case ROSE_OP2_MUL:
            success = fold_mul(ctx, &res, instr->data_type, arg1, arg2);
            break;

        case ROSE_OP2_RSHIFT:
            if (is_signed(arg1->node.data_type))
                res.u.i = arg1->value.u.i >> arg2->value.u.u;
            else
                res.u.u = arg1->value.u.u >> arg2->value.u.u;
            success = true;
            break;

        case ROSE_OP2_SUB:
            success = fold_sub(ctx, &res, instr->data_type, arg1, arg2);
            break;

        default:
            FIXME("Fold expression %#x.\n", expr->op);
            success = false;
            break;
    }

    if (success)
    {
        res_node = rose_new_constant(ctx, instr->data_type, &res, &instr->loc);
        list_add_before(&expr->node.entry, &res_node->entry);
        rose_replace_node(&expr->node, res_node);
    }
    return success;
}

static bool dce(struct rose_ctx *ctx, struct rose_ir_node *instr, void *context)
{
    switch (instr->type)
    {
        case ROSE_IR_ADDR:
        case ROSE_IR_CONSTANT:
        case ROSE_IR_EXPR:
        case ROSE_IR_INDEX:
        case ROSE_IR_LOAD:
            if (list_empty(&instr->uses))
            {
                list_remove(&instr->entry);
                rose_free_instr(instr);
                return true;
            }
            break;

        case ROSE_IR_STORE:
        {
            struct rose_ir_store *store = rose_ir_store(instr);
            struct rose_ir_var *var;

            if ((var = store->lhs.var) && var->func && var->last_read < instr->index)
            {
                list_remove(&instr->entry);
                rose_free_instr(instr);
                return true;
            }
            break;
        }

        case ROSE_IR_CALL:
        case ROSE_IR_IF:
        case ROSE_IR_JUMP:
        case ROSE_IR_LOOP:
            break;
    }

    return false;
}

/* Copy an element of a complex variable. Helper for
 * split_array_copies(), split_struct_copies() and
 * split_matrix_copies(). Inserts new instructions right before
 * "store". */
static bool split_copy(struct rose_ctx *ctx, struct rose_ir_store *store,
        const struct rose_ir_load *load, const unsigned int idx, struct rose_type *type)
{
    struct rose_ir_node *split_store, *c;
    struct rose_ir_load *split_load;

    c = rose_new_uint_constant(ctx, ROSE_TYPE_UINT, idx, &store->node.loc);
    list_add_before(&store->node.entry, &c->entry);

    split_load = rose_new_load_index(ctx, &load->src, c, &store->node.loc);
    list_add_before(&store->node.entry, &split_load->node.entry);

    split_store = rose_new_store_index(ctx, &store->lhs, c, &split_load->node, &store->node.loc);
    list_add_before(&store->node.entry, &split_store->entry);

    return true;
}

static bool split_array_copies(struct rose_ctx *ctx, struct rose_ir_node *instr, void *context)
{
    const struct rose_ir_node *rhs;
    struct rose_type *element_type;
    const struct rose_type *type;
    struct rose_ir_store *store;

    if (instr->type != ROSE_IR_STORE)
        return false;

    store = rose_ir_store(instr);
    rhs = store->rhs.node;
    type = rhs->data_type;
    if (type->class != ROSE_CLASS_ARRAY)
        return false;
    element_type = type->e.array.type;

    if (rhs->type != ROSE_IR_LOAD)
    {
        rose_fixme(ctx, &instr->loc, "Array store rhs of type %#x.", rhs->type);
        return false;
    }

    for (unsigned int i = 0; i < type->e.array.elements_count; ++i)
    {
        if (!split_copy(ctx, store, rose_ir_load(rhs), i, element_type))
            return false;
    }

    /* Remove the store instruction, so that we can split structs which contain
     * other structs. Although assignments produce a value, we don't allow
     * ROSE_IR_STORE to be used as a source. */
    list_remove(&store->node.entry);
    rose_free_instr(&store->node);
    return true;
}

static bool split_struct_copies(struct rose_ctx *ctx, struct rose_ir_node *instr, void *context)
{
    const struct rose_ir_node *rhs;
    const struct rose_type *type;
    struct rose_ir_store *store;

    if (instr->type != ROSE_IR_STORE)
        return false;

    store = rose_ir_store(instr);
    rhs = store->rhs.node;
    type = rhs->data_type;
    if (type->class != ROSE_CLASS_STRUCT)
        return false;

    if (rhs->type != ROSE_IR_LOAD)
    {
        rose_fixme(ctx, &instr->loc, "Struct store rhs of type %#x.", rhs->type);
        return false;
    }

    for (size_t i = 0; i < type->e.record.field_count; ++i)
    {
        const struct rose_struct_field *field = &type->e.record.fields[i];

        if (!split_copy(ctx, store, rose_ir_load(rhs), i, field->type))
            return false;
    }

    /* Remove the store instruction, so that we can split structs which contain
     * other structs. Although assignments produce a value, we don't allow
     * ROSE_IR_STORE to be used as a source. */
    list_remove(&store->node.entry);
    rose_free_instr(&store->node);
    return true;
}

/* Allocate a unique, ordered index to each instruction, which will be used for
 * computing liveness ranges.
 *
 * Also, reset the last_read field. (We are called from compute_liveness(). */
static unsigned int index_instructions(struct rose_block *block, unsigned int index)
{
    struct rose_ir_node *instr;

    LIST_FOR_EACH_ENTRY(instr, &block->instrs, struct rose_ir_node, entry)
    {
        instr->index = index++;
        instr->last_read = 0;

        if (instr->type == ROSE_IR_IF)
        {
            struct rose_ir_if *iff = rose_ir_if(instr);

            index = index_instructions(&iff->then_block, index);
            index = index_instructions(&iff->else_block, index);
        }
        else if (instr->type == ROSE_IR_LOOP)
        {
            struct rose_ir_loop *loop = rose_ir_loop(instr);

            index = index_instructions(&loop->body, index);
            index = index_instructions(&loop->iter, index);
            loop->next_index = index;
        }
    }

    return index;
}

/* External globals are special because they can't be encoded in a single
 * memory argument. All other derefs can be. */
static bool is_external_deref(const struct rose_deref *deref)
{
    return deref->var && !deref->var->func && (deref->var->data_type->modifiers & ROSE_MODIFIER_FAR);
}

/* Rosé is largely not an optimizing compiler, but we have one particular
 * important optimization here.
 *
 * The basic idea is that, for some sources, we "raise" certain value types,
 * folding them into the source itself. This lets us emit sequences like
 *
 * 2: int | y
 * 3:     | func(@2)
 *
 * using a single "push -offset(%bp)".
 *
 * There are two classes that we "raise" in this way: constants, and loads.
 * Whether a source can be raised for one or the other is governed by
 * constraints of the x86 instruction set, so we have to manually encode it.
 *
 * This is not expressed in the HLSL IR. I originally was planning to encode
 * this information in struct rose_src, which would probably then contain a
 * union. This would be cleaner—more explicit, and it would preserve the
 * invariant that every instruction is actually written. But it ends up being
 * simpler just to mark whether an instruction is *actually* written by just
 * not allocating it, and just reaching through and writing. It's a bit ugly,
 * but I don't hate it enough.
 *
 * Why bother with this optimization? The first reason is to alleviate register
 * pressure, which usually isn't awful but gets bad around CALL instructions.
 * We can't afford to put every arg in a register, which means that we need to
 * be able to push derefs directly (hence the "load" restriction). We still
 * might run out of registers, but we can fix that by putting some args in
 * local variables (manually or automatically).
 *
 * The second reason is that it just makes the generated code a lot more
 * readable. This is far less important, but when we're debugging a compiler,
 * it's not something that should be disregarded.
 */

static void mark_deref_read(struct rose_deref *deref, unsigned int last_read,
        unsigned int loop_first, unsigned int loop_last);

static void mark_src_read(struct rose_src *src, unsigned int last_read,
        unsigned int loop_first, unsigned int loop_last, bool raise_load, bool raise_const)
{
    if (raise_const && src->node->type == ROSE_IR_CONSTANT)
        return;

    if (raise_load && src->node->type == ROSE_IR_LOAD)
    {
        struct rose_ir_load *load = rose_ir_load(src->node);

        /* We can only do this if we can express the load using an x86 memory
         * argument. This is true for most derefs, but it's not true for
         * external globals, because we can't encode the segment that way. */

        if (!is_external_deref(&load->src))
        {
            mark_deref_read(&load->src, last_read, loop_first, loop_last);
            return;
        }
    }

    /* If we're referring to a node produced before the loop, we cannot allow
     * that node to be scratched anywhere in the loop, so we have to promote
     * last_read all the way to loop_last. */
    if (loop_first && src->node->index < loop_first)
        last_read = loop_last;

    src->node->last_read = last_read;
}

static void mark_deref_read(struct rose_deref *deref, unsigned int last_read,
        unsigned int loop_first, unsigned int loop_last)
{
    if (!deref->var)
        mark_src_read(&deref->ptr, last_read, loop_first, loop_last, false, false);

    for (size_t i = 0; i < deref->path_len; ++i)
        mark_src_read(&deref->path[i], last_read, loop_first, loop_last, false, false);

    if (deref->offset.node)
        mark_src_read(&deref->offset, last_read, loop_first, loop_last, false, true);
}

/* SSA only. Vars don't go in registers in this compiler. */
static void compute_liveness_recurse(struct rose_block *block, unsigned int loop_first, unsigned int loop_last)
{
    struct rose_ir_node *instr;
    struct rose_ir_var *var;

    LIST_FOR_EACH_ENTRY(instr, &block->instrs, struct rose_ir_node, entry)
    {
        const unsigned int first_write = loop_first ? min(instr->index, loop_first) : instr->index;
        const unsigned int last_read = instr->index;

        switch (instr->type)
        {
            case ROSE_IR_ADDR:
            {
                struct rose_ir_addr *addr = rose_ir_addr(instr);

                /* As soon as we take the address of a local, all bets are off.
                 * Currently the only thing we use variable liveness tracking
                 * for is DCE, so just mark this variable as always alive so we
                 * don't DCE it. */
                if ((var = addr->src.var) && var->func)
                {
                    if (!var->first_write)
                        var->first_write = first_write;
                    var->last_read = UINT_MAX;
                }

                mark_deref_read(&addr->src, last_read, loop_first, loop_last);
                break;
            }

            case ROSE_IR_CALL:
            {
                struct rose_ir_call *call = rose_ir_call(instr);

                mark_deref_read(&call->func, last_read, loop_first, loop_last);
                for (size_t i = 0; i < call->arg_count; ++i)
                    mark_src_read(&call->args[i], last_read, loop_first, loop_last, true, true);
                break;
            }

            case ROSE_IR_CONSTANT:
                break;

            case ROSE_IR_EXPR:
            {
                struct rose_ir_expr *expr = rose_ir_expr(instr);

                static const struct
                {
                    bool raise_load[3];
                    bool raise_const[3];
                }
                can_raise[] =
                {
                    [ROSE_OP2_EQUAL]    = {{true, false}, {false, true}},
                    [ROSE_OP2_GEQUAL]   = {{true, false}, {false, true}},
                    [ROSE_OP2_GREATER]  = {{true, false}, {false, true}},
                    [ROSE_OP2_LEQUAL]   = {{true, false}, {false, true}},
                    [ROSE_OP2_LESS]     = {{true, false}, {false, true}},
                    [ROSE_OP2_NEQUAL]   = {{true, false}, {false, true}},
                };

                for (unsigned int i = 0; i < ARRAY_SIZE(expr->operands) && expr->operands[i].node; ++i)
                {
                    if (expr->op < ARRAY_SIZE(can_raise))
                        mark_src_read(&expr->operands[i], last_read, loop_first,
                                loop_last, can_raise[expr->op].raise_load[i], can_raise[expr->op].raise_const[i]);
                    else
                        mark_src_read(&expr->operands[i], last_read, loop_first, loop_last, false, false);
                }
                break;
            }

            case ROSE_IR_IF:
            {
                struct rose_ir_if *iff = rose_ir_if(instr);

                compute_liveness_recurse(&iff->then_block, loop_first, loop_last);
                compute_liveness_recurse(&iff->else_block, loop_first, loop_last);
                mark_src_read(&iff->condition, last_read, loop_first, loop_last, false, false);
                break;
            }

            case ROSE_IR_INDEX:
                rose_unreachable();

            case ROSE_IR_JUMP:
            {
                struct rose_ir_jump *jump = rose_ir_jump(instr);

                if (jump->return_value.node)
                    mark_src_read(&jump->return_value, last_read, loop_first, loop_last, false, false);
                break;
            }

            case ROSE_IR_LOAD:
            {
                struct rose_ir_load *load = rose_ir_load(instr);

                if ((var = load->src.var) && var->func)
                    var->last_read = max(var->last_read, loop_last ? loop_last : last_read);

                mark_deref_read(&load->src, last_read, loop_first, loop_last);
                break;
            }

            case ROSE_IR_LOOP:
            {
                struct rose_ir_loop *loop = rose_ir_loop(instr);

                compute_liveness_recurse(&loop->body, loop_first ? loop_first : instr->index,
                    loop_last ? loop_last : loop->next_index);
                compute_liveness_recurse(&loop->iter, loop_first ? loop_first : instr->index,
                    loop_last ? loop_last : loop->next_index);
                break;
            }

            case ROSE_IR_STORE:
            {
                struct rose_ir_store *store = rose_ir_store(instr);

                if ((var = store->lhs.var) && var->func && !var->first_write)
                    var->first_write = first_write;

                mark_src_read(&store->rhs, last_read, loop_first, loop_last, false, true);
                mark_deref_read(&store->lhs, last_read, loop_first, loop_last);
                break;
            }
        }
    }
}

static void compute_liveness(struct rose_ctx *ctx, struct rose_function *func)
{
    struct rose_scope *scope;
    struct rose_ir_var *var;

    /* Index 0 means unused; index 1 means function entry, so start at 2. */
    index_instructions(&func->body, 2);

    LIST_FOR_EACH_ENTRY(scope, &ctx->scopes, struct rose_scope, entry)
    {
        LIST_FOR_EACH_ENTRY(var, &scope->vars, struct rose_ir_var, scope_entry)
            var->first_write = var->last_read = 0;
    }

    compute_liveness_recurse(&func->body, 0, 0);
}

struct register_allocator
{
    struct rose_function *func;

    size_t count, capacity;

    struct allocation
    {
        enum x86_reg reg;
        enum x86_seg seg;
        unsigned int first_write, last_read;
    } *allocations;
};

static bool is_available_reg(const struct register_allocator *allocator,
        enum x86_reg reg, unsigned int last_read, unsigned int first_write)
{
    for (size_t i = 0; i < allocator->count; ++i)
    {
        const struct allocation *allocation = &allocator->allocations[i];

        /* We do not overlap if first write == last read:
         * this is the case where we are allocating the result of that
         * expression, e.g. "add r0, r0, r1". */

        if (allocation->reg == reg && first_write < allocation->last_read && last_read > allocation->first_write)
            return false;
    }

    return true;
}

static bool is_available_seg(const struct register_allocator *allocator,
        enum x86_seg seg, unsigned int last_read, unsigned int first_write)
{
    for (size_t i = 0; i < allocator->count; ++i)
    {
        const struct allocation *allocation = &allocator->allocations[i];

        /* We do not overlap if first write == last read:
         * this is the case where we are allocating the result of that
         * expression, e.g. "add r0, r0, r1". */

        if (allocation->seg == seg && first_write < allocation->last_read && last_read > allocation->first_write)
            return false;
    }

    return true;
}

static void record_allocation(struct rose_ctx *ctx, struct register_allocator *allocator,
        struct rose_ir_node *instr, enum x86_reg reg, enum x86_seg seg)
{
    struct rose_function *func = allocator->func;
    struct allocation *allocation;

    rose_array_reserve(ctx, (void **)&allocator->allocations, &allocator->capacity,
            allocator->count + 1, sizeof(*allocator->allocations));
    allocation = &allocator->allocations[allocator->count++];
    allocation->reg = reg;
    if (seg == X86_SEG_SS || seg == X86_SEG_DS)
        allocation->seg = X86_SEG_NONE;
    else
        allocation->seg = seg;
    allocation->first_write = instr->index;
    allocation->last_read = instr->last_read;

    instr->reg.allocated = true;
    instr->reg.reg = reg;
    instr->reg.seg = seg;

    if (reg == X86_REG_SI)
        func->save_si = true;
    else if (reg == X86_REG_DI)
        func->save_di = true;

    if (seg == X86_SEG_FS)
        func->save_fs = true;
    else if (seg == X86_SEG_GS)
        func->save_gs = true;

    TRACE("Allocated anonymous expression @%u to %s:%s (liveness %u-%u).\n", instr->index,
            x86_seg_get_name(seg), x86_reg_get_name(reg), instr->index, instr->last_read);
}

/* Mark a register as used by an instruction, without allocating it exactly.
 * Because of the way our allocator works we can achieve this by setting
 * first_write and last_read to the instruction in question. */
static void scratch_register(struct rose_ctx *ctx, struct register_allocator *allocator,
        unsigned int index, enum x86_reg reg, enum x86_seg seg, const struct rose_location *loc)
{
    struct allocation *allocation;

    if (!is_available_reg(allocator, reg, index, index))
        rose_error(ctx, loc, "Cannot scratch %s at @%u.", x86_reg_get_name(reg), index);
    if (seg != X86_SEG_NONE && !is_available_seg(allocator, seg, index, index))
        rose_error(ctx, loc, "Cannot scratch %s at @%u.", x86_seg_get_name(seg), index);

    rose_array_reserve(ctx, (void **)&allocator->allocations, &allocator->capacity,
            allocator->count + 1, sizeof(*allocator->allocations));
    allocation = &allocator->allocations[allocator->count++];
    allocation->reg = reg;
    allocation->seg = seg;
    allocation->first_write = index;
    allocation->last_read = index;
}

static bool allocate_gpr(struct rose_ctx *ctx, struct register_allocator *allocator, const enum x86_reg *candidates,
        size_t candidate_count, unsigned int last_read, unsigned int first_write, enum x86_reg *reg)
{
    for (size_t i = 0; i < candidate_count; ++i)
    {
        if (is_available_reg(allocator, candidates[i], last_read, first_write))
        {
            *reg = candidates[i];
            return true;
        }
    }

    return false;
}

static bool allocate_pointer_seg(struct rose_ctx *ctx, struct register_allocator *allocator,
        const struct rose_ir_node *instr, enum x86_seg *seg)
{
    /* Prefer ES, which doesn't need to be saved. */
    static const enum x86_seg pointer_selectors[] =
    {
        X86_SEG_ES,
        X86_SEG_FS,
        X86_SEG_GS,
    };

    if (instr->type == ROSE_IR_ADDR)
    {
        const struct rose_ir_addr *addr = rose_ir_addr(instr);

        if (addr->src.var)
        {
            if (!addr->src.var->func)
            {
                *seg = X86_SEG_DS;
                allocator->func->use_ds = true;
            }
            else
            {
                *seg = X86_SEG_SS;
            }
            return true;
        }
        else
        {
            /* Try to preserve the same segment. */
            const struct rose_reg *reg = &addr->src.ptr.node->reg;

            if (reg->allocated && is_available_seg(allocator, reg->seg, instr->last_read, instr->index))
            {
                *seg = reg->seg;
                return true;
            }
        }
    }
    else if (instr->type == ROSE_IR_EXPR)
    {
        const struct rose_ir_expr *expr = rose_ir_expr(instr);

        if (expr->op == ROSE_OP1_CAST && expr->operands[0].node->data_type->class == ROSE_CLASS_POINTER)
        {
            if (rose_type_is_far_pointer(expr->operands[0].node->data_type))
            {
                /* Cast from far to far. Try to preserve the same segment. */
                const struct rose_reg *reg = &expr->operands[0].node->reg;

                if (reg->allocated && is_available_seg(allocator, reg->seg, instr->last_read, instr->index))
                {
                    *seg = reg->seg;
                    return true;
                }
            }
            else
            {
                /* Cast from near to far. We don't know if stack or data, but
                 * we currently only support single-data, ss == ds, so it
                 * doesn't matter. Since we're producing a pointer which will
                 * be allocated into bx (probably), use ds.
                 *
                 * FIXME: Okay, so this is a problem for DLLs. For executables
                 * (or multiple-data) we can just assume ds == ss. For DLLs
                 * that's not the case. How does native even deal with this?
                 *
                 * I wonder if, whenever ds != ss, all pointers are implicitly
                 * far. (Or just all pointers created from stack variables?) */

                rose_fixme(ctx, &instr->loc, "Cast from near to far.");
                return true;
            }
        }
        else if (expr->op == ROSE_OP2_ADD || expr->op == ROSE_OP2_SUB)
        {
            const struct rose_reg *reg = &expr->operands[0].node->reg;

            assert(expr->operands[0].node->data_type->class == ROSE_CLASS_POINTER);

            /* Try to preserve the same segment. */
            if (reg->allocated && is_available_seg(allocator, reg->seg, instr->last_read, instr->index))
            {
                *seg = reg->seg;
                return true;
            }
        }
    }

    for (size_t i = 0; i < ARRAY_SIZE(pointer_selectors); ++i)
    {
        if (is_available_seg(allocator, pointer_selectors[i], instr->last_read, instr->index))
        {
            *seg = pointer_selectors[i];
            return true;
        }
    }

    return false;
}

static void allocate_instr(struct rose_ctx *ctx,
        struct register_allocator *allocator, struct rose_ir_node *instr)
{
    const struct rose_type *type = instr->data_type;
    enum x86_seg seg = X86_SEG_NONE;
    enum x86_reg reg;

    /* Prefer: AX,CX,DX,BX > SI,DI.
     * Avoid SI, DI which need to be saved. */
    static const enum x86_reg integer_registers[] =
    {
        X86_REG_AX,
        X86_REG_CX,
        X86_REG_DX,
        X86_REG_BX,
        X86_REG_SI,
        X86_REG_DI,
    };

    switch (type->class)
    {
        case ROSE_CLASS_ARRAY:
        case ROSE_CLASS_ERROR:
        case ROSE_CLASS_FUNCTION:
        case ROSE_CLASS_STRUCT:
        case ROSE_CLASS_VOID:
            rose_unreachable();

        case ROSE_CLASS_POINTER:
            if (!allocate_gpr(ctx, allocator, integer_registers, ARRAY_SIZE(integer_registers),
                    instr->last_read, instr->index, &reg))
            {
                rose_error(ctx, &instr->loc, "No pointer registers available.");
                return;
            }

            if (rose_type_is_far_pointer(type) && !allocate_pointer_seg(ctx, allocator, instr, &seg))
            {
                rose_error(ctx, &instr->loc, "No pointer selectors available.");
                return;
            }

            record_allocation(ctx, allocator, instr, reg, seg);
            break;

        case ROSE_CLASS_NUMERIC:
        {
            size_t candidate_count = 6;

            switch (type->base_type)
            {
                case ROSE_TYPE_BOOL:
                case ROSE_TYPE_SCHAR:
                case ROSE_TYPE_UCHAR:
                    /* SI, DI can't be used for 8-bit values. */
                    candidate_count = 4;
                    break;

                case ROSE_TYPE_USHORT:
                case ROSE_TYPE_UINT:
                case ROSE_TYPE_ULONG:
                case ROSE_TYPE_SSHORT:
                case ROSE_TYPE_SINT:
                case ROSE_TYPE_SLONG:
                    break;
            }

            if (!allocate_gpr(ctx, allocator, integer_registers, candidate_count,
                    instr->last_read, instr->index, &reg))
            {
                rose_error(ctx, &instr->loc, "No integer registers available for @%u.", instr->index);
                return;
            }

            record_allocation(ctx, allocator, instr, reg, X86_SEG_NONE);

            break;
        }
    }

    assert(instr->reg.allocated);
}

static void allocate_early_deref(struct rose_ctx *ctx,
        struct register_allocator *allocator, const struct rose_deref *deref)
{
    /* Pointers have to go in %bx; offsets have to go in %si or %di.
     *
     * Less restrictive constraints exist but this is simplest for now. */

    static const enum x86_reg offset_registers[] =
    {
        X86_REG_SI,
        X86_REG_DI,
    };

    struct rose_ir_node *offset = deref->offset.node;
    enum x86_reg reg;

    if (!deref->var)
    {
        struct rose_ir_node *ptr = deref->ptr.node;
        enum x86_seg seg = X86_SEG_NONE;

        if (!ptr->reg.allocated)
        {
            if (!is_available_reg(allocator, X86_REG_BX, ptr->last_read, ptr->index))
            {
                rose_error(ctx, &ptr->loc, "Cannot allocate pointer @%u to %%bx.", ptr->index);
                return;
            }

            assert(ptr->data_type->class == ROSE_CLASS_POINTER);
            if (rose_type_is_far_pointer(ptr->data_type) && !allocate_pointer_seg(ctx, allocator, ptr, &seg))
            {
                rose_error(ctx, &ptr->loc, "No pointer selectors available for @%u.", ptr->index);
                return;
            }
            record_allocation(ctx, allocator, ptr, X86_REG_BX, seg);
        }
    }

    if (offset && offset->last_read && !offset->reg.allocated)
    {
        assert(offset->data_type->class == ROSE_CLASS_NUMERIC);
        if (!allocate_gpr(ctx, allocator, offset_registers, ARRAY_SIZE(offset_registers),
                offset->last_read, offset->index, &reg))
        {
            rose_error(ctx, &offset->loc, "No offset registers available for @%u.", offset->index);
            return;
        }
        record_allocation(ctx, allocator, offset, reg, X86_SEG_NONE);
    }
}

/* First pass to deal with instructions that have to be allocated a certain way. */
static void allocate_early_instrs_recurse(struct rose_ctx *ctx,
        struct rose_block *block, struct register_allocator *allocator)
{
    struct rose_ir_node *instr;

    LIST_FOR_EACH_ENTRY(instr, &block->instrs, struct rose_ir_node, entry)
    {
        if (instr->type == ROSE_IR_IF)
        {
            struct rose_ir_if *iff = rose_ir_if(instr);

            allocate_early_instrs_recurse(ctx, &iff->then_block, allocator);
            allocate_early_instrs_recurse(ctx, &iff->else_block, allocator);
        }
        else if (instr->type == ROSE_IR_LOOP)
        {
            struct rose_ir_loop *loop = rose_ir_loop(instr);

            allocate_early_instrs_recurse(ctx, &loop->body, allocator);
            allocate_early_instrs_recurse(ctx, &loop->iter, allocator);
        }
        else if (instr->type == ROSE_IR_LOAD)
        {
            struct rose_ir_load *load = rose_ir_load(instr);

            allocate_early_deref(ctx, allocator, &load->src);
        }
        else if (instr->type == ROSE_IR_STORE)
        {
            struct rose_ir_store *store = rose_ir_store(instr);

            allocate_early_deref(ctx, allocator, &store->lhs);
        }
        else if (instr->type == ROSE_IR_ADDR)
        {
            struct rose_ir_addr *addr = rose_ir_addr(instr);

            allocate_early_deref(ctx, allocator, &addr->src);
        }
        else if (instr->type == ROSE_IR_CALL)
        {
            struct rose_ir_call *call = rose_ir_call(instr);

            allocate_early_deref(ctx, allocator, &call->func);
            scratch_register(ctx, allocator, instr->index, X86_REG_BX, X86_SEG_ES, &instr->loc);
            scratch_register(ctx, allocator, instr->index, X86_REG_AX, X86_SEG_NONE, &instr->loc);
            scratch_register(ctx, allocator, instr->index, X86_REG_CX, X86_SEG_NONE, &instr->loc);
            scratch_register(ctx, allocator, instr->index, X86_REG_DX, X86_SEG_NONE, &instr->loc);
        }
        else if (instr->type == ROSE_IR_EXPR)
        {
            struct rose_ir_expr *expr = rose_ir_expr(instr);

            if (expr->op == ROSE_OP2_DIV || expr->op == ROSE_OP2_MOD)
            {
                struct rose_ir_node *divisor = expr->operands[1].node;
                enum x86_reg reg;

                static const enum x86_reg divisor_registers[] =
                {
                    X86_REG_CX,
                    X86_REG_BX,
                    X86_REG_SI,
                    X86_REG_DI,
                };

                if (!is_available_reg(allocator, X86_REG_DX, instr->last_read, instr->index)
                        || !is_available_reg(allocator, X86_REG_AX, instr->last_read, instr->index))
                {
                    rose_error(ctx, &instr->loc, "Cannot allocate division at @%u to %%dx:%%ax.", instr->index);
                    return;
                }
                record_allocation(ctx, allocator, instr,
                        expr->op == ROSE_OP2_DIV ? X86_REG_AX : X86_REG_DX, X86_SEG_NONE);
                scratch_register(ctx, allocator, instr->index,
                        expr->op == ROSE_OP2_DIV ? X86_REG_DX : X86_REG_AX, X86_SEG_NONE, &instr->loc);

                /* The divisor can't be in %ax or %dx. */

                if (!allocate_gpr(ctx, allocator, divisor_registers, ARRAY_SIZE(divisor_registers),
                        divisor->last_read, divisor->index, &reg))
                {
                    rose_error(ctx, &divisor->loc, "No divisor registers available for @%u.", divisor->index);
                    return;
                }
                record_allocation(ctx, allocator, divisor, reg, X86_SEG_NONE);
            }
            else if (expr->op == ROSE_OP2_LSHIFT || expr->op == ROSE_OP2_RSHIFT)
            {
                struct rose_ir_node *shift = expr->operands[1].node;
                enum x86_reg reg;

                static const enum x86_reg result_registers[] =
                {
                    X86_REG_AX,
                    X86_REG_DX,
                    X86_REG_BX,
                    X86_REG_SI,
                    X86_REG_DI,
                };

                if (!is_available_reg(allocator, X86_REG_CX, shift->last_read, shift->index))
                {
                    rose_error(ctx, &instr->loc, "Cannot allocate shift at @%u to %%cx.", instr->index);
                    return;
                }
                record_allocation(ctx, allocator, shift, X86_REG_CX, X86_SEG_NONE);

                /* The result can't be in %cx. */
                if (!allocate_gpr(ctx, allocator, result_registers, ARRAY_SIZE(result_registers),
                        instr->last_read, instr->index, &reg))
                {
                    rose_error(ctx, &instr->loc, "No shift result registers available for @%u.", instr->index);
                    return;
                }
                record_allocation(ctx, allocator, instr, reg, X86_SEG_NONE);
            }
        }
    }
}

static void allocate_instrs_recurse(struct rose_ctx *ctx,
        struct rose_block *block, struct register_allocator *allocator)
{
    struct rose_ir_node *instr;

    LIST_FOR_EACH_ENTRY(instr, &block->instrs, struct rose_ir_node, entry)
    {
        if (!instr->reg.allocated && instr->last_read)
            allocate_instr(ctx, allocator, instr);

        if (instr->type == ROSE_IR_IF)
        {
            struct rose_ir_if *iff = rose_ir_if(instr);

            allocate_instrs_recurse(ctx, &iff->then_block, allocator);
            allocate_instrs_recurse(ctx, &iff->else_block, allocator);
        }
        else if (instr->type == ROSE_IR_LOOP)
        {
            struct rose_ir_loop *loop = rose_ir_loop(instr);

            allocate_instrs_recurse(ctx, &loop->body, allocator);
            allocate_instrs_recurse(ctx, &loop->iter, allocator);
        }
    }
}

static void allocate_instrs(struct rose_ctx *ctx, struct rose_function *func)
{
    struct register_allocator allocator = {.func = func};

    allocate_early_instrs_recurse(ctx, &func->body, &allocator);
    allocate_instrs_recurse(ctx, &func->body, &allocator);
    rose_free(allocator.allocations);
}

static enum x86_reg8 reg8_from_reg16(enum x86_reg reg)
{
    switch (reg)
    {
        case X86_REG_AX: return X86_REG8_AL;
        case X86_REG_BX: return X86_REG8_BL;
        case X86_REG_CX: return X86_REG8_CL;
        case X86_REG_DX: return X86_REG8_DL;
        default: rose_unreachable();
    }
}

static void build_mem_from_deref(struct x86_arg *arg, struct x86_block *out, const struct rose_deref *deref)
{
    const struct rose_ir_node *offset = deref->offset.node;
    const struct rose_ir_var *var;

    memset(arg, 0, sizeof(*arg));
    arg->type = X86_ARG_MEM;

    if (offset && offset->type == ROSE_IR_CONSTANT)
    {
        arg->u.mem.offset.offset = rose_ir_constant(offset)->value.u.i;
        offset = NULL;
    }

    if ((var = deref->var))
    {
        if (!var->func)
        {
            if (var->data_type->modifiers & ROSE_MODIFIER_FAR)
            {
                /* Unlike all the other cases, this cannot be expressed as
                 * a single x86_arg. Caller needs to handle this case
                 * specially. */
                rose_unreachable();
            }
            else
            {
                uint32_t reloc_index;

                reloc_index = x86_add_reloc(out, X86_RELOC_UNRESOLVED_OFFSET, var->name);

                /* Near variables don't need a segment override. */
                arg->u.mem.seg = X86_SEG_NONE;
                /* Offset is still forced into si or di. It would actually
                 * be fine for it to be bx here, but that requires more
                 * logic. */
                if (!offset)
                    arg->u.mem.reg = X86_MEM_REG_NONE;
                else if (offset->reg.reg == X86_REG_SI)
                    arg->u.mem.reg = X86_MEM_REG_SI;
                else if (offset->reg.reg == X86_REG_DI)
                    arg->u.mem.reg = X86_MEM_REG_DI;
                else
                    rose_unreachable();
                arg->u.mem.offset.reloc = reloc_index;
            }
        }
        else
        {
            arg->u.mem.seg = X86_SEG_NONE;
            /* Offset must be si or di. */
            if (!offset)
                arg->u.mem.reg = X86_MEM_REG_BP;
            else if (offset->reg.reg == X86_REG_SI)
                arg->u.mem.reg = X86_MEM_REG_BP_SI;
            else if (offset->reg.reg == X86_REG_SI)
                arg->u.mem.reg = X86_MEM_REG_BP_DI;
            else
                rose_unreachable();
            assert(var->frame_offset);
            arg->u.mem.offset.offset += var->frame_offset;
        }
    }
    else
    {
        const struct rose_ir_node *ptr = deref->ptr.node;

        /* We force %bx here... */
        assert(ptr->reg.allocated);
        assert(ptr->reg.reg == X86_REG_BX);

        arg->u.mem.seg = ptr->reg.seg;

        /* ...and the offset must be si or di. */
        if (!offset)
            arg->u.mem.reg = X86_MEM_REG_BX;
        else if (offset->reg.reg == X86_REG_SI)
            arg->u.mem.reg = X86_MEM_REG_BX_SI;
        else if (offset->reg.reg == X86_REG_DI)
            arg->u.mem.reg = X86_MEM_REG_BX_DI;
        else
            rose_unreachable();
    }
}

static bool build_arg_from_raised_load(struct x86_arg *arg, struct x86_block *out,
        const struct rose_ir_node *instr)
{
    if (instr->type == ROSE_IR_LOAD)
    {
        struct rose_ir_load *load = rose_ir_load(instr);

        if (is_external_deref(&load->src))
            return false;

        build_mem_from_deref(arg, out, &load->src);
        return true;
    }
    return false;
}

static void build_arg_from_node(struct x86_arg *arg, struct x86_block *out,
        const struct rose_ir_node *instr, bool raise_load, bool raise_const)
{
    if (raise_const && instr->type == ROSE_IR_CONSTANT)
    {
        arg->type = X86_ARG_IMM;
        arg->u.imm.offset = rose_ir_constant(instr)->value.u.i;
        return;
    }

    if (raise_load && build_arg_from_raised_load(arg, out, instr))
        return;

    assert(instr->reg.allocated);

    if (rose_type_get_size(instr->data_type) == 1)
    {
        arg->type = X86_ARG_REG8;
        arg->u.reg8 = reg8_from_reg16(instr->reg.reg);
    }
    else
    {
        /* This function isn't going to deal with far pointers. */
        assert(!rose_type_is_far_pointer(instr->data_type));
        arg->type = X86_ARG_REG16;
        arg->u.reg = instr->reg.reg;
    }
}

static void emit_block(struct rose_ctx *ctx, struct x86_block *out, const struct rose_block *block, bool top);

static void emit_addr(struct rose_ctx *ctx, struct x86_block *out, struct rose_ir_addr *addr)
{
    const struct rose_type *ptr_type = addr->node.data_type;
    struct x86_instr xi = {0};
    struct rose_ir_var *var;

    assert(addr->node.reg.allocated);

    if ((var = addr->src.var))
    {
        if (!var->func)
        {
            assert(ptr_type->class == ROSE_CLASS_POINTER);
            if (var->data_type->modifiers & ROSE_MODIFIER_FAR)
            {
                rose_fixme(ctx, &addr->node.loc, "Address of far variable.");
                return;
            }

            /* Pointer should have been allocated to %ds so there's
             * nothing to do here. */
            if (rose_type_is_far_pointer(ptr_type))
                assert(addr->node.reg.seg == X86_SEG_DS);
        }
        else
        {
            /* Pointer should have been allocated to %ss so there's
             * nothing to do here. */
            if (rose_type_is_far_pointer(ptr_type))
                assert(addr->node.reg.seg == X86_SEG_SS);
        }
    }
    else
    {
        struct rose_ir_node *ptr = addr->src.ptr.node;

        assert(ptr->reg.allocated);
        if (rose_type_is_far_pointer(ptr->data_type) && ptr->reg.seg != addr->node.reg.seg)
        {
            x86_emit_s(out, X86_OP_PUSH, ptr->reg.seg);
            x86_emit_s(out, X86_OP_POP, addr->node.reg.seg);
        }
    }

    /* leaw mem,%REG */
    xi.opcode = X86_OP_LEA;
    build_mem_from_deref(&xi.args[0], out, &addr->src);
    xi.args[1].type = X86_ARG_REG16;
    xi.args[1].u.reg = addr->node.reg.reg;
    xi.size = 16;
    x86_add_instr(out, &xi);
    return;
}

static void emit_call(struct rose_ctx *ctx, struct x86_block *out, struct rose_ir_call *call)
{
    const struct rose_type *return_type = call->node.data_type;
    const struct rose_type *func_type = call->func_type;
    const struct rose_ir_var *var;
    unsigned int param_size = 0;
    struct x86_instr xi = {0};
    bool is_indirect = false;

    if (func_type->class == ROSE_CLASS_POINTER)
    {
        func_type = func_type->e.ptr.type;
        is_indirect = true;
    }

    assert(func_type->class == ROSE_CLASS_FUNCTION);

    for (size_t i = 0; i < call->arg_count; ++i)
    {
        const struct rose_type *arg_type;
        const struct rose_ir_node *arg;

        if (func_type->modifiers & ROSE_MODIFIER_PASCAL)
            arg = call->args[i].node;
        else
            arg = call->args[call->arg_count - 1 - i].node;
        arg_type = arg->data_type;
        param_size += get_type_param_size(arg_type);

        switch (arg_type->class)
        {
            case ROSE_CLASS_ARRAY:
            case ROSE_CLASS_STRUCT:
                rose_fixme(ctx, &arg->loc, "Array or struct parameter.");
                break;

            case ROSE_CLASS_ERROR:
            case ROSE_CLASS_FUNCTION:
            case ROSE_CLASS_VOID:
                rose_unreachable();
                break;

            case ROSE_CLASS_NUMERIC:
                switch (arg_type->base_type)
                {
                    case ROSE_TYPE_BOOL:
                    case ROSE_TYPE_SCHAR:
                    case ROSE_TYPE_UCHAR:
                        /* Agner Fog doesn't *say* that the register gets
                         * extended, so I'm just going to assume that the high
                         * bits can be junk. */
                    case ROSE_TYPE_SSHORT:
                    case ROSE_TYPE_SINT:
                    case ROSE_TYPE_USHORT:
                    case ROSE_TYPE_UINT:
                        memset(&xi, 0, sizeof(xi));
                        xi.opcode = X86_OP_PUSH;
                        xi.size = 16;
                        if (arg->type == ROSE_IR_CONSTANT)
                        {
                            x86_emit_i(out, X86_OP_PUSH, rose_ir_constant(arg)->value.u.i, 16);
                        }
                        else if (build_arg_from_raised_load(&xi.args[0], out, arg))
                        {
                            x86_add_instr(out, &xi);
                        }
                        else
                        {
                            assert(arg->reg.allocated);
                            x86_emit_r(out, X86_OP_PUSH, arg->reg.reg, 16);
                        }
                        break;

                    case ROSE_TYPE_SLONG:
                    case ROSE_TYPE_ULONG:
                        memset(&xi, 0, sizeof(xi));
                        xi.opcode = X86_OP_PUSH;
                        xi.size = 32;
                        if (arg->type == ROSE_IR_CONSTANT)
                        {
                            x86_emit_i(out, X86_OP_PUSH, rose_ir_constant(arg)->value.u.i, 32);
                        }
                        else if (build_arg_from_raised_load(&xi.args[0], out, arg))
                        {
                            x86_add_instr(out, &xi);
                        }
                        else
                        {
                            assert(arg->reg.allocated);
                            x86_emit_r(out, X86_OP_PUSH, arg->reg.reg, 32);
                        }
                        break;
                }
                break;

            case ROSE_CLASS_POINTER:
                memset(&xi, 0, sizeof(xi));
                xi.opcode = X86_OP_PUSH;
                if (arg->type == ROSE_IR_CONSTANT)
                {
                    x86_emit_i(out, X86_OP_PUSH, rose_ir_constant(arg)->value.u.u,
                            rose_type_is_far_pointer(arg_type) ? 32 : 16);
                }
                else if (build_arg_from_raised_load(&xi.args[0], out, arg))
                {
                    xi.size = rose_type_is_far_pointer(arg_type) ? 32 : 16;
                    x86_add_instr(out, &xi);
                }
                else
                {
                    assert(arg->reg.allocated);
                    if (rose_type_is_far_pointer(arg_type))
                        x86_emit_s(out, X86_OP_PUSH, arg->reg.seg);
                    x86_emit_r(out, X86_OP_PUSH, arg->reg.reg, 16);
                }
                break;
        }
    }

    memset(&xi, 0, sizeof(xi));
    if (func_type->modifiers & ROSE_MODIFIER_FAR)
        xi.opcode = X86_OP_LCALL;
    else
        xi.opcode = X86_OP_CALL;
    xi.size = 16;

    if (is_indirect)
    {
        build_mem_from_deref(&xi.args[0], out, &call->func);
        assert(xi.args[0].type == X86_ARG_MEM);
        xi.args[0].type = X86_ARG_ABSMEM;
    }
    else
    {
        uint32_t reloc_index;

        var = call->func.var;
        assert(var);
        assert(!call->func.path_len);
        assert(!call->func.offset.node);

        if (func_type->modifiers & ROSE_MODIFIER_FAR)
            reloc_index = x86_add_reloc(out, X86_RELOC_UNRESOLVED_PAIR, var->name);
        else
            reloc_index = x86_add_reloc(out, X86_RELOC_UNRESOLVED_LOCAL, var->name);

        /* LCALL here expects SEGPTR. CALL expects REL.
         * Both cases are encoded by X86_ARG_MEM. */
        xi.args[0].type = X86_ARG_MEM;
        xi.args[0].u.mem.reg = X86_MEM_REG_NONE;
        xi.args[0].u.mem.seg = X86_SEG_NONE;
        xi.args[0].u.mem.offset.reloc = reloc_index;
    }
    x86_add_instr(out, &xi);

    if (!(func_type->modifiers & ROSE_MODIFIER_PASCAL))
        x86_emit_ir(out, X86_OP_ADD, param_size, X86_REG_SP, 16);

    if (call->node.reg.allocated)
    {
        switch (return_type->class)
        {
            case ROSE_CLASS_STRUCT:
                rose_fixme(ctx, &call->node.loc, "Struct return.");
                break;

            case ROSE_CLASS_ARRAY:
            case ROSE_CLASS_ERROR:
            case ROSE_CLASS_FUNCTION:
            case ROSE_CLASS_VOID:
                rose_unreachable();
                break;

            case ROSE_CLASS_NUMERIC:
                switch (return_type->base_type)
                {
                    case ROSE_TYPE_BOOL:
                    case ROSE_TYPE_SCHAR:
                    case ROSE_TYPE_UCHAR:
                        /* Agner Fog doesn't *say* that the register gets
                         * extended, so I'm just going to assume that the high
                         * bits can be junk. */
                    case ROSE_TYPE_SSHORT:
                    case ROSE_TYPE_SINT:
                    case ROSE_TYPE_USHORT:
                    case ROSE_TYPE_UINT:
                        if (call->node.reg.reg != X86_REG_AX)
                            x86_emit_rr(out, X86_OP_MOV, X86_REG_AX, call->node.reg.reg, 16);
                        break;

                    case ROSE_TYPE_SLONG:
                    case ROSE_TYPE_ULONG:
                        memset(&xi, 0, sizeof(xi));
                        xi.opcode = X86_OP_SHLD;
                        xi.size = 32;
                        xi.args[0].type = X86_ARG_IMM;
                        xi.args[0].u.imm.offset = 16;
                        xi.args[1].type = X86_ARG_REG16;
                        xi.args[1].u.reg = X86_REG_DX;
                        xi.args[2].type = X86_ARG_REG16;
                        xi.args[2].u.reg = X86_REG_AX;
                        x86_add_instr(out, &xi);

                        if (call->node.reg.reg != X86_REG_AX)
                            x86_emit_rr(out, X86_OP_MOV, X86_REG_AX, call->node.reg.reg, 32);
                        break;
                }
                break;

            case ROSE_CLASS_POINTER:
                if (rose_type_is_far_pointer(return_type))
                    x86_emit_mov_rs(out, X86_REG_DX, call->node.reg.seg);
                if (call->node.reg.reg != X86_REG_AX)
                    x86_emit_rr(out, X86_OP_MOV, X86_REG_AX, call->node.reg.reg, 16);
                break;
        }
    }
}

static void emit_constant(struct rose_ctx *ctx, struct x86_block *out, struct rose_ir_constant *c)
{
    /* Constant might not be allocated if it was raised. */
    if (!c->node.reg.allocated)
        return;

    if (c->node.data_type->class == ROSE_CLASS_POINTER)
    {
        if (rose_type_is_far_pointer(c->node.data_type))
        {
            x86_emit_ir(out, X86_OP_MOV, c->value.u.u >> 16, c->node.reg.reg, 16);
            x86_emit_mov_rs(out, c->node.reg.reg, c->node.reg.seg);
        }
        x86_emit_ir(out, X86_OP_MOV, c->value.u.u & 0xffff, c->node.reg.reg, 16);
        return;
    }

    assert(c->node.data_type->class == ROSE_CLASS_NUMERIC);

    switch (c->node.data_type->base_type)
    {
        case ROSE_TYPE_BOOL:
        case ROSE_TYPE_SCHAR:
        case ROSE_TYPE_UCHAR:
            x86_emit_ir8(out, X86_OP_MOV, c->value.u.i, reg8_from_reg16(c->node.reg.reg));
            break;

        case ROSE_TYPE_SINT:
        case ROSE_TYPE_SSHORT:
        case ROSE_TYPE_UINT:
        case ROSE_TYPE_USHORT:
            x86_emit_ir(out, X86_OP_MOV, c->value.u.i, c->node.reg.reg, 16);
            break;

        case ROSE_TYPE_SLONG:
        case ROSE_TYPE_ULONG:
            x86_emit_ir(out, X86_OP_MOV, c->value.u.i, c->node.reg.reg, 32);
            break;
    }
}

static void emit_cast(struct rose_ctx *ctx, struct x86_block *out, struct rose_ir_expr *expr)
{
    const struct rose_ir_node *src = expr->operands[0].node;
    const struct rose_type *dst_type = expr->node.data_type;
    const struct rose_type *src_type = src->data_type;
    struct x86_instr xi = {0};

    assert(src_type->class == ROSE_CLASS_NUMERIC || src_type->class == ROSE_CLASS_POINTER);
    assert(dst_type->class == ROSE_CLASS_NUMERIC || dst_type->class == ROSE_CLASS_POINTER);

    assert(src->reg.allocated);

    if (dst_type->class == ROSE_CLASS_NUMERIC)
    {
        if (rose_type_is_far_pointer(src_type))
        {
            if (dst_type->base_type == ROSE_TYPE_BOOL)
            {
                rose_fixme(ctx, &expr->node.loc, "Cast from far pointer to bool.");
                return;
            }

            assert(rose_type_get_size(dst_type) == 4);

            x86_emit_s(out, X86_OP_PUSH, src->reg.seg);
            x86_emit_r(out, X86_OP_PUSH, src->reg.reg, 16);
            x86_emit_r(out, X86_OP_POP, expr->node.reg.reg, 32);
        }
        else
        {
            unsigned int src_size = rose_type_get_size(src_type);
            unsigned int dst_size = rose_type_get_size(dst_type);

            if (dst_type->base_type == ROSE_TYPE_BOOL)
            {
                if (src_size == 1)
                    x86_emit_rr8(out, X86_OP_TEST, reg8_from_reg16(src->reg.reg), reg8_from_reg16(src->reg.reg));
                else
                    x86_emit_rr(out, X86_OP_TEST, src->reg.reg, src->reg.reg, src_size * 8);
                x86_emit_r8(out, X86_OP_SETNZ, reg8_from_reg16(expr->node.reg.reg));
            }
            else if (dst_size <= src_size)
            {
                if (src->reg.reg == expr->node.reg.reg)
                    return;

                if (dst_size == 1)
                    x86_emit_rr8(out, X86_OP_MOV, reg8_from_reg16(src->reg.reg), reg8_from_reg16(expr->node.reg.reg));
                else
                    x86_emit_rr(out, X86_OP_MOV, src->reg.reg, expr->node.reg.reg, dst_size * 8);
            }
            else
            {
                switch (src_type->base_type)
                {
                    case ROSE_TYPE_BOOL:
                    case ROSE_TYPE_UCHAR:
                        xi.opcode = X86_OP_MOVZB;
                        xi.args[0].type = X86_ARG_REG8;
                        xi.args[0].u.reg8 = reg8_from_reg16(src->reg.reg);
                        xi.args[1].type = X86_ARG_REG16;
                        xi.args[1].u.reg = expr->node.reg.reg;
                        xi.size = dst_size * 8;
                        x86_add_instr(out, &xi);
                        break;

                    case ROSE_TYPE_SCHAR:
                        xi.opcode = X86_OP_MOVSB;
                        xi.args[0].type = X86_ARG_REG8;
                        xi.args[0].u.reg8 = reg8_from_reg16(src->reg.reg);
                        xi.args[1].type = X86_ARG_REG16;
                        xi.args[1].u.reg = expr->node.reg.reg;
                        xi.size = dst_size * 8;
                        x86_add_instr(out, &xi);
                        break;

                    case ROSE_TYPE_USHORT:
                    case ROSE_TYPE_UINT:
                        x86_emit_rr(out, X86_OP_MOVZW, src->reg.reg, expr->node.reg.reg, dst_size * 8);
                        break;

                    case ROSE_TYPE_SSHORT:
                    case ROSE_TYPE_SINT:
                        x86_emit_rr(out, X86_OP_MOVSW, src->reg.reg, expr->node.reg.reg, dst_size * 8);
                        break;

                    case ROSE_TYPE_SLONG:
                    case ROSE_TYPE_ULONG:
                        rose_unreachable();
                }
            }
        }
    }
    else if (rose_type_is_far_pointer(dst_type))
    {
        if (src_type->class == ROSE_CLASS_NUMERIC)
        {
            rose_fixme(ctx, &expr->node.loc, "Cast from numeric type to far pointer.");
        }
        else
        {
            if (rose_type_is_far_pointer(src_type))
            {
                if (src->reg.seg != expr->node.reg.seg)
                {
                    x86_emit_s(out, X86_OP_PUSH, src->reg.seg);
                    x86_emit_s(out, X86_OP_POP, expr->node.reg.seg);
                }
            }
            else
            {
                /* Pointer should have been allocated to %ds so there's nothing to
                 * do here. */
                assert(expr->node.reg.seg == X86_SEG_DS);
            }

            if (expr->node.reg.reg != src->reg.reg)
                x86_emit_rr(out, X86_OP_MOV, src->reg.reg, expr->node.reg.reg, 16);
        }
    }
    else
    {
        assert(src_type->class == ROSE_CLASS_NUMERIC || src_type->class == ROSE_CLASS_POINTER);
        assert(rose_type_get_size(src_type) == 2);

        if (expr->node.reg.reg != src->reg.reg)
            x86_emit_rr(out, X86_OP_MOV, src->reg.reg, expr->node.reg.reg, 16);
    }
}

static void emit_logic_not(struct rose_ctx *ctx, struct x86_block *out, struct rose_ir_expr *expr)
{
    const struct rose_ir_node *src = expr->operands[0].node;
    const struct rose_type *type = src->data_type;
    unsigned int type_size = rose_type_get_size(type);

    if (rose_type_is_far_pointer(type))
    {
        rose_fixme(ctx, &expr->node.loc, "Logical not of far pointer.");
        return;
    }

    assert(src->reg.allocated);
    if (type_size == 1)
        x86_emit_rr8(out, X86_OP_TEST, reg8_from_reg16(src->reg.reg), reg8_from_reg16(src->reg.reg));
    else
        x86_emit_rr(out, X86_OP_TEST, src->reg.reg, src->reg.reg, type_size * 8);
    x86_emit_r8(out, X86_OP_SETZ, reg8_from_reg16(expr->node.reg.reg));
}

static void emit_neg(struct rose_ctx *ctx, struct x86_block *out, struct rose_ir_expr *expr)
{
    const struct rose_ir_node *src = expr->operands[0].node;
    unsigned int type_size = rose_type_get_size(src->data_type);

    assert(src->reg.allocated);

    if (src->reg.reg != expr->node.reg.reg)
    {
        if (type_size == 1)
            x86_emit_rr8(out, X86_OP_MOV, reg8_from_reg16(src->reg.reg), reg8_from_reg16(expr->node.reg.reg));
        else
            x86_emit_rr(out, X86_OP_MOV, src->reg.reg, expr->node.reg.reg, type_size * 8);
    }

    if (type_size == 1)
        x86_emit_r8(out, X86_OP_NEG, reg8_from_reg16(expr->node.reg.reg));
    else
        x86_emit_r(out, X86_OP_NEG, expr->node.reg.reg, type_size * 8);
}

static void emit_selectorof(struct rose_ctx *ctx, struct x86_block *out, struct rose_ir_expr *expr)
{
    const struct rose_ir_node *src = expr->operands[0].node;

    assert(src->reg.allocated);
    x86_emit_mov_sr(out, src->reg.seg, expr->node.reg.reg);
}

static void emit_offsetof(struct rose_ctx *ctx, struct x86_block *out, struct rose_ir_expr *expr)
{
    const struct rose_ir_node *src = expr->operands[0].node;

    assert(src->reg.allocated);
    if (src->reg.reg != expr->node.reg.reg)
        x86_emit_rr(out, X86_OP_MOV, src->reg.reg, expr->node.reg.reg, 16);
}

static void emit_add(struct rose_ctx *ctx, struct x86_block *out, struct rose_ir_expr *expr)
{
    const struct rose_ir_node *src1 = expr->operands[0].node;
    const struct rose_ir_node *src2 = expr->operands[1].node;
    const struct rose_type *type = src2->data_type;
    unsigned int type_size = rose_type_get_size(type);

    if (src1->data_type->class == ROSE_CLASS_POINTER)
        assert(type_size == 2);
    else
        assert(rose_types_are_equal(src1->data_type, src2->data_type, false));
    assert(type->class == ROSE_CLASS_NUMERIC);

    assert(expr->node.reg.allocated);
    assert(src1->reg.allocated);
    assert(src2->reg.allocated);

    if (rose_type_is_far_pointer(src1->data_type) && src1->reg.seg != expr->node.reg.seg)
    {
        x86_emit_s(out, X86_OP_PUSH, src1->reg.seg);
        x86_emit_s(out, X86_OP_POP, expr->node.reg.seg);
    }

    if (expr->node.reg.reg == src2->reg.reg)
    {
        if (type_size == 1)
            x86_emit_rr8(out, X86_OP_ADD, reg8_from_reg16(src1->reg.reg), reg8_from_reg16(expr->node.reg.reg));
        else
            x86_emit_rr(out, X86_OP_ADD, src1->reg.reg, expr->node.reg.reg, type_size * 8);
    }
    else
    {
        if (type_size == 1)
        {
            if (expr->node.reg.reg != src1->reg.reg)
                x86_emit_rr8(out, X86_OP_MOV, reg8_from_reg16(src1->reg.reg), reg8_from_reg16(expr->node.reg.reg));
            x86_emit_rr8(out, X86_OP_ADD, reg8_from_reg16(src2->reg.reg), reg8_from_reg16(expr->node.reg.reg));
        }
        else
        {
            if (expr->node.reg.reg != src1->reg.reg)
                x86_emit_rr(out, X86_OP_MOV, src1->reg.reg, expr->node.reg.reg, type_size * 8);
            x86_emit_rr(out, X86_OP_ADD, src2->reg.reg, expr->node.reg.reg, type_size * 8);
        }
    }
}

static void emit_sub(struct rose_ctx *ctx, struct x86_block *out, struct rose_ir_expr *expr)
{
    const struct rose_ir_node *src1 = expr->operands[0].node;
    const struct rose_ir_node *src2 = expr->operands[1].node;
    const struct rose_type *type = src2->data_type;
    unsigned int type_size = rose_type_get_size(type);

    if (src1->data_type->class == ROSE_CLASS_POINTER)
        assert(type_size == 2);
    else
        assert(rose_types_are_equal(src1->data_type, src2->data_type, false));
    assert(type->class == ROSE_CLASS_NUMERIC);

    assert(expr->node.reg.allocated);
    assert(src1->reg.allocated);
    assert(src2->reg.allocated);

    if (rose_type_is_far_pointer(src1->data_type) && src1->reg.seg != expr->node.reg.seg)
    {
        x86_emit_s(out, X86_OP_PUSH, src1->reg.seg);
        x86_emit_s(out, X86_OP_POP, expr->node.reg.seg);
    }

    if (expr->node.reg.reg == src2->reg.reg)
    {
        /* SUB is asymmetric, so we can't just subtract src1 into src2.
         * What we can do, though, is subtract and then negate. */

        if (type_size == 1)
        {
            x86_emit_rr8(out, X86_OP_SUB, reg8_from_reg16(src1->reg.reg), reg8_from_reg16(expr->node.reg.reg));
            x86_emit_r8(out, X86_OP_NEG, reg8_from_reg16(expr->node.reg.reg));
        }
        else
        {
            x86_emit_rr(out, X86_OP_SUB, src1->reg.reg, expr->node.reg.reg, type_size * 8);
            x86_emit_r(out, X86_OP_NEG, expr->node.reg.reg, type_size * 8);
        }
    }
    else
    {
        if (type_size == 1)
        {
            if (expr->node.reg.reg != src1->reg.reg)
                x86_emit_rr8(out, X86_OP_MOV, reg8_from_reg16(src1->reg.reg), reg8_from_reg16(expr->node.reg.reg));
            x86_emit_rr8(out, X86_OP_SUB, reg8_from_reg16(src2->reg.reg), reg8_from_reg16(expr->node.reg.reg));
        }
        else
        {
            if (expr->node.reg.reg != src1->reg.reg)
                x86_emit_rr(out, X86_OP_MOV, src1->reg.reg, expr->node.reg.reg, type_size * 8);
            x86_emit_rr(out, X86_OP_SUB, src2->reg.reg, expr->node.reg.reg, type_size * 8);
        }
    }
}

static void emit_div(struct rose_ctx *ctx, struct x86_block *out, struct rose_ir_expr *expr)
{
    const struct rose_ir_node *src1 = expr->operands[0].node;
    const struct rose_ir_node *src2 = expr->operands[1].node;
    const struct rose_type *type = src2->data_type;
    unsigned int type_size = rose_type_get_size(type);

    assert(rose_types_are_equal(src1->data_type, src2->data_type, false));
    assert(type->class == ROSE_CLASS_NUMERIC);

    assert(expr->node.reg.allocated);
    assert(src1->reg.allocated);
    assert(src2->reg.allocated);

    if (type_size == 1)
    {
        /* Mostly the same but I'm leaving it alone for now anyway. */
        rose_fixme(ctx, &expr->node.loc, "8-bit division.");
    }
    else
    {
        if (src1->reg.reg != X86_REG_AX)
            x86_emit_rr(out, X86_OP_MOV, src1->reg.reg, X86_REG_AX, type_size * 8);
        if (is_signed(type))
        {
            x86_emit(out, X86_OP_CWD, type_size * 8);
            x86_emit_r(out, X86_OP_IDIV, src2->reg.reg, type_size * 8);
        }
        else
        {
            x86_emit_rr(out, X86_OP_XOR, X86_REG_DX, X86_REG_DX, type_size * 8);
            x86_emit_r(out, X86_OP_DIV, src2->reg.reg, type_size * 8);
        }
        /* RA already put the result in the right register. */
    }
}

static void emit_bit_op(struct rose_ctx *ctx, struct x86_block *out, struct rose_ir_expr *expr, enum x86_opcode op)
{
    const struct rose_ir_node *src1 = expr->operands[0].node;
    const struct rose_ir_node *src2 = expr->operands[1].node;
    unsigned int type_size = rose_type_get_size(src1->data_type);

    assert(src1->reg.allocated);
    assert(src2->reg.allocated);

    if (expr->node.reg.reg == src1->reg.reg)
    {
        if (type_size == 1)
            x86_emit_rr8(out, op, reg8_from_reg16(src2->reg.reg), reg8_from_reg16(src1->reg.reg));
        else
            x86_emit_rr(out, op, src2->reg.reg, src1->reg.reg, type_size * 8);
    }
    else
    {
        if (expr->node.reg.reg != src2->reg.reg)
        {
            if (type_size == 1)
                x86_emit_rr8(out, X86_OP_MOV, reg8_from_reg16(src2->reg.reg), reg8_from_reg16(expr->node.reg.reg));
            else
                x86_emit_rr(out, X86_OP_MOV, src2->reg.reg, expr->node.reg.reg, type_size * 8);
        }

        if (type_size == 1)
            x86_emit_rr8(out, op, reg8_from_reg16(src1->reg.reg), reg8_from_reg16(expr->node.reg.reg));
        else
            x86_emit_rr(out, op, src1->reg.reg, expr->node.reg.reg, type_size * 8);
    }
}

static void emit_mul(struct rose_ctx *ctx, struct x86_block *out, struct rose_ir_expr *expr)
{
    const struct rose_ir_node *src1 = expr->operands[0].node;
    const struct rose_ir_node *src2 = expr->operands[1].node;
    const struct rose_type *type = src2->data_type;
    unsigned int type_size = rose_type_get_size(type);

    assert(rose_types_are_equal(src1->data_type, src2->data_type, false));
    assert(type->class == ROSE_CLASS_NUMERIC);

    assert(expr->node.reg.allocated);
    assert(src1->reg.allocated);
    assert(src2->reg.allocated);

    /* MUL and IMUL are identical for the low N bits, where N is the size of
     * the operands.
     *
     * Unfortunately, there is no 8-bit IMUL. How do we deal with this? */
    if (type_size == 8)
    {
        rose_fixme(ctx, &expr->node.loc, "8-bit multiplication.");
        return;
    }

    if (expr->node.reg.reg == src2->reg.reg)
    {
        x86_emit_rr(out, X86_OP_IMUL, src1->reg.reg, expr->node.reg.reg, type_size * 8);
    }
    else
    {
        if (expr->node.reg.reg != src1->reg.reg)
            x86_emit_rr(out, X86_OP_MOV, src1->reg.reg, expr->node.reg.reg, type_size * 8);
        x86_emit_rr(out, X86_OP_IMUL, src2->reg.reg, expr->node.reg.reg, type_size * 8);
    }
}

static void emit_compare(struct rose_ctx *ctx, struct x86_block *out, struct rose_ir_expr *expr)
{
    enum x86_reg8 dst_reg = reg8_from_reg16(expr->node.reg.reg);
    const struct rose_ir_node *src1 = expr->operands[0].node;
    const struct rose_ir_node *src2 = expr->operands[1].node;
    const struct rose_type *type = src1->data_type;
    unsigned int type_size = rose_type_get_size(type);
    struct x86_instr xi = {0};

    assert(rose_types_are_equal(src1->data_type, src2->data_type, false));
    assert(type->class == ROSE_CLASS_POINTER || type->class == ROSE_CLASS_NUMERIC);

    if (rose_type_is_far_pointer(type))
    {
        rose_fixme(ctx, &expr->node.loc, "Comparison of far pointers.");
        return;
    }

    xi.opcode = X86_OP_CMP;
    xi.size = type_size * 8;
    build_arg_from_node(&xi.args[0], out, src2, false, true);
    build_arg_from_node(&xi.args[1], out, src1, true, false);
    x86_add_instr(out, &xi);

    if (expr->op == ROSE_OP2_EQUAL)
        x86_emit_r8(out, X86_OP_SETZ, dst_reg);
    else if (expr->op == ROSE_OP2_NEQUAL)
        x86_emit_r8(out, X86_OP_SETNZ, dst_reg);
    else if (expr->op == ROSE_OP2_GEQUAL)
        x86_emit_r8(out, is_signed(type) ? X86_OP_SETGE : X86_OP_SETAE, dst_reg);
    else if (expr->op == ROSE_OP2_LEQUAL)
        x86_emit_r8(out, is_signed(type) ? X86_OP_SETLE : X86_OP_SETBE, dst_reg);
    else if (expr->op == ROSE_OP2_GREATER)
        x86_emit_r8(out, is_signed(type) ? X86_OP_SETG : X86_OP_SETA, dst_reg);
    else
        x86_emit_r8(out, is_signed(type) ? X86_OP_SETL : X86_OP_SETB, dst_reg);
}

static void emit_shift(struct rose_ctx *ctx, struct x86_block *out, struct rose_ir_expr *expr)
{
    const struct rose_ir_node *src1 = expr->operands[0].node;
    const struct rose_ir_node *src2 = expr->operands[1].node;
    unsigned int type_size = rose_type_get_size(src1->data_type);
    struct x86_instr xi = {0};

    assert(src1->reg.allocated);
    assert(src2->reg.allocated);
    assert(src2->reg.reg == X86_REG_CX);
    assert(expr->node.reg.reg != X86_REG_CX);

    if (expr->op == ROSE_OP2_LSHIFT)
        xi.opcode = X86_OP_SHL;
    else if (is_signed(src1->data_type))
        xi.opcode = X86_OP_SAR;
    else
        xi.opcode = X86_OP_SHR;

    if (expr->node.reg.reg != src1->reg.reg)
    {
        if (type_size == 1)
            x86_emit_rr8(out, X86_OP_MOV, reg8_from_reg16(src1->reg.reg), reg8_from_reg16(expr->node.reg.reg));
        else
            x86_emit_rr(out, X86_OP_MOV, src1->reg.reg, expr->node.reg.reg, type_size * 8);
    }

    xi.size = type_size * 8;
    xi.args[0].type = X86_ARG_REG8;
    xi.args[0].u.reg8 = X86_REG8_CL;
    build_arg_from_node(&xi.args[1], out, &expr->node, false, false);
    x86_add_instr(out, &xi);
}

static void emit_expr(struct rose_ctx *ctx, struct x86_block *out, struct rose_ir_expr *expr)
{
    assert(expr->node.reg.allocated);

    switch (expr->op)
    {
        case ROSE_OP1_CAST:
            emit_cast(ctx, out, expr);
            break;

        case ROSE_OP1_LOGIC_NOT:
            emit_logic_not(ctx, out, expr);
            break;

        case ROSE_OP1_NEG:
            emit_neg(ctx, out, expr);
            break;

        case ROSE_OP1_OFFSETOF:
            emit_offsetof(ctx, out, expr);
            break;

        case ROSE_OP1_SELECTOROF:
            emit_selectorof(ctx, out, expr);
            break;

        case ROSE_OP2_ADD:
            emit_add(ctx, out, expr);
            break;

        case ROSE_OP2_SUB:
            emit_sub(ctx, out, expr);
            break;

        case ROSE_OP2_DIV:
        case ROSE_OP2_MOD:
            emit_div(ctx, out, expr);
            break;

        case ROSE_OP2_MUL:
            emit_mul(ctx, out, expr);
            break;

        case ROSE_OP2_BIT_AND:
            emit_bit_op(ctx, out, expr, X86_OP_AND);
            break;

        case ROSE_OP2_BIT_OR:
            emit_bit_op(ctx, out, expr, X86_OP_OR);
            break;

        case ROSE_OP2_BIT_XOR:
            emit_bit_op(ctx, out, expr, X86_OP_XOR);
            break;

        case ROSE_OP2_EQUAL:
        case ROSE_OP2_GEQUAL:
        case ROSE_OP2_GREATER:
        case ROSE_OP2_LEQUAL:
        case ROSE_OP2_LESS:
        case ROSE_OP2_NEQUAL:
            emit_compare(ctx, out, expr);
            break;

        case ROSE_OP2_LSHIFT:
        case ROSE_OP2_RSHIFT:
            emit_shift(ctx, out, expr);
            break;

        default:
            rose_fixme(ctx, &expr->node.loc, "Op type %#x.", expr->op);
    }
}

static void emit_if(struct rose_ctx *ctx, struct x86_block *out, struct rose_ir_if *iff)
{
    const struct rose_ir_node *condition = iff->condition.node;
    const struct rose_type *condition_type = condition->data_type;
    unsigned int condition_type_size = rose_type_get_size(condition_type);
    bool has_else = !list_empty(&iff->else_block.instrs);
    uint32_t else_reloc, end_reloc;
    char *else_symbol, *end_symbol;

    assert(condition_type->class == ROSE_CLASS_POINTER || condition_type->class == ROSE_CLASS_NUMERIC);
    if (condition_type->class == ROSE_CLASS_POINTER)
        assert(!rose_type_is_far_pointer(condition_type));

    end_symbol = get_local_symbol_name(ctx, "endif");
    end_reloc = x86_add_reloc(out, X86_RELOC_UNRESOLVED_LOCAL, end_symbol);
    if (has_else)
    {
        else_symbol = get_local_symbol_name(ctx, "else");
        else_reloc = x86_add_reloc(out, X86_RELOC_UNRESOLVED_LOCAL, else_symbol);
    }

    assert(condition->reg.allocated);
    if (condition_type_size == 1)
        x86_emit_rr8(out, X86_OP_TEST, reg8_from_reg16(condition->reg.reg), reg8_from_reg16(condition->reg.reg));
    else
        x86_emit_rr(out, X86_OP_TEST, condition->reg.reg, condition->reg.reg, condition_type_size * 8);

    x86_emit_l(out, X86_OP_JZ, has_else ? else_reloc : end_reloc);

    emit_block(ctx, out, &iff->then_block, false);

    if (has_else)
    {
        x86_emit_l(out, X86_OP_JMP, end_reloc);

        x86_add_symbol(out, else_symbol);
        emit_block(ctx, out, &iff->else_block, false);
    }

    x86_add_symbol(out, end_symbol);
}

static void emit_jump(struct rose_ctx *ctx, struct x86_block *out, struct rose_ir_jump *jump, bool top)
{
    switch (jump->type)
    {
        case ROSE_IR_JUMP_BREAK:
            if (ctx->innermost_loop)
                x86_emit_l(out, X86_OP_JMP, ctx->innermost_loop->break_reloc);
            else
                rose_error(ctx, &jump->node.loc, "Break not inside of a loop.");
            break;

        case ROSE_IR_JUMP_CONTINUE:
            if (ctx->innermost_loop)
                x86_emit_l(out, X86_OP_JMP, ctx->innermost_loop->continue_reloc);
            else
                rose_error(ctx, &jump->node.loc, "Continue not inside of a loop.");
            break;

        case ROSE_IR_JUMP_RETURN:
            if (jump->return_value.node)
            {
                const struct rose_type *type = jump->return_value.node->data_type;

                switch (type->class)
                {
                    case ROSE_CLASS_NUMERIC:
                        switch (type->base_type)
                        {
                            case ROSE_TYPE_BOOL:
                            case ROSE_TYPE_SCHAR:
                            case ROSE_TYPE_UCHAR:
                                if (jump->return_value.node->reg.reg != X86_REG_AX)
                                    x86_emit_rr8(out, X86_OP_MOV,
                                            reg8_from_reg16(jump->return_value.node->reg.reg), X86_REG8_AL);
                                break;

                            case ROSE_TYPE_SINT:
                            case ROSE_TYPE_SSHORT:
                            case ROSE_TYPE_UINT:
                            case ROSE_TYPE_USHORT:
                                if (jump->return_value.node->reg.reg != X86_REG_AX)
                                    x86_emit_rr(out, X86_OP_MOV, jump->return_value.node->reg.reg, X86_REG_AX, 16);
                                break;

                            case ROSE_TYPE_SLONG:
                            case ROSE_TYPE_ULONG:
                                if (jump->return_value.node->reg.reg != X86_REG_DX)
                                    x86_emit_rr(out, X86_OP_MOV, jump->return_value.node->reg.reg, X86_REG_DX, 32);
                                x86_emit_ir(out, X86_OP_SHR, 16, X86_REG_DX, 32);
                                if (jump->return_value.node->reg.reg != X86_REG_AX)
                                    x86_emit_rr(out, X86_OP_MOV, jump->return_value.node->reg.reg, X86_REG_AX, 16);
                                break;
                        }
                        break;

                    case ROSE_CLASS_POINTER:
                        if (jump->return_value.node->reg.reg != X86_REG_AX)
                            x86_emit_rr(out, X86_OP_MOV, jump->return_value.node->reg.reg, X86_REG_AX, 16);
                        if (rose_type_is_far_pointer(type))
                            x86_emit_mov_sr(out, jump->return_value.node->reg.seg, X86_REG_DX);
                        break;

                    case ROSE_CLASS_ARRAY:
                    case ROSE_CLASS_FUNCTION:
                    case ROSE_CLASS_STRUCT:
                        rose_fixme(ctx, &jump->node.loc, "Function return type %#x.\n", type->class);
                        break;

                    case ROSE_CLASS_VOID:
                        break;

                    case ROSE_CLASS_ERROR:
                        assert(0);
                }
            }

            if (!top)
                x86_emit_l(out, X86_OP_JMP, ctx->cur_function->return_reloc);
            break;
    }
}

static void emit_load(struct rose_ctx *ctx, struct x86_block *out, struct rose_ir_load *load)
{
    const struct rose_type *type = load->node.data_type;
    struct x86_instr xi = {0};

    /* Load might not be allocated if it was raised. */
    if (!load->node.reg.allocated)
        return;

    assert(type->class == ROSE_CLASS_POINTER || type->class == ROSE_CLASS_NUMERIC);

    xi.opcode = X86_OP_MOV;

    if (is_external_deref(&load->src))
    {
        rose_fixme(ctx, &load->node.loc, "Load from far global.");
        return;
    }

    /* We don't have to care about any other cases, there's no segment overrides
     * or anything to worry about. */

    build_mem_from_deref(&xi.args[0], out, &load->src);

    if (type->class == ROSE_CLASS_POINTER)
    {
        xi.size = 16;

        xi.args[1].type = X86_ARG_REG16;
        xi.args[1].u.reg = load->node.reg.reg;

        if (rose_type_is_far_pointer(type))
        {
            switch (load->node.reg.seg)
            {
                case X86_SEG_ES:
                    xi.opcode = X86_OP_LES;
                    break;

                case X86_SEG_FS:
                    xi.opcode = X86_OP_LFS;
                    break;

                case X86_SEG_GS:
                    xi.opcode = X86_OP_LGS;
                    break;

                case X86_SEG_DS:
                case X86_SEG_SS:
                    /* If this happens the segment is already correct. */
                    break;

                case X86_SEG_CS:
                case X86_SEG_NONE:
                    rose_unreachable();
            }
        }

        x86_add_instr(out, &xi);
    }
    else
    {
        switch (type->base_type)
        {
            case ROSE_TYPE_BOOL:
            case ROSE_TYPE_SCHAR:
            case ROSE_TYPE_UCHAR:
                xi.args[1].type = X86_ARG_REG8;
                xi.args[1].u.reg8 = reg8_from_reg16(load->node.reg.reg);
                xi.size = 8;
                x86_add_instr(out, &xi);
                break;

            case ROSE_TYPE_SINT:
            case ROSE_TYPE_SSHORT:
            case ROSE_TYPE_UINT:
            case ROSE_TYPE_USHORT:
                xi.args[1].type = X86_ARG_REG16;
                xi.args[1].u.reg = load->node.reg.reg;
                xi.size = 16;
                x86_add_instr(out, &xi);
                break;

            case ROSE_TYPE_SLONG:
            case ROSE_TYPE_ULONG:
                xi.args[1].type = X86_ARG_REG16;
                xi.args[1].u.reg = load->node.reg.reg;
                xi.size = 32;
                x86_add_instr(out, &xi);
                break;
        }
    }
}

static void emit_loop(struct rose_ctx *ctx, struct x86_block *out, struct rose_ir_loop *loop)
{
    struct rose_ir_loop *prev_loop = ctx->innermost_loop;
    char *start_symbol, *break_symbol, *continue_symbol;
    uint32_t start_reloc;

    start_symbol = get_local_symbol_name(ctx, "loop");
    start_reloc = x86_add_reloc(out, X86_RELOC_UNRESOLVED_LOCAL, start_symbol);
    break_symbol = get_local_symbol_name(ctx, "break");
    loop->break_reloc = x86_add_reloc(out, X86_RELOC_UNRESOLVED_LOCAL, break_symbol);
    continue_symbol = get_local_symbol_name(ctx, "continue");
    loop->continue_reloc = x86_add_reloc(out, X86_RELOC_UNRESOLVED_LOCAL, continue_symbol);

    x86_add_symbol(out, start_symbol);

    ctx->innermost_loop = loop;
    emit_block(ctx, out, &loop->body, true);
    x86_add_symbol(out, continue_symbol);
    emit_block(ctx, out, &loop->iter, true);
    ctx->innermost_loop = prev_loop;

    x86_emit_l(out, X86_OP_JMP, start_reloc);

    x86_add_symbol(out, break_symbol);
}

static void emit_store(struct rose_ctx *ctx, struct x86_block *out, struct rose_ir_store *store)
{
    const struct rose_ir_node *rhs = store->rhs.node;
    const struct rose_type *type = rhs->data_type;
    struct x86_instr xi = {0};

    assert(type->class == ROSE_CLASS_POINTER || type->class == ROSE_CLASS_NUMERIC);

    xi.opcode = X86_OP_MOV;
    xi.size = rose_type_get_size(type) * 8;

    if (is_external_deref(&store->lhs))
    {
        rose_fixme(ctx, &store->node.loc, "Store to far global.");
        return;
    }

    /* We don't have to care about any other cases, there's no segment overrides
     * or anything to worry about. */

    build_mem_from_deref(&xi.args[1], out, &store->lhs);

    if (rose_type_is_far_pointer(type))
    {
        if (rhs->type == ROSE_IR_CONSTANT)
        {
            xi.size = 32;
            xi.args[0].type = X86_ARG_IMM;
            xi.args[0].u.imm.offset = rose_ir_constant(rhs)->value.u.i;
            x86_add_instr(out, &xi);
        }
        else
        {
            assert(rhs->reg.allocated);

            xi.size = 16;

            xi.args[0].type = X86_ARG_REG16;
            xi.args[0].u.reg = rhs->reg.reg;
            x86_add_instr(out, &xi);

            xi.args[0].type = X86_ARG_SEG;
            xi.args[0].u.seg = rhs->reg.seg;
            xi.args[1].u.mem.offset.offset += 2;
            x86_add_instr(out, &xi);
        }
    }
    else
    {
        build_arg_from_node(&xi.args[0], out, store->rhs.node, false, true);
        x86_add_instr(out, &xi);
    }
}

static void emit_block(struct rose_ctx *ctx, struct x86_block *out, const struct rose_block *block, bool top)
{
    struct rose_ir_node *instr;

    LIST_FOR_EACH_ENTRY(instr, &block->instrs, struct rose_ir_node, entry)
    {
        switch (instr->type)
        {
            case ROSE_IR_ADDR:
                emit_addr(ctx, out, rose_ir_addr(instr));
                break;

            case ROSE_IR_CALL:
                emit_call(ctx, out, rose_ir_call(instr));
                break;

            case ROSE_IR_CONSTANT:
                emit_constant(ctx, out, rose_ir_constant(instr));
                break;

            case ROSE_IR_EXPR:
                emit_expr(ctx, out, rose_ir_expr(instr));
                break;

            case ROSE_IR_IF:
                emit_if(ctx, out, rose_ir_if(instr));
                break;

            case ROSE_IR_JUMP:
                emit_jump(ctx, out, rose_ir_jump(instr), top);
                /* Return from this block. */
                return;

            case ROSE_IR_LOAD:
                emit_load(ctx, out, rose_ir_load(instr));
                break;

            case ROSE_IR_LOOP:
                emit_loop(ctx, out, rose_ir_loop(instr));
                break;

            case ROSE_IR_STORE:
                emit_store(ctx, out, rose_ir_store(instr));
                break;

            case ROSE_IR_INDEX:
                rose_unreachable();
        }
    }
}

static void allocate_locals(struct rose_ctx *ctx, struct rose_function *func)
{
    struct rose_scope *scope;
    struct rose_ir_var *var;
    uint16_t frame_offset;

    frame_offset = 4;
    if (func->data_type->modifiers & ROSE_MODIFIER_FAR)
        frame_offset = 6;

    /* Parameters... */
    for (size_t i = 0; i < func->parameters.count; ++i)
    {
        struct rose_ir_var *param;

        if (func->data_type->modifiers & ROSE_MODIFIER_PASCAL)
            param = func->parameters.vars[func->parameters.count - 1 - i];
        else
            param = func->parameters.vars[i];

        param->frame_offset = frame_offset;
        frame_offset += get_type_param_size(param->data_type);
    }

    frame_offset = 0;

    LIST_FOR_EACH_ENTRY(scope, &ctx->scopes, struct rose_scope, entry)
    {
        LIST_FOR_EACH_ENTRY(var, &scope->vars, struct rose_ir_var, scope_entry)
        {
            if (var->func == func && var->last_read && !var->frame_offset)
            {
                /* No alignment :-) */
                frame_offset += rose_type_get_size(var->data_type);
                var->frame_offset = -frame_offset;
                TRACE("Allocated local %s to bp-%u.\n", var->name, frame_offset);
            }
        }
    }

    func->frame_size = (frame_offset + 1) & ~1;
}

static void emit_function(struct rose_ctx *ctx, struct rose_function *func)
{
    struct x86_block out = {0};
    struct x86_symbol *symbol;
    char *return_symbol;
    struct x86_instr xi;

    ctx->cur_function = func;

    return_symbol = get_local_symbol_name(ctx, "return");
    func->return_reloc = x86_add_reloc(&out, X86_RELOC_UNRESOLVED_LOCAL, return_symbol);

    symbol = x86_add_symbol(&out, func->name);
    symbol->global = true;

    /* Prologue. */

    memset(&xi, 0, sizeof(xi));
    xi.opcode = X86_OP_ENTER;
    xi.size = 16;
    xi.args[0].type = X86_ARG_IMM;
    xi.args[0].u.imm.offset = func->frame_size;
    xi.args[1].type = X86_ARG_IMM;
    xi.args[1].u.imm.offset = 0;
    x86_add_instr(&out, &xi);

    if (func->save_si)
        x86_emit_r(&out, X86_OP_PUSH, X86_REG_SI, 16);
    if (func->save_di)
        x86_emit_r(&out, X86_OP_PUSH, X86_REG_DI, 16);
    if (func->save_fs)
        x86_emit_s(&out, X86_OP_PUSH, X86_SEG_FS);
    if (func->save_gs)
        x86_emit_s(&out, X86_OP_PUSH, X86_SEG_GS);

    if (func->use_ds)
    {
        /* Assume for the moment that %ds is loaded.
         * FIXME: dlls. */
    }

    /* Instructions. */

    emit_block(ctx, &out, &func->body, true);

    x86_add_symbol(&out, return_symbol);

    /* Epilogue. */

    if (func->save_gs)
        x86_emit_s(&out, X86_OP_POP, X86_SEG_GS);
    if (func->save_fs)
        x86_emit_s(&out, X86_OP_POP, X86_SEG_FS);
    if (func->save_di)
        x86_emit_r(&out, X86_OP_POP, X86_REG_DI, 16);
    if (func->save_si)
        x86_emit_r(&out, X86_OP_POP, X86_REG_SI, 16);

    /* No need to adjust esp, leave does it for us. */

    x86_emit(&out, X86_OP_LEAVE, 16);

    memset(&xi, 0, sizeof(xi));
    if (func->data_type->modifiers & ROSE_MODIFIER_FAR)
        xi.opcode = X86_OP_LRET;
    else
        xi.opcode = X86_OP_RET;
    xi.size = 16;
    if (func->data_type->modifiers & ROSE_MODIFIER_PASCAL)
    {
        xi.args[0].type = X86_ARG_IMM;
        for (size_t i = 0; i < func->data_type->e.func.param_count; ++i)
            xi.args[0].u.imm.offset += get_type_param_size(func->data_type->e.func.param_types[i]);
    }
    x86_add_instr(&out, &xi);

    rose_array_reserve(ctx, (void **)&ctx->asm_blocks, &ctx->asm_blocks_capacity,
            ctx->asm_block_count + 1, sizeof(*ctx->asm_blocks));
    ctx->asm_blocks[ctx->asm_block_count++] = out;
}

void rose_fold_constants(struct rose_ctx *ctx, struct rose_block *block)
{
    transform_ir(ctx, fold_addr_cast, block, NULL);
    transform_ir(ctx, transform_deref_paths_into_offsets, block, NULL);
    while (transform_ir(ctx, fold_constant_exprs, block, NULL));
}

void rose_emit_bytecode(struct rose_ctx *ctx)
{
    struct rose_function *func;

    RB_FOR_EACH_ENTRY(func, &ctx->functions, struct rose_function, entry)
    {
        struct rose_block *const body = &func->body;
        bool progress;

        transform_ir(ctx, lower_string_constants, body, NULL);
        transform_ir(ctx, lower_index_loads, body, func);

        do
        {
            progress = transform_ir(ctx, split_array_copies, body, NULL);
            progress |= transform_ir(ctx, split_struct_copies, body, NULL);
        }
        while (progress);

        transform_ir(ctx, fold_addr_cast, body, NULL);

        transform_ir(ctx, transform_deref_paths_into_offsets, body, NULL);

        transform_ir(ctx, lower_far_pointer_comparisons, body, NULL);
        transform_ir(ctx, lower_far_pointer_to_bool_casts, body, NULL);
        transform_ir(ctx, lower_far_pointer_tests, body, NULL);

        while (transform_ir(ctx, fold_constant_exprs, body, NULL));
        transform_ir(ctx, fold_8bit_comparisons, body, NULL);

        do
            compute_liveness(ctx, func);
        while (transform_ir(ctx, dce, body, NULL));

        compute_liveness(ctx, func);
        rose_dump_function(ctx, func);
        allocate_instrs(ctx, func);
        /* Don't write anything if allocation failed. */
        if (ctx->result)
            return;
        allocate_locals(ctx, func);
        emit_function(ctx, func);
    }
}
