Rosé is an experiment to develop a functional, minimal, non-optimizing, i386,
protected-mode C compiler capable of outputting Win16 NE executables.

If successful, I intend to propose it for use in writing 16-bit tests for Wine,
and possibly even for implementing (some?) 16-bit DLLs.

For the time being this code is licensed under GPL 3.0. It was written starting
from the HLSL compiler which is LGPL 2.1. If accepted into Wine I will probably
relicense it back to LGPL 2.1, but until I'm sure that that's happening I'm
keeping it as GPL 3.0.

Things which Rosé will probably not support:

* goto. Not because it's a misfeature; I use goto all the time; but they are
  hard and not necessary.

* Designated initializers. A lot of work for not much benefit.

* Compound literals.

* Floats.

* Nameless structs and unions.

* Enums? These aren't hard, but for obvious reasons they aren't part of the
  Win16 API, so if these are supported it won't be as a priority.

For these and other reasons Rosé is not conformant, and may never be fully
conformant.
