/*
 * Copyright 2023 Elizabeth Figura
 *
 * This file is part of the Rosé compiler.
 *
 * Rosé is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rosé is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Rosé; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

%{
#define YY_NO_UNISTD_H
#include "asm.h"
#include "asm.tab.h"

#define YYSTYPE ASM_YYSTYPE

/* This is a limited-scope assembler. If there are standards governing
 * assemblers, we do not intend to conform to them.
 * We accept a subset of AT&T syntax.
 *
 * We are not going to bother with locations. This is for "inline" assembly
 * only, and gcc doesn't bother with that either. */

%}

%option bison-bridge
%option extra-type="struct asm_ctx *"
%option never-interactive
%option nodefault
%option noinput
%option nounput
%option noyywrap
%option prefix="asm_yy"
%option reentrant

WS              [ \t]
NEWLINE         (\n)|(\r\n)
IDENTIFIER      [A-Za-z_.][A-Za-z0-9_.]*

%%

\.globl                 {
                            return T_GLOBL;
                        }
offset                  {
                            return T_OFFSET;
                        }
segment                 {
                            return T_SEGMENT;
                        }
segptr                  {
                            return T_SEGPTR;
                        }

0x[0-9a-fA-F]+          {
                            yylval->intval = rose_parse_integer(yytext);
                            return T_INTEGER;
                        }
0[0-7]+                 {
                            yylval->intval = rose_parse_integer(yytext);
                            return T_INTEGER;
                        }
[0-9]+                  {
                            yylval->intval = rose_parse_integer(yytext);
                            return T_INTEGER;
                        }

{WS}+                   {}

{NEWLINE}               {
                            return T_NEWLINE;
                        }
{IDENTIFIER}            {
                            yylval->strval = strdup(yytext);
                            return T_IDENTIFIER;
                        }
(.)                     {
                            return yytext[0];
                        }

%%

int asm_compile(const struct rose_code *source, struct x86_block *output)
{
    struct asm_ctx ctx = {0};
    YY_BUFFER_STATE buffer;
    int ret;

    yylex_init_extra(&ctx, &ctx.scanner);
    buffer = yy_scan_bytes(source->code, source->size, ctx.scanner);
    yy_switch_to_buffer(buffer, ctx.scanner);

    if (!(ret = asm_yyparse(ctx.scanner, &ctx)))
        *output = ctx.output;

    yy_delete_buffer(buffer, ctx.scanner);
    yylex_destroy(ctx.scanner);

    for (size_t i = 0; i < ctx.global_count; ++i)
    {
        struct x86_symbol *symbol;

        if ((symbol = x86_find_symbol(&ctx.output, ctx.globals[i])))
            symbol->global = true;
        free(ctx.globals[i]);
    }
    free(ctx.globals);

    return ret;
}
