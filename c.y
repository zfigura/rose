/*
 * Copyright 2023 Elizabeth Figura
 *
 * This file is part of the Rosé compiler.
 *
 * Rosé is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rosé is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Rosé; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

/*
 * This file incorporates code taken from the vkd3d-shader HLSL compiler and
 * covered under the following license:
 *
 * Copyright 2008 Stefan Dösinger
 * Copyright 2012 Matteo Bruni for CodeWeavers
 * Copyright 2019-2020 Zebediah Figura for CodeWeavers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

%code requires
{

#include "c.h"
#include "asm.h"
#include <stdio.h>

#define ROSE_YYLTYPE struct rose_location

struct parse_decl_spec
{
    struct rose_type *type;
    uint32_t storage_modifiers;
};

struct parse_fields
{
    struct rose_struct_field *fields;
    size_t count, capacity;
};

struct parse_parameter
{
    struct rose_type *type;
    const char *name;
    unsigned int modifiers;
};

/* either a single arg, or a braced list of args */
struct parse_initializer_arg
{
    struct rose_ir_node *instr;
    struct parse_initializer_arg *braced_list;
    size_t braced_count, braced_capacity;
    struct rose_location loc;
};

struct parse_initializer
{
    struct parse_initializer_arg arg;
    struct rose_block *instrs;
};

struct parse_args
{
    struct rose_ir_node **args;
    unsigned int args_count;
    struct rose_block *instrs;
    struct rose_location loc;
};

/* I dunno how to name this structure. It's the list of array / pointer / func
 * types that are chained onto a base type. */
struct parse_type_chain
{
    struct parse_type_chain_element
    {
        enum
        {
            TYPE_CHAIN_ARRAY,
            TYPE_CHAIN_POINTER,
            TYPE_CHAIN_FUNC,
        } type;
        union
        {
            uint32_t array_size;
            uint32_t modifiers;
            struct rose_func_parameters parameters;
        } u;
    } *elements;
    unsigned int count;
};

struct parse_variable_def
{
    struct list entry;
    struct rose_location loc;

    char *name;
    struct parse_type_chain chain;
    struct parse_initializer initializer;

    struct rose_type *basic_type;
    unsigned int modifiers;
    struct rose_location modifiers_loc;
};

struct parse_if_body
{
    struct rose_block *then_block;
    struct rose_block *else_block;
};

enum parse_assign_op
{
    ASSIGN_OP_ASSIGN,
    ASSIGN_OP_ADD,
    ASSIGN_OP_SUB,
    ASSIGN_OP_MUL,
    ASSIGN_OP_DIV,
    ASSIGN_OP_MOD,
    ASSIGN_OP_LSHIFT,
    ASSIGN_OP_RSHIFT,
    ASSIGN_OP_AND,
    ASSIGN_OP_OR,
    ASSIGN_OP_XOR,
};

}

%code provides
{

int yylex(ROSE_YYSTYPE *yylval_param, ROSE_YYLTYPE *yylloc_param, void *yyscanner);

}

%code
{

#define YYLLOC_DEFAULT(cur, rhs, n) (cur) = YYRHSLOC(rhs, !!n)

static void yyerror(YYLTYPE *loc, void *scanner, struct rose_ctx *ctx, const char *s)
{
    rose_error(ctx, loc, "%s", s);
}

static struct rose_ir_node *node_from_block(struct rose_block *block)
{
    return block->value;
}

static struct rose_block *make_empty_block(struct rose_ctx *ctx)
{
    struct rose_block *block;

    if ((block = rose_alloc(ctx, sizeof(*block))))
        rose_block_init(block);
    return block;
}

static struct list *make_empty_list(struct rose_ctx *ctx)
{
    struct list *list;

    if ((list = rose_alloc(ctx, sizeof(*list))))
        list_init(list);
    return list;
}

static void destroy_block(struct rose_block *block)
{
    rose_block_cleanup(block);
    rose_free(block);
}

static bool has_side_effects(struct rose_block *block)
{
    struct rose_ir_node *instr;

    LIST_FOR_EACH_ENTRY(instr, &block->instrs, struct rose_ir_node, entry)
    {
        switch (instr->type)
        {
            case ROSE_IR_ADDR:
            case ROSE_IR_CONSTANT:
            case ROSE_IR_EXPR:
            case ROSE_IR_LOAD:
            case ROSE_IR_INDEX:
                break;

            case ROSE_IR_CALL:
            case ROSE_IR_IF:
            case ROSE_IR_LOOP:
            case ROSE_IR_JUMP:
            case ROSE_IR_STORE:
                return true;
        }
    }

    return false;
}

static void add_addr(struct rose_ctx *ctx, struct rose_block *block,
        struct rose_ir_node *instr, const struct rose_location *loc)
{
    struct rose_ir_node *addr;
    struct rose_deref deref;

    if (instr->data_type->class == ROSE_CLASS_ERROR)
        return;

    if (!rose_init_deref_from_index_chain(ctx, &deref, instr))
    {
        rose_error(ctx, &instr->loc, "Invalid l-value.");
        block->value = ctx->error_instr;
        return;
    }
    addr = rose_new_addr(ctx, &deref, loc);
    rose_block_add_instr(block, addr);
    rose_cleanup_deref(&deref);
}

static void check_pointer_cast(struct rose_ctx *ctx, const struct rose_type *src_inner,
        const struct rose_type *dst_inner, bool explicit, const struct rose_location *loc)
{
    struct rose_string_buffer *src_string, *dst_string;

    src_string = rose_type_to_string(ctx, src_inner);
    dst_string = rose_type_to_string(ctx, dst_inner);

    if ((src_inner->modifiers & ROSE_MODIFIER_FAR) && !(dst_inner->modifiers & ROSE_MODIFIER_FAR))
        rose_error(ctx, loc, "Cannot cast from far pointer type '%s *' to non-far pointer type '%s *'.",
                src_string->buffer, dst_string->buffer);

    if (!explicit && (src_inner->modifiers & ROSE_MODIFIER_CONST) && !(dst_inner->modifiers & ROSE_MODIFIER_CONST))
        rose_warning(ctx, loc, "Implicit cast from const to non-const pointer type.");

    if (!explicit && src_inner->class != ROSE_CLASS_VOID && dst_inner->class != ROSE_CLASS_VOID
            && !rose_types_are_equal(src_inner, dst_inner, false))
        rose_warning(ctx, loc, "Implicit cast between incompatible pointer types, from '%s *' to '%s *'.",
                src_string->buffer, dst_string->buffer);

    rose_release_string_buffer(ctx, src_string);
    rose_release_string_buffer(ctx, dst_string);
}

static struct rose_ir_node *add_implicit_pointer_promotion(struct rose_ctx *ctx,
        struct rose_block *block, struct rose_ir_node *instr)
{
    if (instr->data_type->class == ROSE_CLASS_ARRAY)
    {
        /* Implicitly convert to &a[0]. */
        struct rose_ir_node *index, *zero;

        /* This might be a string constant. If it is we need to lower it right
         * now, otherwise add_addr() can't handle it. */
        if (instr->type == ROSE_IR_CONSTANT)
            instr = rose_convert_string_constant(ctx, rose_ir_constant(instr));

        zero = rose_new_uint_constant(ctx, ROSE_TYPE_UINT, 0, &instr->loc);
        rose_block_add_instr(block, zero);

        index = rose_new_index(ctx, instr, zero, &instr->loc);
        rose_block_add_instr(block, index);

        add_addr(ctx, block, index, &instr->loc);
        return node_from_block(block);
    }
    else if (instr->data_type->class == ROSE_CLASS_FUNCTION)
    {
        /* Implicitly take the reference here too. */
        add_addr(ctx, block, instr, &instr->loc);
        return node_from_block(block);
    }

    return instr;
}

static struct rose_ir_node *add_cast(struct rose_ctx *ctx, struct rose_block *block,
        struct rose_ir_node *node, struct rose_type *dst_type, bool explicit, const struct rose_location *loc)
{
    struct rose_string_buffer *src_string, *dst_string;
    struct rose_type *src_type = node->data_type;
    struct rose_ir_node *cast;

    /* The C spec is not perfectly explicit, but it seems that the following
     * casts are legal:
     *
     * - anything to void (C99 § 6.3.2.2)
     * - between pointers, or between pointers and integers (C99 § 6.3.2.3)
     * - between numerics (C99 § 6.3.1, but this part is more vague)
     * - array to anything as if it were a pointer (C99 § 6.3.2.1, also a bit vague)
     * - function to anything as if it were a pointer (C99 § 6.3.2.1.4)
     *
     * gcc bears this out.
     */

    if (rose_types_are_equal(src_type, dst_type, false))
        return node;

    node = add_implicit_pointer_promotion(ctx, block, node);
    src_type = node->data_type;

    src_string = rose_type_to_string(ctx, src_type);
    dst_string = rose_type_to_string(ctx, dst_type);

    switch (dst_type->class)
    {
        case ROSE_CLASS_VOID:
        case ROSE_CLASS_ERROR:
            break;

        case ROSE_CLASS_POINTER:
            switch (src_type->class)
            {
                case ROSE_CLASS_ARRAY:
                case ROSE_CLASS_FUNCTION:
                    rose_unreachable();

                case ROSE_CLASS_ERROR:
                    break;

                case ROSE_CLASS_POINTER:
                    check_pointer_cast(ctx, src_type->e.ptr.type, dst_type->e.ptr.type, explicit, loc);
                    break;

                case ROSE_CLASS_NUMERIC:
                    if (!explicit)
                        rose_warning(ctx, loc, "Implicit cast from integer to pointer.");
                    if (rose_type_get_size(src_type) != rose_type_get_size(dst_type))
                        rose_error(ctx, loc, "Cast from integer to pointer of different size.");
                    break;

                case ROSE_CLASS_VOID:
                case ROSE_CLASS_STRUCT:
                    rose_error(ctx, loc, "Cannot cast from '%s' to '%s'.",
                            src_string->buffer, dst_string->buffer);
                    break;
            }
            break;

        case ROSE_CLASS_NUMERIC:
            switch (src_type->class)
            {
                case ROSE_CLASS_ARRAY:
                case ROSE_CLASS_FUNCTION:
                    rose_unreachable();

                case ROSE_CLASS_ERROR:
                case ROSE_CLASS_NUMERIC:
                    break;

                case ROSE_CLASS_POINTER:
                    if (!explicit && dst_type->base_type != ROSE_TYPE_BOOL)
                        rose_warning(ctx, loc, "Implicit cast from pointer to integer.");
                    if (dst_type->base_type != ROSE_TYPE_BOOL
                            && rose_type_get_size(src_type) != rose_type_get_size(dst_type))
                        rose_error(ctx, loc, "Cast from pointer to integer of different size.");
                    break;

                case ROSE_CLASS_VOID:
                case ROSE_CLASS_STRUCT:
                    rose_error(ctx, loc, "Cannot cast from '%s' to '%s'.",
                            src_string->buffer, dst_string->buffer);
                    break;
            }
            break;

        case ROSE_CLASS_ARRAY:
        case ROSE_CLASS_STRUCT:
        case ROSE_CLASS_FUNCTION:
            rose_error(ctx, loc, "Cannot cast from '%s' to '%s'.",
                    src_string->buffer, dst_string->buffer);
            break;
    }

    rose_release_string_buffer(ctx, src_string);
    rose_release_string_buffer(ctx, dst_string);

    cast = rose_new_cast(ctx, node, dst_type, loc);
    rose_block_add_instr(block, cast);
    return cast;
}

/* Get the ith element, where the 0th element is the outermost. */
static struct parse_type_chain_element *parse_chain_get_element(
        const struct parse_type_chain *chain, unsigned int i)
{
    return &chain->elements[chain->count - 1 - i];
}

static struct rose_type *apply_type_chain(struct rose_ctx *ctx, struct rose_type *type,
        const struct parse_type_chain *chain, const struct rose_location *loc)
{
    for (unsigned int i = 0; i < chain->count; ++i)
    {
        const struct parse_type_chain_element *e = parse_chain_get_element(chain, i);

        switch (e->type)
        {
            case TYPE_CHAIN_ARRAY:
                if (type->class == ROSE_CLASS_FUNCTION)
                    rose_error(ctx, loc, "Cannot declare an array of functions.");

                if (e->u.array_size == ROSE_ARRAY_ELEMENTS_COUNT_IMPLICIT)
                {
                    if (i < chain->count - 1)
                    {
                        rose_error(ctx, loc, "Only innermost array size can be implicit.");
                        return ctx->builtin_types.error;
                    }
                }
                type = rose_new_array_type(ctx, type, e->u.array_size);
                break;

            case TYPE_CHAIN_POINTER:
                /* Make all pointers implicitly far, even for (small) EXE's.
                 * Making them near adds complexity to libraries that we don't
                 * really want to deal with, and in Wine we don't really care
                 * about the performance/size difference. */
                if (!(type->modifiers & ROSE_MODIFIER_NEAR))
                    type = rose_type_clone(ctx, type, NULL, ROSE_MODIFIER_FAR);
                type = rose_new_pointer_type(ctx, type);
                type = rose_type_clone(ctx, type, NULL, e->u.modifiers);
                break;

            case TYPE_CHAIN_FUNC:
            {
                unsigned int modifiers = type->modifiers;

                if (type->class == ROSE_CLASS_FUNCTION)
                    rose_error(ctx, loc, "Cannot return a function from a function.");
                else if (type->class == ROSE_CLASS_ARRAY)
                    rose_error(ctx, loc, "Cannot return an array from a function.");
                else if (type->class == ROSE_CLASS_STRUCT)
                    rose_fixme(ctx, loc, "Struct return.");

                for (size_t i = 0; i < e->u.parameters.count; ++i)
                {
                    struct rose_ir_var *var = e->u.parameters.vars[i];

                    if (var->data_type->class == ROSE_CLASS_VOID)
                        rose_error(ctx, &var->loc, "Function parameters cannot have void type.");
                    else if (var->data_type->class == ROSE_CLASS_FUNCTION)
                        rose_fixme(ctx, &var->loc, "Function-typed parameter.\n");
                    else if (var->data_type->class == ROSE_CLASS_ARRAY)
                        rose_fixme(ctx, &var->loc, "Array-typed parameter.\n");
                    else if (var->data_type->class == ROSE_CLASS_STRUCT)
                        rose_fixme(ctx, &var->loc, "Struct-typed parameter.\n");
                }

                type = rose_new_function_type(ctx, type, &e->u.parameters);

                /* Modifiers like far/pascal get specified as if they apply to
                 * the return type (compare how pointers are specified), but
                 * they actually should be applied to the function. */
                type = rose_type_clone(ctx, type, NULL, modifiers);

                break;
            }
        }
    }

    return type;
}

static bool type_is_implicit_array(const struct rose_type *type)
{
    return type->class == ROSE_CLASS_ARRAY && type->e.array.elements_count == ROSE_ARRAY_ELEMENTS_COUNT_IMPLICIT;
}

static struct rose_ir_node *add_implicit_conversion(struct rose_ctx *ctx, struct rose_block *block,
        struct rose_ir_node *node, struct rose_type *dst_type, const struct rose_location *loc)
{
    return add_cast(ctx, block, node, dst_type, false, loc);
}

static void add_explicit_conversion(struct rose_ctx *ctx, struct rose_block *block,
        struct rose_type *dst_type, const struct parse_type_chain *chain, const struct rose_location *loc)
{
    struct rose_ir_node *instr = node_from_block(block);

    dst_type = apply_type_chain(ctx, dst_type, chain, loc);
    if (type_is_implicit_array(dst_type))
    {
        rose_error(ctx, loc, "Implicit size arrays not allowed in casts.");
        dst_type = ctx->builtin_types.error;
    }

    add_cast(ctx, block, instr, dst_type, true, loc);
}

static uint32_t add_modifiers(struct rose_ctx *ctx, uint32_t modifiers, uint32_t mod,
        const struct rose_location *loc)
{
    if (modifiers & mod)
    {
        struct rose_string_buffer *string;

        if ((string = rose_modifiers_to_string(ctx, mod)))
            rose_error(ctx, loc, "Modifier '%s' was already specified.", string->buffer);
        rose_release_string_buffer(ctx, string);
        return modifiers;
    }
    return modifiers | mod;
}

static void check_condition_type(struct rose_ctx *ctx, const struct rose_ir_node *condition)
{
    switch (condition->data_type->class)
    {
        case ROSE_CLASS_NUMERIC:
        case ROSE_CLASS_POINTER:
        case ROSE_CLASS_ERROR:
            break;

        case ROSE_CLASS_ARRAY:
        case ROSE_CLASS_STRUCT:
        case ROSE_CLASS_FUNCTION:
        case ROSE_CLASS_VOID:
            rose_error(ctx, &condition->loc, "Condition type must be scalar.");
    }
}

static void append_conditional_break(struct rose_ctx *ctx, struct rose_block *cond_block)
{
    struct rose_ir_node *condition, *not, *iff, *jump;
    struct rose_block then_block;

    /* E.g. "for (i = 0; ; ++i)". */
    if (list_empty(&cond_block->instrs))
        return;

    condition = node_from_block(cond_block);
    check_condition_type(ctx, condition);

    not = rose_new_unary_expr(ctx, ROSE_OP1_LOGIC_NOT, condition,
            rose_get_scalar_type(ctx, ROSE_TYPE_BOOL), &condition->loc);
    rose_block_add_instr(cond_block, not);

    rose_block_init(&then_block);

    jump = rose_new_jump(ctx, ROSE_IR_JUMP_BREAK, NULL, &condition->loc);
    rose_block_add_instr(&then_block, jump);

    iff = rose_new_if(ctx, not, &then_block, NULL, &condition->loc);
    rose_block_add_instr(cond_block, iff);
}

static void free_parse_initializer_arg(struct parse_initializer_arg *arg)
{
    if (!arg->instr)
    {
        size_t i;

        for (i = 0; i < arg->braced_count; ++i)
            free_parse_initializer_arg(&arg->braced_list[i]);
        rose_free(arg->braced_list);
    }
}

static void free_parse_initializer(struct parse_initializer *initializer)
{
    free_parse_initializer_arg(&initializer->arg);
    destroy_block(initializer->instrs);
}

static void add_return(struct rose_ctx *ctx, struct rose_block *block,
        struct rose_ir_node *return_value, const struct rose_location *loc)
{
    struct rose_type *return_type = ctx->cur_function->data_type->e.func.return_type;
    struct rose_ir_node *jump;

    if (return_type->class != ROSE_CLASS_VOID)
    {
        if (return_value)
        {
            if (return_value->data_type->class == ROSE_CLASS_ERROR)
                return;

            return_value = add_implicit_conversion(ctx, block, return_value, return_type, loc);
        }
        else
        {
            rose_error(ctx, loc, "Non-void functions must return a value.");
        }
    }
    else
    {
        if (return_value && return_value->data_type->class != ROSE_CLASS_VOID)
            rose_error(ctx, loc, "Void functions cannot return a value.");
    }

    jump = rose_new_jump(ctx, ROSE_IR_JUMP_RETURN, return_value, loc);
    rose_block_add_instr(block, jump);
}

static const struct rose_struct_field *get_struct_field(const struct rose_struct_field *fields,
        size_t count, const char *name)
{
    for (size_t i = 0; i < count; ++i)
    {
        if (!strcmp(fields[i].name, name))
            return &fields[i];
    }
    return NULL;
}

static void add_record_access(struct rose_ctx *ctx, struct rose_block *block,
        const char *name, const struct rose_location *loc)
{
    struct rose_ir_node *record = node_from_block(block);
    const struct rose_type *record_type = record->data_type;
    const struct rose_struct_field *field;
    struct rose_ir_node *index, *c;
    unsigned int idx;

    if (record_type->class != ROSE_CLASS_STRUCT)
    {
        if (record_type->class != ROSE_CLASS_ERROR)
            rose_error(ctx, loc, "Invalid subscript \"%s\".", name);
        block->value = ctx->error_instr;
        return;
    }

    if (!(field = get_struct_field(record_type->e.record.fields, record_type->e.record.field_count, name)))
    {
        rose_error(ctx, loc, "Field \"%s\" is not defined.", name);
        block->value = ctx->error_instr;
        return;
    }
    idx = field - record_type->e.record.fields;
    assert(idx < record_type->e.record.field_count);

    c = rose_new_uint_constant(ctx, ROSE_TYPE_UINT, idx, loc);
    rose_block_add_instr(block, c);

    index = rose_new_index(ctx, record, c, loc);
    rose_block_add_instr(block, index);
}

static void add_record_deref(struct rose_ctx *ctx, struct rose_block *block,
        const char *name, const struct rose_location *loc)
{
    struct rose_ir_node *ptr = node_from_block(block);
    const struct rose_type *ptr_type = ptr->data_type;
    struct rose_ir_node *load;

    if (ptr_type->class != ROSE_CLASS_POINTER)
    {
        if (ptr_type->class != ROSE_CLASS_ERROR)
            rose_error(ctx, loc, "Invalid subscript \"%s\".", name);
        block->value = ctx->error_instr;
    }

    load = rose_new_pointer_load(ctx, ptr, loc);
    rose_block_add_instr(block, load);
    add_record_access(ctx, block, name, loc);
}

static struct rose_ir_node *add_binary_arithmetic_expr(struct rose_ctx *ctx, struct rose_block *block,
        enum rose_ir_expr_op op, struct rose_ir_node *arg1, struct rose_ir_node *arg2,
        const struct rose_location *loc);

static bool add_array_access(struct rose_ctx *ctx, struct rose_block *block, struct rose_ir_node *array,
        struct rose_ir_node *index, const struct rose_location *loc)
{
    const struct rose_type *expr_type = array->data_type, *index_type = index->data_type;
    struct rose_ir_node *return_index, *cast;

    if (array->data_type->class == ROSE_CLASS_ERROR || index->data_type->class == ROSE_CLASS_ERROR)
    {
        block->value = ctx->error_instr;
        return true;
    }

    if (index_type->class != ROSE_CLASS_NUMERIC)
    {
        rose_error(ctx, &index->loc, "Array index is not scalar.");
        return false;
    }

    cast = rose_new_cast(ctx, index, rose_get_scalar_type(ctx, ROSE_TYPE_UINT), &index->loc);
    rose_block_add_instr(block, cast);
    index = cast;

    if (expr_type->class == ROSE_CLASS_POINTER)
    {
        struct rose_ir_node *add, *load;

        /* gcc forbids indexing function pointers. I can't actually find where
         * in the spec this is forbidden, if it is, but I'm siding with gcc
         * here; that kind of expression has uncertain semantics.
         *
         * Note that dereferencing a function pointer like (*func)() is valid,
         * and does the same thing as just calling it directly.
         * Which makes sense to me. Arguably func[0]() should also do this, but
         * that's hard to detect, so we just forbid indexing categorically. */

        if (expr_type->e.ptr.type->class == ROSE_CLASS_VOID)
            rose_error(ctx, loc, "Void pointers cannot be array-indexed.");
        else if (expr_type->e.ptr.type->class == ROSE_CLASS_FUNCTION)
            rose_error(ctx, loc, "Function pointers cannot be array-indexed.");

        add = rose_new_binary_expr(ctx, ROSE_OP2_ADD, array, index);
        rose_block_add_instr(block, add);
        load = rose_new_pointer_load(ctx, add, loc);
        rose_block_add_instr(block, load);
        return true;
    }

    if (expr_type->class != ROSE_CLASS_ARRAY)
    {
        if (expr_type->class == ROSE_CLASS_NUMERIC)
            rose_error(ctx, loc, "Scalar expressions cannot be array-indexed.");
        else
            rose_error(ctx, loc, "Expression cannot be array-indexed.");
        return false;
    }

    return_index = rose_new_index(ctx, array, index, loc);
    rose_block_add_instr(block, return_index);

    return true;
}

static struct rose_type *apply_type_modifiers(struct rose_ctx *ctx, struct rose_type *type,
        unsigned int *modifiers, const struct rose_location *loc)
{
    struct rose_type *new_type;

    if (type->class == ROSE_CLASS_FUNCTION && type->e.func.vararg && (*modifiers & ROSE_MODIFIER_PASCAL))
        rose_error(ctx, loc, "Vararg functions cannot be pascal.");

    new_type = rose_type_clone(ctx, type, NULL, *modifiers & ROSE_TYPE_MODIFIERS_MASK);
    *modifiers &= ~ROSE_TYPE_MODIFIERS_MASK;
    return new_type;
}

static void free_parse_type_chain(struct parse_type_chain *chain)
{
    for (size_t i = 0; i < chain->count; ++i)
    {
        if (chain->elements[i].type == TYPE_CHAIN_FUNC)
            rose_free(chain->elements[i].u.parameters.vars);
    }
    rose_free(chain->elements);
}

static void free_parse_variable_def(struct parse_variable_def *v)
{
    free_parse_initializer(&v->initializer);
    free_parse_type_chain(&v->chain);
    rose_free(v->name);
    rose_free(v);
}

static bool gen_struct_fields(struct rose_ctx *ctx, struct parse_fields *fields,
        struct rose_type *type, struct list *defs)
{
    struct parse_variable_def *v, *v_next;
    size_t i = 0;

    memset(fields, 0, sizeof(*fields));
    fields->count = list_count(defs);
    rose_array_reserve(ctx, (void **)&fields->fields, &fields->capacity, fields->count, sizeof(*fields->fields));

    LIST_FOR_EACH_ENTRY_SAFE(v, v_next, defs, struct parse_variable_def, entry)
    {
        struct rose_struct_field *field = &fields->fields[i++];

        field->type = apply_type_chain(ctx, type, &v->chain, &v->loc);
        if (type_is_implicit_array(field->type))
        {
            rose_error(ctx, &v->loc, "Implicit size arrays not allowed in struct fields.");
            field->type = ctx->builtin_types.error;
        }

        if (field->type->class == ROSE_CLASS_FUNCTION)
            rose_error(ctx, &v->loc, "Functions are not allowed in struct fields.");

        free_parse_type_chain(&v->chain);
        field->loc = v->loc;
        field->name = v->name;
        if (v->initializer.arg.instr || v->initializer.arg.braced_count)
        {
            rose_error(ctx, &v->loc, "Illegal initializer on a struct field.");
            free_parse_initializer(&v->initializer);
        }
        rose_free(v);
    }
    rose_free(defs);
    return true;
}

static void add_typedef(struct rose_ctx *ctx, struct rose_type *const orig_type, struct list *list)
{
    struct parse_variable_def *v, *v_next;
    struct rose_type *type;

    LIST_FOR_EACH_ENTRY_SAFE(v, v_next, list, struct parse_variable_def, entry)
    {
        if (rose_get_type(ctx->cur_scope, ROSE_TYPE_PREFIX_NONE, v->name, false))
        {
            rose_error(ctx, &v->loc, "'%s' is already defined as a typedef.", v->name);
            free_parse_variable_def(v);
            continue;
        }

        type = apply_type_chain(ctx, orig_type, &v->chain, &v->loc);
        free_parse_type_chain(&v->chain);
        type = rose_type_clone(ctx, type, v->name, 0);
        free_parse_initializer(&v->initializer);
        rose_free(v);
    }
    rose_free(list);
}

static struct rose_ir_var *get_redefined_var(struct rose_ctx *ctx, const char *name, bool func)
{
    struct rose_scope *scope = ctx->cur_scope;
    struct rose_ir_var *var;

    LIST_FOR_EACH_ENTRY(var, &scope->vars, struct rose_ir_var, scope_entry)
    {
        if (var->name && !strcmp(name, var->name))
            return var;
    }
    if (func && scope->upper->upper == ctx->globals)
    {
        /* Check whether the variable redefines a function parameter. */
        LIST_FOR_EACH_ENTRY(var, &scope->upper->vars, struct rose_ir_var, scope_entry)
        {
            if (!strcmp(name, var->name))
                return var;
        }
    }

    return NULL;
}

static void add_func_parameter(struct rose_ctx *ctx, struct rose_func_parameters *parameters,
        struct parse_parameter *param, const struct rose_location *loc)
{
    struct rose_ir_var *var;

    if ((var = get_redefined_var(ctx, param->name, true)))
    {
        rose_error(ctx, loc, "Variable '%s' was already declared in this scope.", param->name);
        rose_note(ctx, &var->loc, ROSE_LOG_ERROR, "'%s' was previously declared here.", var->name);
    }
    else
    {
        /* Function doesn't exist yet so we have to pass NULL here. */
        var = rose_new_var(ctx, param->name, param->type, NULL, param->modifiers, loc);

        rose_array_reserve(ctx, (void **)&parameters->vars, &parameters->capacity,
                parameters->count + 1, sizeof(*parameters->vars));
        parameters->vars[parameters->count++] = var;
    }
}

static struct rose_block *make_block(struct rose_ctx *ctx, struct rose_ir_node *instr)
{
    struct rose_block *block = make_empty_block(ctx);

    rose_block_add_instr(block, instr);
    return block;
}

static struct rose_ir_node *evaluate_static_expression(struct rose_ctx *ctx, struct rose_block *block,
        struct rose_ir_node *instr, struct rose_type *type, const struct rose_location *loc)
{
    struct rose_src src;

    if (has_side_effects(block))
        rose_error(ctx, loc, "Integer expression cannot have side effects.");

    /* Don't just check for an error node; check if we emitted an error at any
     * point. Optimization passes are written with only valid IR in mind so this
     * should be a bit safer. */
    if (ctx->result)
        return NULL;

    instr = add_implicit_conversion(ctx, block, instr, type, loc);
    rose_src_from_node(&src, instr);

    rose_fold_constants(ctx, block);

    instr = src.node;
    rose_src_remove(&src);

    return instr;
}

static unsigned int evaluate_static_expression_as_uint(struct rose_ctx *ctx, struct rose_block *block,
        const struct rose_location *loc)
{
    struct rose_ir_node *node = node_from_block(block);
    struct rose_ir_constant *constant;
    struct rose_block copy;
    unsigned int ret = 0;

    rose_clone_block(ctx, &copy, block, &node);
    if (!(node = evaluate_static_expression(ctx, &copy, node, rose_get_scalar_type(ctx, ROSE_TYPE_UINT), loc)))
    {
        rose_block_cleanup(&copy);
        return 0;
    }

    if (node->type == ROSE_IR_CONSTANT)
    {
        constant = rose_ir_constant(node);
        ret = constant->value.u.u;
    }
    else
    {
        WARN("Node type %#x.\n", node->type);
        rose_error(ctx, &node->loc, "Failed to evaluate constant expression.");
    }
    rose_block_cleanup(&copy);
    return ret;
}

static enum rose_base_type expr_common_base_type(enum rose_base_type t1, enum rose_base_type t2)
{
    /* C99 draft spec § 6.2.1.5 */
    if (t1 == ROSE_TYPE_ULONG || t2 == ROSE_TYPE_ULONG)
        return ROSE_TYPE_ULONG;
    /* note that SLONG is 32 bits and hence can represent the whole UINT range */
    if (t1 == ROSE_TYPE_SLONG || t2 == ROSE_TYPE_SLONG)
        return ROSE_TYPE_SLONG;
    if (t1 == ROSE_TYPE_UINT || t2 == ROSE_TYPE_UINT)
        return ROSE_TYPE_UINT;
    /* all other types are promoted */
    return ROSE_TYPE_SINT;
}

static bool check_numeric_type(struct rose_ctx *ctx, const struct rose_ir_node *instr)
{
    if (instr->data_type->class != ROSE_CLASS_NUMERIC)
    {
        struct rose_string_buffer *string;

        if ((string = rose_type_to_string(ctx, instr->data_type)))
            rose_error(ctx, &instr->loc,
                    "Expression of type \"%s\" cannot be used in a numeric expression.", string->buffer);
        rose_release_string_buffer(ctx, string);
        return false;
    }
    return true;
}

static struct rose_ir_node *add_expr(struct rose_ctx *ctx, struct rose_block *block,
        enum rose_ir_expr_op op, struct rose_ir_node *operands[ROSE_MAX_OPERANDS],
        struct rose_type *type, const struct rose_location *loc)
{
    struct rose_ir_node *expr;

    expr = rose_new_expr(ctx, op, operands, type, loc);
    rose_block_add_instr(block, expr);
    return expr;
}

static void check_integer_type(struct rose_ctx *ctx, const struct rose_ir_node *instr)
{
    const struct rose_type *type = instr->data_type;
    struct rose_string_buffer *string;

    if (type->class == ROSE_CLASS_NUMERIC)
        return;

    if ((string = rose_type_to_string(ctx, type)))
        rose_error(ctx, &instr->loc, "Expression type '%s' is not integer.", string->buffer);
    rose_release_string_buffer(ctx, string);
}

static struct rose_ir_node *add_unary_arithmetic_expr(struct rose_ctx *ctx, struct rose_block *block,
        enum rose_ir_expr_op op, struct rose_ir_node *arg, const struct rose_location *loc)
{
    struct rose_ir_node *args[ROSE_MAX_OPERANDS] = {arg};

    if (arg->data_type->class == ROSE_CLASS_ERROR)
        return arg;

    return add_expr(ctx, block, op, args, arg->data_type, loc);
}

static struct rose_ir_node *add_unary_bitwise_expr(struct rose_ctx *ctx, struct rose_block *block,
        enum rose_ir_expr_op op, struct rose_ir_node *arg, const struct rose_location *loc)
{
    if (arg->data_type->class == ROSE_CLASS_ERROR)
        return arg;

    check_integer_type(ctx, arg);

    return add_unary_arithmetic_expr(ctx, block, op, arg, loc);
}

static struct rose_ir_node *add_unary_logical_expr(struct rose_ctx *ctx, struct rose_block *block,
        enum rose_ir_expr_op op, struct rose_ir_node *arg, const struct rose_location *loc)
{
    struct rose_ir_node *args[ROSE_MAX_OPERANDS] = {0};
    struct rose_type *bool_type;

    if (arg->data_type->class == ROSE_CLASS_ERROR)
        return arg;

    bool_type = rose_get_scalar_type(ctx, ROSE_TYPE_BOOL);

    args[0] = add_implicit_conversion(ctx, block, arg, bool_type, loc);
    return add_expr(ctx, block, op, args, bool_type, loc);
}

static struct rose_type *get_common_numeric_type(struct rose_ctx *ctx, const struct rose_ir_node *arg1,
        const struct rose_ir_node *arg2, const struct rose_location *loc)
{
    if (!check_numeric_type(ctx, arg1) || !check_numeric_type(ctx, arg2))
        return ctx->builtin_types.error;
    return rose_get_scalar_type(ctx, expr_common_base_type(arg1->data_type->base_type, arg2->data_type->base_type));
}

static struct rose_type *get_common_pointer_type(struct rose_ctx *ctx,
        struct rose_type *t1, struct rose_type *t2, const struct rose_location *loc)
{
    struct rose_type *t1_inner = t1->e.ptr.type, *t2_inner = t2->e.ptr.type;
    struct rose_type *common_type;

    /* The C spec is silent on what happens if the pointer types are
     * incompatible. I guess that means it's UB. We'll report an error. */
    if (t1_inner->class == ROSE_CLASS_VOID)
    {
        common_type = t2_inner;
    }
    else if (t2_inner->class == ROSE_CLASS_VOID)
    {
        common_type = t1_inner;
    }
    else if (rose_types_are_equal(t1_inner, t2_inner, false))
    {
        common_type = t1_inner;
    }
    else
    {
        struct rose_string_buffer *t1_string = rose_type_to_string(ctx, t1);
        struct rose_string_buffer *t2_string = rose_type_to_string(ctx, t2);

        rose_error(ctx, loc, "Incompatible pointer types '%s' and '%s'.", t1_string->buffer, t2_string->buffer);
        rose_release_string_buffer(ctx, t1_string);
        rose_release_string_buffer(ctx, t2_string);
        common_type = ctx->builtin_types.error;
    }

    common_type = rose_type_clone(ctx, t1_inner, NULL, t1_inner->modifiers | t2_inner->modifiers);
    common_type = rose_new_pointer_type(ctx, common_type);

    return common_type;
}

static void check_pointer_type_for_arithmetic(struct rose_ctx *ctx, const struct rose_ir_node *instr)
{
    if (instr->data_type->e.ptr.type->class == ROSE_CLASS_VOID)
        rose_error(ctx, &instr->loc, "Cannot perform pointer arithmetic on void pointers.");
    else if (instr->data_type->e.ptr.type->class == ROSE_CLASS_FUNCTION)
        rose_error(ctx, &instr->loc, "Cannot perform pointer arithmetic on function pointers.");
}

static struct rose_ir_node *add_pointer_addition(struct rose_ctx *ctx, struct rose_block *block,
        enum rose_ir_expr_op op, struct rose_ir_node *ptr, struct rose_ir_node *offset, const struct rose_location *loc)
{
    struct rose_type *ptr_type = ptr->data_type;
    unsigned int size = rose_type_get_size(ptr_type->e.ptr.type);
    struct rose_ir_node *add;

    check_pointer_type_for_arithmetic(ctx, ptr);

    if (rose_type_get_size(offset->data_type) != 2)
    {
        offset = rose_new_cast(ctx, offset, rose_get_scalar_type(ctx, ROSE_TYPE_SINT), &offset->loc);
        rose_block_add_instr(block, offset);
    }

    /* The implicit mul is mean to codegen. Lower it to an explicit one. */

    if (size != 1)
    {
        struct rose_ir_node *c;

        c = rose_new_uint_constant(ctx, ROSE_TYPE_SINT, rose_type_get_size(ptr_type->e.ptr.type), loc);
        rose_block_add_instr(block, c);

        offset = rose_new_binary_expr(ctx, ROSE_OP2_MUL, offset, c);
        rose_block_add_instr(block, offset);
    }

    add = rose_new_binary_expr(ctx, op, ptr, offset);
    rose_block_add_instr(block, add);
    return add;
}

static struct rose_ir_node *add_binary_arithmetic_expr(struct rose_ctx *ctx, struct rose_block *block,
        enum rose_ir_expr_op op, struct rose_ir_node *arg1, struct rose_ir_node *arg2,
        const struct rose_location *loc)
{
    arg1 = add_implicit_pointer_promotion(ctx, block, arg1);
    arg2 = add_implicit_pointer_promotion(ctx, block, arg2);

    if (op == ROSE_OP2_SUB && arg1->data_type->class == ROSE_CLASS_POINTER
            && arg2->data_type->class == ROSE_CLASS_POINTER)
    {
        unsigned int pointee_size = rose_type_get_size(arg1->data_type->e.ptr.type);
        struct rose_type *uint_type = rose_get_scalar_type(ctx, ROSE_TYPE_UINT);
        struct rose_ir_node *sub, *c, *div;

        if (!rose_types_are_equal(arg1->data_type->e.ptr.type, arg2->data_type->e.ptr.type, false))
            rose_error(ctx, loc, "Subtracted pointer types are not equal.");
        else
            check_pointer_type_for_arithmetic(ctx, arg1);

        /* The implicit division is painful. Lower it to an explicit one. */

        arg1 = rose_new_unary_expr(ctx, ROSE_OP1_OFFSETOF, arg1, uint_type, loc);
        rose_block_add_instr(block, arg1);
        arg2 = rose_new_unary_expr(ctx, ROSE_OP1_OFFSETOF, arg2, uint_type, loc);
        rose_block_add_instr(block, arg2);

        sub = rose_new_binary_expr(ctx, ROSE_OP2_SUB, arg1, arg2);
        rose_block_add_instr(block, sub);

        if (pointee_size > 1)
        {
            c = rose_new_uint_constant(ctx, ROSE_TYPE_UINT, pointee_size, loc);
            rose_block_add_instr(block, c);

            div = rose_new_binary_expr(ctx, ROSE_OP2_DIV, sub, c);
            rose_block_add_instr(block, div);

            return div;
        }
        return sub;
    }
    else if ((op == ROSE_OP2_ADD || op == ROSE_OP2_SUB) && arg1->data_type->class == ROSE_CLASS_POINTER)
    {
        return add_pointer_addition(ctx, block, op, arg1, arg2, loc);
    }
    else if (op == ROSE_OP2_ADD && arg2->data_type->class == ROSE_CLASS_POINTER)
    {
        return add_pointer_addition(ctx, block, op, arg2, arg1, loc);
    }
    else
    {
        struct rose_type *expr_type = get_common_numeric_type(ctx, arg1, arg2, loc);
        struct rose_ir_node *args[ROSE_MAX_OPERANDS] = {0};

        args[0] = add_implicit_conversion(ctx, block, arg1, expr_type, loc);
        args[1] = add_implicit_conversion(ctx, block, arg2, expr_type, loc);
        return add_expr(ctx, block, op, args, expr_type, loc);
    }
}

static struct rose_ir_node *add_binary_bitwise_expr(struct rose_ctx *ctx, struct rose_block *block,
        enum rose_ir_expr_op op, struct rose_ir_node *arg1, struct rose_ir_node *arg2,
        const struct rose_location *loc)
{
    check_integer_type(ctx, arg1);
    check_integer_type(ctx, arg2);

    return add_binary_arithmetic_expr(ctx, block, op, arg1, arg2, loc);
}

static struct rose_ir_node *add_binary_comparison_expr(struct rose_ctx *ctx, struct rose_block *block,
        enum rose_ir_expr_op op, struct rose_ir_node *arg1, struct rose_ir_node *arg2,
        const struct rose_location *loc)
{
    struct rose_ir_node *args[ROSE_MAX_OPERANDS] = {0};
    struct rose_type *common_type, *return_type;

    arg1 = add_implicit_pointer_promotion(ctx, block, arg1);
    arg2 = add_implicit_pointer_promotion(ctx, block, arg2);

    if (arg1->data_type->class == ROSE_CLASS_POINTER && arg2->data_type->class == ROSE_CLASS_POINTER)
        common_type = get_common_pointer_type(ctx, arg1->data_type, arg2->data_type, loc);
    else
        common_type = get_common_numeric_type(ctx, arg1, arg2, loc);
    return_type = rose_get_scalar_type(ctx, ROSE_TYPE_BOOL);

    args[0] = add_implicit_conversion(ctx, block, arg1, common_type, loc);
    args[1] = add_implicit_conversion(ctx, block, arg2, common_type, loc);
    return add_expr(ctx, block, op, args, return_type, loc);
}

static struct rose_ir_node *add_binary_shift_expr(struct rose_ctx *ctx, struct rose_block *block,
        enum rose_ir_expr_op op, struct rose_ir_node *arg1, struct rose_ir_node *arg2,
        const struct rose_location *loc)
{
    struct rose_ir_node *args[ROSE_MAX_OPERANDS] = {0};
    struct rose_type *return_type, *integer_type;

    if (check_numeric_type(ctx, arg1) && check_numeric_type(ctx, arg2))
    {
        enum rose_base_type base = arg1->data_type->base_type;

        check_integer_type(ctx, arg1);
        check_integer_type(ctx, arg2);

        if (base == ROSE_TYPE_BOOL)
            base = ROSE_TYPE_SINT;

        return_type = rose_get_scalar_type(ctx, base);
    }
    else
    {
        return_type = ctx->builtin_types.error;
    }

    integer_type = rose_get_scalar_type(ctx, ROSE_TYPE_SINT);

    args[0] = add_implicit_conversion(ctx, block, arg1, return_type, loc);
    args[1] = add_implicit_conversion(ctx, block, arg2, integer_type, loc);
    return add_expr(ctx, block, op, args, return_type, loc);
}

static struct rose_block *add_binary_expr_merge(struct rose_ctx *ctx, struct rose_block *block1,
        struct rose_block *block2, enum rose_ir_expr_op op, const struct rose_location *loc)
{
    struct rose_ir_node *arg1 = node_from_block(block1), *arg2 = node_from_block(block2);

    rose_block_add_block(block1, block2);
    destroy_block(block2);

    if (arg1->data_type->class == ROSE_CLASS_ERROR || arg2->data_type->class == ROSE_CLASS_ERROR)
    {
        block1->value = ctx->error_instr;
        return block1;
    }

    switch (op)
    {
        case ROSE_OP2_ADD:
        case ROSE_OP2_DIV:
        case ROSE_OP2_MOD:
        case ROSE_OP2_MUL:
        case ROSE_OP2_SUB:
            add_binary_arithmetic_expr(ctx, block1, op, arg1, arg2, loc);
            break;

        case ROSE_OP2_BIT_AND:
        case ROSE_OP2_BIT_OR:
        case ROSE_OP2_BIT_XOR:
            add_binary_bitwise_expr(ctx, block1, op, arg1, arg2, loc);
            break;

        case ROSE_OP2_LEQUAL:
        case ROSE_OP2_LESS:
        case ROSE_OP2_GEQUAL:
        case ROSE_OP2_GREATER:
        case ROSE_OP2_EQUAL:
        case ROSE_OP2_NEQUAL:
            add_binary_comparison_expr(ctx, block1, op, arg1, arg2, loc);
            break;

        case ROSE_OP2_LSHIFT:
        case ROSE_OP2_RSHIFT:
            add_binary_shift_expr(ctx, block1, op, arg1, arg2, loc);
            break;

        case ROSE_OP0_ERROR:
        case ROSE_OP0_VOID:
        case ROSE_OP1_BIT_NOT:
        case ROSE_OP1_CAST:
        case ROSE_OP1_LOGIC_NOT:
        case ROSE_OP1_NEG:
        case ROSE_OP1_OFFSETOF:
        case ROSE_OP1_SELECTOROF:
            rose_unreachable();
    }

    return block1;
}

static enum rose_ir_expr_op op_from_assignment(enum parse_assign_op op)
{
    static const enum rose_ir_expr_op ops[] =
    {
        0,
        ROSE_OP2_ADD,
        ROSE_OP2_SUB,
        ROSE_OP2_MUL,
        ROSE_OP2_DIV,
        ROSE_OP2_MOD,
        ROSE_OP2_LSHIFT,
        ROSE_OP2_RSHIFT,
        ROSE_OP2_BIT_AND,
        ROSE_OP2_BIT_OR,
        ROSE_OP2_BIT_XOR,
    };

    return ops[op];
}

static void add_assignment(struct rose_ctx *ctx, struct rose_block *block, struct rose_ir_node *lhs,
        enum parse_assign_op assign_op, struct rose_ir_node *rhs)
{
    struct rose_type *lhs_type = lhs->data_type;

    if (lhs->data_type->class == ROSE_CLASS_ERROR || rhs->data_type->class == ROSE_CLASS_ERROR)
    {
        block->value = ctx->error_instr;
        return;
    }

    if (assign_op != ASSIGN_OP_ASSIGN)
    {
        enum rose_ir_expr_op op = op_from_assignment(assign_op);

        assert(op);
        rhs = add_binary_arithmetic_expr(ctx, block, op, lhs, rhs, &rhs->loc);
    }

    rhs = add_implicit_conversion(ctx, block, rhs, lhs_type, &rhs->loc);

    while (lhs->type != ROSE_IR_LOAD && lhs->type != ROSE_IR_INDEX)
    {
        rose_error(ctx, &lhs->loc, "Invalid lvalue.");
        block->value = ctx->error_instr;
        return;
    }

    {
        struct rose_ir_node *store;
        struct rose_deref deref;

        if (!rose_init_deref_from_index_chain(ctx, &deref, lhs))
        {
            rose_error(ctx, &lhs->loc, "Invalid l-value.");
            block->value = ctx->error_instr;
            return;
        }

        store = rose_new_store_index(ctx, &deref, NULL, rhs, &rhs->loc);
        rose_block_add_instr(block, store);
        rose_cleanup_deref(&deref);
    }

    block->value = rhs;
}

static void add_increment(struct rose_ctx *ctx, struct rose_block *block, bool decrement, bool post,
        const struct rose_location *loc)
{
    struct rose_ir_node *lhs = node_from_block(block);
    enum rose_base_type base_type = ROSE_TYPE_SINT;
    struct rose_ir_node *one;

    if (lhs->data_type->class == ROSE_CLASS_ERROR)
        return;

    if (lhs->data_type->modifiers & ROSE_MODIFIER_CONST)
        rose_error(ctx, loc, "Argument to %s%screment operator is const.",
                post ? "post" : "pre", decrement ? "de" : "in");

    if (lhs->data_type->class == ROSE_CLASS_NUMERIC)
        base_type = lhs->data_type->base_type;
    one = rose_new_int_constant(ctx, base_type, 1, loc);
    rose_block_add_instr(block, one);

    add_assignment(ctx, block, lhs, decrement ? ASSIGN_OP_SUB : ASSIGN_OP_ADD, one);

    if (post)
    {
        struct rose_ir_node *copy;

        copy = rose_new_copy(ctx, lhs);
        rose_block_add_instr(block, copy);

        /* Post increment/decrement expressions are considered const. */
        copy->data_type = rose_type_clone(ctx, copy->data_type, NULL, ROSE_MODIFIER_CONST);
    }
}

static void initialize_var_arg(struct rose_ctx *ctx, struct rose_block *block,
        const struct rose_deref *deref, const struct parse_initializer_arg *arg)
{
    struct rose_type *deref_type = rose_deref_get_type(ctx, deref);
    size_t element_count;

    if (arg && arg->instr)
    {
        struct rose_ir_node *rhs, *store;

        rhs = add_implicit_conversion(ctx, block, arg->instr, deref_type, &arg->instr->loc);
        store = rose_new_store_index(ctx, deref, NULL, rhs, &rhs->loc);
        rose_block_add_instr(block, store);
        return;
    }

    if (deref_type->class == ROSE_CLASS_STRUCT)
    {
        element_count = deref_type->e.record.field_count;
    }
    else if (deref_type->class == ROSE_CLASS_ARRAY)
    {
        element_count = deref_type->e.array.elements_count;
    }
    else
    {
        struct rose_string_buffer *string;

        if (!arg)
        {
            struct rose_ir_node *c, *store;

            c = rose_new_int_constant(ctx, ROSE_TYPE_SINT, 0, &deref->var->loc);
            rose_block_add_instr(block, c);

            c = add_implicit_conversion(ctx, block, c, deref_type, &deref->var->loc);
            store = rose_new_store_index(ctx, deref, NULL, c, &deref->var->loc);
            rose_block_add_instr(block, store);
            return;
        }

        string = rose_type_to_string(ctx, deref_type);
        rose_error(ctx, &arg->loc, "Type '%s' cannot have an aggregate initializer.", string->buffer);
        rose_release_string_buffer(ctx, string);
        return;
    }

    if (arg && arg->braced_count > element_count)
    {
        rose_error(ctx, &arg->loc, "Expected at most %zu elements in aggregate initializer, got %zu.",
                element_count, arg->braced_count);
        return;
    }

    for (size_t i = 0; i < element_count; ++i)
    {
        struct rose_deref inner_deref;
        struct rose_ir_node *index;

        index = rose_new_int_constant(ctx, ROSE_TYPE_SINT, i, arg ? &arg->loc : &deref->var->loc);
        rose_copy_deref(ctx, &inner_deref, deref);
        rose_deref_append_index(ctx, &inner_deref, index);
        if (arg && i < arg->braced_count)
            initialize_var_arg(ctx, block, &inner_deref, &arg->braced_list[i]);
        else
            initialize_var_arg(ctx, block, &inner_deref, NULL);
        rose_cleanup_deref(&inner_deref);
    }
}

static void initialize_global_var_arg(struct rose_ctx *ctx, struct rose_block *block,
        struct rose_type *type, const struct parse_initializer_arg *arg)
{
    struct rose_string_buffer *string = rose_type_to_string(ctx, type);

    if (arg && arg->instr)
    {
        struct rose_ir_node *instr = arg->instr;

        switch (type->class)
        {
            case ROSE_CLASS_ERROR:
                break;

            case ROSE_CLASS_NUMERIC:
            case ROSE_CLASS_POINTER:
            {
                struct rose_block copy;

                rose_clone_block(ctx, &copy, block, &instr);
                if (!(instr = evaluate_static_expression(ctx, &copy, instr, type, &arg->loc)))
                {
                    rose_release_string_buffer(ctx, string);
                    return;
                }

                if (instr->type == ROSE_IR_CONSTANT)
                {
                    x86_emit_i(&ctx->data_block, X86_OP_DATA,
                            rose_ir_constant(instr)->value.u.i, rose_type_get_size(type) * 8);
                }
                else if (instr->type == ROSE_IR_ADDR)
                {
                    const struct rose_ir_addr *addr = rose_ir_addr(instr);
                    const struct rose_ir_var *addr_var = addr->src.var;
                    const struct rose_ir_node *offset = addr->src.offset.node;
                    struct x86_instr xi = {0};

                    if (!addr_var)
                    {
                        rose_error(ctx, &instr->loc, "Global initializer element is not constant.");
                        break;
                    }

                    xi.opcode = X86_OP_DATA;
                    xi.args[0].type = X86_ARG_IMM;

                    if (offset)
                    {
                        if (offset->type != ROSE_IR_CONSTANT)
                        {
                            rose_error(ctx, &instr->loc, "Global initializer element is not constant.");
                            break;
                        }

                        xi.args[0].u.imm.offset = rose_ir_constant(offset)->value.u.i;
                    }

                    /* Has to be global; we're in global scope. */
                    assert(!addr_var->func);

                    if (rose_type_is_far_pointer(type))
                    {
                        xi.size = 32;
                        xi.args[0].u.imm.reloc = x86_add_reloc(&ctx->data_block,
                                X86_RELOC_UNRESOLVED_PAIR, addr_var->name);
                    }
                    else
                    {
                        xi.size = 16;
                        xi.args[0].u.imm.reloc = x86_add_reloc(&ctx->data_block,
                                X86_RELOC_UNRESOLVED_OFFSET, addr_var->name);
                    }
                    x86_add_instr(&ctx->data_block, &xi);
                }
                else
                {
                    WARN("Node type %#x.\n", instr->type);
                    rose_error(ctx, &instr->loc, "Global initializer element is not constant.");
                }

                rose_block_cleanup(&copy);
                break;
            }

            case ROSE_CLASS_ARRAY:
                /* Don't evaluate_static_expression() here. The only expression
                 * that's legal is a string constant, and we don't want the
                 * implicit cast that evaluate_static_expression() does. */
                if (instr->type == ROSE_IR_CONSTANT)
                {
                    const struct rose_ir_constant *c = rose_ir_constant(instr);

                    for (size_t i = 0; i < c->value.u.str.len; ++i)
                        x86_emit_i(&ctx->data_block, X86_OP_DATA, c->value.u.str.str[i], 8);
                }
                else
                {
                    WARN("Node type %#x.\n", instr->type);
                    rose_error(ctx, &instr->loc, "Global array initializer is not a string constant.");
                }
                break;

            case ROSE_CLASS_STRUCT:
                rose_error(ctx, &instr->loc, "Type '%s' must have an aggregate initializer.", string->buffer);
                break;

            case ROSE_CLASS_FUNCTION:
            case ROSE_CLASS_VOID:
                rose_unreachable();
        }

        rose_release_string_buffer(ctx, string);
        return;
    }

    switch (type->class)
    {
        case ROSE_CLASS_ERROR:
            break;

        case ROSE_CLASS_STRUCT:
            if (arg && arg->braced_count > type->e.record.field_count)
            {
                rose_error(ctx, &arg->loc, "Expected at most %zu elements in aggregate initializer, got %zu.",
                        type->e.record.field_count, arg->braced_count);
                return;
            }

            for (size_t i = 0; i < type->e.record.field_count; ++i)
            {
                if (arg && i < arg->braced_count)
                    initialize_global_var_arg(ctx, block, type->e.record.fields[i].type, &arg->braced_list[i]);
                else
                    initialize_global_var_arg(ctx, block, type->e.record.fields[i].type, NULL);
            }
            break;

        case ROSE_CLASS_ARRAY:
            if (arg && arg->braced_count > type->e.array.elements_count)
            {
                rose_error(ctx, &arg->loc, "Expected at most %u elements in aggregate initializer, got %zu.",
                        type->e.array.elements_count, arg->braced_count);
                return;
            }

            for (size_t i = 0; i < type->e.array.elements_count; ++i)
            {
                if (arg && i < arg->braced_count)
                    initialize_global_var_arg(ctx, block, type->e.array.type, &arg->braced_list[i]);
                else
                    initialize_global_var_arg(ctx, block, type->e.array.type, NULL);
            }
            break;

        case ROSE_CLASS_NUMERIC:
        case ROSE_CLASS_POINTER:
            if (arg)
                rose_error(ctx, &arg->loc, "Type '%s' cannot have an aggregate initializer.", string->buffer);
            else
                x86_emit_i(&ctx->data_block, X86_OP_DATA, 0, rose_type_get_size(type) * 8);
            break;

        case ROSE_CLASS_FUNCTION:
        case ROSE_CLASS_VOID:
            rose_unreachable();
    }

    rose_release_string_buffer(ctx, string);
}

static void initialize_var(struct rose_ctx *ctx, struct rose_ir_var *var,
        const struct parse_initializer *initializer, uint32_t storage_modifiers)
{
    if (!var->func)
    {
        struct x86_symbol *symbol;

        /* As far as we're concerned, every declaration has to be either a
         * definition or not, determined only by its modifiers.
         *
         * The C spec is more permissive than this. It allows multiple static
         * declarations, and it also effectively allows multiple non-static
         * declarations (without 'extern'). Compilers nowadays do forbid by
         * default declaring the same variable without 'extern' in multiple
         * files, but they still allow it within the same file.
         *
         * That would require some complex logic to implement, however, and it's
         * rarely intended. In practice there should only be two patterns:
         *
         * - a variable used in one file, declared once, with static
         *
         * - a variable used in multiple files, declared in the header with
         *   'extern', and initialized in some file
         *
         * We should never need to deal with a variable declared twice in the
         * same file, and the -fno-common change means we should always get
         * 'extern' on a non-definition.
         */
        if (!initializer->instrs && (storage_modifiers & ROSE_STORAGE_EXTERN))
            return;

        if (var->defined)
        {
            rose_error(ctx, &initializer->arg.loc, "Global variable %s was already defined.", var->name);
            return;
        }
        var->defined = true;
        symbol = x86_add_symbol(&ctx->data_block, var->name);
        if (!(storage_modifiers & ROSE_STORAGE_STATIC))
            symbol->global = true;

        if (initializer->instrs)
        {
            if (has_side_effects(initializer->instrs))
                rose_error(ctx, &initializer->arg.loc, "Global initializer has side effects.");
            initialize_global_var_arg(ctx, initializer->instrs, var->data_type, &initializer->arg);
        }
        else
        {
            initialize_global_var_arg(ctx, NULL, var->data_type, NULL);
        }
    }
    else
    {
        if (initializer->instrs)
        {
            struct rose_deref var_deref;

            rose_init_simple_deref_from_var(&var_deref, var);
            initialize_var_arg(ctx, initializer->instrs, &var_deref, &initializer->arg);
        }
    }
}

static bool var_decl_types_are_compatible(const struct rose_type *t1, const struct rose_type *t2)
{
    if (t1->modifiers != t2->modifiers)
        return false;

    if (rose_types_are_equal(t1, t2, false))
        return true;

    if (t1->class == ROSE_CLASS_ARRAY && t2->class == ROSE_CLASS_ARRAY
            && rose_types_are_equal(t1->e.array.type, t2->e.array.type, true)
            && (t1->e.array.elements_count == ROSE_ARRAY_ELEMENTS_COUNT_IMPLICIT
                    || t2->e.array.elements_count == ROSE_ARRAY_ELEMENTS_COUNT_IMPLICIT))
        return true;

    return false;
}

static void declare_var(struct rose_ctx *ctx, struct parse_variable_def *v)
{
    struct rose_type *basic_type = v->basic_type;
    uint32_t modifiers = v->modifiers;
    struct rose_function *func;
    struct rose_ir_var *prev;
    struct rose_type *type;
    char *var_name;

    assert(basic_type);

    if (v->chain.count && v->chain.elements[0].type == TYPE_CHAIN_ARRAY
            && v->chain.elements[0].u.array_size == ROSE_ARRAY_ELEMENTS_COUNT_IMPLICIT)
    {
        struct rose_ir_node *instr = v->initializer.arg.instr;

        if (instr)
        {
            if (instr->type == ROSE_IR_CONSTANT && instr->data_type->class == ROSE_CLASS_ARRAY)
            {
                v->chain.elements[0].u.array_size = instr->data_type->e.array.elements_count;
            }
            else
            {
                rose_error(ctx, &v->loc, "Arrays must have an aggregate initializer.");
                type = ctx->builtin_types.error;
            }
        }
        else if (!v->initializer.arg.braced_count)
        {
            if (modifiers & ROSE_STORAGE_STATIC)
            {
                rose_error(ctx, &v->loc, "Implicit size arrays need to be initialized.");
                type = ctx->builtin_types.error;
            }
        }
        else
        {
            v->chain.elements[0].u.array_size = v->initializer.arg.braced_count;
        }
    }

    type = apply_type_chain(ctx, basic_type, &v->chain, &v->loc);

    var_name = rose_strdup(ctx, v->name);
    if (ctx->cur_scope == ctx->globals)
    {
        if ((func = rose_get_function(ctx, var_name)))
        {
            rose_error(ctx, &v->loc, "'%s' is already defined as a function.", var_name);
            rose_note(ctx, &func->loc, ROSE_LOG_ERROR, "'%s' was previously defined here.", var_name);
        }

        if ((prev = get_redefined_var(ctx, var_name, false)))
        {
            struct rose_string_buffer *string1, *string2;

            if (!var_decl_types_are_compatible(type, prev->data_type))
            {
                string1 = rose_type_to_string(ctx, type);
                string2 = rose_type_to_string(ctx, prev->data_type);
                rose_error(ctx, &v->loc, "Variable '%s' is declared as '%s', but was previously declared as '%s'.",
                        var_name, string1->buffer, string2->buffer);
                rose_note(ctx, &prev->loc, ROSE_LOG_ERROR, "'%s' was previously declared here.", var_name);
                rose_release_string_buffer(ctx, string1);
                rose_release_string_buffer(ctx, string2);
            }

            /* If this is the initialized version of an implicit size array,
             * we need the var to have this type. */
            prev->data_type = type;

            if ((modifiers & ROSE_STORAGE_STATIC) != (prev->storage_modifiers & ROSE_STORAGE_STATIC))
            {
                string1 = rose_modifiers_to_string(ctx, modifiers);
                string2 = rose_modifiers_to_string(ctx, prev->storage_modifiers);
                rose_error(ctx, &v->loc, "Variable '%s' is declared as '%s', but was previously declared as '%s'.",
                        var_name, string1->buffer, string2->buffer);
                rose_note(ctx, &prev->loc, ROSE_LOG_ERROR, "'%s' was previously declared here.", var_name);
                rose_release_string_buffer(ctx, string1);
                rose_release_string_buffer(ctx, string2);
            }
        }
        else
        {
            rose_new_var(ctx, var_name, type, NULL, modifiers, &v->loc);
        }
    }
    else
    {
        static const unsigned int invalid = ROSE_STORAGE_EXTERN;

        if (modifiers & invalid)
        {
            struct rose_string_buffer *string;

            string = rose_modifiers_to_string(ctx, modifiers & invalid);
            rose_error(ctx, &v->loc, "Modifiers '%s' are not allowed on local variables.", string->buffer);
            rose_release_string_buffer(ctx, string);
        }

        if ((prev = get_redefined_var(ctx, var_name, true)))
        {
            if (prev->func)
            {
                rose_error(ctx, &v->loc, "Variable '%s' was already declared in this scope.", var_name);
                rose_note(ctx, &prev->loc, ROSE_LOG_ERROR, "'%s' was previously declared here.", var_name);
            }
        }
        else
        {
            rose_new_var(ctx, var_name, type, ctx->cur_function, modifiers, &v->loc);
        }
    }
}

static struct rose_block *initialize_vars(struct rose_ctx *ctx, struct list *var_list)
{
    struct rose_block *initializers = make_empty_block(ctx);
    struct parse_variable_def *v, *v_next;
    struct rose_ir_var *var;

    LIST_FOR_EACH_ENTRY_SAFE(v, v_next, var_list, struct parse_variable_def, entry)
    {
        /* If this fails, the variable failed to be declared. */
        if (!(var = rose_get_var(ctx->cur_scope, v->name)))
        {
            free_parse_variable_def(v);
            continue;
        }

        switch (var->data_type->class)
        {
            case ROSE_CLASS_FUNCTION:
                if (v->initializer.instrs)
                    rose_error(ctx, &v->initializer.arg.loc, "Functions cannot be initialized.");
                break;

            case ROSE_CLASS_VOID:
                if (v->initializer.instrs)
                    rose_error(ctx, &v->initializer.arg.loc, "Variables of type void cannot be initialized.");
                break;

            case ROSE_CLASS_ERROR:
                break;

            case ROSE_CLASS_NUMERIC:
            case ROSE_CLASS_POINTER:
            case ROSE_CLASS_ARRAY:
            case ROSE_CLASS_STRUCT:
                initialize_var(ctx, var, &v->initializer, v->modifiers);
                break;
        }

        if (v->initializer.instrs)
        {
            if (var->storage_modifiers & ROSE_STORAGE_STATIC)
                rose_block_add_block(&ctx->static_initializers, v->initializer.instrs);
            else
                rose_block_add_block(initializers, v->initializer.instrs);
        }
        free_parse_variable_def(v);
    }

    rose_free(var_list);
    return initializers;
}

static void add_call(struct rose_ctx *ctx, struct rose_block *block,
        struct parse_args *args, const struct rose_location *loc)
{
    struct rose_ir_node *func = node_from_block(block);
    const struct rose_type *func_type = func->data_type;
    struct rose_deref func_deref;
    struct rose_ir_node *call;

    rose_block_add_block(block, args->instrs);

    for (unsigned int i = 0; i < args->args_count; ++i)
    {
        if (args->args[i]->data_type->class == ROSE_CLASS_ERROR)
        {
            block->value = ctx->error_instr;
            return;
        }
    }

    if (func_type->class == ROSE_CLASS_POINTER)
        func_type = func_type->e.ptr.type;

    if (func_type->class != ROSE_CLASS_FUNCTION)
    {
        if (func_type->class != ROSE_CLASS_ERROR)
        {
            struct rose_string_buffer *string = rose_type_to_string(ctx, func_type);
            rose_error(ctx, loc, "Cannot call expression of type '%s'.", string->buffer);
            rose_release_string_buffer(ctx, string);
        }
        block->value = ctx->error_instr;
        return;
    }

    if (func_type->e.func.vararg)
    {
        if (args->args_count < func_type->e.func.param_count)
            rose_error(ctx, loc, "Wrong number of arguments to function: expected at least %zu, but got %u.",
                func_type->e.func.param_count, args->args_count);
    }
    else
    {
        if (args->args_count != func_type->e.func.param_count)
            rose_error(ctx, loc, "Wrong number of arguments to function: expected %zu, but got %u.",
                func_type->e.func.param_count, args->args_count);
    }

    for (unsigned int i = 0; i < args->args_count; ++i)
    {
        struct rose_ir_node *arg = args->args[i];
        struct rose_type *param_type;

        if (i < func_type->e.func.param_count)
        {
            param_type = func_type->e.func.param_types[i];
        }
        else
        {
            /* In theory other types get promoted, but we don't actually have
             * to do anything here for numeric types; alignment takes care of
             * it.
             *
             * We do need to perform implicit promotions to pointers, though. */

            arg = add_implicit_pointer_promotion(ctx, block, arg);
            param_type = arg->data_type;

            /* Our latent data model is far. However, address-of expressions of
             * near pointers are still near by default. If we're passing those
             * to a function we need to convert them. */

            if (param_type->class == ROSE_CLASS_POINTER)
                param_type = rose_new_pointer_type(ctx,
                        rose_type_clone(ctx, param_type->e.ptr.type, NULL, ROSE_MODIFIER_FAR));
        }

        args->args[i] = add_implicit_conversion(ctx, block, arg, param_type, &arg->loc);
    }

    /* We need a deref in the function, including a deref that ends in a
     * function pointer. This is because we can't lcall a register directly.
     * We can near call a register directly, but it's not worth the separate
     * code path. */
    if (!(rose_init_deref_from_index_chain(ctx, &func_deref, func)))
    {
        /* Perfectly legal if we're calling an anonymous func pointer.
         * We need to copy it into a synthetic variable so that we can do an
         * indirect memory call. */
        rose_fixme(ctx, &func->loc, "Call of non-l-value.");
        block->value = ctx->error_instr;
        return;
    }

    call = rose_new_call(ctx, &func_deref, args->args_count, args->args, loc);
    rose_block_add_instr(block, call);
    rose_cleanup_deref(&func_deref);
    rose_free(args->args);
}

static void add_string_constant(struct rose_ctx *ctx, struct rose_block *block,
        const char *string, const struct rose_location *loc)
{
    struct rose_ir_node *c;
    char *parsed;
    size_t len;

    if (!rose_parse_string(string, &parsed, &len))
    {
        rose_error(ctx, loc, "Failed to parse string.");
        block->value = ctx->error_instr;
        return;
    }

    c = rose_new_string_constant(ctx, parsed, len, loc);
    rose_block_add_instr(block, c);

    free(parsed);
}

static struct rose_block *create_sizeof(struct rose_ctx *ctx,
        const struct rose_type *type, const struct rose_location *loc)
{
    struct rose_ir_node *c;
    unsigned int size;

    if (type_is_implicit_array(type))
    {
        rose_error(ctx, loc, "Cannot take the size of an implicit array.");
        size = 0;
    }
    else
    {
        if (type->class == ROSE_CLASS_VOID)
            rose_error(ctx, loc, "Cannot take the size of void.");
        else if (type->class == ROSE_CLASS_FUNCTION)
            rose_error(ctx, loc, "Cannot take the size of a function type.");

        size = rose_type_get_size(type);
    }
    c = rose_new_int_constant(ctx, ROSE_TYPE_UINT, size, loc);
    return make_block(ctx, c);
}

static struct rose_block *add_logic_and_or(struct rose_ctx *ctx, struct rose_block *left_block,
        struct rose_block *right_block, bool or, const struct rose_location *loc)
{
    struct rose_type *bool_type = rose_get_scalar_type(ctx, ROSE_TYPE_BOOL);
    struct rose_ir_node *right = node_from_block(right_block);
    struct rose_ir_node *left = node_from_block(left_block);
    struct rose_ir_node *cond, *iff, *store;
    struct rose_ir_load *load;
    struct rose_ir_var *var;

    left = add_implicit_conversion(ctx, left_block, left, bool_type, loc);
    right = add_implicit_conversion(ctx, right_block, right, bool_type, loc);

    if (!has_side_effects(right_block))
    {
        rose_block_add_block(left_block, right_block);
        destroy_block(right_block);

        cond = rose_new_binary_expr(ctx, or ? ROSE_OP2_BIT_OR : ROSE_OP2_BIT_AND, left, right);
        rose_block_add_instr(left_block, cond);

        return left_block;
    }

    var = rose_new_synthetic_var(ctx, "logic", bool_type, ctx->cur_function, loc);

    store = rose_new_simple_store(ctx, var, left);
    rose_block_add_instr(left_block, store);

    store = rose_new_simple_store(ctx, var, right);
    rose_block_add_instr(right_block, store);

    cond = left;
    if (or)
    {
        cond = rose_new_unary_expr(ctx, ROSE_OP1_LOGIC_NOT, left, left->data_type, loc);
        rose_block_add_instr(left_block, cond);
    }

    iff = rose_new_if(ctx, cond, right_block, NULL, loc);
    rose_block_add_instr(left_block, iff);

    load = rose_new_var_load(ctx, var, loc);
    rose_block_add_instr(left_block, &load->node);

    destroy_block(right_block);
    return left_block;
}

static struct rose_block *add_ternary(struct rose_ctx *ctx, struct rose_block *cond_block,
        struct rose_block *left_block, struct rose_block *right_block, const struct rose_location *loc)
{
    struct rose_ir_node *cond = node_from_block(cond_block);
    struct rose_type *right_type, *left_type, *common_type;
    struct rose_string_buffer *left_string, *right_string;
    struct rose_ir_node *iff, *store, *right, *left;
    struct rose_ir_load *load;
    struct rose_ir_var *var;

    right = add_implicit_pointer_promotion(ctx, right_block, node_from_block(right_block));
    left = add_implicit_pointer_promotion(ctx, left_block, node_from_block(left_block));
    right_type = right->data_type;
    left_type = left->data_type;
    right_string = rose_type_to_string(ctx, right_type);
    left_string = rose_type_to_string(ctx, left_type);

    if (rose_types_are_equal(left_type, right_type, false))
    {
        common_type = left_type;
    }
    else if (left_type->class == ROSE_CLASS_NUMERIC && right_type->class == ROSE_CLASS_NUMERIC)
    {
        common_type = rose_get_scalar_type(ctx, expr_common_base_type(left_type->base_type, right_type->base_type));
    }
    else if (left_type->class == ROSE_CLASS_POINTER && right_type->class == ROSE_CLASS_POINTER)
    {
        common_type = get_common_pointer_type(ctx, left_type, right_type, loc);
    }
    else
    {
        rose_error(ctx, loc, "Incompatible types '%s' and '%s'.",
                left_string->buffer, right_string->buffer);
        common_type = ctx->builtin_types.error;
    }

    rose_release_string_buffer(ctx, left_string);
    rose_release_string_buffer(ctx, right_string);

    cond = rose_new_cast(ctx, cond, rose_get_scalar_type(ctx, ROSE_TYPE_BOOL), loc);
    rose_block_add_instr(cond_block, cond);

    left = rose_new_cast(ctx, left, common_type, loc);
    rose_block_add_instr(left_block, left);

    right = rose_new_cast(ctx, right, common_type, loc);
    rose_block_add_instr(right_block, right);

    var = rose_new_synthetic_var(ctx, "ternary", common_type, ctx->cur_function, loc);

    store = rose_new_simple_store(ctx, var, left);
    rose_block_add_instr(left_block, store);

    store = rose_new_simple_store(ctx, var, right);
    rose_block_add_instr(right_block, store);

    iff = rose_new_if(ctx, cond, left_block, right_block, loc);
    rose_block_add_instr(cond_block, iff);

    load = rose_new_var_load(ctx, var, loc);
    rose_block_add_instr(cond_block, &load->node);

    return cond_block;
}

}

%locations
%define parse.error verbose
%define api.prefix {rose_yy}
%define api.pure full
%expect 1
%lex-param {yyscan_t scanner}
%parse-param {void *scanner}
%parse-param {struct rose_ctx *ctx}

%union
{
    struct rose_type *type;
    int intval;
    char *name;
    uint32_t modifiers;
    struct rose_ir_node *instr;
    struct rose_block *block;
    struct list *list;
    struct parse_decl_spec decl_spec;
    struct parse_fields fields;
    struct rose_function *function;
    struct parse_parameter parameter;
    struct rose_func_parameters parameters;
    struct parse_initializer initializer;
    struct parse_args args;
    struct parse_type_chain chain;
    struct parse_variable_def *variable_def;
    struct parse_if_body if_body;
    enum parse_assign_op assign_op;
}

%token KW__BOOL
%token KW__CDECL
%token KW__FAR
%token KW__HUGE
%token KW__NEAR
%token KW__PASCAL
%token KW_ASM
%token KW_BREAK
%token KW_CHAR
%token KW_CONST
%token KW_CONTINUE
%token KW_DO
%token KW_DOUBLE
%token KW_ELSE
%token KW_EXTERN
%token KW_FOR
%token KW_IF
%token KW_INLINE
%token KW_INT
%token KW_LONG
%token KW_RETURN
%token KW_REGISTER
%token KW_SHORT
%token KW_SIGNED
%token KW_SIZEOF
%token KW_STATIC
%token KW_STRUCT
%token KW_SWITCH
%token KW_TYPEDEF
%token KW_UNSIGNED
%token KW_VOID
%token KW_VOLATILE
%token KW_WHILE

%token OP_INC
%token OP_DEC
%token OP_AND
%token OP_OR
%token OP_EQ
%token OP_LEFTSHIFT
%token OP_LEFTSHIFTASSIGN
%token OP_RIGHTSHIFT
%token OP_RIGHTSHIFTASSIGN
%token OP_ELLIPSIS
%token OP_LE
%token OP_GE
%token OP_NE
%token OP_ADDASSIGN
%token OP_SUBASSIGN
%token OP_MULASSIGN
%token OP_DIVASSIGN
%token OP_MODASSIGN
%token OP_ANDASSIGN
%token OP_ORASSIGN
%token OP_XORASSIGN
%token OP_POINTER

%token <intval> C_INTEGER

%type <list> type_specs
%type <list> variables_def
%type <list> variables_def_typed

%token <name> VAR_IDENTIFIER
%token <name> NEW_IDENTIFIER
%token <name> STRING
%token <name> TYPE_IDENTIFIER

%type <args> func_arguments
%type <args> initializer_expr_list

%type <assign_op> assign_op

%type <block> add_expr
%type <block> assignment_expr
%type <block> bitand_expr
%type <block> bitor_expr
%type <block> bitxor_expr
%type <block> compound_statement
%type <block> conditional_expr
%type <block> declaration
%type <block> declaration_statement
%type <block> equality_expr
%type <block> expr
%type <block> expr_optional
%type <block> expr_statement
%type <block> for_init
%type <block> initializer_expr
%type <block> jump_statement
%type <block> logicand_expr
%type <block> logicor_expr
%type <block> loop_statement
%type <block> mul_expr
%type <block> postfix_expr
%type <block> primary_expr
%type <block> relational_expr
%type <block> shift_expr
%type <block> selection_statement
%type <block> statement
%type <block> statement_list
%type <block> udt_declaration_without_vars
%type <block> unary_expr

%type <chain> abstract_declarator
%type <chain> abstract_direct_declarator

%type <decl_spec> decl_spec

%type <fields> field
%type <fields> fields_list

%type <function> func_prototype

%type <initializer> complex_initializer
%type <initializer> complex_initializer_list

%type <if_body> if_body

%type <intval> array

%type <modifiers> var_modifier
%type <modifiers> var_modifiers

%type <name> any_identifier
%type <name> string_literal

%type <parameter> parameter

%type <parameters> param_list
%type <parameters> param_list_no_vararg
%type <parameters> parameters

%type <type> any_type
%type <type> udt_spec

%type <variable_def> declarator
%type <variable_def> direct_declarator
%type <variable_def> variable_def
%type <variable_def> variable_def_typed

%%

rose_prog:
      %empty
    | rose_prog func_declaration
    | rose_prog declaration_statement
        {
            destroy_block($2);
        }
    | rose_prog asm_statement
    | rose_prog ';'

asm_statement:
      KW_ASM '(' string_literal ')'
        {
            char *parsed;
            size_t len;

            if (rose_parse_string($3, &parsed, &len))
            {
                struct rose_code code = {.code = parsed, .size = len};

                rose_array_reserve(ctx, (void **)&ctx->asm_blocks, &ctx->asm_blocks_capacity,
                        ctx->asm_block_count + 1, sizeof(*ctx->asm_blocks));
                asm_compile(&code, &ctx->asm_blocks[ctx->asm_block_count++]);
            }
            else
            {
                rose_error(ctx, &@1, "Failed to parse string.");
            }
        }

udt_declaration_without_vars:
      var_modifiers udt_spec ';'
        {
            if (!$2->name)
                rose_error(ctx, &@2, "Anonymous struct type must declare a variable.");

            if ($1)
                rose_error(ctx, &@1, "Modifiers are not allowed on struct type declarations.");

            $$ = make_empty_block(ctx);
        }

udt_spec:
      KW_STRUCT any_identifier '{' fields_list '}'
        {
            if (rose_get_type(ctx->cur_scope, ROSE_TYPE_PREFIX_STRUCT, $2, false))
            {
                rose_error(ctx, &@2, "'%s' is already defined as a struct.", $2);
                $$ = ctx->builtin_types.error;
            }
            else
            {
                $$ = rose_new_struct_type(ctx, $2, $4.fields, $4.count);

                if (rose_get_var(ctx->cur_scope, $2))
                {
                    rose_error(ctx, &@2, "\"%s\" is already declared as a variable.", $2);
                    YYABORT;
                }
            }
        }
    | KW_STRUCT '{' fields_list '}'
        {
            $$ = rose_new_struct_type(ctx, NULL, $3.fields, $3.count);
        }

any_identifier:
      VAR_IDENTIFIER
    | TYPE_IDENTIFIER
    | NEW_IDENTIFIER

fields_list:
      %empty
        {
            $$.fields = NULL;
            $$.count = 0;
            $$.capacity = 0;
        }
    | fields_list field
        {
            size_t i;

            for (i = 0; i < $2.count; ++i)
            {
                const struct rose_struct_field *field = &$2.fields[i];
                const struct rose_struct_field *existing;

                if ((existing = get_struct_field($1.fields, $1.count, field->name)))
                {
                    rose_error(ctx, &field->loc, "Field \"%s\" is already defined.", field->name);
                    rose_note(ctx, &existing->loc, ROSE_LOG_ERROR, "'%s' was previously defined here.", field->name);
                }
            }

            rose_array_reserve(ctx, (void **)&$1.fields, &$1.capacity, $1.count + $2.count, sizeof(*$1.fields));
            memcpy($1.fields + $1.count, $2.fields, $2.count * sizeof(*$2.fields));
            $1.count += $2.count;
            rose_free($2.fields);

            $$ = $1;
        }

field:
      decl_spec variables_def ';'
        {
            if ($1.storage_modifiers)
                rose_error(ctx, &@1, "Modifiers are not allowed on struct fields.");
            if (!gen_struct_fields(ctx, &$$, $1.type, $2))
                YYABORT;
        }

func_declaration:
      func_prototype '{' statement_list '}'
        {
            if ($1->has_body)
            {
                rose_error(ctx, &$1->loc, "Function \"%s\" is already defined.", $1->name);
                rose_note(ctx, &$1->loc, ROSE_LOG_ERROR, "\"%s\" was previously defined here.", $1->name);
                destroy_block($3);
            }
            else
            {
                $1->has_body = true;
                rose_block_add_block(&$1->body, $3);
                destroy_block($3);
            }
            rose_pop_scope(ctx);
        }

func_prototype:
    /* GCC doesn't work like this; it really yields a syntax error if this is
     * anything but a true function declaration. I'm sure there's a clever way
     * to make this a syntax error, but, meh? */
      decl_spec declarator
        {
            unsigned int modifiers = $1.storage_modifiers;
            struct rose_ir_var *var;
            struct rose_type *type;

            if (!$2->chain.count || $2->chain.elements[0].type != TYPE_CHAIN_FUNC)
            {
                rose_error(ctx, &@2, "Syntax error while parsing function definition.");
                YYABORT;
            }

            type = apply_type_chain(ctx, $1.type, &$2->chain, &@2);

            if ((var = rose_get_var(ctx->globals, $2->name)))
            {
                if (!rose_types_are_equal(var->data_type, type, true))
                {
                    rose_error(ctx, &@2, "\"%s\" is already declared as a variable.", $2->name);
                    rose_note(ctx, &var->loc, ROSE_LOG_ERROR, "\"%s\" was previously declared here.", $2->name);
                }
            }
            else
            {
                var = rose_new_var(ctx, rose_strdup(ctx, $2->name), type, NULL, modifiers, &@2);
                list_remove(&var->scope_entry);
                list_add_tail(&ctx->globals->vars, &var->scope_entry);
            }

            if (($$ = rose_get_function(ctx, $2->name)))
            {
                if (!rose_types_are_equal(type, $$->data_type->e.func.return_type, true))
                {
                    rose_error(ctx, &@2, "\"%s\" was already declared with a different return type.", $2->name);
                    rose_note(ctx, &$$->loc, ROSE_LOG_ERROR, "\"%s\" was previously declared here.", $2->name);
                }

                if (modifiers != $$->storage_modifiers)
                    rose_error(ctx, &@2, "\"%s\" was already declared with a different storage class.", $2->name);

                free_parse_variable_def($2);
            }
            else
            {
                $$ = rose_new_function(ctx, $2->name, type, &$2->chain.elements[0].u.parameters, modifiers, &@2);
            }

            ctx->cur_function = $$;
            /* Aaand push the function's scope again. We don't need to worry
             * about stacking; its upper is already globals. */
            assert(ctx->cur_scope == ctx->globals);
            assert($$->parameters.scope->upper == ctx->globals);
            ctx->cur_scope = $$->parameters.scope;
        }

compound_statement:
      '{' scope_start statement_list '}'
        {
            rose_pop_scope(ctx);
            $$ = $3;
        }

scope_start:
      %empty
        {
            rose_push_scope(ctx);
        }

parameters:
    /* Forbid empty (K&R style) parameter lists. We are being intentionally
     * non-conformant here. */
      scope_start param_list
        {
            $$ = $2;

            /* Scope handling gets complicated. Function bodies need to be in
             * the scope of their declarators (to look up their parameters), but
             * lots of other places use declarators, and a declarator isn't
             * necessarily exactly one function anyway. So what we do is: pop
             * the scope here, but keep track of it in the chain, and if we're
             * actually going to define the function, push it again. */
            $$.scope = ctx->cur_scope;
            rose_pop_scope(ctx);
        }

param_list:
      param_list_no_vararg
    | param_list_no_vararg ',' OP_ELLIPSIS
        {
            $$ = $1;
            $$.vararg = true;
        }

param_list_no_vararg:
      parameter
        {
            memset(&$$, 0, sizeof($$));
            add_func_parameter(ctx, &$$, &$1, &@1);
        }
    | param_list_no_vararg ',' parameter
        {
            $$ = $1;
            add_func_parameter(ctx, &$$, &$3, &@3);
        }

parameter:
      decl_spec declarator
        {
            $$.type = apply_type_chain(ctx, $1.type, &$2->chain, &@2);
            $$.modifiers = $1.storage_modifiers;

            $$.name = $2->name;
            $2->name = NULL;
            free_parse_variable_def($2);
        }
    | decl_spec abstract_declarator
        {
            $$.type = apply_type_chain(ctx, $1.type, &$2, &@2);
            $$.modifiers = $1.storage_modifiers;

            $$.name = NULL;
            free_parse_type_chain(&$2);
        }

signed_opt:
      %empty
    | KW_SIGNED

any_type:
      TYPE_IDENTIFIER
        {
            $$ = rose_get_type(ctx->cur_scope, ROSE_TYPE_PREFIX_NONE, $1, true);
            assert($$);
            rose_free($1);
        }
    | KW_STRUCT any_identifier
        {
            if (!($$ = rose_get_type(ctx->cur_scope, ROSE_TYPE_PREFIX_STRUCT, $2, true)))
            {
                rose_error(ctx, &@2, "Type '%s' is not defined as a struct.", $2);
                $$ = ctx->builtin_types.error;
            }
            rose_free($2);
        }
    | udt_spec
    | KW__BOOL
        {
            $$ = rose_get_scalar_type(ctx, ROSE_TYPE_BOOL);
        }
    | signed_opt KW_CHAR
        {
            $$ = rose_get_scalar_type(ctx, ROSE_TYPE_SCHAR);
        }
    | KW_UNSIGNED KW_CHAR
        {
            $$ = rose_get_scalar_type(ctx, ROSE_TYPE_UCHAR);
        }
    | signed_opt KW_SHORT
        {
            $$ = rose_get_scalar_type(ctx, ROSE_TYPE_SSHORT);
        }
    | KW_UNSIGNED KW_SHORT
        {
            $$ = rose_get_scalar_type(ctx, ROSE_TYPE_USHORT);
        }
    | signed_opt KW_INT
        {
            $$ = rose_get_scalar_type(ctx, ROSE_TYPE_SINT);
        }
    | KW_UNSIGNED KW_INT
        {
            $$ = rose_get_scalar_type(ctx, ROSE_TYPE_UINT);
        }
    | signed_opt KW_LONG
        {
            $$ = rose_get_scalar_type(ctx, ROSE_TYPE_SLONG);
        }
    | KW_UNSIGNED KW_LONG
        {
            $$ = rose_get_scalar_type(ctx, ROSE_TYPE_ULONG);
        }
    | KW_VOID
        {
            $$ = ctx->builtin_types.Void;
        }

declaration_statement:
      declaration
    | udt_declaration_without_vars
    | typedef
        {
            $$ = make_empty_block(ctx);
        }

typedef:
      KW_TYPEDEF decl_spec type_specs ';'
        {
            if ($2.storage_modifiers)
                rose_error(ctx, &@1, "Storage modifiers are not allowed on typedefs.");

            add_typedef(ctx, $2.type, $3);
        }

type_specs:
      declarator
        {
            $$ = make_empty_list(ctx);
            list_add_head($$, &$1->entry);
        }
    | type_specs ',' declarator
        {
            $$ = $1;
            list_add_tail($$, &$3->entry);
        }

declaration:
      variables_def_typed ';'
        {
            $$ = initialize_vars(ctx, $1);
        }

variables_def:
      variable_def
        {
            $$ = make_empty_list(ctx);
            list_add_head($$, &$1->entry);
        }
    | variables_def ',' variable_def
        {
            $$ = $1;
            list_add_tail($$, &$3->entry);
        }

variables_def_typed:
      variable_def_typed
        {
            $$ = make_empty_list(ctx);
            list_add_head($$, &$1->entry);

            declare_var(ctx, $1);
        }
    | variables_def_typed ',' variable_def
        {
            struct parse_variable_def *head_def;

            assert(!list_empty($1));
            head_def = LIST_ENTRY(list_head($1), struct parse_variable_def, entry);

            assert(head_def->basic_type);
            $3->basic_type = head_def->basic_type;
            $3->modifiers = head_def->modifiers;
            $3->modifiers_loc = head_def->modifiers_loc;

            declare_var(ctx, $3);

            $$ = $1;
            list_add_tail($$, &$3->entry);
        }

declarator:
      direct_declarator
    | '*' var_modifiers declarator
        {
            $$ = $3;
            $$->chain.elements = rose_realloc(ctx, $$->chain.elements,
                    ($$->chain.count + 1) * sizeof(*$$->chain.elements));
            $$->chain.elements[$$->chain.count].type = TYPE_CHAIN_POINTER;
            $$->chain.elements[$$->chain.count].u.modifiers = $2;
            ++$$->chain.count;
        }

direct_declarator:
      any_identifier
        {
            $$ = rose_alloc(ctx, sizeof(*$$));
            $$->loc = @1;
            $$->name = $1;
        }
    | '(' declarator ')'
        {
            $$ = $2;
        }
    | direct_declarator array
        {
            $$ = $1;
            $$->chain.elements = rose_realloc(ctx, $$->chain.elements,
                    ($$->chain.count + 1) * sizeof(*$$->chain.elements));
            $$->chain.elements[$$->chain.count].type = TYPE_CHAIN_ARRAY;
            $$->chain.elements[$$->chain.count].u.array_size = $2;
            ++$$->chain.count;
        }
    | direct_declarator '(' parameters ')'
        {
            $$ = $1;

            if ($3.count == 1 && !$3.vars[0]->name && $3.vars[0]->data_type->class == ROSE_CLASS_VOID)
                $3.count = 0;

            $$->chain.elements = rose_realloc(ctx, $$->chain.elements,
                    ($$->chain.count + 1) * sizeof(*$$->chain.elements));
            $$->chain.elements[$$->chain.count].type = TYPE_CHAIN_FUNC;
            $$->chain.elements[$$->chain.count].u.parameters = $3;
            ++$$->chain.count;
        }

variable_def:
      declarator
    | declarator '=' complex_initializer
        {
            $$ = $1;
            $$->initializer = $3;
        }

variable_def_typed:
      decl_spec variable_def
        {
            $$ = $2;
            $$->basic_type = $1.type;
            $$->modifiers = $1.storage_modifiers;
            $$->modifiers_loc = @1;
        }

array:
      '[' ']'
        {
            $$ = ROSE_ARRAY_ELEMENTS_COUNT_IMPLICIT;
        }
    | '[' expr ']'
        {
            $$ = evaluate_static_expression_as_uint(ctx, $2, &@2);

            destroy_block($2);

            if (!$$)
                rose_error(ctx, &@2, "Array size is not a positive integer constant.");

            if ($$ > 65536)
                rose_error(ctx, &@2, "Array size %u is not between 1 and 65536.", $$);
        }

abstract_declarator:
      abstract_direct_declarator
    | '*' var_modifiers abstract_declarator
        {
            $$ = $3;
            $$.elements = rose_realloc(ctx, $$.elements, ($$.count + 1) * sizeof(*$$.elements));
            $$.elements[$$.count].type = TYPE_CHAIN_POINTER;
            $$.elements[$$.count].u.modifiers = $2;
            ++$$.count;
        }

abstract_direct_declarator:
      %empty
        {
            memset(&$$, 0, sizeof($$));
        }
    | '(' abstract_declarator ')'
        {
            $$ = $2;
        }
    | abstract_direct_declarator array
        {
            $$ = $1;
            $$.elements = rose_realloc(ctx, $$.elements, ($$.count + 1) * sizeof(*$$.elements));
            $$.elements[$$.count].type = TYPE_CHAIN_ARRAY;
            $$.elements[$$.count].u.array_size = $2;
            ++$$.count;
        }

var_modifier:
      KW_EXTERN
        {
            $$ = ROSE_STORAGE_EXTERN;
        }
    | KW_STATIC
        {
            $$ = ROSE_STORAGE_STATIC;
        }
    | KW_VOLATILE
        {
            $$ = ROSE_MODIFIER_VOLATILE;
        }
    | KW_CONST
        {
            $$ = ROSE_MODIFIER_CONST;
        }
    | KW_INLINE
        {
            $$ = ROSE_MODIFIER_INLINE;
        }
    | KW__CDECL
        {
            $$ = ROSE_MODIFIER_CDECL;
        }
    | KW__NEAR
        {
            $$ = ROSE_MODIFIER_NEAR;
        }
    | KW__FAR
        {
            $$ = ROSE_MODIFIER_FAR;
        }
    | KW__HUGE
        {
            /* Just alias this to _far. */
            $$ = ROSE_MODIFIER_FAR;
        }
    | KW__PASCAL
        {
            $$ = ROSE_MODIFIER_PASCAL;
        }

var_modifiers:
      %empty
        {
            $$ = 0;
        }
    | var_modifiers var_modifier
        {
            $$ = add_modifiers(ctx, $1, $2, &@2);
        }

decl_spec:
    /* This isn't conformant, but it covers the way declspecs are actually
     * written. The trouble with being conformant is that
     *   - it forces us to handle stupid declarations where we don't get
     *     exactly one type
     *   - it's hard to tell the parser whether "typedef int apple; int apple;"
     *     is a variable or not
     */
      var_modifiers any_type var_modifiers
        {
            uint32_t modifiers = add_modifiers(ctx, $1, $3, &@3);
            $$.type = apply_type_modifiers(ctx, $2, &modifiers, &@$);
            $$.storage_modifiers = modifiers;
        }

comma_opt:
      %empty
    | ','

complex_initializer:
      initializer_expr
        {
            memset(&$$, 0, sizeof($$));
            $$.arg.instr = node_from_block($1);
            $$.arg.loc = @$;
            $$.instrs = $1;
        }
    | '{' complex_initializer_list comma_opt '}'
        {
            $$ = $2;
        }

complex_initializer_list:
      complex_initializer
        {
            memset(&$$, 0, sizeof($$));
            rose_array_reserve(ctx, (void **)&$$.arg.braced_list,
                    &$$.arg.braced_capacity, 1, sizeof(*$$.arg.braced_list));
            $$.arg.braced_list[$$.arg.braced_count++] = $1.arg;
            $$.arg.loc = @$;
            $$.instrs = $1.instrs;
        }
    | complex_initializer_list ',' complex_initializer
        {
            $$ = $1;
            rose_array_reserve(ctx, (void **)&$$.arg.braced_list, &$$.arg.braced_capacity,
                    $$.arg.braced_count + 1, sizeof(*$$.arg.braced_list));
            $$.arg.braced_list[$$.arg.braced_count++] = $3.arg;
            $$.arg.loc = @$;
            rose_block_add_block($$.instrs, $3.instrs);
            destroy_block($3.instrs);
        }

initializer_expr:
      assignment_expr

initializer_expr_list:
      initializer_expr
        {
            $$.args_count = 1;
            $$.args = rose_alloc(ctx, sizeof(*$$.args));
            $$.args[0] = node_from_block($1);
            $$.instrs = $1;
            $$.loc = @$;
        }
    | initializer_expr_list ',' initializer_expr
        {
            $$ = $1;
            $$.args = rose_realloc(ctx, $$.args, ($$.args_count + 1) * sizeof(*$$.args));
            $$.args[$$.args_count++] = node_from_block($3);
            rose_block_add_block($$.instrs, $3);
            destroy_block($3);
        }

statement_list:
      %empty
        {
            $$ = make_empty_block(ctx);
        }
    | statement_list statement
        {
            $$ = $1;
            rose_block_add_block($$, $2);
            destroy_block($2);
        }

statement:
      declaration_statement
    | expr_statement
    | compound_statement
    | jump_statement
    | selection_statement
    | loop_statement

jump_statement:
      KW_RETURN expr ';'
        {
            $$ = $2;
            add_return(ctx, $$, node_from_block($$), &@1);
        }
    | KW_RETURN ';'
        {
            $$ = make_empty_block(ctx);
            add_return(ctx, $$, NULL, &@1);
        }
    | KW_BREAK
        {
            struct rose_ir_node *jump;

            $$ = make_empty_block(ctx);
            jump = rose_new_jump(ctx, ROSE_IR_JUMP_BREAK, NULL, &@1);
            rose_block_add_instr($$, jump);
        }
    | KW_CONTINUE
        {
            struct rose_ir_node *jump;

            $$ = make_empty_block(ctx);
            jump = rose_new_jump(ctx, ROSE_IR_JUMP_CONTINUE, NULL, &@1);
            rose_block_add_instr($$, jump);
        }

selection_statement:
      KW_IF '(' expr ')' if_body
        {
            struct rose_ir_node *condition = node_from_block($3);
            struct rose_ir_node *instr;

            $$ = $3;

            check_condition_type(ctx, condition);

            instr = rose_new_if(ctx, condition, $5.then_block, $5.else_block, &@1);
            rose_block_add_instr($$, instr);

            destroy_block($5.then_block);
            destroy_block($5.else_block);
        }

if_body:
      statement
        {
            $$.then_block = $1;
            $$.else_block = NULL;
        }
    | statement KW_ELSE statement
        {
            $$.then_block = $1;
            $$.else_block = $3;
        }

for_init:
      expr_statement
    | declaration

loop_statement:
      KW_WHILE '(' expr ')' statement
        {
            struct rose_ir_node *loop;

            append_conditional_break(ctx, $3);
            rose_block_add_block($3, $5);
            destroy_block($5);

            loop = rose_new_loop(ctx, $3, NULL, &@$);
            destroy_block($3);

            $$ = make_block(ctx, loop);
        }
    | KW_DO statement KW_WHILE '(' expr ')' ';'
        {
            struct rose_ir_node *loop;

            append_conditional_break(ctx, $5);

            loop = rose_new_loop(ctx, $2, $5, &@$);
            destroy_block($2);
            destroy_block($5);

            $$ = make_block(ctx, loop);
        }
    | KW_FOR '(' scope_start for_init expr_statement expr_optional ')' statement
        {
            struct rose_ir_node *loop;

            $$ = $4;

            append_conditional_break(ctx, $5);
            rose_block_add_block($5, $8);
            destroy_block($8);

            loop = rose_new_loop(ctx, $5, $6, &@$);
            destroy_block($5);
            destroy_block($6);

            rose_block_add_instr($$, loop);
            rose_pop_scope(ctx);
        }

expr_optional:
      %empty
        {
            $$ = make_empty_block(ctx);
        }
    | expr

expr_statement:
      expr_optional ';'
        {
            $$ = $1;
        }

func_arguments:
      %empty
        {
            $$.args = NULL;
            $$.args_count = 0;
            $$.instrs = make_empty_block(ctx);
            $$.loc = @$;
        }
    | initializer_expr_list

string_literal:
      STRING
    | string_literal STRING
        {
            size_t len1 = strlen($1), len2 = strlen($2);

            $$ = malloc(len1 + len2 + 1);
            memcpy($$, $1, len1);
            memcpy($$ + len1, $2, len2 + 1);
        }

primary_expr:
      C_INTEGER
        {
            enum rose_base_type base_type;
            struct rose_ir_node *c;

            if ($1 >= INT16_MIN && $1 <= INT16_MAX)
                base_type = ROSE_TYPE_SINT;
            else
                base_type = ROSE_TYPE_SLONG;

            c = rose_new_int_constant(ctx, base_type, $1, &@1);
            $$ = make_block(ctx, c);
        }
    | VAR_IDENTIFIER
        {
            struct rose_ir_load *load;
            struct rose_ir_var *var;

            if ((var = rose_get_var(ctx->cur_scope, $1)))
            {
                load = rose_new_var_load(ctx, var, &@1);
                $$ = make_block(ctx, &load->node);
            }
            else
            {
                rose_error(ctx, &@1, "Variable \"%s\" is not defined.", $1);
                rose_free($1);

                $$ = make_empty_block(ctx);
                $$->value = ctx->error_instr;
            }
        }
    | '(' expr ')'
        {
            $$ = $2;
        }
    | NEW_IDENTIFIER
        {
            rose_error(ctx, &@1, "Identifier \"%s\" is not declared.", $1);
            rose_free($1);

            $$ = make_empty_block(ctx);
            $$->value = ctx->error_instr;
        }
    | string_literal
        {
            $$ = make_empty_block(ctx);
            add_string_constant(ctx, $$, $1, &@1);
        }
    | KW_SIZEOF '(' decl_spec abstract_declarator ')'
        {
            struct rose_type *type;

            if ($3.storage_modifiers)
                rose_error(ctx, &@3, "Storage modifiers are not allowed on casts.");

            type = apply_type_chain(ctx, $3.type, &$4, &@4);
            $$ = create_sizeof(ctx, type, &@$);
        }
    | KW_SIZEOF '(' expr ')'
        {
            /* It's legal to put expressions with side-effects in here, but
             * they get ignored. */
            $$ = create_sizeof(ctx, node_from_block($3)->data_type, &@$);
            destroy_block($3);
        }

postfix_expr:
      primary_expr
    | postfix_expr OP_INC
        {
            add_increment(ctx, $1, false, true, &@2);
            $$ = $1;
        }
    | postfix_expr OP_DEC
        {
            add_increment(ctx, $1, true, true, &@2);
            $$ = $1;
        }
    | postfix_expr '.' any_identifier
        {
            $$ = $1;
            add_record_access(ctx, $$, $3, &@2);
            rose_free($3);
        }
    | postfix_expr OP_POINTER any_identifier
        {
            $$ = $1;
            add_record_deref(ctx, $$, $3, &@2);
            rose_free($3);
        }
    | postfix_expr '[' expr ']'
        {
            struct rose_ir_node *array = node_from_block($1), *index = node_from_block($3);

            rose_block_add_block($3, $1);
            destroy_block($1);

            if (!add_array_access(ctx, $3, array, index, &@2))
            {
                destroy_block($3);
                YYABORT;
            }
            $$ = $3;
        }
    | postfix_expr '(' func_arguments ')'
        {
            $$ = $1;
            add_call(ctx, $$, &$3, &@1);
        }

unary_expr:
      postfix_expr
    | OP_INC unary_expr
        {
            add_increment(ctx, $2, false, false, &@1);
            $$ = $2;
        }
    | OP_DEC unary_expr
        {
            add_increment(ctx, $2, true, false, &@1);
            $$ = $2;
        }
    | '+' unary_expr
        {
            $$ = $2;
        }
    | '-' unary_expr
        {
            add_unary_arithmetic_expr(ctx, $2, ROSE_OP1_NEG, node_from_block($2), &@1);
            $$ = $2;
        }
    | '~' unary_expr
        {
            add_unary_bitwise_expr(ctx, $2, ROSE_OP1_BIT_NOT, node_from_block($2), &@1);
            $$ = $2;
        }
    | '!' unary_expr
        {
            add_unary_logical_expr(ctx, $2, ROSE_OP1_LOGIC_NOT, node_from_block($2), &@1);
            $$ = $2;
        }
    | '&' unary_expr
        {
            $$ = $2;

            add_addr(ctx, $$, node_from_block($$), &@1);
        }
    | '*' unary_expr
        {
            struct rose_ir_node *src = node_from_block($2);
            struct rose_ir_node *load;

            $$ = $2;

            if (src->data_type->class == ROSE_CLASS_ARRAY)
            {
                /* Implement this as a load of element 0.
                 * That way, pointer derefs only apply to true pointer types,
                 * which simplifies the IR a bit. */
                struct rose_ir_node *c;

                c = rose_new_uint_constant(ctx, ROSE_TYPE_UINT, 0, &@$);
                rose_block_add_instr($$, c);
                load = rose_new_index(ctx, src, c, &@$);
                rose_block_add_instr($$, load);
            }
            else if (src->data_type->class == ROSE_CLASS_POINTER)
            {
                if (src->data_type->e.ptr.type->class == ROSE_CLASS_VOID)
                    rose_error(ctx, &@1, "Void pointers cannot be dereferenced.");

                load = rose_new_pointer_load(ctx, src, &@$);
                rose_block_add_instr($$, load);
            }
            else
            {
                struct rose_string_buffer *string;

                if ((string = rose_type_to_string(ctx, src->data_type)))
                    rose_error(ctx, &@1, "Cannot dereference type '%s'.", string->buffer);
                rose_release_string_buffer(ctx, string);
                $$->value = ctx->error_instr;
            }
        }
    | '(' decl_spec abstract_declarator ')' unary_expr
        {
            if ($2.storage_modifiers)
                rose_error(ctx, &@2, "Storage modifiers are not allowed on casts.");

            add_explicit_conversion(ctx, $5, $2.type, &$3, &@2);
            free_parse_type_chain(&$3);
            $$ = $5;
        }

mul_expr:
      unary_expr
    | mul_expr '*' unary_expr
        {
            $$ = add_binary_expr_merge(ctx, $1, $3, ROSE_OP2_MUL, &@2);
        }
    | mul_expr '/' unary_expr
        {
            $$ = add_binary_expr_merge(ctx, $1, $3, ROSE_OP2_DIV, &@2);
        }
    | mul_expr '%' unary_expr
        {
            $$ = add_binary_expr_merge(ctx, $1, $3, ROSE_OP2_MOD, &@2);
        }

add_expr:
      mul_expr
    | add_expr '+' mul_expr
        {
            $$ = add_binary_expr_merge(ctx, $1, $3, ROSE_OP2_ADD, &@2);
        }
    | add_expr '-' mul_expr
        {
            $$ = add_binary_expr_merge(ctx, $1, $3, ROSE_OP2_SUB, &@2);
        }

shift_expr:
      add_expr
    | shift_expr OP_LEFTSHIFT add_expr
        {
            $$ = add_binary_expr_merge(ctx, $1, $3, ROSE_OP2_LSHIFT, &@2);
        }
    | shift_expr OP_RIGHTSHIFT add_expr
        {
            $$ = add_binary_expr_merge(ctx, $1, $3, ROSE_OP2_RSHIFT, &@2);
        }

relational_expr:
      shift_expr
    | relational_expr '<' shift_expr
        {
            $$ = add_binary_expr_merge(ctx, $1, $3, ROSE_OP2_LESS, &@2);
        }
    | relational_expr '>' shift_expr
        {
            $$ = add_binary_expr_merge(ctx, $1, $3, ROSE_OP2_GREATER, &@2);
        }
    | relational_expr OP_LE shift_expr
        {
            $$ = add_binary_expr_merge(ctx, $1, $3, ROSE_OP2_LEQUAL, &@2);
        }
    | relational_expr OP_GE shift_expr
        {
            $$ = add_binary_expr_merge(ctx, $1, $3, ROSE_OP2_GEQUAL, &@2);
        }

equality_expr:
      relational_expr
    | equality_expr OP_EQ relational_expr
        {
            $$ = add_binary_expr_merge(ctx, $1, $3, ROSE_OP2_EQUAL, &@2);
        }
    | equality_expr OP_NE relational_expr
        {
            $$ = add_binary_expr_merge(ctx, $1, $3, ROSE_OP2_NEQUAL, &@2);
        }

bitand_expr:
      equality_expr
    | bitand_expr '&' equality_expr
        {
            $$ = add_binary_expr_merge(ctx, $1, $3, ROSE_OP2_BIT_AND, &@2);
        }

bitxor_expr:
      bitand_expr
    | bitxor_expr '^' bitand_expr
        {
            $$ = add_binary_expr_merge(ctx, $1, $3, ROSE_OP2_BIT_XOR, &@2);
        }

bitor_expr:
      bitxor_expr
    | bitor_expr '|' bitxor_expr
        {
            $$ = add_binary_expr_merge(ctx, $1, $3, ROSE_OP2_BIT_OR, &@2);
        }

logicand_expr:
      bitor_expr
    | logicand_expr OP_AND bitor_expr
        {
            $$ = add_logic_and_or(ctx, $1, $3, false, &@2);
        }

logicor_expr:
      logicand_expr
    | logicor_expr OP_OR logicand_expr
        {
            $$ = add_logic_and_or(ctx, $1, $3, true, &@2);
        }

conditional_expr:
      logicor_expr
    | logicor_expr '?' expr ':' assignment_expr
        {
            add_ternary(ctx, $1, $3, $5, &@2);
        }

assignment_expr:

      conditional_expr
    | unary_expr assign_op assignment_expr
        {
            struct rose_ir_node *lhs = node_from_block($1), *rhs = node_from_block($3);

            if (lhs->data_type->modifiers & ROSE_MODIFIER_CONST)
                rose_error(ctx, &@2, "Statement modifies a const expression.");
            rose_block_add_block($3, $1);
            destroy_block($1);
            add_assignment(ctx, $3, lhs, $2, rhs);
            $$ = $3;
        }

assign_op:
      '='
        {
            $$ = ASSIGN_OP_ASSIGN;
        }
    | OP_ADDASSIGN
        {
            $$ = ASSIGN_OP_ADD;
        }
    | OP_SUBASSIGN
        {
            $$ = ASSIGN_OP_SUB;
        }
    | OP_MULASSIGN
        {
            $$ = ASSIGN_OP_MUL;
        }
    | OP_DIVASSIGN
        {
            $$ = ASSIGN_OP_DIV;
        }
    | OP_MODASSIGN
        {
            $$ = ASSIGN_OP_MOD;
        }
    | OP_LEFTSHIFTASSIGN
        {
            $$ = ASSIGN_OP_LSHIFT;
        }
    | OP_RIGHTSHIFTASSIGN
        {
            $$ = ASSIGN_OP_RSHIFT;
        }
    | OP_ANDASSIGN
        {
            $$ = ASSIGN_OP_AND;
        }
    | OP_ORASSIGN
        {
            $$ = ASSIGN_OP_OR;
        }
    | OP_XORASSIGN
        {
            $$ = ASSIGN_OP_XOR;
        }

expr:
      assignment_expr
    | expr ',' assignment_expr
        {
            $$ = $1;
            rose_block_add_block($$, $3);
            destroy_block($3);
        }
