/*
 * Copyright 2023 Elizabeth Figura
 *
 * This file is part of the Rosé compiler.
 *
 * Rosé is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rosé is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Rosé; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef __ROSE_H
#define __ROSE_H

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(*(x)))
#define CONTAINING_RECORD(address, type, field) \
        ((type *)((char *)(address) - offsetof(type, field)))

#define min(a, b) (((a) <= (b)) ? (a) : (b))
#define max(a, b) (((a) >= (b)) ? (a) : (b))

#ifdef __GNUC__
# define DECLSPEC_NORETURN __attribute__((noreturn))
# ifdef __MINGW_PRINTF_FORMAT
#  define PRINTF_FUNC(fmt, args) __attribute__((format(__MINGW_PRINTF_FORMAT, fmt, args)))
# else
#  define PRINTF_FUNC(fmt, args) __attribute__((format(printf, fmt, args)))
# endif
#else
# define DECLSPEC_NORETURN
# define PRINTF_FUNC(fmt, args)
#endif  /* __GNUC__ */

DECLSPEC_NORETURN static inline void rose_unreachable_(const char *filename, unsigned int line)
{
    fprintf(stderr, "%s:%u: Aborting, reached unreachable code.\n", filename, line);
    abort();
}

#define rose_unreachable() rose_unreachable_(__FILE__, __LINE__)

enum rose_log_level
{
    ROSE_LOG_ERROR,
    ROSE_LOG_WARNING,
};

struct rose_location
{
    const char *source_name;
    unsigned int line, column;
};

enum rose_result
{
    ROSE_OK = 0,
    ROSE_ERROR_INVALID_SHADER,
    ROSE_ERROR_NOT_IMPLEMENTED,
};

void array_reserve(void **elements, size_t *capacity, size_t element_count, size_t element_size);

static inline int rose_u32_compare(uint32_t x, uint32_t y)
{
    return (x > y) - (x < y);
}

struct rose_string_buffer
{
    char *buffer;
    size_t buffer_size, content_size;
};

struct rose_string_buffer_cache
{
    struct rose_string_buffer **buffers;
    size_t count, max_count, capacity;
};

void rose_string_buffer_cleanup(struct rose_string_buffer *buffer);
struct rose_string_buffer *rose_string_buffer_get(struct rose_string_buffer_cache *list);
void rose_string_buffer_init(struct rose_string_buffer *buffer);
void rose_string_buffer_cache_cleanup(struct rose_string_buffer_cache *list);
void rose_string_buffer_cache_init(struct rose_string_buffer_cache *list);
void rose_string_buffer_printf(struct rose_string_buffer *buffer, const char *format, ...) PRINTF_FUNC(2, 3);
void rose_string_buffer_release(struct rose_string_buffer_cache *list, struct rose_string_buffer *buffer);
#define rose_string_buffer_trace(buffer) \
        rose_string_buffer_trace_(buffer, __FUNCTION__)
void rose_string_buffer_trace_(const struct rose_string_buffer *buffer, const char *function);
void rose_string_buffer_vprintf(struct rose_string_buffer *buffer, const char *format, va_list args);

uint32_t rose_parse_integer(const char *s);
bool rose_parse_string(const char *string, char **ret_string, size_t *ret_len);

enum rose_dbg_level
{
    ROSE_DBG_LEVEL_NONE,
    ROSE_DBG_LEVEL_ERR,
    ROSE_DBG_LEVEL_FIXME,
    ROSE_DBG_LEVEL_WARN,
    ROSE_DBG_LEVEL_TRACE,
};

extern enum rose_dbg_level rose_dbg_level;

void rose_dbg_printf(enum rose_dbg_level level, const char *function, const char *fmt, ...) PRINTF_FUNC(3, 4);

#define ROSE_DBG_LOG(level) \
        do { \
        const enum rose_dbg_level rose_dbg_level = ROSE_DBG_LEVEL_##level; \
        ROSE_DBG_PRINTF

#define ROSE_DBG_PRINTF(...) \
        rose_dbg_printf(rose_dbg_level, __FUNCTION__, __VA_ARGS__); } while (0)

#define TRACE ROSE_DBG_LOG(TRACE)
#define WARN  ROSE_DBG_LOG(WARN)
#define FIXME ROSE_DBG_LOG(FIXME)
#define ERR   ROSE_DBG_LOG(ERR)
#define TRACE_ON() (rose_dbg_level == ROSE_DBG_LEVEL_TRACE)

struct rose_code
{
    const void *code;
    size_t size;
};

struct rose_export
{
    const char *name;
    uint16_t ordinal;
    struct rose_def *module;
};

struct rose_def
{
    bool dll;
    const char *name;
    struct rose_export *exports;
    size_t export_count, exports_capacity;

    /* for ld */
    bool used;
    uint16_t import_index;
    uint16_t imptab_offset;
};

struct rose_program
{
    struct x86_block *blocks;
    size_t block_count, blocks_capacity;

    struct rose_def def;

    struct rose_def *imports;
    size_t import_count, imports_capacity;
};

enum rose_result rose_compile_shader(const struct rose_code *source,
        const char *source_name, struct rose_program *out);

void rose_link(struct rose_program *prog, struct rose_code *output);

#endif
