/*
 * Copyright 2023 Elizabeth Figura
 *
 * This file is part of the Rosé compiler.
 *
 * Rosé is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rosé is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Rosé; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef __ROSE_X86_H
#define __ROSE_X86_H

enum x86_opcode
{
    X86_OP_AAA,
    X86_OP_AAS,
    X86_OP_ADC,
    X86_OP_ADD,
    X86_OP_AND,
    X86_OP_ARPL,
    X86_OP_BOUND,
    X86_OP_CALL,
    X86_OP_CBW,
    X86_OP_CLC,
    X86_OP_CLD,
    X86_OP_CLI,
    X86_OP_CMC,
    X86_OP_CMP,
    X86_OP_CMPS,
    X86_OP_CWD,
    X86_OP_DAA,
    X86_OP_DAS,
    X86_OP_DEC,
    X86_OP_DIV,
    X86_OP_ENTER,
    X86_OP_HLT,
    X86_OP_IDIV,
    X86_OP_IMUL,
    X86_OP_INC,
    X86_OP_INT,
    X86_OP_INT3,
    X86_OP_INTO,
    X86_OP_IRET,
    X86_OP_JA,
    X86_OP_JAE,
    X86_OP_JB,
    X86_OP_JBE,
    X86_OP_JCXZ,
    X86_OP_JG,
    X86_OP_JGE,
    X86_OP_JL,
    X86_OP_JLE,
    X86_OP_JMP,
    X86_OP_JNO,
    X86_OP_JNP,
    X86_OP_JNS,
    X86_OP_JNZ,
    X86_OP_JO,
    X86_OP_JP,
    X86_OP_JS,
    X86_OP_JZ,
    X86_OP_LAHF,
    X86_OP_LCALL,
    X86_OP_LDS,
    X86_OP_LEA,
    X86_OP_LEAVE,
    X86_OP_LES,
    X86_OP_LFS,
    X86_OP_LGS,
    X86_OP_LJMP,
    X86_OP_LODS,
    X86_OP_LOOP,
    X86_OP_LOOPNZ,
    X86_OP_LOOPZ,
    X86_OP_LRET,
    X86_OP_LSS,
    X86_OP_MOV,
    X86_OP_MOVS,
    X86_OP_MOVSB,
    X86_OP_MOVSW,
    X86_OP_MOVZB,
    X86_OP_MOVZW,
    X86_OP_MUL,
    X86_OP_NEG,
    X86_OP_NOP,
    X86_OP_NOT,
    X86_OP_OR,
    X86_OP_POP,
    X86_OP_POPA,
    X86_OP_POPF,
    X86_OP_PUSH,
    X86_OP_PUSHA,
    X86_OP_PUSHF,
    X86_OP_RCL,
    X86_OP_RCR,
    X86_OP_RET,
    X86_OP_ROL,
    X86_OP_ROR,
    X86_OP_SAHF,
    X86_OP_SAL,
    X86_OP_SAR,
    X86_OP_SBB,
    X86_OP_SCAS,
    X86_OP_SETA,
    X86_OP_SETAE,
    X86_OP_SETB,
    X86_OP_SETBE,
    X86_OP_SETG,
    X86_OP_SETGE,
    X86_OP_SETL,
    X86_OP_SETLE,
    X86_OP_SETNO,
    X86_OP_SETNP,
    X86_OP_SETNS,
    X86_OP_SETNZ,
    X86_OP_SETO,
    X86_OP_SETP,
    X86_OP_SETS,
    X86_OP_SETZ,
    X86_OP_SHL,
    X86_OP_SHLD,
    X86_OP_SHR,
    X86_OP_SHRD,
    X86_OP_STOS,
    X86_OP_STC,
    X86_OP_STD,
    X86_OP_STI,
    X86_OP_SUB,
    X86_OP_TEST,
    X86_OP_WAIT,
    X86_OP_XCHG,
    X86_OP_XOR,

    X86_OP_DATA,
};

enum x86_argtype
{
    X86_ARG_NONE = 0,
    X86_ARG_REG8,
    X86_ARG_REG16,
    X86_ARG_SEG,
    X86_ARG_MEM,
    X86_ARG_IMM,
    X86_ARG_ABSREG,
    X86_ARG_ABSMEM,
};

enum x86_reg8
{
    X86_REG8_AL = 0,
    X86_REG8_CL = 1,
    X86_REG8_DL = 2,
    X86_REG8_BL = 3,
    X86_REG8_AH = 4,
    X86_REG8_CH = 5,
    X86_REG8_DH = 6,
    X86_REG8_BH = 7,
};

enum x86_reg
{
    X86_REG_AX = 0,
    X86_REG_CX = 1,
    X86_REG_DX = 2,
    X86_REG_BX = 3,
    X86_REG_SP = 4,
    X86_REG_BP = 5,
    X86_REG_SI = 6,
    X86_REG_DI = 7,
};

enum x86_seg
{
    X86_SEG_NONE = -1,
    X86_SEG_ES = 0,
    X86_SEG_CS = 1,
    X86_SEG_SS = 2,
    X86_SEG_DS = 3,
    X86_SEG_FS = 4,
    X86_SEG_GS = 5,
};

enum x86_mem_reg
{
    X86_MEM_REG_BX_SI = 0,
    X86_MEM_REG_BX_DI = 1,
    X86_MEM_REG_BP_SI = 2,
    X86_MEM_REG_BP_DI = 3,
    X86_MEM_REG_SI = 4,
    X86_MEM_REG_DI = 5,
    X86_MEM_REG_BP = 6,
    X86_MEM_REG_BX = 7,
    X86_MEM_REG_NONE = 8, /* no disp */
};

struct x86_integer
{
    /* reloc is represented as an index into the x86 block's reloc list.
     * Zero is reserved and means no reloc. */
    uint32_t reloc;
    int32_t offset;
};

struct x86_arg
{
    enum x86_argtype type;
    union
    {
        enum x86_reg8 reg8;
        enum x86_reg reg;
        enum x86_seg seg;
        struct x86_integer imm;
        struct x86_mem
        {
            enum x86_seg seg;
            enum x86_mem_reg reg;
            struct x86_integer offset;
        } mem;
    } u;
};

struct x86_instr
{
    size_t byte_offset;
    int8_t size;
    enum x86_opcode opcode;
    struct x86_arg args[3];
};

struct x86_reloc
{
    enum x86_reloc_type
    {
        /* Unresolved, but should resolve to a local symbol. */
        X86_RELOC_UNRESOLVED_LOCAL,
        /* Should resolve to a cross-segment relocation; take the segment part. */
        X86_RELOC_UNRESOLVED_SEGMENT,
        /* Should resolve to a cross-segment relocation; take the offset part. */
        X86_RELOC_UNRESOLVED_OFFSET,
        /* Should resolve to an internal or external relocation; take the pair. */
        X86_RELOC_UNRESOLVED_PAIR,
        /* Reference to a symbol in the same segment; not a relocation in the
         * final executable. */
        X86_RELOC_LOCAL,
        /* Segment of a cross-segment relocation. Type 0, size 2. */
        X86_RELOC_INTERNAL_SEGMENT,
        /* Offset of a cross-segment relocation.
         * Not actually written, but would be type 0, size 5. */
        X86_RELOC_INTERNAL_OFFSET,
        /* Segment/offset pair of a cross-segment relocation.
         * We write the segment but leave the offset alone, so this is also
         * encoded as type 0, size 2. */
        X86_RELOC_INTERNAL_PAIR,
        /* External segment/offset pair. Type 1 or 2, size 3. */
        X86_RELOC_EXTERNAL_PAIR,
    } type;
    union
    {
        const char *name;
        const struct x86_symbol *symbol;
        const struct rose_export *import;
    } u;
    /* previous location in the chain; set when linking */
    const struct x86_reloc *prev;
    /* byte offset of this relocation */
    size_t byte_offset;
};

struct x86_symbol
{
    const char *name;
    size_t instr_offset;
    bool global;
    /* These can't be set until the block is done being built */
    const struct x86_instr *instr;
    const struct x86_block *block;
};

struct x86_buffer
{
    bool writing;
    enum rose_result result;
    uint8_t *output;
    size_t size, capacity;
};

struct x86_block
{
    bool data;

    struct x86_instr *instrs;
    size_t count, capacity;

    struct x86_symbol *symbols;
    size_t symbol_count, symbols_capacity;

    struct x86_reloc *relocs;
    size_t reloc_count, reloc_capacity;

    struct ld_segment *segment;
};

void x86_emit(struct x86_block *block, enum x86_opcode opcode, int8_t size);
void x86_emit_i(struct x86_block *block, enum x86_opcode opcode, int32_t r1, int8_t size);
void x86_emit_l(struct x86_block *block, enum x86_opcode opcode, uint32_t r1);
void x86_emit_r(struct x86_block *block, enum x86_opcode opcode, enum x86_reg r1, int8_t size);
void x86_emit_r8(struct x86_block *block, enum x86_opcode opcode, enum x86_reg8 r1);
void x86_emit_s(struct x86_block *block, enum x86_opcode opcode, enum x86_seg r1);
void x86_emit_ir8(struct x86_block *block, enum x86_opcode opcode, int32_t r1, enum x86_reg8 r2);
void x86_emit_ir(struct x86_block *block, enum x86_opcode opcode, int32_t r1, enum x86_reg r2, int8_t size);
void x86_emit_rr8(struct x86_block *block, enum x86_opcode opcode, enum x86_reg8 r1, enum x86_reg8 r2);
void x86_emit_rr(struct x86_block *block, enum x86_opcode opcode, enum x86_reg r1, enum x86_reg r2, int8_t size);
void x86_emit_mov_rs(struct x86_block *block, enum x86_reg r1, enum x86_seg r2);
void x86_emit_mov_sr(struct x86_block *block, enum x86_seg r1, enum x86_reg r2);

uint32_t x86_add_reloc(struct x86_block *block, enum x86_reloc_type type, const char *name);
void x86_add_instr(struct x86_block *block, const struct x86_instr *instr);
struct x86_symbol *x86_add_symbol(struct x86_block *block, const char *name);
struct x86_symbol *x86_find_symbol(const struct x86_block *block, const char *name);
const char *x86_reg_get_name(enum x86_reg reg);
const char *x86_seg_get_name(enum x86_seg seg);
void x86_write_block(struct x86_buffer *buffer, struct x86_block *block);
uint32_t x86_symbol_get_byte_offset(const struct x86_block *block, const struct x86_symbol *symbol);


#endif
