/*
 * Copyright 2023 Elizabeth Figura
 *
 * This file is part of the Rosé compiler.
 *
 * Rosé is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rosé is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Rosé; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

/*
 * This file incorporates code taken from the vkd3d-shader HLSL compiler and
 * covered under the following license:
 *
 * Copyright 2012 Matteo Bruni for CodeWeavers
 * Copyright 2019-2020 Zebediah Figura for CodeWeavers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef __ROSE_C_H
#define __ROSE_C_H

#include <assert.h>
#include "rose.h"
#include "x86.h"
#include "list.h"
#include "rbtree.h"

/* The general IR structure is inspired by Mesa GLSL hir, even though the code
 * ends up being quite different in practice. Anyway, here comes the relevant
 * licensing information.
 *
 * Copyright © 2010 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

enum rose_type_prefix
{
    /* NONE includes typedefs. */
    ROSE_TYPE_PREFIX_NONE,
    ROSE_TYPE_PREFIX_STRUCT,
    ROSE_TYPE_PREFIX_COUNT,
};

enum rose_type_class
{
    ROSE_CLASS_NUMERIC,
    ROSE_CLASS_STRUCT,
    ROSE_CLASS_ARRAY,
    ROSE_CLASS_VOID,
    ROSE_CLASS_ERROR,
    ROSE_CLASS_POINTER,
    ROSE_CLASS_FUNCTION,
};

enum rose_base_type
{
    ROSE_TYPE_UCHAR,
    ROSE_TYPE_USHORT,
    ROSE_TYPE_UINT,
    ROSE_TYPE_ULONG,
    ROSE_TYPE_SCHAR,
    ROSE_TYPE_SSHORT,
    ROSE_TYPE_SINT,
    ROSE_TYPE_SLONG,
    ROSE_TYPE_BOOL,
    ROSE_TYPE_LAST_SCALAR = ROSE_TYPE_BOOL,
};

/* An C source-level data type, including anonymous structs and typedefs. */
struct rose_type
{
    /* Item entry in rose_ctx->types. */
    struct list entry;
    /* Item entry in rose_scope->types. rose_type->name is used as key (if not NULL). */
    struct rb_entry scope_entry;

    enum rose_type_class class;
    /* Base type, for ROSE_CLASS_NUMERIC. */
    enum rose_base_type base_type;

    enum rose_type_prefix prefix;
    /* Name, in case the type is a named struct or a typedef. */
    const char *name;
    /* Bitfield for storing type modifiers, subset of ROSE_TYPE_MODIFIERS_MASK.
     * Modifiers that don't fall inside this mask are to be stored in the variable in
     *   rose_ir_var.modifiers, or in the struct field in rose_ir_field.modifiers. */
    unsigned int modifiers;

    union
    {
        /* Additional information if type is ROSE_CLASS_STRUCT. */
        struct
        {
            struct rose_struct_field *fields;
            size_t field_count;
        } record;
        /* Additional information if type is ROSE_CLASS_ARRAY. */
        struct
        {
            struct rose_type *type;
            /* Array length, or ROSE_ARRAY_ELEMENTS_COUNT_IMPLICIT if it is not known yet at parse time. */
            unsigned int elements_count;
        } array;
        struct
        {
            struct rose_type *type;
        } ptr;
        struct
        {
            struct rose_type *return_type;
            struct rose_type **param_types;
            size_t param_count;
            bool vararg;
        } func;
    } e;
};

/* A field within a struct type declaration, used in rose_type.e.fields. */
struct rose_struct_field
{
    struct rose_location loc;
    struct rose_type *type;
    const char *name;
};

struct rose_reg
{
    bool allocated;
    enum x86_reg reg;
    enum x86_seg seg;
};

/* Types of instruction nodes for the IR.
 * Each type of instruction node is associated to a struct with the same name in lower case.
 *   e.g. for ROSE_IR_CONSTANT there exists struct rose_ir_constant.
 * Each one of these structs start with a struct rose_ir_node field, so pointers to values of these
 *   types can be casted seamlessly to (struct rose_ir_node *) and vice-versa. */
enum rose_ir_node_type
{
    ROSE_IR_ADDR,
    ROSE_IR_CALL,
    ROSE_IR_CONSTANT,
    ROSE_IR_EXPR,
    ROSE_IR_IF,
    ROSE_IR_INDEX,
    ROSE_IR_LOAD,
    ROSE_IR_LOOP,
    ROSE_IR_JUMP,
    ROSE_IR_STORE,
};

/* Common data for every type of IR instruction node. */
struct rose_ir_node
{
    /* Item entry for storing the instruction in a list of instructions. */
    struct list entry;

    /* Type of node, which means that a pointer to this struct rose_ir_node can be casted to a
     *   pointer to the struct with the same name. */
    enum rose_ir_node_type type;
    /* C data type of the node, when used by other nodes as a source (through an rose_src).
     * ROSE_IR_CONSTANT, ROSE_IR_EXPR, ROSE_IR_LOAD
     *   have a data type and can be used through an rose_src; other types of node don't. */
    struct rose_type *data_type;

    /* List containing all the struct rose_src·s that point to this node; linked by the
     *   rose_src.entry fields. */
    struct list uses;

    struct rose_location loc;

    unsigned int index, last_read;
    struct rose_reg reg;
};

struct rose_block
{
    /* List containing instruction nodes; linked by the rose_ir_node.entry fields. */
    struct list instrs;
    /* Instruction representing the "value" of this block, if applicable.
     * This may point to an instruction outside of this block! */
    struct rose_ir_node *value;
};

/* A reference to an instruction node (struct rose_ir_node), usable as a field in other structs.
 *   struct rose_src is more powerful than a mere pointer to an rose_ir_node because it also
 *   contains a linked list item entry, which is used by the referenced instruction node to keep
 *   track of all the rose_src·s that reference it.
 * This allows replacing any rose_ir_node with any other in all the places it is used, or checking
 *   that a node has no uses before it is removed. */
struct rose_src
{
    struct rose_ir_node *node;
    /* Item entry for node->uses. */
    struct list entry;
};

/* Reference to a variable, or a part of it (e.g. a field within a struct). */
struct rose_deref
{
    struct rose_ir_var *var;
    struct rose_src ptr;

    /* An array of references to instruction nodes, of data type uint, that are used to reach the
     *   desired part of the variable.
     * If path_len is 0, then this is a reference to the whole variable.
     * The value of each instruction node in the path corresponds to the index of the element/field
     *   that has to be selected on each nesting level to reach this part.
     * The path shall not contain additional values once a type that cannot be subdivided
     *   (a.k.a. "component") is reached. */
    unsigned int path_len;
    struct rose_src *path;

    /* Byte offset, post-lowering. */
    struct rose_src offset;
};

#define ROSE_STORAGE_EXTERN          0x00000001
#define ROSE_STORAGE_STATIC          0x00000002
#define ROSE_MODIFIER_VOLATILE       0x00000004
#define ROSE_MODIFIER_CONST          0x00000008
#define ROSE_MODIFIER_FAR            0x00000010
#define ROSE_MODIFIER_CDECL          0x00000020
#define ROSE_MODIFIER_PASCAL         0x00000040
#define ROSE_MODIFIER_INLINE         0x00000080
#define ROSE_MODIFIER_NEAR           0x00000100

#define ROSE_TYPE_MODIFIERS_MASK     (ROSE_MODIFIER_VOLATILE | ROSE_MODIFIER_CONST \
        | ROSE_MODIFIER_NEAR | ROSE_MODIFIER_FAR | ROSE_MODIFIER_CDECL | ROSE_MODIFIER_PASCAL | ROSE_MODIFIER_INLINE)

#define ROSE_ARRAY_ELEMENTS_COUNT_IMPLICIT 0

struct rose_ir_var
{
    struct rose_type *data_type;
    struct rose_location loc;
    const char *name;
    /* Bitfield for storage modifiers (type modifiers are stored in data_type->modifiers). */
    unsigned int storage_modifiers;

    /* Function to which this belongs, if any, including for params. */
    const struct rose_function *func;

    /* Item entry in rose_scope.vars. Specifically rose_ctx.globals.vars if the variable is global. */
    struct list scope_entry;
    /* Item entry in rose_ctx.extern_vars, if the variable is extern. */
    struct list extern_entry;

    bool defined;

    /* Only meaningful for function-scoped non-static variables. */
    unsigned int first_write, last_read;

    int16_t frame_offset;
};

/* Sized array of variables representing a function's parameters. */
struct rose_func_parameters
{
    struct rose_ir_var **vars;
    size_t count, capacity;
    bool vararg;
    struct rose_scope *scope;
};

struct rose_function
{
    /* Item entry in rose_ctx.functions */
    struct rb_entry entry;

    const char *name;

    /* Data type of the entire function. */
    struct rose_type *data_type;

    /* Storage modifiers. */
    unsigned int storage_modifiers;

    struct rose_location loc;

    struct rose_func_parameters parameters;

    struct rose_block body;
    bool has_body;

    /* Callee-save registers. %bp is also callee-save, but we always use it
     * as the frame pointer.
     *
     * Agner Fog doesn't mention this but %fs and %gs do seem to be callee-save,
     * at least from examining mspam.dll. */
    bool save_si, save_di, save_fs, save_gs;
    bool use_ds;
    /* Saved registers + all locals. */
    uint16_t frame_size;
    /* Label for exiting the function after setting ax/dx. */
    uint32_t return_reloc;
};

struct rose_ir_call
{
    struct rose_ir_node node;
    /* For a direct call this is a var deref, which always has to have zero
     * length. For an indirect call this can be anything. It has to be a deref
     * in that case too because we can't lcall an arbitrary register. */
    struct rose_deref func;
    /* We need to store the func type because emit_call() needs to know it
     * even after the deref has been lowered to offsets.
     * This points to the *original* type, which might be a pointer, because
     * emit_call() needs to know whether this is direct or indirect. */
    const struct rose_type *func_type;
    size_t arg_count;
    struct rose_src args[];
};

struct rose_ir_if
{
    struct rose_ir_node node;
    struct rose_src condition;
    struct rose_block then_block;
    struct rose_block else_block;
};

struct rose_ir_loop
{
    struct rose_ir_node node;
    struct rose_block body;
    /* "iter" is what gets applied at the end of the loop, after a "continue". */
    struct rose_block iter;
    unsigned int next_index;

    /* Symbol to jump to when performing a break. */
    uint32_t break_reloc;
    /* Symbol to jump to when performing a continue. */
    uint32_t continue_reloc;
};

enum rose_ir_expr_op
{
    ROSE_OP0_ERROR,
    ROSE_OP0_VOID,

    ROSE_OP1_BIT_NOT,
    ROSE_OP1_CAST,
    ROSE_OP1_LOGIC_NOT,
    ROSE_OP1_NEG,
    ROSE_OP1_OFFSETOF,
    ROSE_OP1_SELECTOROF,

    /* WARNING: ADD and SUB can operate on pointer types, but regardless of the
     * pointer type, the offset is specified in bytes in the IR! */

    ROSE_OP2_ADD,
    ROSE_OP2_BIT_AND,
    ROSE_OP2_BIT_OR,
    ROSE_OP2_BIT_XOR,
    ROSE_OP2_DIV,
    ROSE_OP2_EQUAL,
    ROSE_OP2_GEQUAL,
    ROSE_OP2_GREATER,
    ROSE_OP2_LEQUAL,
    ROSE_OP2_LESS,
    ROSE_OP2_LSHIFT,
    ROSE_OP2_MOD,
    ROSE_OP2_MUL,
    ROSE_OP2_NEQUAL,
    ROSE_OP2_RSHIFT,
    ROSE_OP2_SUB,
};

#define ROSE_MAX_OPERANDS 3

struct rose_ir_expr
{
    struct rose_ir_node node;
    enum rose_ir_expr_op op;
    struct rose_src operands[ROSE_MAX_OPERANDS];
};

enum rose_ir_jump_type
{
    ROSE_IR_JUMP_BREAK,
    ROSE_IR_JUMP_CONTINUE,
    ROSE_IR_JUMP_RETURN,
};

struct rose_ir_jump
{
    struct rose_ir_node node;
    enum rose_ir_jump_type type;
    struct rose_src return_value;
};

struct rose_ir_index
{
    struct rose_ir_node node;
    struct rose_src val, idx;
};

struct rose_ir_load
{
    struct rose_ir_node node;
    struct rose_deref src;
};

struct rose_ir_store
{
    struct rose_ir_node node;
    struct rose_deref lhs;
    struct rose_src rhs;
};

struct rose_ir_addr
{
    struct rose_ir_node node;
    struct rose_deref src;
};

struct rose_ir_constant
{
    struct rose_ir_node node;
    struct rose_constant_value
    {
        union rose_constant_value_component
        {
            uint32_t u;
            int32_t i;
            struct
            {
                const char *str;
                size_t len; /* including terminating null */
            } str;
        } u;
    } value;
};

struct rose_scope
{
    /* Item entry for rose_ctx.scopes. */
    struct list entry;

    /* List containing the variables declared in this scope; linked by rose_ir_var->scope_entry. */
    struct list vars;
    /* Tree map containing the types declared in this scope, using rose_tree.name as key.
     * The types are attached through the rose_type.scope_entry fields. */
    struct rb_tree types[ROSE_TYPE_PREFIX_COUNT];
    /* Scope containing this scope. This value is NULL for the global scope. */
    struct rose_scope *upper;
};

struct rose_ctx
{
    const char **source_files;
    size_t source_file_count, source_files_capacity;
    /* Current location being read in the C source, updated while parsing. */
    struct rose_location location;
    /* Cache for temporary string allocations. */
    struct rose_string_buffer_cache string_buffers;
    enum rose_result result;

    /* Pointer to an opaque data structure managed by FLEX (during lexing), that encapsulates the
     *   current state of the scanner. This pointer is required by all FLEX API functions when the
     *   scanner is declared as reentrant, which is the case. */
    void *scanner;

    /* Pointer to the current scope; changes as the parser reads the code. */
    struct rose_scope *cur_scope;
    /* Scope of global variables. */
    struct rose_scope *globals;
    /* List of all the scopes in the program; linked by the rose_scope.entry fields. */
    struct list scopes;
    /* List of all the extern variables; linked by the rose_ir_var.extern_entry fields.
     * This exists as a convenience because it is often necessary to iterate all extern variables
     *   and these can be declared in global scope, as function parameters, or as the function
     *   return value. */
    struct list extern_vars;

    /* List containing all created rose_types, except builtin_types; linked by the rose_type.entry
     *   fields. */
    struct list types;
    /* Tree map for the declared functions, using rose_function.name as key.
     * The functions are attached through the rose_function.entry fields. */
    struct rb_tree functions;
    /* Pointer to the current function; changes as the parser reads the code.
     * Reused to point to the current function being written in codegen.
     */
    const struct rose_function *cur_function;

    /* Basic data types stored for convenience. */
    struct
    {
        struct rose_type *scalar[ROSE_TYPE_LAST_SCALAR + 1];
        struct rose_type *Void;
        struct rose_type *error;
    } builtin_types;

    /* Pre-allocated "error" expression. */
    struct rose_ir_node *error_instr;

    /* List of the instruction nodes for initializing static variables. */
    struct rose_block static_initializers;

    /* Assembly functions. */
    struct x86_block *asm_blocks;
    size_t asm_block_count, asm_blocks_capacity;

    /* Output block for global data.
     * This gets written at parse time rather than codegen time. */
    struct x86_block data_block;

    /* Counter for generating private symbol names. */
    unsigned int symbol_counter;

    /* Innermost loop we are currently emitting code for. */
    struct rose_ir_loop *innermost_loop;
};

enum rose_error_level
{
    ROSE_LEVEL_ERROR = 0,
    ROSE_LEVEL_WARNING,
    ROSE_LEVEL_NOTE,
};

static inline struct rose_ir_addr *rose_ir_addr(const struct rose_ir_node *node)
{
    assert(node->type == ROSE_IR_ADDR);
    return CONTAINING_RECORD(node, struct rose_ir_addr, node);
}

static inline struct rose_ir_call *rose_ir_call(const struct rose_ir_node *node)
{
    assert(node->type == ROSE_IR_CALL);
    return CONTAINING_RECORD(node, struct rose_ir_call, node);
}

static inline struct rose_ir_constant *rose_ir_constant(const struct rose_ir_node *node)
{
    assert(node->type == ROSE_IR_CONSTANT);
    return CONTAINING_RECORD(node, struct rose_ir_constant, node);
}

static inline struct rose_ir_expr *rose_ir_expr(const struct rose_ir_node *node)
{
    assert(node->type == ROSE_IR_EXPR);
    return CONTAINING_RECORD(node, struct rose_ir_expr, node);
}

static inline struct rose_ir_if *rose_ir_if(const struct rose_ir_node *node)
{
    assert(node->type == ROSE_IR_IF);
    return CONTAINING_RECORD(node, struct rose_ir_if, node);
}

static inline struct rose_ir_jump *rose_ir_jump(const struct rose_ir_node *node)
{
    assert(node->type == ROSE_IR_JUMP);
    return CONTAINING_RECORD(node, struct rose_ir_jump, node);
}

static inline struct rose_ir_load *rose_ir_load(const struct rose_ir_node *node)
{
    assert(node->type == ROSE_IR_LOAD);
    return CONTAINING_RECORD(node, struct rose_ir_load, node);
}

static inline struct rose_ir_loop *rose_ir_loop(const struct rose_ir_node *node)
{
    assert(node->type == ROSE_IR_LOOP);
    return CONTAINING_RECORD(node, struct rose_ir_loop, node);
}

static inline struct rose_ir_store *rose_ir_store(const struct rose_ir_node *node)
{
    assert(node->type == ROSE_IR_STORE);
    return CONTAINING_RECORD(node, struct rose_ir_store, node);
}

static inline struct rose_ir_index *rose_ir_index(const struct rose_ir_node *node)
{
    assert(node->type == ROSE_IR_INDEX);
    return CONTAINING_RECORD(node, struct rose_ir_index, node);
}

static inline void rose_block_init(struct rose_block *block)
{
    list_init(&block->instrs);
    block->value = NULL;
}

static inline void rose_block_add_instr(struct rose_block *block, struct rose_ir_node *instr)
{
    list_add_tail(&block->instrs, &instr->entry);
    block->value = (instr->data_type ? instr : NULL);
}

static inline void rose_block_add_block(struct rose_block *block, struct rose_block *add)
{
    list_move_tail(&block->instrs, &add->instrs);
    block->value = add->value;
}

static inline void rose_src_from_node(struct rose_src *src, struct rose_ir_node *node)
{
    src->node = node;
    if (node)
        list_add_tail(&node->uses, &src->entry);
}

static inline void rose_src_remove(struct rose_src *src)
{
    if (src->node)
        list_remove(&src->entry);
    src->node = NULL;
}

static inline void *rose_alloc(struct rose_ctx *ctx, size_t size)
{
    void *ptr = calloc(1, size);

    if (!ptr)
    {
        ERR("Out of memory.\n");
        abort();
    }
    return ptr;
}

static inline void *rose_calloc(struct rose_ctx *ctx, size_t count, size_t size)
{
    void *ptr = calloc(count, size);

    if (!ptr)
    {
        ERR("Out of memory.\n");
        abort();
    }
    return ptr;
}

static inline void *rose_realloc(struct rose_ctx *ctx, void *ptr, size_t size)
{
    void *ret = realloc(ptr, size);

    if (!ret)
    {
        ERR("Out of memory.\n");
        abort();
    }
    return ret;
}

static inline char *rose_strdup(struct rose_ctx *ctx, const char *string)
{
    size_t len = strlen(string);
    char *ret;

    if ((ret = rose_alloc(ctx, len + 1)))
        memcpy(ret, string, len + 1);
    return ret;
}

static inline void rose_array_reserve(struct rose_ctx *ctx, void **elements,
        size_t *capacity, size_t element_count, size_t element_size)
{
    array_reserve(elements, capacity, element_count, element_size);
}

static inline void rose_free(void *ptr)
{
    free(ptr);
}

static inline struct rose_string_buffer *rose_get_string_buffer(struct rose_ctx *ctx)
{
    return rose_string_buffer_get(&ctx->string_buffers);
}

static inline void rose_release_string_buffer(struct rose_ctx *ctx, struct rose_string_buffer *buffer)
{
    rose_string_buffer_release(&ctx->string_buffers, buffer);
}

static inline struct rose_type *rose_get_scalar_type(const struct rose_ctx *ctx, enum rose_base_type base_type)
{
    return ctx->builtin_types.scalar[base_type];
}

struct rose_string_buffer *rose_type_to_string(struct rose_ctx *ctx, const struct rose_type *type);
struct rose_string_buffer *rose_modifiers_to_string(struct rose_ctx *ctx, unsigned int modifiers);

struct rose_ir_node *rose_add_conditional(struct rose_ctx *ctx, struct rose_block *block,
        struct rose_ir_node *condition, struct rose_ir_node *if_true, struct rose_ir_node *if_false);

void rose_block_cleanup(struct rose_block *block);
void rose_clone_block(struct rose_ctx *ctx, struct rose_block *dst_block,
        const struct rose_block *src_block, struct rose_ir_node **instr);

void rose_dump_block(struct rose_ctx *ctx, const struct rose_block *block);
void rose_dump_function(struct rose_ctx *ctx, const struct rose_function *func);

bool rose_init_deref_from_index_chain(struct rose_ctx *ctx, struct rose_deref *deref, struct rose_ir_node *chain);
void rose_copy_deref(struct rose_ctx *ctx, struct rose_deref *deref, const struct rose_deref *other);
void rose_deref_append_index(struct rose_ctx *ctx, struct rose_deref *deref, struct rose_ir_node *index);
void rose_deref_remove_path(struct rose_deref *deref);
void rose_cleanup_deref(struct rose_deref *deref);

void rose_replace_node(struct rose_ir_node *old, struct rose_ir_node *new);

void rose_free_instr(struct rose_ir_node *node);
void rose_free_instr_list(struct list *list);
void rose_free_type(struct rose_type *type);
void rose_free_var(struct rose_ir_var *decl);

struct rose_function *rose_get_function(struct rose_ctx *ctx, const char *name);
struct rose_type *rose_get_type(struct rose_scope *scope,
        enum rose_type_prefix prefix, const char *name, bool recursive);
struct rose_ir_var *rose_get_var(struct rose_scope *scope, const char *name);

struct rose_ir_node *rose_new_addr(struct rose_ctx *ctx,
        const struct rose_deref *deref, const struct rose_location *loc);
struct rose_type *rose_new_array_type(struct rose_ctx *ctx, struct rose_type *basic_type, unsigned int array_size);
struct rose_ir_node *rose_new_binary_expr(struct rose_ctx *ctx, enum rose_ir_expr_op op, struct rose_ir_node *arg1,
        struct rose_ir_node *arg2);
struct rose_ir_node *rose_new_bool_constant(struct rose_ctx *ctx, bool b, const struct rose_location *loc);
struct rose_ir_node *rose_new_call(struct rose_ctx *ctx, const struct rose_deref *func,
        size_t arg_count, struct rose_ir_node *const *args, const struct rose_location *loc);
struct rose_ir_node *rose_new_cast(struct rose_ctx *ctx, struct rose_ir_node *node, struct rose_type *type,
        const struct rose_location *loc);
struct rose_ir_node *rose_new_constant(struct rose_ctx *ctx, struct rose_type *type,
        const struct rose_constant_value *value, const struct rose_location *loc);
struct rose_ir_node *rose_new_copy(struct rose_ctx *ctx, struct rose_ir_node *node);
struct rose_ir_node *rose_new_expr(struct rose_ctx *ctx, enum rose_ir_expr_op op,
        struct rose_ir_node *operands[ROSE_MAX_OPERANDS],
        struct rose_type *data_type, const struct rose_location *loc);
struct rose_function *rose_new_function(struct rose_ctx *ctx, const char *name, struct rose_type *data_type,
        const struct rose_func_parameters *parameters, unsigned int modifiers, const struct rose_location *loc);
struct rose_type *rose_new_function_type(struct rose_ctx *ctx, struct rose_type *return_type,
        const struct rose_func_parameters *parameters);
struct rose_ir_node *rose_new_if(struct rose_ctx *ctx, struct rose_ir_node *condition,
        struct rose_block *then_block, struct rose_block *else_block, const struct rose_location *loc);
struct rose_ir_node *rose_new_int_constant(struct rose_ctx *ctx,
        enum rose_base_type type, int32_t n, const struct rose_location *loc);
struct rose_ir_node *rose_new_jump(struct rose_ctx *ctx, enum rose_ir_jump_type type,
        struct rose_ir_node *return_value, const struct rose_location *loc);

void rose_init_simple_deref_from_var(struct rose_deref *deref, struct rose_ir_var *var);

struct rose_ir_load *rose_new_var_load(struct rose_ctx *ctx, struct rose_ir_var *var,
        const struct rose_location *loc);
struct rose_ir_load *rose_new_load_index(struct rose_ctx *ctx, const struct rose_deref *deref,
        struct rose_ir_node *idx, const struct rose_location *loc);

struct rose_ir_node *rose_new_simple_store(struct rose_ctx *ctx, struct rose_ir_var *lhs, struct rose_ir_node *rhs);
struct rose_ir_node *rose_new_store_index(struct rose_ctx *ctx, const struct rose_deref *lhs,
        struct rose_ir_node *idx, struct rose_ir_node *rhs, const struct rose_location *loc);

struct rose_ir_node *rose_new_index(struct rose_ctx *ctx, struct rose_ir_node *val,
        struct rose_ir_node *idx, const struct rose_location *loc);
struct rose_ir_node *rose_new_loop(struct rose_ctx *ctx, struct rose_block *block,
        struct rose_block *iter, const struct rose_location *loc);
struct rose_ir_node *rose_new_pointer_load(struct rose_ctx *ctx,
        struct rose_ir_node *instr, const struct rose_location *loc);
struct rose_type *rose_new_pointer_type(struct rose_ctx *ctx, struct rose_type *pointee);
struct rose_ir_node *rose_new_string_constant(struct rose_ctx *ctx,
        const char *str, size_t len, const struct rose_location *loc);
struct rose_type *rose_new_struct_type(struct rose_ctx *ctx, const char *name,
        struct rose_struct_field *fields, size_t field_count);
struct rose_ir_var *rose_new_synthetic_var(struct rose_ctx *ctx, const char *template,
        struct rose_type *type, const struct rose_function *func, const struct rose_location *loc);
struct rose_ir_node *rose_new_uint_constant(struct rose_ctx *ctx,
        enum rose_base_type type, uint32_t n, const struct rose_location *loc);
struct rose_ir_node *rose_new_unary_expr(struct rose_ctx *ctx, enum rose_ir_expr_op op,
        struct rose_ir_node *arg, struct rose_type *data_type, const struct rose_location *loc);
struct rose_ir_var *rose_new_var(struct rose_ctx *ctx, const char *name, struct rose_type *type,
        const struct rose_function *func, unsigned int modifiers, const struct rose_location *loc);

void rose_error(struct rose_ctx *ctx, const struct rose_location *loc,
        const char *fmt, ...) PRINTF_FUNC(3, 4);
void rose_fixme(struct rose_ctx *ctx, const struct rose_location *loc,
        const char *fmt, ...) PRINTF_FUNC(3, 4);
void rose_warning(struct rose_ctx *ctx, const struct rose_location *loc,
        const char *fmt, ...) PRINTF_FUNC(3, 4);
void rose_note(struct rose_ctx *ctx, const struct rose_location *loc,
        enum rose_log_level level, const char *fmt, ...) PRINTF_FUNC(4, 5);

void rose_push_scope(struct rose_ctx *ctx);
void rose_pop_scope(struct rose_ctx *ctx);

struct rose_type *rose_get_element_type_from_path_index(struct rose_ctx *ctx,
        const struct rose_type *type, struct rose_ir_node *idx);
struct rose_type *rose_type_clone(struct rose_ctx *ctx, struct rose_type *old, char *name, unsigned int modifiers);
bool rose_types_are_equal(const struct rose_type *t1, const struct rose_type *t2, bool check_type_qualifiers);
unsigned int rose_type_get_field_offset(const struct rose_type *type, unsigned int field_idx);
unsigned int rose_type_get_size(const struct rose_type *type);

static inline bool rose_type_is_far_pointer(const struct rose_type *type)
{
    return type->class == ROSE_CLASS_POINTER && type->e.ptr.type->modifiers & ROSE_MODIFIER_FAR;
}

struct rose_type *rose_deref_get_type(struct rose_ctx *ctx, const struct rose_deref *deref);

void rose_fold_constants(struct rose_ctx *ctx, struct rose_block *block);

struct rose_ir_node *rose_convert_string_constant(struct rose_ctx *ctx, struct rose_ir_constant *c);

int rose_lexer_compile(struct rose_ctx *ctx, const struct rose_code *source);
void rose_emit_bytecode(struct rose_ctx *ctx);

#endif
