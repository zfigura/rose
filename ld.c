/*
 * Copyright 2023 Elizabeth Figura
 *
 * This file is part of the Rosé compiler.
 *
 * Rosé is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rosé is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Rosé; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "rose.h"
#include "x86.h"

#define SEGMENT_ALIGN_SHIFT 4

struct buffer
{
    uint8_t *buffer;
    size_t size, capacity;

    size_t segtab_ptr;
};

struct ld_reloc_record
{
    const struct x86_reloc *tail;
};

struct ld_segment
{
    struct ld_reloc_record *relocs;
    size_t reloc_count, relocs_capacity;

    size_t byte_size;

    bool data;
};

struct ld_module
{
    struct rose_program *program;

    struct ld_segment *segments;
    size_t segment_count, segments_capacity;

    const struct ld_segment *data_segment;

    const struct x86_symbol *entry_symbol;
    const struct x86_block *entry_block;

    uint16_t used_import_count;
    char *imptab;
    size_t imptab_len;
};

/* These structs from Semblance. */

struct header_mz
{
    uint16_t e_magic;      /* 00: MZ Header signature */
    uint16_t e_cblp;       /* 02: Bytes on last page of file */
    uint16_t e_cp;         /* 04: Pages in file */
    uint16_t e_crlc;       /* 06: Relocations */
    uint16_t e_cparhdr;    /* 08: Size of header in paragraphs */
    uint16_t e_minalloc;   /* 0a: Minimum extra paragraphs needed */
    uint16_t e_maxalloc;   /* 0c: Maximum extra paragraphs needed */
    uint16_t e_ss;         /* 0e: Initial (relative) SS value */
    uint16_t e_sp;         /* 10: Initial SP value */
    uint16_t e_csum;       /* 12: Checksum */
    uint16_t e_ip;         /* 14: Initial IP value */
    uint16_t e_cs;         /* 16: Initial (relative) CS value */
    uint16_t e_lfarlc;     /* 18: File address of relocation table */
    uint16_t e_ovno;       /* 1a: Overlay number */
    uint16_t e_res[4];
    uint16_t e_oemid;
    uint16_t e_oeminfo;
    uint32_t e_res2[5];
    uint32_t e_lfanew;
};

struct header_ne
{
    uint16_t ne_magic;             /* 00 NE signature 'NE' */
    uint8_t  ne_ver;               /* 02 Linker version number */
    uint8_t  ne_rev;               /* 03 Linker revision number */
    uint16_t ne_enttab;            /* 04 Offset to entry table */
    uint16_t ne_cbenttab;          /* 06 Length of entry table in uint8_ts */
    uint32_t ne_crc;               /* 08 Checksum */
    uint16_t ne_flags;             /* 0c Flags about segments in this file */
    uint8_t  ne_autodata;          /* 0e Automatic data segment number */
    uint8_t  ne_unused;            /* 0f */
    uint16_t ne_heap;              /* 10 Initial size of local heap */
    uint16_t ne_stack;             /* 12 Initial size of stack */
    uint16_t ne_ip;                /* 14 Initial IP */
    uint16_t ne_cs;                /* 16 Initial CS */
    uint16_t ne_sp;                /* 18 Initial SP */
    uint16_t ne_ss;                /* 1a Initial SS */
    uint16_t ne_cseg;              /* 1c # of entries in segment table */
    uint16_t ne_cmod;              /* 1e # of entries in import module table */
    uint16_t ne_cbnrestab;         /* 20 Length of nonresident-name table */
    uint16_t ne_segtab;            /* 22 Offset to segment table */
    uint16_t ne_rsrctab;           /* 24 Offset to resource table */
    uint16_t ne_restab;            /* 26 Offset to resident-name table */
    uint16_t ne_modtab;            /* 28 Offset to import module table */
    uint16_t ne_imptab;            /* 2a Offset to name table */
    uint32_t ne_nrestab;           /* 2c ABSOLUTE Offset to nonresident-name table */
    uint16_t ne_cmovent;           /* 30 # of movable entry points */
    uint16_t ne_align;             /* 32 Logical sector alignment shift count */
    uint16_t ne_cres;              /* 34 # of resource segments */
    uint8_t  ne_exetyp;            /* 36 Flags indicating target OS */
    uint8_t  ne_flagsothers;       /* 37 Additional information flags */
    uint16_t ne_pretthunks;        /* 38 Offset to return thunks */
    uint16_t ne_psegrefbytes;      /* 3a Offset to segment ref. uint8_ts */
    uint16_t ne_swaparea;          /* 3c Reserved by Microsoft */
    uint8_t  ne_expver_min;        /* 3e Expected Windows version number (minor) */
    uint8_t  ne_expver_maj;        /* 3f Expected Windows version number (major) */
};

/* These flags from Wine, slightly modified.
 * Copyright information is as follows:
 *
 * Copyright (C) the Wine project
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#define NE_FFLAGS_SINGLEDATA    0x0001
#define NE_FFLAGS_MULTIPLEDATA  0x0002
#define NE_FFLAGS_GLOBALINIT    0x0004
#define NE_FFLAGS_PROTECTEDONLY 0x0008
#define NE_FFLAGS_FRAMEBUF      0x0100
#define NE_FFLAGS_CONSOLE       0x0200
#define NE_FFLAGS_GUI           0x0300
#define NE_FFLAGS_SELFLOAD      0x0800
#define NE_FFLAGS_LINKERROR     0x2000
#define NE_FFLAGS_CALLWEP       0x4000
#define NE_FFLAGS_LIBMODULE     0x8000

#define NE_OSFLAGS_UNKNOWN      0x00
#define NE_OSFLAGS_OS2          0x01
#define NE_OSFLAGS_WINDOWS      0x02

static void DECLSPEC_NORETURN linker_error(const char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    printf("Linker error: ");
    vprintf(fmt, args);
    printf("\n");
    va_end(args);
    exit(ROSE_ERROR_INVALID_SHADER);
}

static void align_buffer(struct buffer *buffer, unsigned int shift)
{
    size_t mask = (1u << shift) - 1;
    size_t aligned_size = (buffer->size + mask) & ~mask;

    if (aligned_size > buffer->size)
    {
        array_reserve((void **)&buffer->buffer, &buffer->capacity, aligned_size, 1);
        memset(buffer->buffer + buffer->size, 0, aligned_size - buffer->size);
        buffer->size = aligned_size;
    }
}

static void write_bytes(struct buffer *buffer, const void *data, size_t size)
{
    array_reserve((void **)&buffer->buffer, &buffer->capacity, buffer->size + size, 1);
    memcpy(buffer->buffer + buffer->size, data, size);
    buffer->size += size;
}

static void write_u8(struct buffer *buffer, uint8_t n)
{
    write_bytes(buffer, &n, sizeof(n));
}

static void write_u16(struct buffer *buffer, uint16_t n)
{
    write_bytes(buffer, &n, sizeof(n));
}

static void set_u16(struct buffer *buffer, size_t offset, uint16_t n)
{
    memcpy(buffer->buffer + offset, &n, sizeof(n));
}

/* sort blocks into segments and initialize the list */
static void init_segments(struct ld_module *module)
{
    bool need_code = false, need_data = false;
    struct ld_segment *segment;

    for (size_t i = 0; i < module->program->block_count; ++i)
    {
        if (module->program->blocks[i].data)
            need_data = true;
        else
            need_code = true;
    }

    /* We need a data segment anyway, for the stack.
     * FIXME: DLLs don't need this. */
    need_data = true;

    /* For now we only have the two segments, code and data. */
    array_reserve((void **)&module->segments, &module->segments_capacity, 2, sizeof(*module->segments));
    module->segment_count = 0;
    memset(module->segments, 0, 2 * sizeof(*module->segments));

    if (need_code)
    {
        segment = &module->segments[module->segment_count++];

        for (size_t i = 0; i < module->program->block_count; ++i)
        {
            struct x86_block *block = &module->program->blocks[i];

            if (!block->data)
                block->segment = segment;
        }
    }

    if (need_data)
    {
        segment = &module->segments[module->segment_count++];
        segment->data = true;

        for (size_t i = 0; i < module->program->block_count; ++i)
        {
            struct x86_block *block = &module->program->blocks[i];

            if (block->data)
                block->segment = segment;
        }

        module->data_segment = segment;
    }
}

static uint16_t ld_segment_get_number(const struct ld_module *module, const struct ld_segment *segment)
{
    return (segment - module->segments) + 1;
}

static bool is_resolved(const struct x86_reloc *reloc)
{
    switch (reloc->type)
    {
        case X86_RELOC_UNRESOLVED_LOCAL:
        case X86_RELOC_UNRESOLVED_SEGMENT:
        case X86_RELOC_UNRESOLVED_OFFSET:
        case X86_RELOC_UNRESOLVED_PAIR:
            return false;

        case X86_RELOC_LOCAL:
        case X86_RELOC_INTERNAL_OFFSET:
        case X86_RELOC_INTERNAL_PAIR:
        case X86_RELOC_INTERNAL_SEGMENT:
        case X86_RELOC_EXTERNAL_PAIR:
            return true;
    }

    rose_unreachable();
}

static bool reloc_matches_record(const struct ld_reloc_record *record, const struct x86_reloc *reloc)
{
    const struct x86_reloc *tail = record->tail;

    switch (tail->type)
    {
        case X86_RELOC_INTERNAL_PAIR:
        case X86_RELOC_INTERNAL_SEGMENT:
            /* I don't love this... */
            if (reloc->type != X86_RELOC_INTERNAL_SEGMENT && reloc->type != X86_RELOC_INTERNAL_PAIR)
                return false;
            return tail->u.symbol == reloc->u.symbol;

        case X86_RELOC_EXTERNAL_PAIR:
            if (reloc->type != X86_RELOC_EXTERNAL_PAIR)
                return false;
            return tail->u.import == reloc->u.import;

        case X86_RELOC_INTERNAL_OFFSET:
        case X86_RELOC_LOCAL:
        case X86_RELOC_UNRESOLVED_LOCAL:
        case X86_RELOC_UNRESOLVED_SEGMENT:
        case X86_RELOC_UNRESOLVED_OFFSET:
        case X86_RELOC_UNRESOLVED_PAIR:
            assert(0);
    }

    rose_unreachable();
}

static void add_reloc_record(const struct ld_module *module,
        const struct x86_block *reloc_block, struct x86_reloc *reloc)
{
    struct ld_segment *segment = reloc_block->segment;

    for (size_t i = 0; i < segment->reloc_count; ++i)
    {
        if (reloc_matches_record(&segment->relocs[i], reloc))
        {
            reloc->prev = segment->relocs[i].tail;
            segment->relocs[i].tail = reloc;
            TRACE("Appending %p to %p.\n", reloc, reloc->prev);
            return;
        }
    }

    array_reserve((void **)&segment->relocs, &segment->relocs_capacity,
            segment->reloc_count + 1, sizeof(*segment->relocs));
    segment->relocs[segment->reloc_count++].tail = reloc;
    TRACE("Starting new relocation chain for %p.\n", reloc);
}

static void resolve_internal_reloc(const struct ld_module *module,
        const struct x86_block *reloc_segment, struct x86_reloc *reloc)
{
    switch (reloc->type)
    {
        case X86_RELOC_UNRESOLVED_LOCAL:
        case X86_RELOC_UNRESOLVED_OFFSET:
        case X86_RELOC_UNRESOLVED_SEGMENT:
        case X86_RELOC_UNRESOLVED_PAIR:
            break;

        /* Non-global relocation already resolved at the compilation step.
         * For local and offset-only relocations, we don't need to do anything
         * here, but for cross-segment relocations we still need to add a
         * relocation record. */
        case X86_RELOC_LOCAL:
        case X86_RELOC_INTERNAL_OFFSET:
            return;
        case X86_RELOC_INTERNAL_SEGMENT:
        case X86_RELOC_INTERNAL_PAIR:
            add_reloc_record(module, reloc_segment, reloc);
            return;

        case X86_RELOC_EXTERNAL_PAIR:
            rose_unreachable();
    }

    for (size_t i = 0; i < module->program->block_count; ++i)
    {
        const struct x86_block *target_segment = &module->program->blocks[i];

        for (size_t j = 0; j < target_segment->symbol_count; ++j)
        {
            const struct x86_symbol *symbol = &target_segment->symbols[j];

            /* Local symbols should already have been resolved. */
            if (!symbol->global)
                continue;

            if (strcmp(symbol->name, reloc->u.name))
                continue;

            switch (reloc->type)
            {
                case X86_RELOC_UNRESOLVED_LOCAL:
                    if (reloc_segment->segment != target_segment->segment)
                        continue;
                    reloc->type = X86_RELOC_LOCAL;
                    reloc->u.symbol = symbol;
                    break;

                case X86_RELOC_UNRESOLVED_SEGMENT:
                    reloc->type = X86_RELOC_INTERNAL_SEGMENT;
                    reloc->u.symbol = symbol;
                    add_reloc_record(module, reloc_segment, reloc);
                    break;

                case X86_RELOC_UNRESOLVED_OFFSET:
                    reloc->type = X86_RELOC_INTERNAL_OFFSET;
                    reloc->u.symbol = symbol;
                    break;

                case X86_RELOC_UNRESOLVED_PAIR:
                    reloc->type = X86_RELOC_INTERNAL_PAIR;
                    reloc->u.symbol = symbol;
                    add_reloc_record(module, reloc_segment, reloc);
                    break;

                default:
                    assert(0);
            }

            TRACE("Found internal symbol %s, type %#x.\n", symbol->name, reloc->type);
            return;
        }
    }
}

static void resolve_internal(const struct ld_module *module)
{
    for (size_t i = 0; i < module->program->block_count; ++i)
    {
        const struct x86_block *block = &module->program->blocks[i];

        /* Set the symbol back-pointers while we're here. */

        for (size_t j = 0; j < block->symbol_count; ++j)
        {
            block->symbols[j].instr = &block->instrs[block->symbols[j].instr_offset];
            block->symbols[j].block = block;
        }

        for (size_t j = 0; j < block->reloc_count; ++j)
            resolve_internal_reloc(module, block, &block->relocs[j]);
    }
}

static void resolve_imported_symbol(const struct ld_module *module,
        const struct x86_block *reloc_segment, struct x86_reloc *reloc)
{
    if (is_resolved(reloc))
        return;

    for (size_t i = 0; i < module->program->import_count; ++i)
    {
        const struct rose_def *import_module = &module->program->imports[i];

        for (size_t j = 0; j < import_module->export_count; ++j)
        {
            const struct rose_export *export = &import_module->exports[j];

            if (strcmp(export->name, reloc->u.name))
                continue;

            switch (reloc->type)
            {
                case X86_RELOC_UNRESOLVED_PAIR:
                    reloc->type = X86_RELOC_EXTERNAL_PAIR;
                    reloc->u.import = export;
                    export->module->used = true;
                    TRACE("Found imported symbol %s.\n", export->name);
                    break;

                default:
                    assert(0);
            }

            add_reloc_record(module, reloc_segment, reloc);
            return;
        }
    }
}

static void resolve_imports(struct ld_module *module)
{
    uint16_t import_index = 1;

    for (size_t i = 0; i < module->program->block_count; ++i)
    {
        const struct x86_block *block = &module->program->blocks[i];

        for (size_t j = 0; j < block->reloc_count; ++j)
            resolve_imported_symbol(module, block, &block->relocs[j]);
    }

    /* Windows 98, at least, really wants there to be an empty entry at the
     * beginning. I don't know why. */
    module->imptab = malloc(1);
    module->imptab[0] = 0;
    module->imptab_len = 1;

    for (size_t i = 0; i < module->program->import_count; ++i)
    {
        struct rose_def *import = &module->program->imports[i];
        size_t len = strlen(import->name);

        if (!import->used)
            continue;
        import->import_index = import_index++;
        import->imptab_offset = module->imptab_len;
        module->imptab = realloc(module->imptab, module->imptab_len + 1 + len);
        assert(len <= UCHAR_MAX);
        module->imptab[module->imptab_len++] = len;
        memcpy(module->imptab + module->imptab_len, import->name, len);
        module->imptab_len += len;
    }
    module->used_import_count = import_index - 1;
}

static void check_unresolved(const struct rose_program *prog)
{
    bool resolved = true;

    for (size_t i = 0; i < prog->block_count; ++i)
    {
        const struct x86_block *block = &prog->blocks[i];

        for (size_t j = 0; j < block->reloc_count; ++j)
        {
            const struct x86_reloc *reloc = &block->relocs[j];

            if (!is_resolved(reloc))
            {
                printf("Failed to resolve symbol '%s'.\n", reloc->u.name);
                resolved = false;
            }
        }
    }

    if (!resolved)
        exit(ROSE_ERROR_INVALID_SHADER);
}

static void write_headers(struct buffer *buffer, const struct ld_module *module)
{
    const struct rose_program *program = module->program;
    size_t ne_header_ptr, enttab_ptr;
    const char *module_name;
    struct header_mz mz;
    struct header_ne ne;

    memset(&mz, 0, sizeof(mz));
    mz.e_magic = 0x5a4d;
    mz.e_lfanew = sizeof(mz);
    write_bytes(buffer, &mz, sizeof(mz));

    ne_header_ptr = buffer->size;

    memset(&ne, 0, sizeof(ne));
    ne.ne_magic = 0x454e;
    /* Windows really doesn't want these to be zero */
    ne.ne_ver = 5;
    ne.ne_rev = 60;
    ne.ne_flags = NE_FFLAGS_PROTECTEDONLY | NE_FFLAGS_GUI;
    if (module->data_segment)
    {
        ne.ne_flags |= NE_FFLAGS_SINGLEDATA;
        ne.ne_autodata = ld_segment_get_number(module, module->data_segment);
    }
    ne.ne_heap = 1024;
    ne.ne_stack = 20480;
    if (module->data_segment->byte_size + ne.ne_stack >= 0x10000)
        linker_error("Data segment size %zu plus stack size %u is too large.",
                module->data_segment->byte_size, ne.ne_stack);
    ne.ne_cs = ld_segment_get_number(module, module->entry_block->segment);
    ne.ne_ip = x86_symbol_get_byte_offset(module->entry_block, module->entry_symbol);
    ne.ne_ss = ld_segment_get_number(module, module->data_segment);
    ne.ne_sp = 0; /* automatically set to end of segment + stack size */
    ne.ne_cseg = module->segment_count;
    ne.ne_cmod = module->used_import_count;
    ne.ne_align = SEGMENT_ALIGN_SHIFT;
    ne.ne_exetyp = NE_OSFLAGS_WINDOWS;
    ne.ne_expver_maj = 3;
    ne.ne_expver_min = 10;
    write_bytes(buffer, &ne, sizeof(ne));

    set_u16(buffer, ne_header_ptr + offsetof(struct header_ne, ne_segtab), buffer->size - ne_header_ptr);

    buffer->segtab_ptr = buffer->size;

    for (size_t i = 0; i < module->segment_count; ++i)
    {
        const struct ld_segment *segment = &module->segments[i];
        uint16_t flags = 0;

        if (segment->byte_size >= UINT16_MAX)
            linker_error("Segment %u has too large size %u.", i, segment->byte_size);

        flags |= 0x0c00; /* ring 3 */
        if (segment->reloc_count)
            flags |= 0x0100;
        if (segment->data)
            flags |= 0x0001;

        write_u16(buffer, 0);
        write_u16(buffer, segment->byte_size); /* TODO: bss at end */
        write_u16(buffer, flags);
        write_u16(buffer, segment->byte_size);
    }

    set_u16(buffer, ne_header_ptr + offsetof(struct header_ne, ne_rsrctab), buffer->size - ne_header_ptr);

    set_u16(buffer, ne_header_ptr + offsetof(struct header_ne, ne_restab), buffer->size - ne_header_ptr);

    if (!(module_name = program->def.name))
        module_name = "ROSEEXE";
    write_u8(buffer, strlen(module_name));
    write_bytes(buffer, module_name, strlen(module_name));
    write_u16(buffer, 0); /* ordinal, ignored for module name */
    write_u8(buffer, 0); /* null terminator */

    set_u16(buffer, ne_header_ptr + offsetof(struct header_ne, ne_modtab), buffer->size - ne_header_ptr);
    for (size_t i = 0; i < module->program->import_count; ++i)
    {
        struct rose_def *import = &module->program->imports[i];

        if (import->used)
            write_u16(buffer, import->imptab_offset);
    }

    set_u16(buffer, ne_header_ptr + offsetof(struct header_ne, ne_imptab), buffer->size - ne_header_ptr);
    write_bytes(buffer, module->imptab, module->imptab_len);

    enttab_ptr = buffer->size;
    set_u16(buffer, ne_header_ptr + offsetof(struct header_ne, ne_enttab), buffer->size - ne_header_ptr);

    write_u8(buffer, 0);

    set_u16(buffer, ne_header_ptr + offsetof(struct header_ne, ne_cbenttab), buffer->size - enttab_ptr);
}

static void write_segments(struct buffer *buffer, const struct ld_module *module, bool writing)
{
    for (size_t i = 0; i < module->segment_count; ++i)
    {
        struct x86_buffer x86_buffer = {.writing = writing};
        struct ld_segment *segment = &module->segments[i];

        TRACE("Segment %zu, writing %d.\n", i, writing);

        if (segment == module->data_segment)
        {
            /* Write the PSP at the beginning. */
            struct x86_block psp_block = {0};

            for (int j = 0; j < 8; ++j)
                x86_emit_i(&psp_block, X86_OP_DATA, 0, 16);
            x86_write_block(&x86_buffer, &psp_block);
        }

        for (size_t j = 0; j < module->program->block_count; ++j)
        {
            struct x86_block *block = &module->program->blocks[j];

            if (block->segment != segment)
                continue;

            x86_write_block(&x86_buffer, block);

            if (writing)
            {
                TRACE("Dumping symbol addresses:\n");
                for (size_t k = 0; k < block->symbol_count; ++k)
                    TRACE("%u:%04zx: %s.\n", ld_segment_get_number(module, segment),
                            block->symbols[k].instr->byte_offset, block->symbols[k].name);
            }
        }

        if (x86_buffer.result)
            exit(x86_buffer.result);

        if (!writing)
        {
            segment->byte_size = x86_buffer.size;
            continue;
        }

        align_buffer(buffer, SEGMENT_ALIGN_SHIFT);
        set_u16(buffer, buffer->segtab_ptr + (i * 8), buffer->size >> SEGMENT_ALIGN_SHIFT);

        write_bytes(buffer, x86_buffer.output, x86_buffer.size);

        if (!segment->reloc_count)
            continue;
        write_u16(buffer, segment->reloc_count);
        for (size_t j = 0; j < segment->reloc_count; ++j)
        {
            const struct ld_reloc_record *record = &segment->relocs[j];

            switch (record->tail->type)
            {
                case X86_RELOC_INTERNAL_PAIR:
                case X86_RELOC_INTERNAL_SEGMENT:
                    write_u8(buffer, 2);
                    write_u8(buffer, 0);
                    write_u16(buffer, record->tail->byte_offset);
                    write_u16(buffer, ld_segment_get_number(module, record->tail->u.symbol->block->segment));
                    /* FIXME: What do we write for the offset? Does it matter? */
                    write_u16(buffer, 0);
                    break;

                case X86_RELOC_EXTERNAL_PAIR:
                    write_u8(buffer, 3);
                    write_u8(buffer, 1);
                    write_u16(buffer, record->tail->byte_offset);
                    write_u16(buffer, record->tail->u.import->module->import_index);
                    write_u16(buffer, record->tail->u.import->ordinal);
                    break;

                case X86_RELOC_LOCAL:
                case X86_RELOC_INTERNAL_OFFSET:
                case X86_RELOC_UNRESOLVED_LOCAL:
                case X86_RELOC_UNRESOLVED_SEGMENT:
                case X86_RELOC_UNRESOLVED_OFFSET:
                case X86_RELOC_UNRESOLVED_PAIR:
                    assert(0);
            }
        }
    }
}

static void find_entry_point(struct ld_module *module)
{
    /* I haven't checked what the true entry point symbol is called on Windows,
     * if there even is one. But the analog on Linux is called _start(), so
     * that's what we're going to use.
     *
     * Generally this entry point will call WinMain() which itself will call
     * main(). */

    for (size_t i = 0; i < module->program->block_count; ++i)
    {
        const struct x86_block *block = &module->program->blocks[i];
        const struct x86_symbol *symbol;

        if ((symbol = x86_find_symbol(block, "_start")))
        {
            module->entry_block = block;
            module->entry_symbol = symbol;
            return;
        }
    }

    linker_error("Cannot find entry point _start.");
}

void rose_link(struct rose_program *prog, struct rose_code *output)
{
    struct ld_module module = {.program = prog};
    struct buffer buffer = {0};

    memset(output, 0, sizeof(*output));

    /* After this point our relocs go from unresolved to potentially pointing at
     * an x86_symbol, so we *cannot* add more relocs or symbols! Make sure we
     * take care of that before this point! */

    init_segments(&module);
    resolve_internal(&module);
    resolve_imports(&module);
    check_unresolved(prog);

    find_entry_point(&module);

    /* first compute offsets without writing */
    write_segments(&buffer, &module, false);
    write_headers(&buffer, &module);
    /* then actually write */
    write_segments(&buffer, &module, true);

    output->code = buffer.buffer;
    output->size = buffer.size;
}
