/*
 * Copyright 2023 Elizabeth Figura
 *
 * This file is part of the Rosé compiler.
 *
 * Rosé is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rosé is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Rosé; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

/*
 * This file incorporates code taken from the vkd3d-shader HLSL compiler and
 * covered under the following license:
 *
 * Copyright 2008 Stefan Dösinger
 * Copyright 2012 Matteo Bruni for CodeWeavers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

%{
#define YY_NO_UNISTD_H
#include "c.h"
#include "c.tab.h"

#define YYSTYPE ROSE_YYSTYPE
#define YYLTYPE ROSE_YYLTYPE

static void update_location(struct rose_ctx *ctx, YYLTYPE *loc);

#define YY_USER_ACTION update_location(yyget_extra(yyscanner), yyget_lloc(yyscanner));

%}

%option bison-bridge
%option bison-locations
%option extra-type="struct rose_ctx *"
%option never-interactive
%option nodefault
%option noinput
%option nounput
%option noyywrap
%option prefix="rose_yy"
%option reentrant

%x pp pp_pragma pp_ignore

RESERVED1               auto|case|default|enum
RESERVED2               goto
RESERVED4               union

WS                      [ \t]
NEWLINE                 (\n)|(\r\n)
STRING                  \"[^\"]*\"
IDENTIFIER              [A-Za-z_][A-Za-z0-9_]*

ANY                     (.)

%%
{RESERVED1}             |
{RESERVED2}             |
{RESERVED4}             {
                            struct rose_ctx *ctx = yyget_extra(yyscanner);

                            rose_error(ctx, yylloc, "Reserved keyword \"%s\" used.", yytext);
                        }

_Bool                   {return KW__BOOL;               }
_cdecl                  {return KW__CDECL;              }
_far                    {return KW__FAR;                }
_huge                   {return KW__HUGE;               }
_near                   {return KW__NEAR;               }
_pascal                 {return KW__PASCAL;             }
asm                     {return KW_ASM;                 }
break                   {return KW_BREAK;               }
char                    {return KW_CHAR;                }
const                   {return KW_CONST;               }
continue                {return KW_CONTINUE;            }
do                      {return KW_DO;                  }
double                  {return KW_DOUBLE;              }
else                    {return KW_ELSE;                }
extern                  {return KW_EXTERN;              }
for                     {return KW_FOR;                 }
if                      {return KW_IF;                  }
inline                  {return KW_INLINE;              }
int                     {return KW_INT;                 }
long                    {return KW_LONG;                }
register                {return KW_REGISTER;            }
return                  {return KW_RETURN;              }
short                   {return KW_SHORT;               }
signed                  {return KW_SIGNED;              }
sizeof                  {return KW_SIZEOF;              }
static                  {return KW_STATIC;              }
struct                  {return KW_STRUCT;              }
switch                  {return KW_SWITCH;              }
typedef                 {return KW_TYPEDEF;             }
unsigned                {return KW_UNSIGNED;            }
void                    {return KW_VOID;                }
volatile                {return KW_VOLATILE;            }
while                   {return KW_WHILE;               }

\+\+                    {return OP_INC;                 }
\-\-                    {return OP_DEC;                 }
&&                      {return OP_AND;                 }
\|\|                    {return OP_OR;                  }
==                      {return OP_EQ;                  }
\<\<                    {return OP_LEFTSHIFT;           }
\<\<=                   {return OP_LEFTSHIFTASSIGN;     }
\>\>                    {return OP_RIGHTSHIFT;          }
\>\>=                   {return OP_RIGHTSHIFTASSIGN;    }
\.\.\.                  {return OP_ELLIPSIS;            }
\<=                     {return OP_LE;                  }
\>=                     {return OP_GE;                  }
!=                      {return OP_NE;                  }
\+=                     {return OP_ADDASSIGN;           }
\-=                     {return OP_SUBASSIGN;           }
\*=                     {return OP_MULASSIGN;           }
\/=                     {return OP_DIVASSIGN;           }
%=                      {return OP_MODASSIGN;           }
&=                      {return OP_ANDASSIGN;           }
\|=                     {return OP_ORASSIGN;            }
^=                      {return OP_XORASSIGN;           }
\-\>                    {return OP_POINTER;             }

{IDENTIFIER}            {
                            struct rose_ctx *ctx = yyget_extra(yyscanner);

                            yylval->name = rose_strdup(ctx, yytext);
                            if (rose_get_var(ctx->cur_scope, yytext) || rose_get_function(ctx, yytext))
                                return VAR_IDENTIFIER;
                            else if (rose_get_type(ctx->cur_scope, ROSE_TYPE_PREFIX_NONE, yytext, true))
                                return TYPE_IDENTIFIER;
                            else
                                return NEW_IDENTIFIER;
                        }

0x[0-9a-fA-F]+          {
                            yylval->intval = rose_parse_integer(yytext);
                            return C_INTEGER;
                        }
0[0-7]+                 {
                            yylval->intval = rose_parse_integer(yytext);
                            return C_INTEGER;
                        }
[0-9]+                  {
                            yylval->intval = rose_parse_integer(yytext);
                            return C_INTEGER;
                        }
'([^'\\]|\\.)+'         {
                            struct rose_ctx *ctx = yyget_extra(yyscanner);
                            char *parsed;
                            size_t len;

                            yylval->intval = 0;

                            yytext[yyleng - 1] = 0;
                            if (rose_parse_string(yytext + 1, &parsed, &len))
                            {
                                if (len != 2)
                                    rose_error(ctx, yylloc, "Character constant is %zu characters long.", len);
                                yylval->intval = parsed[0];
                                free(parsed);
                            }
                            else
                            {
                                rose_error(ctx, yylloc, "Failed to parse character.");
                            }
                            return C_INTEGER;
                        }
<INITIAL>\"([^"\\]|\\.)*\" {
                            struct rose_ctx *ctx = yyget_extra(yyscanner);

                            yylval->name = rose_strdup(ctx, yytext + 1);
                            yylval->name[yyleng - 2] = 0;
                            return STRING;
                        }

{WS}+                   {}
{NEWLINE}               {
                            struct rose_ctx *ctx = yyget_extra(yyscanner);

                            ++ctx->location.line;
                            ctx->location.column = 1;
                        }

^#                      {
                            BEGIN(pp);
                        }

<pp>pragma{WS}+         {
                            BEGIN(pp_pragma);
                        }
<pp_pragma>{NEWLINE}    {
                            struct rose_ctx *ctx = yyget_extra(yyscanner);

                            FIXME("Unsupported preprocessor #pragma directive at line %u.\n", ctx->location.line);
                            BEGIN(INITIAL);
                        }
<pp_pragma>{ANY}        {}
<pp>[0-9]+{WS}+{STRING} {
                            struct rose_ctx *ctx = yyget_extra(yyscanner);
                            char *rest;

                            BEGIN(pp_ignore);
                            ctx->location.line = strtoul(yytext, &rest, 10);
                            if (strcmp(rest, ctx->location.source_name))
                            {
                                char *name = strchr(rest, '"');

                                name = rose_strdup(ctx, name + 1);
                                name[strlen(name) - 1] = 0;

                                rose_array_reserve(ctx, (void **)&ctx->source_files, &ctx->source_files_capacity,
                                        ctx->source_file_count + 1, sizeof(*ctx->source_files));
                                ctx->source_files[ctx->source_file_count] = name;
                                ctx->location.source_name = ctx->source_files[ctx->source_file_count];
                                ++ctx->source_file_count;
                            }
                        }
<pp_ignore>{NEWLINE}    {
                            BEGIN(INITIAL);
                        }
<pp_ignore>{ANY}        {}
<pp>{NEWLINE}           {
                            FIXME("Unexpected preprocessor directive.\n");
                            BEGIN(INITIAL);
                        }
<pp>{ANY}               {}

{ANY}                   {
                            return yytext[0];
                        }

%%

static void update_location(struct rose_ctx *ctx, YYLTYPE *lloc)
{
    *lloc = ctx->location;
    ctx->location.column += yyget_leng(ctx->scanner);
}

int rose_lexer_compile(struct rose_ctx *ctx, const struct rose_code *source)
{
    YY_BUFFER_STATE buffer;
    int ret;

    yylex_init_extra(ctx, &ctx->scanner);
    buffer = yy_scan_bytes(source->code, source->size, ctx->scanner);
    yy_switch_to_buffer(buffer, ctx->scanner);

    ret = rose_yyparse(ctx->scanner, ctx);

    yy_delete_buffer(buffer, ctx->scanner);
    yylex_destroy(ctx->scanner);
    return ret;
}
