/*
 * Copyright 2023 Elizabeth Figura
 *
 * This file is part of the Rosé compiler.
 *
 * Rosé is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rosé is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Rosé; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "rose.h"
#include "x86.h"

const char *x86_reg_get_name(enum x86_reg reg)
{
    switch (reg)
    {
        case X86_REG_AX: return "%ax";
        case X86_REG_CX: return "%cx";
        case X86_REG_DX: return "%dx";
        case X86_REG_BX: return "%bx";
        case X86_REG_SP: return "%sp";
        case X86_REG_BP: return "%bp";
        case X86_REG_SI: return "%si";
        case X86_REG_DI: return "%di";
    }

    rose_unreachable();
}

const char *x86_seg_get_name(enum x86_seg seg)
{
    switch (seg)
    {
        case X86_SEG_NONE: return "";
        case X86_SEG_ES: return "%es";
        case X86_SEG_CS: return "%cs";
        case X86_SEG_SS: return "%ss";
        case X86_SEG_DS: return "%ds";
        case X86_SEG_FS: return "%fs";
        case X86_SEG_GS: return "%gs";
    }

    rose_unreachable();
}

static bool is_in_int8_range(int32_t offset)
{
    return offset >= INT8_MIN && offset <= INT8_MAX;
}

static bool is_in_int16_range(int32_t offset)
{
    return offset >= INT16_MIN && offset <= INT16_MAX;
}

void x86_add_instr(struct x86_block *block, const struct x86_instr *instr)
{
    array_reserve((void **)&block->instrs, &block->capacity, block->count + 1, sizeof(*block->instrs));
    block->instrs[block->count++] = *instr;
}

uint32_t x86_add_reloc(struct x86_block *block, enum x86_reloc_type type, const char *name)
{
    struct x86_reloc *reloc;

    array_reserve((void **)&block->relocs, &block->reloc_capacity, block->reloc_count + 1, sizeof(*block->relocs));
    reloc = &block->relocs[block->reloc_count++];
    memset(reloc, 0, sizeof(*reloc));
    reloc->type = type;
    reloc->u.name = strdup(name);
    /* Return the index biased by 1. Zero reloc means no reloc. */
    return block->reloc_count - 1 + 1;
}

struct x86_symbol *x86_add_symbol(struct x86_block *block, const char *name)
{
    struct x86_symbol *symbol;

    array_reserve((void **)&block->symbols, &block->symbols_capacity,
            block->symbol_count + 1, sizeof(*block->symbols));
    symbol = &block->symbols[block->symbol_count++];
    symbol->name = strdup(name);
    symbol->instr_offset = block->count;
    symbol->global = false;
    return symbol;
}

struct x86_symbol *x86_find_symbol(const struct x86_block *block, const char *name)
{
    for (size_t i = 0; i < block->symbol_count; ++i)
    {
        if (!strcmp(name, block->symbols[i].name))
            return &block->symbols[i];
    }

    return NULL;
}

uint32_t x86_symbol_get_byte_offset(const struct x86_block *block, const struct x86_symbol *symbol)
{
    return block->instrs[symbol->instr_offset].byte_offset;
}

void x86_emit(struct x86_block *block, enum x86_opcode opcode, int8_t size)
{
    struct x86_instr xi =
    {
        .opcode = opcode,
        .size = size,
    };

    x86_add_instr(block, &xi);
}

void x86_emit_r(struct x86_block *block, enum x86_opcode opcode, enum x86_reg r1, int8_t size)
{
    struct x86_instr xi =
    {
        .opcode = opcode,
        .args[0].type = X86_ARG_REG16,
        .args[0].u.reg = r1,
        .size = size,
    };

    x86_add_instr(block, &xi);
}

void x86_emit_r8(struct x86_block *block, enum x86_opcode opcode, enum x86_reg8 r1)
{
    struct x86_instr xi =
    {
        .opcode = opcode,
        .args[0].type = X86_ARG_REG8,
        .args[0].u.reg8 = r1,
        .size = 8,
    };

    x86_add_instr(block, &xi);
}

void x86_emit_s(struct x86_block *block, enum x86_opcode opcode, enum x86_seg r1)
{
    struct x86_instr xi =
    {
        .opcode = opcode,
        .args[0].type = X86_ARG_SEG,
        .args[0].u.seg = r1,
        .size = 16,
    };

    x86_add_instr(block, &xi);
}

void x86_emit_i(struct x86_block *block, enum x86_opcode opcode, int32_t r1, int8_t size)
{
    struct x86_instr xi =
    {
        .opcode = opcode,
        .args[0].type = X86_ARG_IMM,
        .args[0].u.imm.offset = r1,
        .size = size,
    };

    x86_add_instr(block, &xi);
}

void x86_emit_l(struct x86_block *block, enum x86_opcode opcode, uint32_t r1)
{
    struct x86_instr xi =
    {
        .opcode = opcode,
        .args[0].type = X86_ARG_MEM,
        .args[0].u.mem.seg = X86_SEG_NONE,
        .args[0].u.mem.reg = X86_MEM_REG_NONE,
        .args[0].u.mem.offset.reloc = r1,
    };

    assert(r1);

    x86_add_instr(block, &xi);
}

void x86_emit_ir8(struct x86_block *block, enum x86_opcode opcode, int32_t r1, enum x86_reg8 r2)
{
    struct x86_instr xi =
    {
        .opcode = opcode,
        .args[0].type = X86_ARG_IMM,
        .args[0].u.imm.offset = r1,
        .args[1].type = X86_ARG_REG8,
        .args[1].u.reg8 = r2,
        .size = 8,
    };

    x86_add_instr(block, &xi);
}

void x86_emit_ir(struct x86_block *block, enum x86_opcode opcode, int32_t r1, enum x86_reg r2, int8_t size)
{
    struct x86_instr xi =
    {
        .opcode = opcode,
        .args[0].type = X86_ARG_IMM,
        .args[0].u.imm.offset = r1,
        .args[1].type = X86_ARG_REG16,
        .args[1].u.reg = r2,
        .size = size,
    };

    x86_add_instr(block, &xi);
}

void x86_emit_rr8(struct x86_block *block, enum x86_opcode opcode, enum x86_reg8 r1, enum x86_reg8 r2)
{
    struct x86_instr xi =
    {
        .opcode = opcode,
        .args[0].type = X86_ARG_REG8,
        .args[0].u.reg8 = r1,
        .args[1].type = X86_ARG_REG8,
        .args[1].u.reg8 = r2,
        .size = 8,
    };

    x86_add_instr(block, &xi);
}

void x86_emit_rr(struct x86_block *block, enum x86_opcode opcode, enum x86_reg r1, enum x86_reg r2, int8_t size)
{
    struct x86_instr xi =
    {
        .opcode = opcode,
        .args[0].type = X86_ARG_REG16,
        .args[0].u.reg = r1,
        .args[1].type = X86_ARG_REG16,
        .args[1].u.reg = r2,
        .size = size,
    };

    x86_add_instr(block, &xi);
}

void x86_emit_mov_rs(struct x86_block *block, enum x86_reg r1, enum x86_seg r2)
{
    struct x86_instr xi =
    {
        .opcode = X86_OP_MOV,
        .args[0].type = X86_ARG_REG16,
        .args[0].u.reg = r1,
        .args[1].type = X86_ARG_SEG,
        .args[1].u.seg = r2,
        .size = 16,
    };

    x86_add_instr(block, &xi);
}

void x86_emit_mov_sr(struct x86_block *block, enum x86_seg r1, enum x86_reg r2)
{
    struct x86_instr xi =
    {
        .opcode = X86_OP_MOV,
        .args[0].type = X86_ARG_SEG,
        .args[0].u.seg = r1,
        .args[1].type = X86_ARG_REG16,
        .args[1].u.reg = r2,
        .size = 16,
    };

    x86_add_instr(block, &xi);
}

static void x86_error(struct x86_buffer *buffer, const char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    printf("Error while writing x86 code:");
    vprintf(fmt, args);
    printf("\n");
    va_end(args);
    buffer->result = ROSE_ERROR_INVALID_SHADER;
}

static void write_bytes(struct x86_buffer *buffer, const void *data, size_t size)
{
    if (buffer->writing)
    {
        array_reserve((void **)&buffer->output, &buffer->capacity, buffer->size + size, 1);
        memcpy(buffer->output + buffer->size, data, size);
    }
    buffer->size += size;
}

static void write_u8(struct x86_buffer *buffer, uint8_t n)
{
    write_bytes(buffer, &n, sizeof(n));
}

static void write_u16(struct x86_buffer *buffer, uint16_t n)
{
    write_bytes(buffer, &n, sizeof(n));
}

static void write_u32(struct x86_buffer *buffer, uint32_t n)
{
    write_bytes(buffer, &n, sizeof(n));
}

/* Copied and adapted from Semblance. */

enum table_arg_type
{
    NONE = 0,

    /* the literal value 1, used for bit shift ops */
    ONE,

    /* specific registers */
    AL, CL, AX, ES, CS, SS, DS, FS, GS,

    /* absolute or relative numbers, given as 1/2/4 bytes */
    IMM8, IMM16, IMM,   /* immediate number */
    REL8, REL,          /* relative to current instruction */
    SEGPTR,     /* absolute instruction, used for far calls/jumps */
    MOFFS,      /* absolute location in memory, for A0-A3 MOV */

    /* to be read from ModRM, appropriately */
    RM,         /* register/memory */
    MEM,        /* memory only (using 0x11xxxxxx is invalid) */
    REG8,       /* 8-bit register (even for a 16-bit instr) */
    REG,        /* register */
    SEG16,      /* segment register */

    /* absolute (*-prefixed) call/jmp */
    ABSRM, ABSMEM,
};


static const struct table_instruction
{
    uint16_t opcode_bytes;
    uint8_t subcode;
    int8_t size;
    char name[16];
    enum x86_opcode opcode;
    enum table_arg_type arg0, arg1, arg2;
    /* Set if the lowest 3 bytes of the opcode itself encodes arg0 or arg1 respectively. */
    bool opcode_arg0, opcode_arg1;
}
instruction_table[] =
{
#define X(name) #name, X86_OP_##name
    /* Some of these should be matched in a certain order. */
    {  0xD0, 0,  8, X(ROL),        ONE,    RM},
    {  0xD0, 1,  8, X(ROR),        ONE,    RM},
    {  0xD0, 2,  8, X(RCL),        ONE,    RM},
    {  0xD0, 3,  8, X(RCR),        ONE,    RM},
    {  0xD0, 4,  8, X(SHL),        ONE,    RM},
    {  0xD0, 5,  8, X(SHR),        ONE,    RM},
    {  0xD0, 6,  8, X(SAL),        ONE,    RM},
    {  0xD0, 7,  8, X(SAR),        ONE,    RM},
    {  0xD1, 0, -1, X(ROL),        ONE,    RM},
    {  0xD1, 1, -1, X(ROR),        ONE,    RM},
    {  0xD1, 2, -1, X(RCL),        ONE,    RM},
    {  0xD1, 3, -1, X(RCR),        ONE,    RM},
    {  0xD1, 4, -1, X(SHL),        ONE,    RM},
    {  0xD1, 5, -1, X(SHR),        ONE,    RM},
    {  0xD1, 6, -1, X(SAL),        ONE,    RM},
    {  0xD1, 7, -1, X(SAR),        ONE,    RM},
    {  0xC0, 0,  8, X(ROL),        IMM8,   RM},
    {  0xC0, 1,  8, X(ROR),        IMM8,   RM},
    {  0xC0, 2,  8, X(RCL),        IMM8,   RM},
    {  0xC0, 3,  8, X(RCR),        IMM8,   RM},
    {  0xC0, 4,  8, X(SHL),        IMM8,   RM},
    {  0xC0, 5,  8, X(SHR),        IMM8,   RM},
    {  0xC0, 6,  8, X(SAL),        IMM8,   RM},
    {  0xC0, 7,  8, X(SAR),        IMM8,   RM},
    {  0xC1, 0, -1, X(ROL),        IMM8,   RM},
    {  0xC1, 1, -1, X(ROR),        IMM8,   RM},
    {  0xC1, 2, -1, X(RCL),        IMM8,   RM},
    {  0xC1, 3, -1, X(RCR),        IMM8,   RM},
    {  0xC1, 4, -1, X(SHL),        IMM8,   RM},
    {  0xC1, 5, -1, X(SHR),        IMM8,   RM},
    {  0xC1, 6, -1, X(SAL),        IMM8,   RM},
    {  0xC1, 7, -1, X(SAR),        IMM8,   RM},
    {  0xD2, 0,  8, X(ROL),        CL,     RM},
    {  0xD2, 1,  8, X(ROR),        CL,     RM},
    {  0xD2, 2,  8, X(RCL),        CL,     RM},
    {  0xD2, 3,  8, X(RCR),        CL,     RM},
    {  0xD2, 4,  8, X(SHL),        CL,     RM},
    {  0xD2, 5,  8, X(SHR),        CL,     RM},
    {  0xD2, 6,  8, X(SAL),        CL,     RM},
    {  0xD2, 7,  8, X(SAR),        CL,     RM},
    {  0xD3, 0, -1, X(ROL),        CL,     RM},
    {  0xD3, 1, -1, X(ROR),        CL,     RM},
    {  0xD3, 2, -1, X(RCL),        CL,     RM},
    {  0xD3, 3, -1, X(RCR),        CL,     RM},
    {  0xD3, 4, -1, X(SHL),        CL,     RM},
    {  0xD3, 5, -1, X(SHR),        CL,     RM},
    {  0xD3, 6, -1, X(SAL),        CL,     RM},
    {  0xD3, 7, -1, X(SAR),        CL,     RM},

    {  0x06, 8, -1, X(PUSH),       ES},
    {  0x0E, 8, -1, X(PUSH),       CS},
    {  0x16, 8, -1, X(PUSH),       SS},
    {  0x1E, 8, -1, X(PUSH),       DS},
    {0x0FA0, 8, -1, X(PUSH),       FS},
    {0x0FA8, 8, -1, X(PUSH),       GS},
    {  0x6A, 8, -1, X(PUSH),       IMM8},
    {  0x68, 8, -1, X(PUSH),       IMM},
    {  0x50, 8, -1, X(PUSH),       REG, .opcode_arg0 = true},
    {  0xFF, 6, -1, X(PUSH),       RM},

    {  0x07, 8, -1, X(POP),        ES},
    {  0x17, 8, -1, X(POP),        SS},
    {  0x1F, 8, -1, X(POP),        DS},
    {0x0FA1, 8, -1, X(POP),        FS},
    {0x0FA9, 8, -1, X(POP),        GS},
    {  0x58, 8, -1, X(POP),        REG, .opcode_arg0 = true},
    {  0x8F, 0, -1, X(POP),        RM},

    {  0x6B, 8, -1, X(IMUL),       IMM8,   RM,     REG},
    {  0x69, 8, -1, X(IMUL),       IMM,    RM,     REG},

    {  0x90, 8, -1, X(XCHG),       REG,    AX,  .opcode_arg0 = true},
    {  0x90, 8, -1, X(XCHG),       AX,     REG, .opcode_arg1 = true},
    {  0x86, 8,  8, X(XCHG),       RM,     REG},
    {  0x86, 8,  8, X(XCHG),       REG,    RM},
    {  0x87, 8, -1, X(XCHG),       RM,     REG},
    {  0x87, 8, -1, X(XCHG),       REG,    RM},

    {  0x40, 8, -1, X(INC),        REG, .opcode_arg0 = true},
    {  0x48, 8, -1, X(DEC),        REG, .opcode_arg0 = true},
    {  0xFE, 0,  8, X(INC),        RM},
    {  0xFE, 1,  8, X(DEC),        RM},
    {  0xFF, 0, -1, X(INC),        RM},
    {  0xFF, 1, -1, X(DEC),        RM},

    {  0xA8, 8,  8, X(TEST),       IMM,    AL},
    {  0xA9, 8, -1, X(TEST),       IMM,    AX},
    {  0xF6, 0,  8, X(TEST),       IMM,    RM},
    {  0xF6, 1,  8, X(TEST),       IMM,    RM},
    {  0xF7, 0, -1, X(TEST),       IMM,    RM},
    {  0xF7, 1, -1, X(TEST),       IMM,    RM},
    {  0x84, 8,  8, X(TEST),       RM,     REG},
    {  0x84, 8,  8, X(TEST),       REG,    RM},
    {  0x85, 8, -1, X(TEST),       RM,     REG},
    {  0x85, 8, -1, X(TEST),       REG,    RM},

    {  0xA0, 8,  8, X(MOV),        MOFFS,  AL},
    {  0xA1, 8, -1, X(MOV),        MOFFS,  AX},
    {  0xA2, 8,  8, X(MOV),        AL,     MOFFS},
    {  0xA3, 8, -1, X(MOV),        AX,     MOFFS},
    {  0x88, 8,  8, X(MOV),        REG,    RM},
    {  0x89, 8, -1, X(MOV),        REG,    RM},
    {  0x8A, 8,  8, X(MOV),        RM,     REG},
    {  0x8B, 8, -1, X(MOV),        RM,     REG},
    {  0x8C, 8, -1, X(MOV),        SEG16,  RM},
    {  0x8E, 8, -1, X(MOV),        RM,     SEG16},
    {  0xB0, 8,  8, X(MOV),        IMM,    REG, .opcode_arg1 = true},
    {  0xB8, 8, -1, X(MOV),        IMM,    REG, .opcode_arg1 = true},
    {  0xC6, 0,  8, X(MOV),        IMM,    RM},
    {  0xC7, 0, -1, X(MOV),        IMM,    RM},

    /* gcc prefers the imm8 forms with %ax */
    {  0x82, 0,  8, X(ADD),        IMM8,   RM},
    {  0x82, 1,  8, X(OR),         IMM8,   RM},
    {  0x82, 2,  8, X(ADC),        IMM8,   RM},
    {  0x82, 3,  8, X(SBB),        IMM8,   RM},
    {  0x82, 4,  8, X(AND),        IMM8,   RM},
    {  0x82, 5,  8, X(SUB),        IMM8,   RM},
    {  0x82, 6,  8, X(XOR),        IMM8,   RM},
    {  0x82, 7,  8, X(CMP),        IMM8,   RM},
    {  0x83, 0, -1, X(ADD),        IMM8,   RM},
    {  0x83, 1, -1, X(OR),         IMM8,   RM},
    {  0x83, 2, -1, X(ADC),        IMM8,   RM},
    {  0x83, 3, -1, X(SBB),        IMM8,   RM},
    {  0x83, 4, -1, X(AND),        IMM8,   RM},
    {  0x83, 5, -1, X(SUB),        IMM8,   RM},
    {  0x83, 6, -1, X(XOR),        IMM8,   RM},
    {  0x83, 7, -1, X(CMP),        IMM8,   RM},
    {  0x04, 8,  8, X(ADD),        IMM,    AL},
    {  0x05, 8, -1, X(ADD),        IMM,    AX},
    {  0x0C, 8,  8, X(OR),         IMM,    AL},
    {  0x0D, 8, -1, X(OR),         IMM,    AX},
    {  0x14, 8,  8, X(ADC),        IMM,    AL},
    {  0x15, 8, -1, X(ADC),        IMM,    AX},
    {  0x1C, 8,  8, X(SBB),        IMM,    AL},
    {  0x1D, 8, -1, X(SBB),        IMM,    AX},
    {  0x24, 8,  8, X(AND),        IMM,    AL},
    {  0x25, 8, -1, X(AND),        IMM,    AX},
    {  0x2C, 8,  8, X(SUB),        IMM,    AL},
    {  0x2D, 8, -1, X(SUB),        IMM,    AX},
    {  0x34, 8,  8, X(XOR),        IMM,    AL},
    {  0x35, 8, -1, X(XOR),        IMM,    AX},
    {  0x3C, 8,  8, X(CMP),        IMM,    AL},
    {  0x3D, 8, -1, X(CMP),        IMM,    AX},
    {  0x80, 0,  8, X(ADD),        IMM,    RM},
    {  0x80, 1,  8, X(OR),         IMM,    RM},
    {  0x80, 2,  8, X(ADC),        IMM,    RM},
    {  0x80, 3,  8, X(SBB),        IMM,    RM},
    {  0x80, 4,  8, X(AND),        IMM,    RM},
    {  0x80, 5,  8, X(SUB),        IMM,    RM},
    {  0x80, 6,  8, X(XOR),        IMM,    RM},
    {  0x80, 7,  8, X(CMP),        IMM,    RM},
    {  0x81, 0, -1, X(ADD),        IMM,    RM},
    {  0x81, 1, -1, X(OR),         IMM,    RM},
    {  0x81, 2, -1, X(ADC),        IMM,    RM},
    {  0x81, 3, -1, X(SBB),        IMM,    RM},
    {  0x81, 4, -1, X(AND),        IMM,    RM},
    {  0x81, 5, -1, X(SUB),        IMM,    RM},
    {  0x81, 6, -1, X(XOR),        IMM,    RM},
    {  0x81, 7, -1, X(CMP),        IMM,    RM},

    {  0x00, 8,  8, X(ADD),        REG,    RM},
    {  0x01, 8, -1, X(ADD),        REG,    RM},
    {  0x02, 8,  8, X(ADD),        RM,     REG},
    {  0x03, 8, -1, X(ADD),        RM,     REG},
    {  0x08, 8,  8, X(OR),         REG,    RM},
    {  0x09, 8, -1, X(OR),         REG,    RM},
    {  0x0A, 8,  8, X(OR),         RM,     REG},
    {  0x0B, 8, -1, X(OR),         RM,     REG},
    {  0x10, 8,  8, X(ADC),        REG,    RM},
    {  0x11, 8, -1, X(ADC),        REG,    RM},
    {  0x12, 8,  8, X(ADC),        RM,     REG},
    {  0x13, 8, -1, X(ADC),        RM,     REG},
    {  0x18, 8,  8, X(SBB),        REG,    RM},
    {  0x19, 8, -1, X(SBB),        REG,    RM},
    {  0x1A, 8,  8, X(SBB),        RM,     REG},
    {  0x1B, 8, -1, X(SBB),        RM,     REG},
    {  0x20, 8,  8, X(AND),        REG,    RM},
    {  0x21, 8, -1, X(AND),        REG,    RM},
    {  0x22, 8,  8, X(AND),        RM,     REG},
    {  0x23, 8, -1, X(AND),        RM,     REG},
    {  0x27, 8,  0, X(DAA)},
    {  0x28, 8,  8, X(SUB),        REG,    RM},
    {  0x29, 8, -1, X(SUB),        REG,    RM},
    {  0x2A, 8,  8, X(SUB),        RM,     REG},
    {  0x2B, 8, -1, X(SUB),        RM,     REG},
    {  0x2F, 8,  0, X(DAS)},
    {  0x30, 8,  8, X(XOR),        REG,    RM},
    {  0x31, 8, -1, X(XOR),        REG,    RM},
    {  0x32, 8,  8, X(XOR),        RM,     REG},
    {  0x33, 8, -1, X(XOR),        RM,     REG},
    {  0x37, 8,  0, X(AAA)},
    {  0x38, 8,  8, X(CMP),        REG,    RM},
    {  0x39, 8, -1, X(CMP),        REG,    RM},
    {  0x3A, 8,  8, X(CMP),        RM,     REG},
    {  0x3B, 8, -1, X(CMP),        RM,     REG},
    {  0x3F, 8,  0, X(AAS)},


    {  0x60, 8, -1, X(PUSHA)},
    {  0x61, 8, -1, X(POPA)},
    {  0x62, 8, -1, X(BOUND),      MEM,    REG},
    {  0x63, 8, 16, X(ARPL),       REG,    RM},

    {  0x70, 8,  0, X(JO),         REL8},
    {  0x71, 8,  0, X(JNO),        REL8},
    {  0x72, 8,  0, X(JB),         REL8},
    {  0x73, 8,  0, X(JAE),        REL8},
    {  0x74, 8,  0, X(JZ),         REL8},
    {  0x75, 8,  0, X(JNZ),        REL8},
    {  0x76, 8,  0, X(JBE),        REL8},
    {  0x77, 8,  0, X(JA),         REL8},
    {  0x78, 8,  0, X(JS),         REL8},
    {  0x79, 8,  0, X(JNS),        REL8},
    {  0x7A, 8,  0, X(JP),         REL8},
    {  0x7B, 8,  0, X(JNP),        REL8},
    {  0x7C, 8,  0, X(JL),         REL8},
    {  0x7D, 8,  0, X(JGE),        REL8},
    {  0x7E, 8,  0, X(JLE),        REL8},
    {  0x7F, 8,  0, X(JG),         REL8},
    {0x0F80, 8,  0, X(JO),         REL},
    {0x0F81, 8,  0, X(JNO),        REL},
    {0x0F82, 8,  0, X(JB),         REL},
    {0x0F83, 8,  0, X(JAE),        REL},
    {0x0F84, 8,  0, X(JZ),         REL},
    {0x0F85, 8,  0, X(JNZ),        REL},
    {0x0F86, 8,  0, X(JBE),        REL},
    {0x0F87, 8,  0, X(JA),         REL},
    {0x0F88, 8,  0, X(JS),         REL},
    {0x0F89, 8,  0, X(JNS),        REL},
    {0x0F8A, 8,  0, X(JP),         REL},
    {0x0F8B, 8,  0, X(JNP),        REL},
    {0x0F8C, 8,  0, X(JL),         REL},
    {0x0F8D, 8,  0, X(JGE),        REL},
    {0x0F8E, 8,  0, X(JLE),        REL},
    {0x0F8F, 8,  0, X(JG),         REL},

    {  0x8D, 8, -1, X(LEA),        MEM,    REG},

    {  0x90, 8, -1, X(NOP)},

    {  0x98, 8, -1, X(CBW)},
    {  0x99, 8, -1, X(CWD)},
    {  0x9A, 8, -1, X(LCALL),      SEGPTR},
    {  0x9B, 8,  0, X(WAIT)},
    {  0x9C, 8, -1, X(PUSHF)},
    {  0x9D, 8, -1, X(POPF)},
    {  0x9E, 8,  0, X(SAHF)},
    {  0x9F, 8,  0, X(LAHF)},
    {  0xA4, 8,  8, X(MOVS)},
    {  0xA5, 8, -1, X(MOVS)},
    {  0xA6, 8,  8, X(CMPS)},
    {  0xA7, 8, -1, X(CMPS)},
    {  0xAA, 8,  8, X(STOS)},
    {  0xAB, 8, -1, X(STOS)},
    {  0xAC, 8,  8, X(LODS)},
    {  0xAD, 8, -1, X(LODS)},
    {  0xAE, 8,  8, X(SCAS)},
    {  0xAF, 8, -1, X(SCAS)},

    {  0xC2, 8, -1, X(RET),        IMM16},
    {  0xC3, 8, -1, X(RET)},
    {  0xC4, 8, -1, X(LES),        MEM,    REG},
    {  0xC5, 8, -1, X(LDS),        MEM,    REG},


    {  0xC8, 8, -1, X(ENTER),      IMM16,  IMM8},
    {  0xC9, 8, -1, X(LEAVE)},
    {  0xCA, 8, -1, X(LRET),       IMM16},
    {  0xCB, 8, -1, X(LRET)},
    {  0xCC, 8,  0, X(INT3)},
    {  0xCD, 8,  0, X(INT),        IMM8},
    {  0xCE, 8,  0, X(INTO)},
    {  0xCF, 8, -1, X(IRET)},

    {  0xE0, 8,  0, X(LOOPNZ),     REL8},
    {  0xE1, 8,  0, X(LOOPZ),      REL8},
    {  0xE2, 8,  0, X(LOOP),       REL8},
    {  0xE3, 8,  0, X(JCXZ),       REL8},
    {  0xE8, 8, -1, X(CALL),       REL},
    {  0xE9, 8,  0, X(JMP),        REL},
    {  0xEA, 8, -1, X(LJMP),       SEGPTR},
    {  0xEB, 8,  0, X(JMP),        REL8},
    {  0xF4, 8,  0, X(HLT)},
    {  0xF5, 8,  0, X(CMC)},

    {  0xF6, 2,  8, X(NOT),        RM},
    {  0xF6, 3,  8, X(NEG),        RM},
    {  0xF6, 4,  8, X(MUL),        RM},
    {  0xF6, 5,  8, X(IMUL),       RM},
    {  0xF6, 6,  8, X(DIV),        RM},
    {  0xF6, 7,  8, X(IDIV),       RM},
    {  0xF7, 2, -1, X(NOT),        RM},
    {  0xF7, 3, -1, X(NEG),        RM},
    {  0xF7, 4, -1, X(MUL),        RM},
    {  0xF7, 5, -1, X(IMUL),       RM},
    {  0xF7, 6, -1, X(DIV),        RM},
    {  0xF7, 7, -1, X(IDIV),       RM},

    {  0xF8, 8,  0, X(CLC)},
    {  0xF9, 8,  0, X(STC)},
    {  0xFA, 8,  0, X(CLI)},
    {  0xFB, 8,  0, X(STI)},
    {  0xFC, 8,  0, X(CLD)},
    {  0xFD, 8,  0, X(STD)},

    {  0xFF, 2, -1, X(CALL),       ABSRM},
    {  0xFF, 3, -1, X(LCALL),      ABSMEM},
    {  0xFF, 4, -1, X(JMP),        ABSRM},
    {  0xFF, 5, -1, X(LJMP),       ABSMEM},

    {0x0F90, 0,  8, X(SETO),       RM},
    {0x0F91, 0,  8, X(SETNO),      RM},
    {0x0F92, 0,  8, X(SETB),       RM},
    {0x0F93, 0,  8, X(SETAE),      RM},
    {0x0F94, 0,  8, X(SETZ),       RM},
    {0x0F95, 0,  8, X(SETNZ),      RM},
    {0x0F96, 0,  8, X(SETBE),      RM},
    {0x0F97, 0,  8, X(SETA),       RM},
    {0x0F98, 0,  8, X(SETS),       RM},
    {0x0F99, 0,  8, X(SETNS),      RM},
    {0x0F9A, 0,  8, X(SETP),       RM},
    {0x0F9B, 0,  8, X(SETNP),      RM},
    {0x0F9C, 0,  8, X(SETL),       RM},
    {0x0F9D, 0,  8, X(SETGE),      RM},
    {0x0F9E, 0,  8, X(SETLE),      RM},
    {0x0F9F, 0,  8, X(SETG),       RM},
    {0x0FA4, 8, -1, X(SHLD),       IMM8,   REG,    RM},
    {0x0FA5, 8, -1, X(SHLD),       CL,     REG,    RM},
    {0x0FAC, 8, -1, X(SHRD),       IMM8,   REG,    RM},
    {0x0FAD, 8, -1, X(SHRD),       CL,     REG,    RM},
    {0x0FAF, 8, -1, X(IMUL),       RM,     REG},
    {0x0FB2, 8, -1, X(LSS),        MEM,    REG},
    {0x0FB4, 8, -1, X(LFS),        MEM,    REG},
    {0x0FB5, 8, -1, X(LGS),        MEM,    REG},
    {0x0FB6, 8, -1, X(MOVZB),      RM,     REG},
    {0x0FB7, 8, -1, X(MOVZW),      RM,     REG},
    {0x0FBE, 8, -1, X(MOVSB),      RM,     REG},
    {0x0FBF, 8, -1, X(MOVSW),      RM,     REG},
#undef X
};

static bool table_arg_matches(enum table_arg_type type, const struct x86_arg *arg)
{
    switch (type)
    {
        case NONE:
            return arg->type == X86_ARG_NONE;
        case ONE:
            return arg->type == X86_ARG_IMM && !arg->u.imm.reloc && arg->u.imm.offset == 1;
        case AL:
            return arg->type == X86_ARG_REG8 && arg->u.reg8 == X86_REG8_AL;
        case CL:
            return arg->type == X86_ARG_REG8 && arg->u.reg8 == X86_REG8_CL;
        case AX:
            return arg->type == X86_ARG_REG16 && arg->u.reg == X86_REG_AX;
        case ES:
            return arg->type == X86_ARG_SEG && arg->u.seg == X86_SEG_ES;
        case CS:
            return arg->type == X86_ARG_SEG && arg->u.seg == X86_SEG_CS;
        case SS:
            return arg->type == X86_ARG_SEG && arg->u.seg == X86_SEG_SS;
        case DS:
            return arg->type == X86_ARG_SEG && arg->u.seg == X86_SEG_DS;
        case FS:
            return arg->type == X86_ARG_SEG && arg->u.seg == X86_SEG_FS;
        case GS:
            return arg->type == X86_ARG_SEG && arg->u.seg == X86_SEG_GS;
        case IMM8:
            return arg->type == X86_ARG_IMM && is_in_int8_range(arg->u.imm.offset) && !arg->u.imm.reloc;
        case IMM16:
            /* The only cases of IMM16 are enter/ret which should never take a reloc. */
            return arg->type == X86_ARG_IMM && is_in_int16_range(arg->u.imm.offset) && !arg->u.imm.reloc;
        case IMM:
            return arg->type == X86_ARG_IMM;
        case REL8:
            /* These are hard, because there's a circular dependency: we can
             * only use a rel8 jump if it's in range, but we don't know if it's
             * in range until we know what instructions we're using. There are
             * probably clever or complex ways to resolve this, but I'm just
             * going to go ahead and declare this out of scope for Rosé, which
             * is pointedly a barebones compiler. Just always use the REL16
             * versions. */
            return false;
        case REL:
        case SEGPTR:
            return arg->type == X86_ARG_MEM && arg->u.mem.seg == X86_SEG_NONE && arg->u.mem.reg == X86_MEM_REG_NONE
                    && arg->u.mem.offset.reloc && !arg->u.mem.offset.offset;
        case MOFFS:
            return arg->type == X86_ARG_MEM && arg->u.mem.seg == X86_SEG_NONE && arg->u.mem.reg == X86_MEM_REG_NONE;
        case RM:
            return arg->type == X86_ARG_MEM || arg->type == X86_ARG_REG8 || arg->type == X86_ARG_REG16;
        case MEM:
            return arg->type == X86_ARG_MEM;
        case REG8:
            return arg->type == X86_ARG_REG8;
        case REG:
            return arg->type == X86_ARG_REG8 || arg->type == X86_ARG_REG16;
        case SEG16:
            return arg->type == X86_ARG_SEG;
        case ABSRM:
            return arg->type == X86_ARG_ABSMEM || arg->type == X86_ARG_ABSREG;
        case ABSMEM:
            return arg->type == X86_ARG_ABSMEM;
    }

    rose_unreachable();
}

static bool x86_instr_matches(const struct table_instruction *ti, const struct x86_instr *instr)
{
    if (instr->opcode != ti->opcode)
        return false;

    if (ti->size == -1)
    {
        /* Operand size. These instructions must have size 16 or 32; they
         * cannot be implicit. */
        if (instr->size != 16 && instr->size != 32)
            return false;
    }
    else
    {
        if (instr->size != ti->size)
            return false;
    }

    if (!table_arg_matches(ti->arg0, &instr->args[0])
            || !table_arg_matches(ti->arg1, &instr->args[1])
            || !table_arg_matches(ti->arg2, &instr->args[2]))
        return false;

    return true;
}

static const struct table_instruction *get_table_instr(const struct x86_instr *instr)
{
    for (size_t i = 0; i < ARRAY_SIZE(instruction_table); ++i)
    {
        const struct table_instruction *ti = &instruction_table[i];

        if (x86_instr_matches(ti, instr))
            return ti;
    }

    return NULL;
}

static uint8_t get_modrm_byte(const struct x86_arg *modrm_arg,
        const struct x86_arg *other_arg, enum table_arg_type other_type, uint8_t subcode)
{
    uint8_t modrm_mod, modrm_reg, modrm_mem;

    if (modrm_arg->type == X86_ARG_MEM)
    {
        int32_t offset = modrm_arg->u.mem.offset.offset;
        uint32_t reloc = modrm_arg->u.mem.offset.reloc;
        enum x86_mem_reg reg = modrm_arg->u.mem.reg;

        modrm_mem = reg;

        if (reg == X86_MEM_REG_NONE)
        {
            modrm_mem = 6;
            modrm_mod = 0;
        }
        else if (!reloc && !offset && reg != X86_MEM_REG_BP)
            modrm_mod = 0;
        else if (!reloc && is_in_int8_range(offset))
            modrm_mod = 1;
        else
            modrm_mod = 2;
    }
    else /* X86_ARG_REG* */
    {
        modrm_mod = 3;
        modrm_mem = modrm_arg->u.reg; /* or reg8. Hooray for aliasing! */
    }

    if (subcode < 8)
        modrm_reg = subcode;
    else if (other_type == REG8 || other_type == REG || other_type == SEG16)
        modrm_reg = other_arg->u.reg; /* or reg8, or seg */
    else
        assert(0);

    assert(modrm_mod <= 3);
    assert(modrm_reg <= 7);
    assert(modrm_mem <= 7);
    return (modrm_mod << 6) | (modrm_reg << 3) | modrm_mem;
}

static void write_integer(struct x86_buffer *buffer, struct x86_block *block,
        int8_t size, const struct x86_integer *integer)
{
    uint32_t target = 0xdeadbeef;

    assert(size);

    if (integer->reloc)
    {
        struct x86_reloc *reloc = &block->relocs[integer->reloc - 1];

        assert(size == 16 || size == 32);

        switch (reloc->type)
        {
            case X86_RELOC_UNRESOLVED_LOCAL:
            case X86_RELOC_UNRESOLVED_SEGMENT:
            case X86_RELOC_UNRESOLVED_OFFSET:
            case X86_RELOC_UNRESOLVED_PAIR:
                /* Caller (the linker) should prevent this.
                 * FIXME: If (when) we separate the compiler and linker,
                 * what happens? */
                assert(0);
                break;

            case X86_RELOC_LOCAL:
            case X86_RELOC_INTERNAL_OFFSET:
                target = reloc->u.symbol->instr->byte_offset;
                target += integer->offset;
                break;

            case X86_RELOC_INTERNAL_PAIR:
                TRACE("Writing internal pair %p.\n", reloc);
                write_u16(buffer, reloc->u.symbol->instr->byte_offset + integer->offset);
                reloc->byte_offset = buffer->size;
                write_u16(buffer, reloc->prev ? reloc->prev->byte_offset : 0xffff);
                return;

            case X86_RELOC_INTERNAL_SEGMENT:
                TRACE("Writing internal segment %p.\n", reloc);
                assert(!integer->offset);
                reloc->byte_offset = buffer->size;
                write_u16(buffer, reloc->prev ? reloc->prev->byte_offset : 0xffff);
                return;

            case X86_RELOC_EXTERNAL_PAIR:
                TRACE("Writing external pair %p.\n", reloc);
                if (integer->offset)
                    x86_error(buffer, "Cannot add offset to external relocation.");

                /* Order here is different than internal pairs!
                 * This is because krnl386 is going to replace all 4 bytes here,
                 * but for internal pairs it only replaces the high 2 bytes. */
                reloc->byte_offset = buffer->size;
                write_u16(buffer, reloc->prev ? reloc->prev->byte_offset : 0xffff);
                write_u16(buffer, 0);
                return;
        }
    }
    else
    {
        target = integer->offset;
    }

    if (size == 8)
        write_u8(buffer, target);
    else if (size == 16)
        write_u16(buffer, target);
    else
        write_u32(buffer, target);
}

static void write_arg(struct x86_buffer *buffer, struct x86_block *block, int8_t size,
        enum table_arg_type table_type, const struct x86_arg *arg, uint8_t modrm_byte)
{
    const struct x86_reloc *reloc;

    switch (table_type)
    {
        case NONE:
        case ONE:
        case AL:
        case CL:
        case AX:
        case ES:
        case CS:
        case SS:
        case DS:
        case FS:
        case GS:
            /* Nothing to write. */
            break;

        case IMM8:
            assert(!arg->u.imm.reloc);
            write_u8(buffer, arg->u.imm.offset);
            break;

        case IMM16:
            assert(!arg->u.imm.reloc);
            write_u16(buffer, arg->u.imm.offset);
            break;

        case IMM:
            write_integer(buffer, block, size, &arg->u.imm);
            break;

        /* These are relative to the end of the instruction.
         * Fortunately, in practice the REL is always the *only* argument. */
        case REL8:
            assert(0);
            break;
        case REL:
        {
            assert(arg->u.mem.offset.reloc);
            reloc = &block->relocs[arg->u.mem.offset.reloc - 1];
            if (reloc->type == X86_RELOC_LOCAL)
            {
                uint32_t target = reloc->u.symbol->instr->byte_offset;

                /* 32-bit support goes here. */
                write_u16(buffer, target - (buffer->size + sizeof(uint16_t)));
            }
            else
            {
                x86_error(buffer, "Cannot resolve jump target.");
            }
            break;
        }

        case SEGPTR:
            assert(arg->u.mem.offset.reloc);
            reloc = &block->relocs[arg->u.mem.offset.reloc - 1];
            if (reloc->type == X86_RELOC_INTERNAL_PAIR || reloc->type == X86_RELOC_EXTERNAL_PAIR)
                write_integer(buffer, block, 16, &arg->u.mem.offset);
            else
                x86_error(buffer, "Cannot resolve long jump target.");
            break;

        case MOFFS:
            write_integer(buffer, block, 16, &arg->u.mem.offset);
            break;

        case RM:
        case MEM:
        case ABSRM:
        case ABSMEM:
        {
            uint8_t modrm_mod = modrm_byte >> 6;
            uint8_t modrm_mem = modrm_byte & 7;

            if (modrm_mod == 1)
            {
                assert(!arg->u.mem.offset.reloc);
                write_u8(buffer, arg->u.mem.offset.offset);
            }
            else if (modrm_mod == 2 || (modrm_mod == 0 && modrm_mem == 6))
            {
                write_integer(buffer, block, 16, &arg->u.mem.offset);
            }
            /* Nothing to write otherwise. */
            break;
        }

        case REG8:
        case REG:
        case SEG16:
            /* Already handled in the modrm byte or opcode_arg. */
            break;
    }
}

static void write_addr_override(struct x86_buffer *buffer, const struct x86_arg *arg)
{
    if (arg->type != X86_ARG_MEM && arg->type != X86_ARG_ABSMEM)
        return;

    switch (arg->u.mem.reg)
    {
        case X86_MEM_REG_BP:
        case X86_MEM_REG_BP_DI:
        case X86_MEM_REG_BP_SI:
            if (arg->u.mem.seg == X86_SEG_SS)
                return;
            break;

        case X86_MEM_REG_BX:
        case X86_MEM_REG_BX_DI:
        case X86_MEM_REG_BX_SI:
        case X86_MEM_REG_NONE:
        case X86_MEM_REG_DI:
        case X86_MEM_REG_SI:
            if (arg->u.mem.seg == X86_SEG_DS)
                return;
            break;
    }

    switch (arg->u.mem.seg)
    {
        case X86_SEG_CS:
            write_u8(buffer, 0x2e);
            break;

        case X86_SEG_DS:
            write_u8(buffer, 0x3e);
            break;

        case X86_SEG_ES:
            write_u8(buffer, 0x26);
            break;

        case X86_SEG_FS:
            write_u8(buffer, 0x64);
            break;

        case X86_SEG_GS:
            write_u8(buffer, 0x65);
            break;

        case X86_SEG_SS:
            write_u8(buffer, 0x36);
            break;

        case X86_SEG_NONE:
            break;
    }
}

static void x86_write_instr(struct x86_buffer *buffer, struct x86_block *block, struct x86_instr *instr)
{
    const struct x86_arg *arg0 = &instr->args[0];
    const struct x86_arg *arg1 = &instr->args[1];
    const struct x86_arg *arg2 = &instr->args[2];
    const struct table_instruction *ti;
    uint8_t modrm_byte = 0;
    uint16_t opcode_bytes;

    instr->byte_offset = buffer->size;

    if (instr->opcode == X86_OP_DATA)
    {
        TRACE("Writing data, size %u.\n", instr->size);
        write_integer(buffer, block, instr->size, &instr->args[0].u.imm);
        return;
    }

    ti = get_table_instr(instr);
    if (!ti)
    {
        x86_error(buffer, "Cannot map instruction %u.", instr->opcode);
        return;
    }

    TRACE("Writing instruction %02X %s, offset %#04zx.\n", ti->opcode_bytes, ti->name, buffer->size);

    /* Size prefix. */

    if (instr->size == 32)
        write_u8(buffer, 0x66);

    write_addr_override(buffer, arg0);
    write_addr_override(buffer, arg1);
    write_addr_override(buffer, arg2);

    /* Opcode byte(s). */

    opcode_bytes = ti->opcode_bytes;
    if (ti->opcode_arg0)
    {
        assert(arg0->type == X86_ARG_REG8 || arg0->type == X86_ARG_REG16);
        opcode_bytes += arg0->u.reg; /* doesn't matter */
    }
    else if (ti->opcode_arg1)
    {
        assert(arg1->type == X86_ARG_REG8 || arg1->type == X86_ARG_REG16);
        opcode_bytes += arg1->u.reg; /* doesn't matter */
    }
    if (opcode_bytes >> 8)
        write_u8(buffer, opcode_bytes >> 8);
    write_u8(buffer, opcode_bytes);

    /* ModR/M byte; this always goes first. */

    if (ti->arg0 == RM || ti->arg0 == MEM || ti->arg0 == ABSRM || ti->arg0 == ABSMEM)
    {
        modrm_byte = get_modrm_byte(arg0, arg1, ti->arg1, ti->subcode);
        write_u8(buffer, modrm_byte);
    }
    else if (ti->arg1 == RM || ti->arg1 == MEM)
    {
        /* IMUL breaks the rules and puts the "other" arg in arg2 instead of arg0. */
        if (ti->arg2 == REG)
            modrm_byte = get_modrm_byte(arg1, arg2, ti->arg2, ti->subcode);
        else
            modrm_byte = get_modrm_byte(arg1, arg0, ti->arg0, ti->subcode);
        write_u8(buffer, modrm_byte);
    }
    else if (ti->arg2 == RM || ti->arg2 == MEM)
    {
        /* SHLD, SHRD. */
        modrm_byte = get_modrm_byte(arg2, arg1, ti->arg1, ti->subcode);
        write_u8(buffer, modrm_byte);
    }

    /* Remaining args. */

    if (instr->opcode == X86_OP_ENTER)
    {
        /* Not reversed on GAS for... some reason. */
        write_arg(buffer, block, instr->size, ti->arg0, arg0, modrm_byte);
        write_arg(buffer, block, instr->size, ti->arg1, arg1, modrm_byte);
    }
    else
    {
        write_arg(buffer, block, instr->size, ti->arg2, arg2, modrm_byte);
        write_arg(buffer, block, instr->size, ti->arg1, arg1, modrm_byte);
        write_arg(buffer, block, instr->size, ti->arg0, arg0, modrm_byte);
    }
}

void x86_write_block(struct x86_buffer *buffer, struct x86_block *block)
{
    for (size_t i = 0; i < block->count; ++i)
        x86_write_instr(buffer, block, &block->instrs[i]);
}
