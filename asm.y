/*
 * Copyright 2023 Elizabeth Figura
 *
 * This file is part of the Rosé compiler.
 *
 * Rosé is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rosé is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Rosé; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

%code requires
{

#include "asm.h"

}

%code provides
{

int yylex(ASM_YYSTYPE *yylval_param, void *yyscanner);

}

%code
{

static void asm_error(struct asm_ctx *ctx, const char *fmt, ...)
{
    va_list a;

    va_start(a, fmt);
    printf("Assembler error: ");
    vprintf(fmt, a);
    printf("\n");
    va_end(a);
    ctx->failed = true;
}

static void yyerror(void *scanner, struct asm_ctx *ctx, const char *s)
{
    asm_error(ctx, "%s", s);
}

static bool get_reg(const char *str, struct x86_arg *arg)
{
    static const struct
    {
        const char *str;
        struct x86_arg reg;
    }
    regs[] =
    {
        {"al",  {X86_ARG_REG8,  .u.reg8 = X86_REG8_AL}},
        {"cl",  {X86_ARG_REG8,  .u.reg8 = X86_REG8_CL}},
        {"dl",  {X86_ARG_REG8,  .u.reg8 = X86_REG8_DL}},
        {"bl",  {X86_ARG_REG8,  .u.reg8 = X86_REG8_BL}},
        {"ah",  {X86_ARG_REG8,  .u.reg8 = X86_REG8_AH}},
        {"ch",  {X86_ARG_REG8,  .u.reg8 = X86_REG8_CH}},
        {"dh",  {X86_ARG_REG8,  .u.reg8 = X86_REG8_DH}},
        {"bh",  {X86_ARG_REG8,  .u.reg8 = X86_REG8_BH}},
        {"ax",  {X86_ARG_REG16, .u.reg = X86_REG_AX}},
        {"cx",  {X86_ARG_REG16, .u.reg = X86_REG_CX}},
        {"dx",  {X86_ARG_REG16, .u.reg = X86_REG_DX}},
        {"bx",  {X86_ARG_REG16, .u.reg = X86_REG_BX}},
        {"sp",  {X86_ARG_REG16, .u.reg = X86_REG_SP}},
        {"bp",  {X86_ARG_REG16, .u.reg = X86_REG_BP}},
        {"si",  {X86_ARG_REG16, .u.reg = X86_REG_SI}},
        {"di",  {X86_ARG_REG16, .u.reg = X86_REG_DI}},
        {"es",  {X86_ARG_SEG,   .u.seg = X86_SEG_ES}},
        {"cs",  {X86_ARG_SEG,   .u.seg = X86_SEG_CS}},
        {"ss",  {X86_ARG_SEG,   .u.seg = X86_SEG_SS}},
        {"ds",  {X86_ARG_SEG,   .u.seg = X86_SEG_DS}},
        {"fs",  {X86_ARG_SEG,   .u.seg = X86_SEG_FS}},
        {"gs",  {X86_ARG_SEG,   .u.seg = X86_SEG_GS}},
    };

    unsigned int i;

    for (i = 0; i < ARRAY_SIZE(regs); ++i)
    {
        if (!strcmp(str, regs[i].str))
        {
            *arg = regs[i].reg;
            return true;
        }
    }

    return false;
}

static bool get_opcode(const char *str, struct x86_instr *instr)
{
    /* GAS allows sizes to be implicit in many cases.
     * Because Rosé is minimal, to make our job easier, we always require
     * instructions to have a size attached to the mnemonic.
     * Personally I think this is an improvement anyway; it forces the language
     * to be more explicit. Also, it generally matches what's done in Wine
     * already. */
    static const struct
    {
        const char *str;
        enum x86_opcode opcode;
        int8_t size;
    }
    opcodes[] =
    {
        {"callw",   X86_OP_CALL, 16},
        {"calll",   X86_OP_CALL, 32},
        {"imulw",   X86_OP_IMUL, 16},
        {"int",     X86_OP_INT, 0},
        {"jnc",     X86_OP_JAE, 0},
        {"lcallw",  X86_OP_LCALL, 16},
        {"lcalll",  X86_OP_LCALL, 32},
        {"ldsw",    X86_OP_LDS, 16},
        {"ldsl",    X86_OP_LDS, 32},
        {"lesw",    X86_OP_LES, 16},
        {"leavew",  X86_OP_LEAVE, 16},
        {"leavel",  X86_OP_LEAVE, 32},
        {"lretw",   X86_OP_LRET, 16},
        {"lretl",   X86_OP_LRET, 32},
        {"movb",    X86_OP_MOV, 8},
        {"movw",    X86_OP_MOV, 16},
        {"movl",    X86_OP_MOV, 32},
        {"popw",    X86_OP_POP, 16},
        {"popl",    X86_OP_POP, 32},
        {"pushw",   X86_OP_PUSH, 16},
        {"pushl",   X86_OP_PUSH, 32},
        {"retw",    X86_OP_RET, 16},
        {"sbbw",    X86_OP_SBB, 16},
        {"xorw",    X86_OP_XOR, 16},
    };

    unsigned int i;

    for (i = 0; i < ARRAY_SIZE(opcodes); ++i)
    {
        if (!strcmp(str, opcodes[i].str))
        {
            instr->opcode = opcodes[i].opcode;
            instr->size = opcodes[i].size;
            return true;
        }
    }

    return false;
}

}

%define parse.error verbose
%define api.prefix {asm_yy}
%define api.pure full
%lex-param {yyscan_t scanner}
%parse-param {void *scanner}
%parse-param {struct asm_ctx *ctx}

%union
{
    int intval;
    char *strval;
    struct x86_instr instr;
    struct x86_arg arg;
    enum x86_seg seg;
    struct x86_integer x86_integer;
}

%token T_GLOBL
%token T_NEWLINE
%token T_OFFSET
%token T_SEGMENT
%token T_SEGPTR
%token <intval> T_INTEGER
%token <strval> T_IDENTIFIER

%type <arg> arg
%type <arg> mem
%type <arg> reg
%type <arg> reg_dereference

%type <instr> args

%type <seg> segment_prefix

%type <x86_integer> integer

%%

asm_prog:
      %empty
    | asm_prog line T_NEWLINE
    | asm_prog line ';'
    | asm_prog T_NEWLINE

line:
      T_IDENTIFIER args
        {
            struct x86_instr instr = $2;

            if (!get_opcode($1, &instr))
                asm_error(ctx, "Invalid opcode %s.", $1);
            x86_add_instr(&ctx->output, &instr);
        }
    | T_IDENTIFIER ':'
        {
            x86_add_symbol(&ctx->output, $1);
        }
    | T_GLOBL T_IDENTIFIER
        {
            array_reserve((void **)&ctx->globals, &ctx->globals_capacity,
                    ctx->global_count + 1, sizeof(*ctx->globals));
            ctx->globals[ctx->global_count++] = strdup($2);
        }

args:
      %empty
        {
            memset(&$$, 0, sizeof($$));
        }
    | arg
        {
            memset(&$$, 0, sizeof($$));
            $$.args[0] = $1;
        }
    | arg ',' arg
        {
            memset(&$$, 0, sizeof($$));
            $$.args[0] = $1;
            $$.args[1] = $3;
        }
    | arg ',' arg ',' arg
        {
            memset(&$$, 0, sizeof($$));
            $$.args[0] = $1;
            $$.args[1] = $3;
            $$.args[2] = $5;
        }

arg:
      '$' integer
        {
            $$.type = X86_ARG_IMM;
            $$.u.imm = $2;
        }
    | reg
    | mem
    | '*' reg
        {
            $$ = $2;
            if ($$.type != X86_ARG_REG16)
                asm_error(ctx, "Invalid absolute register.");
            $$.type = X86_ARG_ABSREG;
        }
    | '*' mem
        {
            $$ = $2;
            $$.type = X86_ARG_ABSMEM;
        }

mem:
      integer
        {
            $$.type = X86_ARG_MEM;
            $$.u.mem.seg = X86_SEG_NONE;
            $$.u.mem.reg = X86_MEM_REG_NONE;
            $$.u.mem.offset = $1;
        }
    | reg_dereference
    | integer reg_dereference
        {
            $$ = $2;
            $$.u.mem.offset = $1;
        }
    | segment_prefix reg_dereference
        {
            $$ = $2;
            $$.u.mem.seg = $1;
        }
    | segment_prefix integer reg_dereference
        {
            $$ = $3;
            $$.u.mem.seg = $1;
            $$.u.mem.offset = $2;
        }

reg_dereference:
      '(' reg ')'
        {
            $$.type = X86_ARG_MEM;
            $$.u.mem.seg = X86_SEG_NONE;
            $$.u.mem.offset.offset = 0;
            $$.u.mem.offset.reloc = 0;

            if ($2.type == X86_ARG_REG16)
            {
                if ($2.u.reg == X86_REG_BX)
                    $$.u.mem.reg = X86_MEM_REG_BX;
                else if ($2.u.reg == X86_REG_BP)
                    $$.u.mem.reg = X86_MEM_REG_BP;
                else if ($2.u.reg == X86_REG_SI)
                    $$.u.mem.reg = X86_MEM_REG_SI;
                else if ($2.u.reg == X86_REG_DI)
                    $$.u.mem.reg = X86_MEM_REG_DI;
                else
                    asm_error(ctx, "Invalid dereferenced register %#x.", $2.u.reg);
            }
            else
            {
                asm_error(ctx, "Invalid dereferenced register type %#x.", $2.type);
            }
        }
    | '(' reg ',' reg ')'
        {
            $$.type = X86_ARG_MEM;
            $$.u.mem.seg = X86_SEG_NONE;
            $$.u.mem.offset.offset = 0;
            $$.u.mem.offset.reloc = 0;

            if ($2.type == X86_ARG_REG16 && $4.type == X86_ARG_REG16)
            {
                if ($2.u.reg == X86_REG_BX && $4.u.reg == X86_REG_SI)
                    $$.u.mem.reg = X86_MEM_REG_BX_SI;
                else if ($2.u.reg == X86_REG_BX && $4.u.reg == X86_REG_DI)
                    $$.u.mem.reg = X86_MEM_REG_BX_DI;
                else if ($2.u.reg == X86_REG_BP && $4.u.reg == X86_REG_SI)
                    $$.u.mem.reg = X86_MEM_REG_BP_SI;
                else if ($2.u.reg == X86_REG_BP && $4.u.reg == X86_REG_DI)
                    $$.u.mem.reg = X86_MEM_REG_BP_DI;
                else
                    asm_error(ctx, "Invalid dereferenced registers %#x, %#x.", $2.u.reg, $4.u.reg);
            }
            else
            {
                asm_error(ctx, "Invalid dereferenced register types %#x, %#x.", $2.type, $4.type);
            }
        }

segment_prefix:
      reg ':'
        {
            if ($1.type == X86_ARG_SEG)
            {
                $$ = $1.u.seg;
            }
            else
            {
                asm_error(ctx, "Invalid segment register type %#x.", $1.type);
            }
        }

integer:
      T_INTEGER
        {
            $$.offset = $1;
            $$.reloc = 0;
        }
    | T_IDENTIFIER
        {
            $$.reloc = x86_add_reloc(&ctx->output, X86_RELOC_UNRESOLVED_LOCAL, $1);
            $$.offset = 0;
        }
    | T_OFFSET ':' T_IDENTIFIER
        {
            $$.reloc = x86_add_reloc(&ctx->output, X86_RELOC_UNRESOLVED_OFFSET, $3);
            $$.offset = 0;
        }
    | T_SEGMENT ':' T_IDENTIFIER
        {
            $$.reloc = x86_add_reloc(&ctx->output, X86_RELOC_UNRESOLVED_SEGMENT, $3);
            $$.offset = 0;
        }
    | T_SEGPTR ':' T_IDENTIFIER
        {
            $$.reloc = x86_add_reloc(&ctx->output, X86_RELOC_UNRESOLVED_PAIR, $3);
            $$.offset = 0;
        }

reg:
      '%' T_IDENTIFIER
        {
            if (!get_reg($2, &$$))
                asm_error(ctx, "Invalid register %s.", $2);
        }
