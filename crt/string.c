/*
 * Copyright 2023 Elizabeth Figura
 *
 * This file is part of the Rosé compiler.
 *
 * Rosé is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rosé is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Rosé; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stddef.h>
#include <stdlib.h>
#include <string.h>

void _far * _far _cdecl memchr(const void _far *s, int c, size_t n)
{
    const char _far *p = s;

    for (; n > 0; --n)
    {
        if (*p == (char)c)
            return (void _far *)p;
        ++p;
    }

    return NULL;
}

int _far _cdecl memcmp(const void _far *a, const void _far *b, size_t n)
{
    const char _far *p = a, *q = b;

    for (; n > 0; --n)
    {
        if (*p < *q)
            return -1;
        if (*p > *q)
            return 1;
        ++p;
        ++q;
    }
    return 0;
}

void _far * _far _cdecl memcpy(void _far *dst, const void _far *src, size_t n)
{
    return memmove(dst, src, n);
}

void _far * _far _cdecl memmove(void _far *dst, const void _far *src, size_t n)
{
    const char _far *s = src;
    char _far *d = dst;

    if (d - s >= n)
    {
        for (; n > 0; --n)
            *d++ = *s++;
    }
    else
    {
        d += n;
        s += n;
        for (; n > 0; --n)
            *--d = *--s;
    }
    return d;
}

void _far * _far _cdecl memset(void _far *dst, int c, size_t n)
{
    char _far *p = dst;

    for (; n > 0; --n)
        *p++ = c;
    return p;
}

char _far * _far _cdecl strchr(const char _far *s, int c)
{
    while (*s)
    {
        if (*s == (char)c)
            return (char _far *)s;
        ++s;
    }
    return NULL;
}

char _far * _far _cdecl strrchr(const char _far *s, int c)
{
    char _far *ret = NULL;

    while (*s)
    {
        if (*s == (char)c)
            ret = (char _far *)s;
        ++s;
    }
    return ret;
}

int _far _cdecl strcmp(const char _far *a, const char _far *b)
{
    while (*a == *b && *a)
    {
        ++a;
        ++b;
    }

    if (*a < *b)
        return -1;
    if (*a > *b)
        return 1;
    return 0;
}

char _far * _far _cdecl strcpy(char _far *dst, const char _far *src)
{
    char _far *ret = dst;
    while ((*dst++ = *src++));
    return ret;
}

size_t _far _cdecl strlen(const char _far *s)
{
    const char _far *p = s;

    while (*p)
        ++p;
    return p - s;
}

char _far * _far _cdecl strdup(const char _far *s)
{
    size_t len = strlen(s) + 1;
    char _far *dst;

    if ((dst = malloc(len)))
        memcpy(dst, s, len);
    return dst;
}
