/*
 * ntdll printf functions
 *
 * Copyright 1999, 2009 Alexandre Julliard
 * Copyright 2000 Jon Griffiths
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdarg.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define size_max (~(size_t)0 >> 1)

typedef struct pf_flags_t
{
    bool is_short, is_long;
    bool LeftAlign, Alternate, PadZero;
    int FieldLength, Precision;
    char Sign, Format;
} pf_flags;

typedef struct
{
    char _far *buf;
    size_t len;
    size_t used;
} pf_output;

static int pf_output_string( pf_output _far *out, const char _far *str, int len )
{
    size_t space = out->len - out->used;
    char _far *p = out->buf + out->used;

    if (len < 0) len = strlen( str );

    if (out->buf)
    {
        if (space < len) /* overflow */
        {
            memcpy( p, str, space );
            out->used = out->len;
            return -1;
        }
        memcpy( p, str, len );
    }
    out->used += len;
    return len;
}

/* pf_fill: takes care of signs, alignment, zero and field padding */
static int pf_fill_left( pf_output _far *out, int len, pf_flags _far *flags )
{
    int i, r = 0;

    if (flags->Sign && !(flags->Format == 'd' || flags->Format == 'i')) flags->Sign = 0;

    if (flags->Sign)
    {
        char ch = flags->Sign;
        flags->FieldLength--;
        if (flags->PadZero) r = pf_output_string( out, &ch, 1 );
    }
    if (!flags->LeftAlign)
    {
        char ch = flags->PadZero ? '0' : ' ';
        for (i = 0; i < flags->FieldLength - len && r >= 0; i++)
            r = pf_output_string( out, &ch, 1 );
    }
    if (flags->Sign && !flags->PadZero && r >= 0)
    {
        char ch = flags->Sign;
        r = pf_output_string( out, &ch, 1 );
    }
    return r;
}

static int pf_fill_right( pf_output _far *out, int len, pf_flags _far *flags )
{
    int i, r = 0;
    char ch = ' ';

    if (!flags->LeftAlign) return 0;

    for (i = 0; i < flags->FieldLength - len && r >= 0; i++)
        r = pf_output_string( out, &ch, 1 );
    return r;
}

static int pf_output_format( pf_output _far *out, const char _far *str, int len, pf_flags _far *flags )
{
    int r = 0;

    if (len < 0)
    {
        /* Do not search past the length specified by the precision. */
        if (flags->Precision >= 0)
        {
            for (len = 0; len < flags->Precision; len++) if (!str[len]) break;
        }
        else len = strlen(str);
    }

    if (flags->Precision >= 0 && flags->Precision < len) len = flags->Precision;

    r = pf_fill_left( out, len, flags );
    if (r >= 0) r = pf_output_string( out, str, len );
    if (r >= 0) r = pf_fill_right( out, len, flags );
    return r;
}

static int pf_handle_string_format( pf_output _far *out, const void _far *str, int len, pf_flags _far *flags )
{
    if (!str)
        return pf_output_format( out, "(null)", -1, flags );
    return pf_output_format( out, str, len, flags );
}

/* pf_integer_conv:  prints x to buf, including alternate formats and
   additional precision digits, but not field characters or the sign */
static void pf_integer_conv( char _far *buf, pf_flags _far *flags, long x )
{
    unsigned int base;
    const char *digits;
    int i, j, k;

    if( flags->Format == 'o' )
        base = 8;
    else if( flags->Format == 'x' || flags->Format == 'X' )
        base = 16;
    else
        base = 10;

    if( flags->Format == 'X' )
        digits = "0123456789ABCDEFX";
    else
        digits = "0123456789abcdefx";

    if( x < 0 && ( flags->Format == 'd' || flags->Format == 'i' ) )
    {
        x = -x;
        flags->Sign = '-';
    }

    i = 0;
    if( x == 0 )
    {
        flags->Alternate = false;
        if( flags->Precision )
            buf[i++] = '0';
    }
    else
        while( x != 0 )
        {
            j = (unsigned long) x % base;
            x = (unsigned long) x / base;
            buf[i++] = digits[j];
        }
    k = flags->Precision - i;
    while( k-- > 0 )
        buf[i++] = '0';
    if( flags->Alternate )
    {
        if( base == 16 )
        {
            buf[i++] = digits[16];
            buf[i++] = '0';
        }
        else if( base == 8 && buf[i-1] != '0' )
            buf[i++] = '0';
    }

    /* Adjust precision so pf_fill won't truncate the number later */
    flags->Precision = i;

    buf[i] = '\0';
    j = 0;
    while(--i > j) {
        char tmp = buf[j];
        buf[j] = buf[i];
        buf[i] = tmp;
        j++;
    }
}

static int pf_vsnprintf( pf_output _far *out, const char _far *format, va_list valist )
{
    int r;
    const char _far *q, *p = format;
    pf_flags flags;

    while (*p)
    {
        for (q = p; *q; q++) if (*q == '%') break;

        if (q != p)
        {
            r = pf_output_string(out, p, q - p);
            if (r < 0) return r;
            p = q;
            if (!*p) break;
        }
        p++;

        /* output a single % character */
        if( *p == '%' )
        {
            r = pf_output_string(out, p++, 1);
            if( r<0 )
                return r;
            continue;
        }

        /* parse the flags */
        memset( &flags, 0, sizeof(flags) );
        while (*p)
        {
            if( *p == '+' || *p == ' ' )
            {
                if ( flags.Sign != '+' )
                    flags.Sign = *p;
            }
            else if( *p == '-' )
                flags.LeftAlign = true;
            else if( *p == '0' )
                flags.PadZero = true;
            else if( *p == '#' )
                flags.Alternate = true;
            else
                break;
            p++;
        }

        /* deal with the field width specifier */
        flags.FieldLength = 0;
        if( *p == '*' )
        {
            flags.FieldLength = va_arg( valist, int );
            if (flags.FieldLength < 0)
            {
                flags.LeftAlign = true;
                flags.FieldLength = -flags.FieldLength;
            }
            p++;
        }
        else while (*p >= '0' && *p <= '9')
        {
            flags.FieldLength *= 10;
            flags.FieldLength += *p++ - '0';
        }

        /* deal with precision */
        flags.Precision = -1;
        if( *p == '.' )
        {
            flags.Precision = 0;
            p++;
            if( *p == '*' )
            {
                flags.Precision = va_arg( valist, int );
                p++;
            }
            else while (*p >= '0' && *p <= '9')
            {
                flags.Precision *= 10;
                flags.Precision += *p++ - '0';
            }
        }

        /* deal with integer width modifier */
        while( *p )
        {
            if (*p == 'l')
            {
                flags.is_long = true;
                p++;
            }
            else if( *p == 'L' )
            {
                p++;
            }
            else if( *p == 'h')
            {
                flags.is_short = true;
                p++;
            }
            else
                break;
        }

        flags.Format = *p;
        r = 0;

        /* output a string */
        if (flags.Format == 's')
            r = pf_handle_string_format( out, va_arg(valist, const void _far *), -1, &flags );

        /* output a single character */
        else if (flags.Format == 'c')
        {
            char ch = (char)va_arg( valist, int );
            r = pf_handle_string_format( out, &ch, 1, &flags );
        }

        /* output a pointer */
        else if( flags.Format == 'p' )
        {
            char pointer[32];
            void _far *ptr = va_arg( valist, void _far * );
            int prec = flags.Precision;
            flags.Format = 'X';
            flags.PadZero = true;
            flags.Precision = 2 * sizeof(void _far *);
            pf_integer_conv( pointer, &flags, (unsigned long)ptr );
            flags.PadZero = false;
            flags.Precision = prec;
            r = pf_output_format( out, pointer, -1, &flags );
        }

        /* deal with %n */
        else if( flags.Format == 'n' )
        {
            int _far *x = va_arg(valist, int _far *);
            *x = out->used;
        }
        else if( flags.Format && strchr("diouxX", flags.Format ))
        {
            char number[40];
            char _far *x = number;
            int max_len;

            /* 0 padding is added after '0x' if Alternate flag is in use */
            if((flags.Format=='x' || flags.Format=='X') && flags.PadZero && flags.Alternate
                    && !flags.LeftAlign && flags.Precision<flags.FieldLength-2)
                flags.Precision = flags.FieldLength - 2;

            max_len = (flags.FieldLength>flags.Precision ? flags.FieldLength : flags.Precision) + 10;
            if(max_len > sizeof(number))
                if (!(x = malloc( max_len ))) return -1;

            if (flags.Format == 'd' || flags.Format == 'i')
                pf_integer_conv( x, &flags, flags.is_long ? va_arg(valist, long) : va_arg(valist, int) );
            else
                pf_integer_conv( x, &flags, flags.is_long ? va_arg(valist, unsigned long) : va_arg(valist, unsigned int) );

            r = pf_output_format( out, x, -1, &flags );
            if( x != number )
                free( x );
        }
        else
            continue;

        if( r<0 )
            return r;
        p++;
    }
    return out->used;
}

int _far _cdecl vsnprintf( char _far *str, size_t len, const char _far *format, va_list args )
{
    pf_output out = { str, len };
    int r = pf_vsnprintf( &out, format, args );

    if (out.used < len) str[out.used] = 0;
    return r;
}

int _far _cdecl snprintf( char _far *str, size_t len, const char _far *format, ... )
{
    int ret;
    va_list valist;

    va_start( valist, format );
    ret = vsnprintf( str, len, format, valist );
    va_end( valist );
    return ret;
}

int _far _cdecl vsprintf( char _far *str, const char _far *format, va_list args )
{
    return vsnprintf( str, size_max, format, args );
}

int _far _cdecl sprintf( char _far *str, const char _far *format, ... )
{
    int ret;
    va_list valist;

    va_start( valist, format );
    ret = vsnprintf( str, size_max, format, valist );
    va_end( valist );
    return ret;
}
