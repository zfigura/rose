/*
 * Copyright 2023 Elizabeth Figura
 *
 * This file is part of the Rosé compiler.
 *
 * Rosé is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rosé is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Rosé; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>

void _far * _far _cdecl malloc(size_t size)
{
    HGLOBAL handle = GlobalAlloc(GMEM_FIXED, size + 2);
    /* stash the handle in the first 2 bytes */
    HGLOBAL _far *ptr = GlobalLock(handle);

    *ptr = handle;
    return ptr + 1;
}

void _far * _far _cdecl realloc(void _far *mem, size_t size)
{
    HGLOBAL _far *ptr = mem;
    HGLOBAL handle = ptr[-1];
    HGLOBAL new_handle;

    GlobalUnlock(handle);
    new_handle = GlobalReAlloc(handle, size + 2, GMEM_FIXED);
    ptr = GlobalLock(new_handle);
    /* handle may have changed, so stash the new handle */
    *ptr = handle;
    return ptr + 1;
}

void _far _cdecl free(void _far *mem)
{
    HGLOBAL _far *ptr = mem;
    HGLOBAL handle = ptr[-1];

    GlobalUnlock(handle);
    GlobalFree(handle);
}

asm (
    ".globl exit\n"
    "exit:\n\t"
    "popw %ax\n\t"
    "popw %ax\n\t"
    "popw %ax\n\t"
    "movb $0x4c,%ah\n\t"
    "int $0x21\n\t"
)

char _far * _far _cdecl getenv(const char _far *name)
{
    char _far *env = GetDosEnvironment();
    size_t name_len = strlen(name);

    for (char _far *p = env; *p; p += strlen(p) + 1)
    {
        size_t len = strchr(p, '=') - p;

        if (len == name_len && !memcmp(p, name, len))
            return p + len + 1;
    }

    return NULL;
}

int _far _cdecl atoi(const char _far *s)
{
    int negative = 0;
    int ret = 0;

    while (*s == ' ' || *s >= '\t' && *s <= '\r')
        ++s;

    if (*s == '-')
        negative = 1;
    else if (*s == '+')
        ++s;

    while (*s >= '0' && *s <= '9')
    {
        ret = ret * 10 + (*s - '0');
        ++s;
    }

    if (negative)
        ret = -ret;
    return ret;
}
