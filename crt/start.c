/*
 * Copyright 2023 Elizabeth Figura
 *
 * This file is part of the Rosé compiler.
 *
 * Rosé is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rosé is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Rosé; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>

asm (
    ".globl _start\n"
    "_start:\n\t"
    "lcallw segptr:InitTask\n\t"
    "pushw %di\n\t"
    "pushw %si\n\t"
    "pushw %es\n\t"
    "pushw %bx\n\t"
    "pushw %dx\n\t"
    /* WinMain() is not declared as a far function. Curiously, though, when I
     * look at disassembly of generated NE executables, it seems as though it
     * gets called like one anyway (even when there's only the one segment).
     * What's up with that?
     * Anyway, we're going to treat it as a near function. */
    "callw WinMain\n\t"
    "movb $0x4c,%ah\n\t"
    "int $0x21\n\t"
)
