/*
 * Variable argument definitions
 *
 * Copyright 2022 Jacek Caban
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef _INC_VADEFS
#define _INC_VADEFS

#include <stddef.h>

typedef char _far *va_list;

#define _INTSIZEOF(n) ((sizeof(n) + sizeof(int) - 1) & ~(sizeof(int) - 1))
#define va_start(v,l)  ((v) = (va_list)&(l) + _INTSIZEOF(l))
#define va_arg(v,l)    (*(l _far *)(((v) += _INTSIZEOF(l)) - _INTSIZEOF(l)))
#define va_end(v)      ((v) = (va_list)NULL)
#define va_copy(d,s)   ((d) = (s))

#endif /* _INC_VADEFS */
