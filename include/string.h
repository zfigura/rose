/*
 * Copyright 2023 Elizabeth Figura
 *
 * This file is part of the Rosé compiler.
 *
 * Rosé is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rosé is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Rosé; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef __ROSE_STRING_H
#define __ROSE_STRING_H

void _far * _far _cdecl memchr(const void _far *s, int c, size_t n);
int         _far _cdecl memcmp(const void _far *a, const void _far *b, size_t n);
void _far * _far _cdecl memcpy(void _far *dst, const void _far *src, size_t n);
void _far * _far _cdecl memmove(void _far *dst, const void _far *src, size_t n);
void _far * _far _cdecl memset(void _far *dst, int c, size_t n);
char _far * _far _cdecl strchr(const char _far *s, int c);
int         _far _cdecl strcmp(const char _far *a, const char _far *b);
char _far * _far _cdecl strcpy(char _far *dst, const char _far *src);
char _far * _far _cdecl strdup(const char _far *s);
size_t      _far _cdecl strlen(const char _far *s);
char _far * _far _cdecl strrchr(const char _far *s, int c);

#endif
