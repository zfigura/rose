/*
 * Copyright 2023 Elizabeth Figura
 *
 * This file is part of the Rosé compiler.
 *
 * Rosé is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rosé is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Rosé; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef _INC_WINDOWS
#define _INC_WINDOWS

#define FAR _far
#define NEAR _near
#define WINAPI FAR _pascal
#define CALLBACK FAR _pascal

typedef int BOOL;
typedef unsigned long DWORD;
typedef long LONG;
typedef unsigned int UINT;
typedef unsigned int WPARAM;
typedef long LPARAM;
typedef long LRESULT;

#define DECLARE_HANDLE(a) typedef struct a##__ { int unused; } NEAR *a
DECLARE_HANDLE(HINSTANCE);
DECLARE_HANDLE(HWND);

typedef HINSTANCE HMODULE;

typedef const void NEAR *HANDLE;
typedef HANDLE HGLOBAL;

typedef void CALLBACK (*FARPROC)(void);

typedef int HFILE;

int _pascal WinMain( HINSTANCE instance, HINSTANCE prev, char FAR *cmdline, int show );

#define TRUE 1
#define FALSE 0

#define GMEM_FIXED          0x0000
#define GMEM_MOVEABLE       0x0002
#define GMEM_NOCOMPACT      0x0010
#define GMEM_NODISCARD      0x0020
#define GMEM_ZEROINIT       0x0040
#define GMEM_MODIFY         0x0080
#define GMEM_DISCARDABLE    0x0100
#define GMEM_NOT_BANKED     0x1000
#define GMEM_SHARE          0x2000
#define GMEM_DDESHARE       0x2000
#define GMEM_NOTIFY         0x4000

#define MB_OK               0x0000
#define MB_OKCANCEL         0x0001
#define MB_ABORTRETRYIGNORE 0x0002
#define MB_YESNOCANCEL      0x0003
#define MB_YESNO            0x0004
#define MB_RETRYCANCEL      0x0005
#define MB_TYPEMASK         0x000f
#define MB_ICONHAND         0x0010
#define MB_ICONQUESTION     0x0020
#define MB_ICONEXCLAMATION  0x0030
#define MB_ICONASTERISK     0x0040
#define MB_ICONMASK         0x00f0
#define MB_DEFBUTTON1       0x0000
#define MB_DEFBUTTON2       0x0100
#define MB_DEFBUTTON3       0x0200
#define MB_DEFMASK          0x0f00
#define MB_APPLMODAL        0x0000
#define MB_SYSTEMMODAL      0x1000
#define MB_TASKMODAL        0x2000
#define MB_NOFOCUS          0x8000

#define SEM_FAILCRITICALERRORS  0x0001
#define SEM_NOGPFAULTERRORBOX   0x0002
#define SEM_NOOPENFILEERRORBOX  0x8000

#define WM_QUIT             0x0012
#define WM_USER             0x0400

HFILE WINAPI _lclose(HFILE file);
HFILE WINAPI _lcreat(const char FAR *name, int attr);
unsigned int WINAPI _lwrite(HFILE file, const void _huge *ptr, unsigned int size);
char FAR * WINAPI GetDosEnvironment(void);
HMODULE WINAPI GetModuleHandle(const char FAR *name);
FARPROC WINAPI GetProcAddress(HINSTANCE module, const char FAR *name);
DWORD WINAPI GetTickCount(void);
HGLOBAL WINAPI GlobalAlloc(unsigned int flags, DWORD size);
HGLOBAL WINAPI GlobalFree(HGLOBAL mem);
void FAR * WINAPI GlobalLock(HGLOBAL mem);
HGLOBAL WINAPI GlobalReAlloc(HGLOBAL mem, DWORD size, unsigned int flags);
BOOL WINAPI GlobalUnlock(HGLOBAL mem);
int WINAPI MessageBox(HWND window, const char FAR *message, const char FAR *title, unsigned int flags);
BOOL WINAPI PostMessage(HWND window, UINT message, WPARAM wparam, LPARAM lparam);
LRESULT WINAPI SendMessage(HWND window, UINT message, WPARAM wparam, LPARAM lparam);
UINT WINAPI SetErrorMode(UINT mode);

#endif
