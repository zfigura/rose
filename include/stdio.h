/*
 * Copyright 2023 Elizabeth Figura
 *
 * This file is part of the Rosé compiler.
 *
 * Rosé is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rosé is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Rosé; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef __ROSE_STDIO_H
#define __ROSE_STDIO_H

#include <stdarg.h>
#include <stddef.h>

int _far _cdecl sprintf(char _far *str, const char _far *format, ...);
int _far _cdecl snprintf(char _far *str, size_t len, const char _far *format, ...);
int _far _cdecl vsprintf(char _far *str, const char _far *format, va_list args);
int _far _cdecl vsnprintf(char _far *str, size_t len, const char _far *format, va_list args);

#endif
