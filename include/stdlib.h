/*
 * Copyright 2023 Elizabeth Figura
 *
 * This file is part of the Rosé compiler.
 *
 * Rosé is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rosé is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Rosé; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef __ROSE_STDLIB_H
#define __ROSE_STDLIB_H

#include <stddef.h>

int         _far _cdecl atoi(const char _far *s);
void        _far _cdecl exit(int code);
char _far * _far _cdecl getenv(const char _far *name);
void _far * _far _cdecl malloc(size_t size);
void _far * _far _cdecl realloc(void _far *mem, size_t size);
void        _far _cdecl free(void _far *mem);

#endif
