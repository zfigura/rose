/*
 * Copyright 2023 Elizabeth Figura
 *
 * This file is part of the Rosé compiler.
 *
 * Rosé is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rosé is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Rosé; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef __ROSE_STDDEF_H
#define __ROSE_STDDEF_H

typedef unsigned int size_t;

/* FIXME: How the hell are NULL near pointers supposed to work?
 * A far pointer is NULL when both the segment and offset are zero (or just
 * the segment?)
 * A near pointer is apparently also NULL when zero, according to Microsoft's
 * windows.h. This would make sense if the PSP is considered off limits.
 * But then how do you cast a near pointer to a far pointer? Normally you just
 * add %ds, but if it's NULL that won't work.
 *
 * Dodge all this for now by forcing NULL to be a far pointer.
 */
#define NULL ((void _far *)(long)0)

#endif
