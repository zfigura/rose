/*
 * Copyright 2023 Elizabeth Figura
 *
 * This file is part of the Rosé compiler.
 *
 * Rosé is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rosé is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Rosé; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "rose.h"
#include "x86.h"
#include <ctype.h>
#include <getopt.h>
#include <sys/stat.h>

extern const char *const rose_dbg_env_name;

static const char *const debug_level_names[] =
{
    [ROSE_DBG_LEVEL_NONE ] = "none",
    [ROSE_DBG_LEVEL_ERR  ] = "err",
    [ROSE_DBG_LEVEL_FIXME] = "fixme",
    [ROSE_DBG_LEVEL_WARN ] = "warn",
    [ROSE_DBG_LEVEL_TRACE] = "trace",
};

enum rose_dbg_level rose_dbg_level = ROSE_DBG_LEVEL_NONE;

static void rose_dbg_set_level(void)
{
    const char *string;
    unsigned int i;

    if (!(string = getenv("ROSE_DEBUG")))
        string = "";

    for (i = 0; i < ARRAY_SIZE(debug_level_names); ++i)
    {
        if (!strcmp(debug_level_names[i], string))
        {
            rose_dbg_level = i;
            return;
        }
    }

    /* Default debug level. */
    rose_dbg_level = ROSE_DBG_LEVEL_FIXME;
}

static void rose_dbg_voutput(const char *fmt, va_list args)
{
    vfprintf(stderr, fmt, args);
}

static void rose_dbg_output(const char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    rose_dbg_voutput(fmt, args);
    va_end(args);
}

void rose_dbg_printf(enum rose_dbg_level level, const char *function, const char *fmt, ...)
{
    va_list args;

    if (rose_dbg_level < level)
        return;

    assert(level < ARRAY_SIZE(debug_level_names));

    rose_dbg_output("rose:%s:%s ", debug_level_names[level], function);
    va_start(args, fmt);
    rose_dbg_voutput(fmt, args);
    va_end(args);
}

void array_reserve(void **elements, size_t *capacity, size_t element_count, size_t element_size)
{
    size_t new_capacity, max_capacity;

    if (element_count <= *capacity)
        return;

    max_capacity = ~(size_t)0 / element_size;
    if (max_capacity < element_count)
        abort();

    new_capacity = max(*capacity, 4);
    while (new_capacity < element_count && new_capacity <= max_capacity / 2)
        new_capacity *= 2;

    if (new_capacity < element_count)
        new_capacity = element_count;

    *elements = realloc(*elements, new_capacity * element_size);
    *capacity = new_capacity;
}

void rose_string_buffer_init(struct rose_string_buffer *buffer)
{
    buffer->buffer_size = 16;
    buffer->content_size = 0;
    buffer->buffer = malloc(buffer->buffer_size);
    assert(buffer->buffer);
    memset(buffer->buffer, 0, buffer->buffer_size);
}

void rose_string_buffer_cleanup(struct rose_string_buffer *buffer)
{
    free(buffer->buffer);
}

static void rose_string_buffer_clear(struct rose_string_buffer *buffer)
{
    buffer->buffer[0] = '\0';
    buffer->content_size = 0;
}

static void rose_string_buffer_resize(struct rose_string_buffer *buffer, int rc)
{
    unsigned int new_buffer_size = rc >= 0 ? buffer->content_size + rc + 1 : buffer->buffer_size * 2;

    array_reserve((void **)&buffer->buffer, &buffer->buffer_size, new_buffer_size, 1);
}

void rose_string_buffer_vprintf(struct rose_string_buffer *buffer, const char *format, va_list args)
{
    unsigned int rem;
    va_list a;
    int rc;

    for (;;)
    {
        rem = buffer->buffer_size - buffer->content_size;
        va_copy(a, args);
        rc = vsnprintf(&buffer->buffer[buffer->content_size], rem, format, a);
        va_end(a);
        if (rc >= 0 && (unsigned int)rc < rem)
        {
            buffer->content_size += rc;
            return;
        }

        rose_string_buffer_resize(buffer, rc);
    }
}

void rose_string_buffer_printf(struct rose_string_buffer *buffer, const char *format, ...)
{
    va_list args;

    va_start(args, format);
    rose_string_buffer_vprintf(buffer, format, args);
    va_end(args);
}

static void rose_trace_text_(const char *text, size_t size, const char *function)
{
    const char *p, *q, *end = text + size;

    if (!TRACE_ON())
        return;

    for (p = text; p < end; p = q)
    {
        if (!(q = memchr(p, '\n', end - p)))
            q = end;
        else
            ++q;
        rose_dbg_printf(ROSE_DBG_LEVEL_TRACE, function, "%.*s", (int)(q - p), p);
    }
}

void rose_string_buffer_trace_(const struct rose_string_buffer *buffer, const char *function)
{
    rose_trace_text_(buffer->buffer, buffer->content_size, function);
}

void rose_string_buffer_cache_init(struct rose_string_buffer_cache *cache)
{
    memset(cache, 0, sizeof(*cache));
}

void rose_string_buffer_cache_cleanup(struct rose_string_buffer_cache *cache)
{
    unsigned int i;

    for (i = 0; i < cache->count; ++i)
    {
        rose_string_buffer_cleanup(cache->buffers[i]);
        free(cache->buffers[i]);
    }
    free(cache->buffers);
    rose_string_buffer_cache_init(cache);
}

struct rose_string_buffer *rose_string_buffer_get(struct rose_string_buffer_cache *cache)
{
    struct rose_string_buffer *buffer;

    if (!cache->count)
    {
        array_reserve((void **)&cache->buffers, &cache->capacity, cache->max_count + 1, sizeof(*cache->buffers));
        ++cache->max_count;

        buffer = malloc(sizeof(*buffer));
        rose_string_buffer_init(buffer);
    }
    else
    {
        buffer = cache->buffers[--cache->count];
    }
    rose_string_buffer_clear(buffer);
    return buffer;
}

void rose_string_buffer_release(struct rose_string_buffer_cache *cache, struct rose_string_buffer *buffer)
{
    if (!buffer)
        return;
    assert(cache->count + 1 <= cache->max_count);
    cache->buffers[cache->count++] = buffer;
}

static inline int char_to_int(char c)
{
    if ('0' <= c && c <= '9')
        return c - '0';
    if ('A' <= c && c <= 'F')
        return c - 'A' + 10;
    if ('a' <= c && c <= 'f')
        return c - 'a' + 10;
    return -1;
}

static uint32_t ascii_strtoul(const char *s, const char **endptr, uint32_t base)
{
    uint32_t ret = 0;
    int digit;

    while ((digit = char_to_int(*s++)) >= 0)
        ret = ret * base + (uint32_t)digit;
    *endptr = s;
    return ret;
}

uint32_t rose_parse_integer(const char *s)
{
    uint32_t base = 10;
    const char *end;

    if (*s == '0')
    {
        base = 8;
        ++s;
        if (*s == 'x' || *s == 'X')
        {
            base = 16;
            ++s;
        }
    }

    return ascii_strtoul(s, &end, base);
}

static char get_escape_char(char s)
{
    switch (s)
    {
        case '\'': return '\'';
        case '"': return '"';
        case '?': return '\?';
        case '\\': return '\\';
        case 'a': return '\a';
        case 'b': return '\b';
        case 'f': return '\f';
        case 'n': return '\n';
        case 'r': return '\r';
        case 't': return '\t';
        case 'v': return '\v';
        default: return 0;
    }
}

static void DECLSPEC_NORETURN fatal_error(const char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    printf("Compiler error: ");
    vprintf(fmt, args);
    printf("\n");
    va_end(args);
    exit(ROSE_ERROR_INVALID_SHADER);
}

bool rose_parse_string(const char *string, char **ret_string, size_t *ret_len)
{
    size_t len = strlen(string);
    const char *const end = string + len;
    char *parsed_string = malloc(len + 1);
    const char *src = string;
    char *dst = parsed_string;
    char esc;

    while (src < end)
    {
        if (*src != '\\')
        {
            *dst++ = *src++;
        }
        else if (src + 3 <= end && src[1] == 'x')
        {
            *dst++ = ascii_strtoul(src + 2, &src, 16);
        }
        else if (src + 2 <= end && src[1] >= '0' && src[1] <= '7')
        {
            *dst++ = ascii_strtoul(src + 1, &src, 8);
        }
        else if (src + 2 <= end && (esc = get_escape_char(src[1])))
        {
            *dst++ = esc;
            src += 2;
        }
        else
        {
            goto err;
        }
    }

    *dst++ = 0;
    *ret_string = parsed_string;
    *ret_len = dst - parsed_string;

    return true;
err:

    free(parsed_string);
    return false;
}

static bool match_string(const char *line, const char *const end, const char *token, const char **const rest)
{
    size_t token_len = strlen(token);

    if (end - line >= token_len && !memcmp(line, token, token_len) && isspace(line[token_len]))
    {
        *rest = line + token_len;
        while (isspace(**rest))
            ++*rest;
        return true;
    }
    return false;
}

static char *dup_string(const char *line, const char *const end, const char **const rest)
{
    size_t len = 0;
    char *ret;

    while (line < end && isspace(*line))
        ++line;

    while (len < end - line && !isspace(line[len]))
        ++len;
    ret = malloc(len + 1);
    memcpy(ret, line, len);
    ret[len] = 0;

    *rest = line + len;
    while (isspace(**rest))
        ++*rest;
    return ret;
}

static void parse_def_file(const struct rose_code *code, struct rose_def *def)
{
    const char *const end = (const char *)code->code + code->size;
    const char *line = code->code, *next;

    memset(def, 0, sizeof(*def));

    for (;;)
    {
        if (!(next = memchr(line, '\n', end - line)))
            return;

        while (line < next && isspace(*line))
            ++line;
        if (line == next)
        {
            line = next + 1;
            continue;
        }

        if (match_string(line, next, "NAME", &line))
        {
            def->name = dup_string(line, next, &line);
            def->dll = false;
        }
        else if (match_string(line, next, "LIBRARY", &line))
        {
            def->name = dup_string(line, next, &line);
            def->dll = true;
        }
        else if (match_string(line, next, "EXPORTS", &line))
        {
            break;
        }
        else
        {
            fatal_error("Unrecognized directive in def file: %.*s\n", next - line, line);
        }

        line = next + 1;
    }

    for (;;)
    {
        struct rose_export *export;

        if (!(next = memchr(line, '\n', end - line)))
            return;

        while (line < next && isspace(*line))
            ++line;
        if (line == next)
        {
            line = next + 1;
            continue;
        }

        array_reserve((void **)&def->exports, &def->exports_capacity,
                def->export_count + 1, sizeof(*def->exports));
        export = &def->exports[def->export_count++];

        export->name = dup_string(line, next, &line);
        if (line == next || line[0] != '@')
            fatal_error("Malformed export '%.*s' in def file.\n", (int)(next - line), line);
        export->ordinal = strtoul(line + 1, NULL, 10);
        export->module = def;

        line = next + 1;
    }
}

enum
{
    OPTION_HELP = CHAR_MAX + 1,
    OPTION_LIBRARY,
    OPTION_OUTPUT,
    OPTION_VERSION,
};


static bool read_code(struct rose_code *code, FILE *f)
{
    size_t size = 4096;
    struct stat st;
    size_t pos = 0;
    uint8_t *data;
    size_t ret;

    memset(code, 0, sizeof(*code));

    if (fstat(fileno(f), &st) == -1)
    {
        fprintf(stderr, "Could not stat input.\n");
        return false;
    }

    if (S_ISREG(st.st_mode))
        size = st.st_size;

    if (!(data = malloc(size)))
    {
        fprintf(stderr, "Out of memory.\n");
        return false;
    }

    for (;;)
    {
        if (pos >= size)
        {
            if (size > SIZE_MAX / 2 || !(data = realloc(data, size * 2)))
            {
                fprintf(stderr, "Out of memory.\n");
                free(data);
                return false;
            }
            size *= 2;
        }

        if (!(ret = fread(&data[pos], 1, size - pos, f)))
            break;
        pos += ret;
    }

    if (!feof(f))
    {
        free(data);
        return false;
    }

    code->code = data;
    code->size = pos;

    return true;
}

static void add_import(struct rose_program *program, const char *filename)
{
    struct rose_code code;
    FILE *f;

    array_reserve((void **)&program->imports, &program->imports_capacity,
            program->import_count + 1, sizeof(*program->imports));

    if (!(f = fopen(filename, "rb")))
        fatal_error("Cannot open %s: %s\n", filename, strerror(errno));
    if (!read_code(&code, f))
        exit(1);
    parse_def_file(&code, &program->imports[program->import_count++]);
    fclose(f);
}

static bool match_ext(const char *filename, const char *ext)
{
    return strlen(filename) >= strlen(ext) && !strcmp(filename + strlen(filename) - strlen(ext), ext);
}

int main(int argc, char **argv)
{
    struct rose_program program = {0};
    struct rose_code output_code;
    char *output_name = NULL;
    enum rose_result ret;
    FILE *output_file;
    int option;

    static const struct option long_options[] =
    {
        {"help",        no_argument,    NULL,   OPTION_HELP},
        {"library",     no_argument,    NULL,   OPTION_LIBRARY},
        {"output",      no_argument,    NULL,   OPTION_OUTPUT},
        {"version",     no_argument,    NULL,   OPTION_VERSION},
        {0}
    };

    rose_dbg_set_level();

    while ((option = getopt_long(argc, argv, "hl:o:v", long_options, NULL)) != -1)
    {
        switch (option)
        {
            case OPTION_HELP:
            case 'h':
                fprintf(stderr, "Usage: rosecc <input> -o <output>\n");
                return 0;

            case OPTION_LIBRARY:
            case 'l':
                add_import(&program, optarg);
                break;

            case OPTION_OUTPUT:
            case 'o':
                output_name = optarg;
                break;

            case OPTION_VERSION:
            case 'v':
                fprintf(stderr, "<unversioned prerelease>\n");
                return 0;
        }
    }

    if (!output_name)
    {
        fprintf(stderr, "No output file given.\n");
        return 1;
    }

    if (optind == argc)
    {
        fprintf(stderr, "No file given.\n");
        return 1;
    }

    for (; optind < argc; ++optind)
    {
        struct rose_program file_program;
        char *input_name = argv[optind];
        struct rose_code source_code;
        FILE *input_file;

        if (!(input_file = fopen(input_name, "rb")))
        {
            fprintf(stderr, "Failed to open %s: %s\n", input_name, strerror(errno));
            return 1;
        }

        if (!read_code(&source_code, input_file))
            return 1;
        fclose(input_file);

        if (match_ext(input_name, ".def"))
        {
            parse_def_file(&source_code, &program.def);
        }
        else if (match_ext(input_name, ".c") || match_ext(input_name, ".i"))
        {
            if ((ret = rose_compile_shader(&source_code, input_name, &file_program)))
                fatal_error("Failed to compile %s, error %d.\n", input_name, ret);
        }
        else
        {
            fatal_error("Unknown file extension for %s.\n", input_name);
        }

        array_reserve((void **)&program.blocks, &program.blocks_capacity,
                program.block_count + file_program.block_count, sizeof(*program.blocks));
        memcpy(program.blocks + program.block_count, file_program.blocks,
                file_program.block_count * sizeof(*program.blocks));
        program.block_count += file_program.block_count;
    }

    rose_link(&program, &output_code);

    if (!(output_file = fopen(output_name, "wb")))
    {
        fprintf(stderr, "Failed to open %s: %s\n", output_name, strerror(errno));
        return 1;
    }
    if (fwrite(output_code.code, 1, output_code.size, output_file) != output_code.size)
    {
        fprintf(stderr, "Error while writing: %s\n", strerror(errno));
        return 1;
    }
    fclose(output_file);

    return 0;
}
